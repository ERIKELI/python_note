class QueueEmptyException(Exception): pass


class QueueFullException(Exception): pass


class Queue:
    """
    Queue:
        <--| 1 | 2 | 3 | 4 | 5 |<--
    """
    def __init__(self, max=0):
        self.queue = []
        self._max = max
        self.max = max

    def __str__(self):
        return ' | '.join(map(str, self.queue))

    def init(self, iterable=()):
        if not iterable:
            return 
        self.queue.extend(list(iterable))

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, m):
        m = int(m)
        if m < self.length:
            raise Exception('Resize queue failed, please dequeue some elements first.')
        self._max = m
        if self._max < 0:
            self._max = 0

    def show(self):
        print(self)

    @property
    def length(self):
        return len(self.queue)

    @property
    def is_empty(self):
        return not bool(self.queue)

    @property
    def is_full(self):
        return bool(self._max and self.length == self._max)

    def enqueue(self, item):
        if self.is_full:
            raise QueueFullException('Error: trying to enqueue element into a full queue')
        self.queue.append(item)
    
    def dequeue(self):
        if self.is_empty:
            raise QueueEmptyException('Error: trying to dequeue element from an empty queue')
        front = self.queue[0]
        self.queue = self.queue[1:]
        return front

    def clear(self):
        self.queue = []


def test(queue):
    print('\nInit queue:')
    queue.init([1, 2, 3, 4, 5, 6, 7])
    queue.show()

    print('\nEnqueue element to queue:')
    queue.enqueue('like')
    queue.show()

    print('\nDequeue element from queue:')
    e = queue.dequeue()
    print('Element %s deququed,' % e)
    queue.show()

    print('\nSet queue max size:')
    try:
        queue.max = 1
    except Exception as e:
        print(e)

    print('\nSet queue max size:')
    queue.max = 7
    print(queue.max)

    print('\nEnqueue full queue:')
    try:
        queue.enqueue(7)
    except QueueFullException as e:
        print(e)

    print('\nClear queue:')
    queue.clear()
    queue.show()

    print('\nQueue is empty:')
    print(queue.is_empty)

    print('\nDequeue empty queue:')
    try:
        queue.dequeue()
    except QueueEmptyException as e:
        print(e)

if __name__ == '__main__':
    test(Queue())

