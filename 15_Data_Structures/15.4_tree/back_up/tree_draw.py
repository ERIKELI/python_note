
def print_tree(depth):
    """
    0**********************
    |___________***********
    |***********|**********
    00**********00*********
    |_____******|_____*****
    |*****|*****|*****|****
    00****00****00****00***
    |__***|__***|__***|__**  
    |**|**|**|**|**|**|**|*
    00*00*00*00*00*00*00*00  
    """
    for d in range(depth+1):
        h = depth-d
        stuff_num = lambda x: int(2+(2**x-1)*3)
        stuff_leaf = (stuff_num(h)-1)*' '
        stuff_branch = stuff_num(h)*' '
        stuff_stem = (stuff_num(h+1)-stuff_num(h))*' '

        leaf = int(2**d)*['00']
        branch = int(2**d)*['|']
        stem = int(2**(d-1))*['|'+stuff_num(h)*'_']
        
        if d:
            print(stuff_stem.join(stem))
            print(stuff_branch.join(branch))
        print(stuff_leaf.join(leaf))

#print_tree(2)
print_tree(3)
#print_tree(4)
