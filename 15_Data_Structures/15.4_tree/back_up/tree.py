class TreeNode:
    def __init__(self, val=None, chd=None, sib=None):
        self.value = val
        self.child = chd    # First Child
        self.sibling = sib  # Next Sibling

    def __str__(self):
        return str(self.value)
    

class Tree:
    def __init__(self):
        self._root = None

    def init(self):
        pass
