from linked_list_stack import Stack


class TreeNode:
    def __init__(self, val=None, lef=None, rgt=None):
        self.value = val
        self.left = lef
        self.right = rgt

    def __str__(self):
        return str(self.value)
    
class BinaryTree:
    def __init__(self, root=None):
        self._root = root

    def __str__(self):
        return '\n'.join(map(lambda x: x[1]*4*' '+x[0], self.pre_traversal()))

    def pre_traversal(self):
        x = []
        depth = -1
        def _traversal(self):
            nonlocal depth
            depth += 1
            x.append((self.value, depth))
            if self.left is not None:
                _traversal(self.left)
            else:
                x.append(('NA', depth+1))
            if self.right is not None:
                _traversal(self.right)
            else:
                x.append(('NA', depth+1))
            #if self.left is not None or self.right is not None:
            #    if self.left is None:
            #        x.append(('NA', depth+1))
            #    elif self.right is None:
            #        x.append(('NA', depth+1))
            depth -= 1
            return x
        return _traversal(self._root)

    def post_traversal(self):
        x = []
        depth = -1
        def _traversal(self):
            nonlocal depth
            depth += 1
            if self.left is not None:
                _traversal(self.left)
            else:
                x.append(('NA', depth+1))
            if self.right is not None:
                _traversal(self.right)
            else:
                x.append(('NA', depth+1))
            x.append((self.value, depth))
            depth -= 1
            return x
        return _traversal(self._root)

    @property
    def max_depth(self):
        return sorted(self.pre_traversal(), key=lambda x: x[1])[-1][1]

    def show(self):
        print(self)

    def paint_tree(self):
        """
        0**********************
        |___________***********
        |***********|**********
        00**********00*********
        |_____******|_____*****
        |*****|*****|*****|****
        00****00****00****00***
        |__***|__***|__***|__**  
        |**|**|**|**|**|**|**|*
        00*00*00*00*00*00*00*00  
        """
        depth = self.max_depth
        leaf_list = self.pre_traversal()
        for d in range(depth+1):
            h = depth-d
            stuff_num = lambda x: int(2+(2**x-1)*3)
            stuff_leaf = (stuff_num(h)-1)*' '
            stuff_branch = stuff_num(h)*' '
            stuff_stem = (stuff_num(h+1)-stuff_num(h))*' '
    
            leaf = list(map(lambda x: '%-2s'%x[0], filter(lambda x: x[1]==d, leaf_list)))

            if all(map(lambda x: x=='NA', leaf)):
                break
            #leaf = int(2**d)*['00']
            branch = int(2**d)*['|']
            stem = int(2**(d-1))*['|'+stuff_num(h)*'_']
            
            if d:
                print(stuff_stem.join(stem))
                print(stuff_branch.join(branch))
            print(stuff_leaf.join(leaf))

    #def make_empty(self):
    #    self.__init__()
        
    #def insert


SIGN = {'+': 1, '-': 1, '*': 2, '/': 2, '(': 3}

def postfix_to_tree(expr):
    global SIGN
    s = Stack()
    for i in expr:
        if i in SIGN.keys():
            right = s.pop()
            left = s.pop()
            node = TreeNode(i, left, right)
            s.push(node)
        else:
            s.push(TreeNode(i))
    return s.pop()

ep = 'a b + c d e + * *'
#print(postfix_to_tree(ep.split(' ')).left.left.left)
#print(BinaryTree(postfix_to_tree(ep.split(' '))))
t = BinaryTree(postfix_to_tree(ep.split(' ')))
#print(t)
#print(t.pre_traversal())
#print(t.max_depth)
t.paint_tree()
