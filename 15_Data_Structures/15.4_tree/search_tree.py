from binary_tree import TreeNode, BinaryTree


class SearchTree(BinaryTree):
    """
    Search Tree:
        5
        |_____
        |     |
        2     7
        |__   |__
        |  |  |  |
        1  3  6  9
    """
    def find(self, item):
        if self._root is None:
            return None

        def _find(item, node):
            if not node:
                return node
            if item < node.value:
                return _find(item, node.left)
            elif item > node.value:
                return _find(item, node.right)
            else:
                return node
        return _find(item, self._root)
        
    def find_min(self, node=None):
        if self._root is None:
            return None
        if not node:
            node = self._root
        if node.left:
            return self.find_min(node.left)
        return node

    def find_max(self, node=None):
        if self._root is None:
            return None
        if not node:
            node = self._root
        if node.right:
            return self.find_max(node.right)
        return node

    def find_previous(self, item):
        if self._root is None:
            return None

        def _find(item, node):
            if not node.left and not node.right:
                return None
            if item < node.value:
                if item == node.left.value:
                    return node
                return _find(item, node.left)
            elif item > node.value:
                if item == node.right.value:
                    return node
                return _find(item, node.right)
        return _find(item, self._root)
   
    def insert(self, item):
        if self._root is None:
            self._root = TreeNode(item)
            return

        def _insert(item, node):
            if not node:
                return TreeNode(item)
            if item < node.value:
                node.left = _insert(item, node.left)
            elif item > node.value:
                node.right = _insert(item, node.right)
            else: pass
            return node
        self._root = _insert(item, self._root)

    def delete(self, item):
        if self._root is None:
            return

        def _delete(item, node):
            if not node:    # Node no found
                # return None
                raise Exception('Element not in tree.')
            if item < node.value:
                node.left = _delete(item, node.left)
            elif item > node.value:
                node.right = _delete(item, node.right)
            else:   # Node found
                if node.left and node.right:
                    # Minimum node in right sub-tree has no left sub-node, can be used to make replacement
                    # Find minimum node in right sub-tree
                    min_node = self.find_min(node.right)    
                    # Replace current node with min_node
                    node.value = min_node.value
                    # Delete min_node in right sub-tree
                    node.right = _delete(min_node.value, node.right)
                else:
                    if node.left: 
                        node = node.left
                    elif node.right:
                        node = node.right
                    else:
                        node = None
            return node
        self._root = _delete(item, self._root)


def test(t):
    print('\nInit Search tree:')
    for i in [6, 2, 8, 1, 4, 3, 1]:
        t.insert(i)
    t.show()
    '''
          6
        __|__
       |     |
       2     8
     __|__   
    |     |  
    1     4  
        __|
       |
       3
    '''
    print('\nFind min value:')
    print(t.find_min())
    print('\nFind max value:')
    print(t.find_max())
    print('\nFind certain value:')
    print(t.find(3))
    print('\nFind certain value (not exist):')
    print(t.find(7))
    print('\nFind previous value of certain value:')
    print(t.find_previous(3))
    print('\nFind previous value of certain value (not exist):')
    print(t.find(7))
    print('\nDelete certain value (with one sub-node):')
    t.delete(4)
    t.show()
    '''
          6
        __|__
       |     |
       2     8
     __|  
    |  |   |  
    1  |   4  
       | 
       |
       3
    '''
    print('\nMake tree empty:')
    t.make_empty()
    t.show()
    print('\nInit Search tree:')
    for i in [6, 2, 8, 1, 5, 3, 4]:
        t.insert(i)
    t.show()
    '''
          6
        __|__
       |     |
       2     8
     __|__   
    |     |  
    1     5  
        __|
       |
       3
       |__
          |
          4
    '''
    print('\nDelete certain value (with two sub-node):')
    t.delete(2)
    t.show()
    '''
          6
        __|__
       |     |
       3     8
     __|__   
    |     |  
    1     5  
        __|
       |
       4
    '''
    print('\nDelete certain value (not exist):')
    try:
        t.delete(7)
    except Exception as e:
        print(e)
    

if __name__ == '__main__':
    test(SearchTree())
