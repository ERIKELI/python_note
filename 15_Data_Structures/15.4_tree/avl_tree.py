from search_tree import SearchTree


class AvlNode:
    def __init__(self, val=None, lef=None, rgt=None, hgt=0):
        self.value = val
        self.left = lef
        self.right = rgt
        self.height = hgt

    def __str__(self):
        return str(self.value)


class AvlTree(SearchTree):
    """
    AVL Tree:
                0 
           _____|_____
          |           |
          0           0 
          |___     ___|___
              |   |       |
              0   0       0 
                          |__
                             |
                             0 
    Insert unbalance:
    1:             |  2:           |  3:            | 4:
           A(2)    |        A(2)   |       A(2)     |    A(2)             
         __|       |      __|      |       |__      |    |__        
        |          |     |         |          |     |       |           
        B(1)       |     B(1)      |          B(1)  |       B(1)        
      __|          |     |__       |        __|     |       |__         
     |             |        |      |       |        |          |        
    ----           |       ----    |      ----      |         ----     
   |X(0)|          |      |X(0)|   |     |X(0)|     |        |X(0)|    
    ----           |       ----    |      ----      |         ----         

    Delete unbalance:
    1:             |  2:           |  3:            |  4:
           A(2)    |      A(2)     |        A(2)    |       A(2)        
         __|__     |    __|__      |      __|__     |     __|__        
        |     |    |   |     |     |     |     |    |    |     |            
        B(1) ----  |   B(1)  ----  |  ----     B(1) |  ----    B(1)          
      __|   |X(0)| |   |__  |X(0)| | |X(0)|  __|    | |X(0)|   |__            
     |       ----  |      |  ----  |  ----  |       |  ----       |          
     C(0)          |      C(0)     |        C(0)    |             C(0)
    """                                                
    def get_height(self, node):
        if isinstance(node, AvlNode):
            return node.height
        return -1

    def single_rotate_left(self, k2):
        """
                 K2(2)                      K1(1)
               __|__                      ____|_____
              |     |                    |          |
              K1(1) None(-1)  -------->  X(0)       K2(1)
            __|__                                 __|__
           |     |                               |     |
           X(0)  Y(0)                            Y(0)  None(-1)
        """
        # Left rotate
        k1 = k2.left
        k2.left = k1.right
        k1.right = k2
        # Update height
        k2.height = max(self.get_height(k2.left), self.get_height(k2.right)) + 1
        k1.height = max(self.get_height(k1.left), k2.height) + 1
        return k1

    def single_rotate_right(self, k2):
        """
                 K2(2)                      K1(2)
               __|__                      ____|_____
              |     |                    |          |
          (-1)None  K1(1)   -------->    K2(1)      Y(0)
                  __|__                __|__
                 |     |              |     |
                 X(0)  Y(0)       (-1)None  X(0)
        """
        # Right rotate
        k1 = k2.right
        k2.right = k1.left
        k1.left = k2
        # Update height
        k2.height = max(self.get_height(k2.left), self.get_height(k2.right)) + 1
        k1.height = max(k2.height, self.get_height(k1.right)) + 1
        return k1

    def double_rotate_left(self, k3):   
        """
                 K3(3)                     K3(3)                     K2(2)  
               __|__                     __|__                  _____|_____ 
              |     |      right        |     |      left      |           |   
              K1(2) D(0)  -------->     K2(2) D(0) -------->   K1(1)       K3(1)
            __|__                     __|__                  __|__       __|__ 
           |     |                   |     |                |     |     |     |        
           A(0)  K2(1)               K1(1) C(0)             A(0)  B(0) C(0)  D(0)    
               __|__               __|__                   
              |     |             |     |                  
              B(0)  C(0)          A(0)  B(0)               
        """
        # Rotate between k1 and k2
        k3.left = self.single_rotate_right(k3.left)
        # Rotate between k3 and k2
        return self.single_rotate_left(k3)

    def double_rotate_right(self, k3):
        """
                 K3(3)                     K3(3)                     K2(2)  
               __|__                     __|__                  _____|_____ 
              |     |         left      |     |       right    |           |   
              D(0)  K1(2)   -------->   D(0)  K2(2) -------->  K3(1)       K1(1)
                  __|__                     __|__            __|__       __|__ 
                 |     |                   |     |          |     |     |     |        
                 K2(1) A(0)                C(0)  K1(1)      D(0)  C(0)  B(0)  A(0)    
               __|__                           __|__                   
              |     |                         |     |                  
              C(0)  B(0)                      B(0)  A(0)               
        """
        # Rotate between k1 and k2
        k3.right = self.single_rotate_left(k3.right)
        # Rotate between k3 and k2
        return self.single_rotate_right(k3)

    def insert(self, item):
        if self._root is None:
            self._root = AvlNode(item)
            return
        
        def _insert(item, node):
            if not node:
                return AvlNode(item)
            if item < node.value:
                node.left = _insert(item, node.left)
                if self.get_height(node.left) - self.get_height(node.right) == 2:
                    if item < node.left.value:      # Situation 1: left --> left
                        node = self.single_rotate_left(node)
                    else:                           # Situation 2: left --> right
                        node = self.double_rotate_left(node)
            elif item > node.value:
                node.right = _insert(item, node.right)
                if self.get_height(node.right) - self.get_height(node.left) == 2:
                    if item < node.right.value:     # Situation 3: right --> left
                        node = self.double_rotate_right(node)
                    else:                           # Situation 4: right --> right
                        node = self.single_rotate_right(node)
            else: pass
            # Update node height (if not rotated, update is necessary.)
            node.height = max(self.get_height(node.left), self.get_height(node.right)) + 1
            return node
        self._root = _insert(item, self._root)

    def delete(self, item):
        if self._root is None:
            return

        def _delete(item, node):
            if not node:    # Node no found
                # return None
                raise Exception('Element not in tree.')

            if item < node.value:
                node.left = _delete(item, node.left)
                # Delete left, rotate right
                if self.get_height(node.right) - self.get_height(node.left) == 2:
                    if self.get_height(node.right.right) >= self.get_height(node.right.left):   # Situation 4
                        node = self.single_rotate_right(node)
                    else:                                                                       # Situation 3
                        node = self.double_rotate_right(node)

            elif item > node.value:
                node.right = _delete(item, node.right)
                # Delete right, rotate left
                if self.get_height(node.left) - self.get_height(node.right) == 2:
                    if self.get_height(node.left.left) >= self.get_height(node.left.right):     # Situation 1
                        node = self.single_rotate_left(node)
                    else:                                                                       # Situation 3
                        node = self.double_rotate_left(node)

            else:   # Node found
                if node.left and node.right:
                    # Minimum node in right sub-tree has no left sub-node, can be used to make replacement
                    # Find minimum node in right sub-tree
                    min_node = self.find_min(node.right)    
                    # Replace current node with min_node
                    node.value = min_node.value
                    # Delete min_node in right sub-tree
                    node.right = _delete(min_node.value, node.right)
                else:
                    if node.left: 
                        node = node.left
                    elif node.right:
                        node = node.right
                    else:
                        node = None
            # Update node height (if not ratated, update is necessary.)
            # If node is None, height is -1 already.
            if node:
                node.height = max(self.get_height(node.left), self.get_height(node.right)) + 1
            return node
        self._root = _delete(item, self._root)

                 
def test(avl):
    print('\nInit an AVL tree:')
    for i in [1, 2, 3, 4, 5, 6, 7]:
        avl.insert(i)
    avl.show()

    print('\nLeft single rotate:')
    avl.delete(5)
    avl.delete(7)
    avl.delete(6)
    avl.show()

    avl.make_empty()
    print('\nInit an AVL tree:')
    for i in [1, 2, 3, 4, 5, 6, 7]:
        avl.insert(i)
    
    print('\nRight single rotate:')
    avl.delete(1)
    avl.delete(3)
    avl.delete(2)
    avl.show()

    avl.make_empty()
    print('\nInit an AVL tree:')
    for i in [6, 2, 8, 1, 4, 9, 3, 5]:
        avl.insert(i)
    avl.show()

    print('\nRight-Left double rotate:')
    avl.delete(9)
    avl.show()

    avl.make_empty()
    print('\nInit an AVL tree:')
    for i in [4, 2, 8, 1, 6, 9, 5, 7]:
        avl.insert(i)
    avl.show()

    print('\nLeft-Right double rotate:')
    avl.delete(1)
    avl.show()

if __name__ == '__main__':
    test(AvlTree())

