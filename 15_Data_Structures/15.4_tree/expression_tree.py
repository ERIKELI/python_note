from binary_tree import BinaryTree, TreeNode
from stack.linked_list_stack import Stack


class ExpressionTree(BinaryTree):
    SIGN = {'+': 1, '-': 1, '*': 2, '/': 2, '(': 3}
    
    def gene_tree_by_postfix(self, expr):
        s = Stack()
        for i in expr:
            if i in self.SIGN.keys():
                right = s.pop()
                left = s.pop()
                node = TreeNode(i, left, right)
                s.push(node)
            else:
                s.push(TreeNode(i))
        self._root = s.pop()


def test_expression_tree(ep):
    t = ExpressionTree()
    t.gene_tree_by_postfix(ep)
    print('\n------Pre-traversal-------')
    print(t)

    print('\n------Post-traversal------')
    t.show(t.post_traversal())
    print('\n-------In-traversal-------')
    t.show(t.in_traversal())


if __name__ == '__main__':
    ep = 'a b + c d e + * *'
    '''
    *
    |___________
    |           |
    +           *
    |_____      |_____
    |     |     |     |
    a     b     c     +
                      |__
                      |  |
                      d  c
    '''
    test_expression_tree(ep.split(' '))

