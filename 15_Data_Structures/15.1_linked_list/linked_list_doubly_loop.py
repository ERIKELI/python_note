from linked_list_doubly import NodeDual, DoublyLinkedList, test


class DoublyLinkedListLoop(DoublyLinkedList):
    """
    Doubly linked list with loop:
         ________________________________________________________
        |                                                        |
        <===> node_1 <---> node_2 <---> node_3 <---> node_4 <===>
        |________________________________________________________|
    """
    def __str__(self):
        def _traversal(self):
            node = self.header
            while node and node.next is not self.header:
                yield node
                node = node.next
            yield node
        return '<->\n'.join(map(lambda x: str(x), _traversal(self)))

    def init(self, iterable=()):
        if not iterable:
            return
        self.header = NodeDual(iterable[0])     # header value
        pre = None
        node = self.header
        for i in iterable[1:]:                  # add all node
            node.prev = pre
            node.next = NodeDual(i)
            pre = node
            node = node.next
        node.prev = pre

        node.next = self.header
        self.header.prev = node

    @property
    def length(self):
        if self.header is None:
            return 0
        node = self.header
        i = 1
        while node.next is not self.header:
            node = node.next
            i += 1
        return i

    def find(self, item):
        node = self.header
        while node.next is not self.header and node.value != item:
            node = node.next
        if node.value == item:
            return node
        return None

    def find_previous(self, item):
        node = self.header
        while node.next is not self.header and node.next.value != item:
            node = node.next
        if node.next is not self.header and node.next.value == item:
            return node
        return None


if __name__ == '__main__':
    test(DoublyLinkedListLoop())
 
