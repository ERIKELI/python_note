from linked_list import LinkedList, test


class NodeDual:
    def __init__(self, val=None, nxt=None, pre=None):
        self.value = val
        self.next = nxt
        self.prev = pre

    def __str__(self):
        return '<NodeDual, prev=%s, value=%s, next=%s>' % (self.prev.value if self.prev else self.prev,
                                                           self.value, 
                                                           self.next.value if self.next else self.next)


class DoublyLinkedList(LinkedList):
    """
    Doubly linked list:
            node_1 <---> node_2 <---> node_3
    """
    def __str__(self):
        def _traversal(self):
            node = self.header
            while node and node.next:
                yield node
                node = node.next
            yield node
        return '<->\n'.join(map(lambda x: str(x), _traversal(self)))

    def init(self, iterable=()):
        if not iterable:
            return
        self.header = NodeDual(iterable[0])     # header value
        pre = None
        node = self.header
        for i in iterable[1:]:                  # add all node
            node.prev = pre
            node.next = NodeDual(i)
            pre = node
            node = node.next
        node.prev = pre

    def find_previous(self, item):
        return self.find(item).prev

    def delete(self, item):
        pre = self.find_previous(item)
        if pre:
            pre.next = pre.next.next
            pre.next.prev = pre

    def insert(self, item, index):
        if abs(index) > self.length:
            return
        if index < 0:
            self.insert(item, self.length+index+1)
            return
        elif index == 0:
            self.insert(self.header.value, 1)
            self.header.value = item
            return
        node = self.header
        i = 0
        while i < index-1:
            node = node.next
            i += 1
        n = node.next
        node.next = NodeDual(item, nxt=n, pre=node)
        if n:
            n.prev = node.next


if __name__ == '__main__':
    test(DoublyLinkedList())

