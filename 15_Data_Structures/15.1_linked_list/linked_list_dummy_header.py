from linked_list import Node, LinkedList, test


class LinkedListDummyHeader(LinkedList):
    """
    Linked list with dummy header node:
            Header -> node_1 -> node_2 -> node_3
    """
    def __init__(self, iterable=()):
        self.header = Node('Header', None)
        if iterable:
            self.init(iterable)

    def init(self, iterable=()):
        if not iterable:
            return
        node = self.header
        for i in iterable:
            node.next = Node(i)
            node = node.next

    @property
    def is_empty(self):
        return self.header.next is None
        # if self.length == 1:
        #     return True
        # return False

    def insert(self, item, index):
        if index == 0:
            return
        super(LinkedListDummyHeader, self).insert(item, index)

if __name__ == '__main__':
    test(LinkedListDummyHeader())
