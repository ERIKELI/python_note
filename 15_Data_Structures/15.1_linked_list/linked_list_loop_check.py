from linked_list import LinkedList


def check_loop(chain):
    has_loop, entry_node, loop_length, chain_length = False, None, 0, 0

    # Get header for fast and slow
    step = 0
    fast = slow = head = chain.header
    while fast and fast.next:
        fast = fast.next.next
        slow = slow.next
        step += 1
        # Note:
        # Do remember to use ,is' rather than '==' here (assure the id is same).
        if fast is slow:    
            break
    has_loop = not(fast is None or fast.next is None)
    pass_length = (step * 2) if fast is None else (step * 2 + 1)

    if has_loop:
        step = 0
        while True:
            if head is slow:
                entry_node = slow
                pass_length = step
            if not entry_node:
                head = head.next
            fast = fast.next.next
            slow = slow.next
            step += 1
            if fast is slow:
                break
        loop_length = step

    chain_length = pass_length + loop_length
    return has_loop, entry_node, loop_length, chain_length


if __name__ == '__main__':
    print('------------ Loop check ------------------')
    print('''
    0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9
    ''')
    loop_chain = LinkedList(range(10))
    print('Linked list has loop: %s, entry node: %s, loop length: %s, chain length: %s' % check_loop(loop_chain))

    # Create a loop for linked list.
    print('''
                    _____________________________
                   |                             |
                   V                             |
    0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9
    ''')
    node_9 = loop_chain.find(9)
    node_3 = loop_chain.find(3)
    node_9.next = node_3
    print('Linked list has loop: %s, entry node: %s, loop length: %s, chain length: %s' % check_loop(loop_chain))
