from linked_list_stack import StackEmptyException, StackFullException, test


class Stack:
    """
    Stack based on array/list:
        |   4   |
        |   3   |
        |   2   |
        |   1   |
         -------
    """
    def __init__(self, max=0):
        self._array = []
        self._max = 0
        self.max = max

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, m):
        m = int(m)
        if m < self.length:
            raise Exception('Resize stack failed, please pop some elements first.')
        self._max = m
        if self._max < 0:
            self._max = 0

    def init(self, iterable=()):
        if not iterable:
            return
        for i in iterable:
            self._array.append(i)
        
    def show(self):
        def _traversal(self):
            if not self._array:
                return [None]
            return self._array[::-1]
        print('\n'.join(map(lambda x: '|{:^7}|'.format(str(x)), _traversal(self)))+'\n '+7*'-')

    @property
    def length(self):
        return len(self._array)

    @property
    def is_empty(self):
        return self._array == []

    @property
    def is_full(self):
        return bool(self._max and self.length == self._max)

    def push(self, item):
        if self.is_full:
            raise StackFullException('Error: trying to push element into a full stack!')
        self._array.append(item)

    def pop(self):
        if self.is_empty:
            raise StackEmptyException('Error: trying to pop element from an empty stack!')
        return self._array.pop()

    def top(self):
        return self._array[-1]

    def clear(self):
        # self._array = []
        while self._array:
            self.pop()


if __name__ == '__main__':
    test(Stack())

