from linked_list_stack import Stack

SIGN = {'+': 1, '-': 1, '*': 2, '/': 2, '(': 3}


def infix_to_postfix(expr):
    global SIGN
    out = []
    s = Stack()
    for i in expr:
        if i in SIGN.keys():
            # Pop all high level sign except left bracket
            while s.top():
                if SIGN[s.top()] < SIGN[i] or s.top() == '(':
                    break
                out.append(s.pop())
            # Push sign
            s.push(i)
        elif i == ')':
            # Pop all sign until left bracket encountered
            while s.top() != '(':
                out.append(s.pop())
            # Pop left bracket
            s.pop()
        else:
            # Push number
            out.append(i)

    while s.top():
        out.append(s.pop())
    return out


def postfix_calc(expr):
    global SIGN
    s = Stack()
    for i in expr:
        if i in SIGN.keys():
            right = str(s.pop())
            left = str(s.pop())
            cal = ' '.join((left, i, right))
            # cal = ' '.join([str(s.pop()), i, str(s.pop())][::-1])
            s.push(eval(cal))
        else:
            s.push(i)
    return s.pop()

if __name__ == '__main__':
    ep = 'a + b * c + ( d * e + f ) * g'
    print(' '.join(infix_to_postfix(ep.split(' '))))

    ep = '( ( 2 + 3 ) * 8 + 5 + 3 ) * 6'
    print(eval(ep))
    print(postfix_calc(infix_to_postfix(ep.split(' '))))

    ep = '3 + ( 2 * 9 ) / 2 * ( 3 + 6 ) * 7'
    print(eval(ep))
    print(postfix_calc(infix_to_postfix(ep.split(' '))))

