#!/usr/bin/python
# =============================================================
# File Name: binary_heap.py
# Author: LI Ke
# Created Time: 3/22/2018 14:00:02
# =============================================================


class BinaryHeap:
    def __init__(self):
        # The element 0 is a sentinel,
        # Like the dummy piece of information in linked list.
        self.heap_list = [0]

    def __str__(self):
        return str(self.heap_list)

    @property
    def heap_size(self):
        return len(self.heap_list)-1

    def insert(self, value):
        self.heap_list.append(value)
        self._percolate_up()

    def delete_min(self):
        # Delete the min value
        self.heap_list[1], self.heap_list[self.heap_size] = self.heap_list[self.heap_size], self.heap_list[1]
        min_data = self.heap_list.pop()
        self._percolate_down()
        return min_data

    def _percolate_up(self, i=None):
        assert i != 0, 'Invalid percolate index %s' % i
        if i is None:
            i = self.heap_size
        if i < 0:
            i = self.heap_size - i - 1
        # i // 2 to get index of father node
        # i // 2 == 0 means the top node (sub of sentinel)
        while i // 2 > 0:
            if self.heap_list[i] < self.heap_list[i//2]:
                self.heap_list[i//2], self.heap_list[i] = self.heap_list[i], self.heap_list[i//2]
            i //= 2

    def _percolate_down(self, i=1):
        assert i != 0, 'Invalid percolate index %s' % i
        if i < 0:
            i = self.heap_size - i
        while (i * 2) <= self.heap_size:
            sc = self._smaller_child(i)
            if self.heap_list[i] > self.heap_list[sc]:
                self.heap_list[i], self.heap_list[sc] = self.heap_list[sc], self.heap_list[i]
            i = sc

    def _smaller_child(self, i):
        # Index out of range (no right sub node or no sub node, return left sub one)
        if i * 2 + 1 > self.heap_size:
            return i * 2
        if self.heap_list[i*2] < self.heap_list[i*2+1]:
            return i * 2
        else:
            return i * 2 + 1

    def clear(self):
        # self.heap_list = [0]
        self.__init__()

    def build_heap(self, iterable):
        self.heap_list = [0] + list(iterable)
        for i in range(self.heap_size//2, 0, -1):
            self._percolate_down(i)

    def increase_key(self, pos, cnt):
        self.heap_list[pos] += cnt
        self._percolate_down(pos)

    def decrease_key(self, pos, cnt):
        self.heap_list[pos] -= cnt
        self._percolate_up(pos)

    def delete(self, pos):
        # Decrease key to infinity(negative)
        self.decrease_key(pos, self.heap_list[pos])
        self.delete_min()


if __name__ == '__main__':
    binary_heap = BinaryHeap()
    print('Generate heap')
    for i in [13, 21, 16, 24, 31, 19, 68, 65, 26, 32]:
        binary_heap.insert(i)
    print(binary_heap)
    print('Insert element')
    binary_heap.insert(14)
    print(binary_heap)

    binary_heap.clear()

    print('Generate heap')
    for i in [13, 14, 16, 19, 21, 19, 68, 65, 26, 32, 31]:
        binary_heap.insert(i)
    print(binary_heap)
    print('Delete minimum element')
    binary_heap.delete_min()
    print(binary_heap)

    print('Build heap')
    binary_heap.build_heap([13, 21, 16, 24, 31, 19, 68, 65, 26, 32, 14])
    print(binary_heap)

    print('Increase key')
    binary_heap.build_heap([13, 14, 21, 16, 24, 31, 19, 68, 65, 32])
    binary_heap.increase_key(2, 12)
    print(binary_heap)

    print('Decrease key')
    binary_heap.build_heap([13, 21, 16, 24, 31, 19, 68, 65, 32])
    binary_heap.decrease_key(-1, 18)
    print(binary_heap)

    print('Delete key')
    binary_heap.build_heap([13, 14, 21, 16, 24, 31, 19, 68, 65, 32])
    binary_heap.delete(1)
    print(binary_heap)
