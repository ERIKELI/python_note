#!/usr/bin/python
# =============================================================
# File Name: binomial_queue.py
# Author: LI Ke
# Created Time: 3/28/2018 14:13:55
# =============================================================


class BinomialNode:
    def __init__(self, value, left_child=None, next_sibling=None):
        self.value = value
        self.left_child = left_child
        self.next_sibling = next_sibling

    def __str__(self):
        return str(self.value)


class BinomialTree:
    def __init__(self, root=None, size=-1):
        self.root = root
        self.size = size

    def __str__(self):
        return str(self.traverse(self.root)) if self.root else str(None)

    def traverse(self, root=None):
        elements = []
        def _traverse(node):
            if node is None:
                return
            elements.append(node.value)
            _traverse(node.left_child)
            _traverse(node.next_sibling)
            return elements
        if not root:
            root = self.root
        return _traverse(root)
        

    def merge(self, tree):
        assert isinstance(tree, BinomialTree), 'Invalid merge type: %s' % type(tree)
        assert self.size == tree.size, 'Invalid merge for unequal-size tree: %s and %s' % (self.size, tree.size)
        self.root = self.merge_node(self.root, tree.root)
        x = self.root.next_sibling
        y = self.root.left_child
        return self

    @staticmethod
    def merge_node(node_1, node_2):
        if node_1 is None:
            return node_2
        if node_2 is None:
            return node_1
        if node_1.value > node_2.value:
            return BinomialTree.merge_node(node_2, node_1)
        # node_1 should be small than node_2
        node_2.next_sibling = node_1.left_child
        node_1.left_child = node_2
        return node_1


class BinomialQueue:
    def __init__(self, *args, size=0):
        self.trees = [i for i in args]
        if self.size < size:
            self.trees.extend((size-self.size)*[None])

    def __str__(self):
        return str([str(t) for t in self.trees]) 

    @property
    def size(self):
        return len(self.trees)

    def merge(self, queue):
        assert isinstance(queue, BinomialQueue), 'Invalid merge type: %s' % type(queue)
        long_queue, short_queue = (self, queue) if self.size > queue.size else (queue, self)
        short_queue.trees.extend((long_queue.size-short_queue.size)*[None])

        carry = None
        for i, (h_1, h_2) in enumerate(zip(self.trees, queue.trees)):
            if h_1 is None and h_2 is None and carry is None:               # No trees
                pass
            elif h_1 is not None and h_2 is None and carry is None:         # Only h_1
                pass
            elif h_1 is None and h_2 is not None and carry is None:         # Only h_2
                self.trees[i] = h_2
                queue.trees[i] = None
            elif h_1 is None and h_2 is None and carry is not None:         # Only carry
                self.trees[i] = carry
                carry = None
            elif h_1 is not None and h_2 is not None and carry is None:     # h_1 and h_2
                carry = h_1.merge(h_2)
                self.trees[i] = queue.trees[i] = None
            elif h_1 is not None and h_2 is None and carry is not None:     # h_1 and carry
                carry = h_1.merge(carry)
                self.trees[i] = None
            elif h_1 is None and h_2 is not None and carry is not None:     # h_2 and carry
                carry = h_2.merge(carry)
                queue.trees[i] = None
            elif h_1 is not None and h_2 is not None and carry is not None: # All trees
                self.trees[i] = carry
                carry = h_1.merge(h_2)
                queue.trees[i] = None
        if carry:
            self.trees.append(carry)


if __name__ == '__main__':
    # Build first queue
    node_18 = BinomialNode(18)
    node_16 = BinomialNode(16, node_18, None)

    node_65 = BinomialNode(65)
    node_51 = BinomialNode(51)
    node_24 = BinomialNode(24, node_65, node_51)
    node_12 = BinomialNode(12, node_24, None)

    b_1_0 = None
    b_1_1 = BinomialTree(node_16)
    b_1_2 = BinomialTree(node_12)

    q_1 = BinomialQueue(b_1_0, b_1_1, b_1_2)

    # Build second queue
    node_13 = BinomialNode(13)

    node_26 = BinomialNode(26)
    node_14 = BinomialNode(14, node_26, None)

    node_65_2 = BinomialNode(65)
    node_51_2 = BinomialNode(51)
    node_24_2 = BinomialNode(24, node_65_2, node_51_2)
    node_23 = BinomialNode(23, node_24_2, None)

    b_2_0 = BinomialTree(node_13)
    b_2_1 = BinomialTree(node_14)
    b_2_2 = BinomialTree(node_23)

    b_2_2.traverse()

    q_2 = BinomialQueue(b_2_0, b_2_1, b_2_2)

    # Show the two queues
    print('-----------Original queues-------------')
    print(q_1)
    print(q_2)

    # Merge two queues
    q_1.merge(q_2)
    print('-----------Merged queue--------------')
    print(q_1)
    

