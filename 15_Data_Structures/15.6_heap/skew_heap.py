#!/usr/bin/python
# =============================================================
# File Name: skew_heap.py
# Author: LI Ke
# Created Time: 3/28/2018 10:51:47
# =============================================================


from leftist_heap import LeftistHeap, merge_test


class SkewHeap(LeftistHeap):
    @staticmethod
    def merge_node(node_1, node_2):
        if node_1 is None:
            return node_2
        if node_2 is None:
            return node_1
        smaller, bigger = (node_1, node_2) if node_1.value < node_2.value else (node_2, node_1)

        def _merge_node(sm, bg):
            if sm.left is None:
                # single node
                sm.left = bg
            else:
                sm.right = SkewHeap.merge_node(sm.right, bg)
                # No need for npl information, just swap.
                sm.swap_children()
            return sm

        return _merge_node(smaller, bigger)

if __name__ == '__main__':
    merge_test(SkewHeap)
