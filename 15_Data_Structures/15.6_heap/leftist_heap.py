#!/usr/bin/python
# =============================================================
# File Name: leftist_heap.py
# Author: LI Ke
# Created Time: 3/23/2018 14:19:27
# =============================================================


class TreeNode:
    def __init__(self, val, lft=None, rgt=None, npl=0):
        self.value = val
        self.left = lft
        self.right = rgt
        self.npl = npl      # null path length

    def __str__(self):
        return str(self.value)

    def swap_children(self):
        self.left, self.right = self.right, self.left


class LeftistHeap:
    def __init__(self, root=None):
        self.root = root

    def __str__(self):
        return '\n'.join(map(lambda x: x[1]*4*' '+str(x[0]), self.traversal()))

    def traversal(self, root=None):
        # pre-traversal
        if not root:
            root = self.root
        x = []
        depth = -1

        def _traversal(node):
            nonlocal depth
            depth += 1
            x.append((node, depth))
            if node and node.left is not None:
                _traversal(node.left)
            if node and node.right is not None:
                _traversal(node.right)
            depth -= 1
            return x
        return _traversal(root)

    def merge(self, heap):
        assert isinstance(heap, LeftistHeap), 'Invalid merge type: %s' % type(heap)
        self.root = self.merge_node(self.root, heap.root)

    @staticmethod
    def merge_node(node_1, node_2):
        if node_1 is None:
            return node_2
        if node_2 is None:
            return node_1
        smaller, bigger = (node_1, node_2) if node_1.value < node_2.value else (node_2, node_1)

        def _merge_node(sm, bg):
            if sm.left is None:
                # single node
                # sm.right is already None, and sm.npl is already 0
                sm.left = bg
            else:
                sm.right = LeftistHeap.merge_node(sm.right, bg)
                # Check the npl for children swap
                if sm.left.npl < sm.right.npl:
                    sm.swap_children()
                # Update npl of return node
                sm.npl = sm.right.npl + 1
            return sm

        return _merge_node(smaller, bigger)

    def insert(self, element):
        if not isinstance(element, LeftistHeap):
            element = LeftistHeap(TreeNode(element))
        self.merge(element)

    def delete_min(self):
        if self.root is None:
            return
        left = self.root.left
        right = self.root.right
        self.root = self.merge_node(left, right)


def merge_test(heap):
    print('---------- First heap -------------')
    node_3 = TreeNode(3, npl=1)
    node_10 = TreeNode(10, npl=1)
    node_8 = TreeNode(8, npl=0)
    node_21 = TreeNode(21, npl=0)
    node_14 = TreeNode(14, npl=0)
    node_17 = TreeNode(17, npl=0)
    node_23 = TreeNode(23, npl=0)
    node_26 = TreeNode(26, npl=0)
    node_3.left = node_10
    node_3.right = node_8
    node_10.left = node_21
    node_10.right = node_14
    node_8.left = node_17
    node_14.left = node_23
    node_17.left = node_26
    heap_a = heap(node_3)
    print(heap_a)

    print('---------- Second heap -------------')
    node_6 = TreeNode(6, npl=2)
    node_12 = TreeNode(12, npl=1)
    node_7 = TreeNode(7, npl=1)
    node_18_1 = TreeNode(18, npl=0)
    node_24 = TreeNode(24, npl=0)
    node_37 = TreeNode(37, npl=0)
    node_18_2 = TreeNode(18, npl=0)
    node_33 = TreeNode(33, npl=0)
    node_6.left = node_12
    node_6.right = node_7
    node_12.left = node_18_1
    node_12.right = node_24
    node_7.left = node_37
    node_7.right = node_18_2
    node_24.left = node_33
    heap_b = heap(node_6)
    print(heap_b)

    print('---------- Merged heap -------------')
    heap_a.merge(heap_b)
    print(heap_a)


def insert_test(heap):
    print('---------- Insert element -------------')
    node_6 = TreeNode(6, npl=2)
    node_12 = TreeNode(12, npl=1)
    node_7 = TreeNode(7, npl=1)
    node_18_1 = TreeNode(18, npl=0)
    node_24 = TreeNode(24, npl=0)
    node_37 = TreeNode(37, npl=0)
    node_18_2 = TreeNode(18, npl=0)
    node_33 = TreeNode(33, npl=0)
    node_6.left = node_12
    node_6.right = node_7
    node_12.left = node_18_1
    node_12.right = node_24
    node_7.left = node_37
    node_7.right = node_18_2
    node_24.left = node_33
    h = heap(node_6)
    h.insert(10)
    print(h)
    

def delete_min_test(heap):
    print('---------- Delete min -------------')
    node_6 = TreeNode(6, npl=2)
    node_12 = TreeNode(12, npl=1)
    node_7 = TreeNode(7, npl=1)
    node_18_1 = TreeNode(18, npl=0)
    node_24 = TreeNode(24, npl=0)
    node_37 = TreeNode(37, npl=0)
    node_18_2 = TreeNode(18, npl=0)
    node_33 = TreeNode(33, npl=0)
    node_6.left = node_12
    node_6.right = node_7
    node_12.left = node_18_1
    node_12.right = node_24
    node_7.left = node_37
    node_7.right = node_18_2
    node_24.left = node_33
    h = heap(node_6)
    h.delete_min()
    print(h)
   
    
if __name__ == '__main__':
    merge_test(LeftistHeap)
    insert_test(LeftistHeap)
    delete_min_test(LeftistHeap)


