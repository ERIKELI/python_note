from collections import Iterable


class CollisionError(Exception):
    pass


class HashTable:
    """
    Hash Table:
        [0] -> A
        [1] -> B
        [2] -> C
        [3] -> D
        [4] -> E
    """
    def __init__(self, size, fn):
        self._array = [None for i in range(size)]
        self._hashing = fn

    def __str__(self):
        return '\n'.join('[%d] %s' % (index, item) for index, item in enumerate(self._array))

    def find(self, item):
        hash_code = self._hashing(item)
        value = self._array[hash_code]
        return value if value == item else None, hash_code

    def insert(self, *args):
        for i in args:
            if isinstance(i, Iterable):
                for j in i:
                    self._insert(j)
            else:
                self._insert(i)

    def _insert(self, item):
        if item is None:
            return
        hash_code = self._hashing(item)
        value = self._array[hash_code]
        if value is not None and value != item:      # Handle value 0 and value existed situation.
            raise CollisionError('Hashing value collided!')
        self._array[hash_code] = item

    def delete(self, item):
        hash_code = self._hashing(item)
        if self._array[hash_code] != item:
            raise KeyError('Key error with %s' % item)
        self._array[hash_code] = None

    def show(self):
        print(self)

    @property
    def size(self):
        return len(self._array)

    @property
    def load_factor(self):
        element_num = sum(map(lambda x: 0 if x is None else 1, self._array))
        return element_num/self.size

    def make_empty(self):
        self._array = [None for i in range(self.size)]


def kmt_hashing(size):
    # Key = Key mod TableSize
    return lambda x: x % size


def test(h):
    print('\nShow hash table:')
    h.show()

    print('\nInsert values:')
    h.insert(7, 8, 9)
    h.insert(range(7))
    h.show()
    print('\nInsert values (existed):')
    h.insert(1)
    h.show()
    print('\nInsert value (collided):')
    try:
        h.insert(11)
    except CollisionError as e:
        print(e)

    print('\nFind value:')
    print(h.find(7))
    print('\nFind value (not existed):')
    print(h.find(77))

    print('\nDelete value:')
    h.delete(7)
    h.show()
    print('\nDelete value (not existed):')
    try:
        h.delete(111)
    except KeyError as e:
        print(e)

    print('\nLoad factor is:', h.load_factor)
    print('\nClear hash table:')
    h.make_empty()
    h.show()

if __name__ == '__main__':
    test(HashTable(10, kmt_hashing(10)))

