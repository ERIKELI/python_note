from hash_table import HashTable, kmt_hashing
from linked_list.linked_list_dummy_header import LinkedListDummyHeader as List


class SeparateChainHashing(HashTable):
    """
    Separate Chain Hashing:
        [0]  Header->11->0->110
        [1]  Header->12->1->111
        [2]  Header->2->112
        [3]  Header->14->3->113
        [4]  Header->15->4->114
        [5]  Header->16->5
        [6]  Header->17->6
        [7]  Header->18->7
        [8]  Header->19->8
        [9]  Header->9
        [10] Header->10
    """
    def __init__(self, size, fn):
        self._array = [List() for i in range(size)]
        self._hashing = fn

    def find(self, item):
        linked_list = self._array[self._hashing(item)]
        node = linked_list.header.next
        while node and node.value != item:
            node = node.next
        return node

    def _insert(self, item):
        """
                   item
                    |
                    V
        [n]  Header->node_1->node_2->node_3
        """
        if item is None:
            return
        linked_list = self._array[self._hashing(item)]
        node = linked_list.header
        while node.next:
            if node.next.value == item:  # Element existed
                return
            node = node.next
        linked_list.insert(item, 1)

    def delete(self, item):
        linked_list = self._array[self._hashing(item)]
        linked_list.delete(item)

    def show(self):
        print(self)

    @property
    def load_factor(self):
        element_num = sum(x.length-1 for x in self._array)
        return element_num/self.size

    def make_empty(self):
        # self._array = [List() for i in range(len(self._array))]
        for chain in self._array:
            chain.clear()


def test(h):
    print('\nShow hash table:')
    h.insert(110, 111, 112, 113, 114)
    h.insert(range(20))
    h.delete(13)
    h.show()
    print('\nLoad factor is:', h.load_factor)
    print('\nClear hash table:')
    h.make_empty()
    h.show()

if __name__ == '__main__':
    test(SeparateChainHashing(11, kmt_hashing(11)))

