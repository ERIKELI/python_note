from functools import partial as pro
from math import ceil, sqrt
from hash_table import HashTable, kmt_hashing


class RehashError(Exception):
    pass


class OpenAddressingHashing(HashTable):
    def __init__(self, size, hs, pb, fn=None, rf=0.5):
        self._array = [None for i in range(size)]
        self._get_hashing = hs
        self._hashing = hs(size) if not fn else fn
        self._probing = pb
        self._rehashing_factor = rf

    def _sniffing(self, item, num, hash_code=None):
        # Avoid redundant hashing calculation, if hashing calculation is heavy, this would count much.
        if not hash_code:
            hash_code = self._hashing(item)
        return (hash_code + self._probing(num, item, self.size)) % self.size

    def _get_rehashing_size(self):
        size = self.size * 2 + 1
        while not is_prime(size):
            size += 1
        return size

    def rehashing(self, size=None, fn=None):
        if not size:
            size = self._get_rehashing_size()
        if size <= (self.size * self.load_factor):
            raise RehashError('Rehash size is too small!')
        array = self._array
        self._array = [None for i in range(size)]
        self._hashing = self._get_hashing(size) if not fn else fn
        self.insert(filter(lambda x: x is not None, array))

    def find(self, item):
        hash_code = ori_hash_code = self._hashing(item)
        collision_count = 1
        value = self._array[hash_code]

        # Build up partial function to shorten time consuming when heavy sniffing encountered.
        collision_handler = pro(self._sniffing, hash_code=ori_hash_code)

        while value is not None and value != item:
            hash_code = collision_handler(item, collision_count)
            value = self._array[hash_code]
            collision_count += 1
        return value, hash_code

    def _insert(self, item):
        if item is None:
            return
        value, hash_code = self.find(item)
        if value is None:
            self._array[hash_code] = item
        if self.load_factor > self._rehashing_factor:
            self.rehashing()


def is_prime(num):  # O(sqrt(n)) algorithm
    if num < 2:
        raise Exception('Invalid number.')
    if num == 2:
        return True
    for i in range(2, ceil(sqrt(num))+1):
        if num % i == 0:
            return False
    return True


def linear_probing(x, *args):
    return x


def square_probing(x, *args):
    return x**2


def double_hashing(x, item, size, *args):
    r = size - 1
    while not is_prime(r):
        r -= 1
    return x * (r - (item % r))


def test(h):
    print('\nShow hash table:')
    h.show()

    print('\nInsert values:')
    h.insert(range(9))
    h.show()

    print('\nInsert value (existed):')
    h.insert(1)
    h.show()

    print('\nInsert value (collided):')
    h.insert(24, 47)
    h.show()

    print('\nFind value:')
    print(h.find(7))
    print('\nFind value (not existed):')
    print(h.find(77))

    print('\nLoad factor is:', h.load_factor)


if __name__ == '__main__':
    test(OpenAddressingHashing(11, kmt_hashing, linear_probing))
    print(30*'-')
    test(OpenAddressingHashing(11, kmt_hashing, square_probing))
    print(30*'-')
    test(OpenAddressingHashing(11, kmt_hashing, double_hashing))
