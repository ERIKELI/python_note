from linked_list.linked_list import Node, LinkedList, test

class LinkedListDummyHeader(LinkedList):
    def __init__(self):
        self.header = Node('Header', None)

    def init(self, iterable=()):
        if not iterable:
            return
        node = self.header
        for i in iterable:
            node.next = Node(i)
            node = node.next

    @property
    def is_empty(self):
        if self.length == 1:
            return True
        return False

    def insert(self, item, index):
        if index == 0: return
        super(LinkedListDummyHeader, self).insert(item, index)

if __name__ == '__main__':
    test(LinkedListDummyHeader())
