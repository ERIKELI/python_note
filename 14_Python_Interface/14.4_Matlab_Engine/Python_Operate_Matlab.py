import matlab.engine

eng = matlab.engine.start_matlab()

print(eng.sqrt(4.))     # 2.0
eng.plot(matlab.int32([1, 2, 3, 4]), matlab.int32([1, 2, 3, 4]))

eng.eval("hold on", nargout=0)
eng.eval("plot([4, 3, 2, 1], [1, 2, 3, 4])", nargout=0)

eng.eval("x = 3", nargout=0)
eng.eval("y = 41", nargout=0)
eng.eval("z = [213, 123]", nargout=0)
print(eng.workspace)
print(eng.workspace['x'], eng.workspace['z'])
"""
  Name      Size            Bytes  Class     Attributes

  x         1x1                 8  double
  y         1x1                 8  double
  z         1x2                16  double

3.0 [[213.0,123.0]]
"""

input("Press Enter to exit.")
eng.quit()
