import matlab.engine

# Basic usage
int_8 = matlab.int8([1, 2, 3, 4, 5, 6])
print(int_8)    # [[1, 2, 3, 4, 5, 6]]
print(int_8.size)   # (1, 6)
int_8.reshape((2, 3))   # reshape function is different from numpy
print(int_8)    # [[1, 3, 5], [2, 4, 6]]

double = matlab.double([[1, 2, 3], [4, 5, 6]])
print(double)   # [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]
print(double[0])    # [1.0, 2.0, 3.0]
print(double[1][2]) # 6.0

# Slice array
py = [[1, 2, 3], [4, 5, 6]]
mt = matlab.int32([[1, 2, 3], [4, 5, 6]])
py[0] = py[0][::-1]
mt[0] = mt[0][::-1]
# Slicing a Matlab array returns a view instead of a shallow copy
print(py)   # [[3, 2, 1], [4, 5, 6]]
print(mt)   # [[3, 2, 3], [4, 5, 6]]

