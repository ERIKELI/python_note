from win32com import client  
# interface: <module 'win32com.gen_py.2C485B34-1437-4F59-8128-C387F8E8147Bx0x1x0' from 'C:\\Users\\EKELIKE\\AppData\\Local\\Temp\\gen_py\\3.4\\2C485B34-1437-4F59-8128-C387F8E8147Bx0x1x0.py'>  
interface = client.gencache.EnsureModule("{2C485B34-1437-4F59-8128-C387F8E8147B}", 0x0, 1, 0)  
  
# interface.RuControl: <class 'win32com.gen_py.2C485B34-1437-4F59-8128-C387F8E8147Bx0x1x0.RuControl'>  
ru_control = interface.RuControl()  
# Use below code to generate instance directly without via py interface  
# ru_control = client.Dispatch("RuMaster.RuControl")  
# ru_control = client.Dispatch("{75BF5D19-5AB0-4004-97EE-F039164681A3}")  

# Note: Hardware is required to generate above instance, otherwise an error raised as below shows,  
# pywintypes.com_error: (-2147024894, 'The system cannot find the file specified.', None, None)  

# Call inside function  
ru_control.MasterReady()  
