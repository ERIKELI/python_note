# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.01
# By python version 3.4.4 (v3.4.4:737efcadf5a6, Dec 20 2015, 19:28:18) [MSC v.1600 32 bit (Intel)]
# From type library 'RuMasterLib.tlb'
# On Thu Oct 12 22:28:05 2017
'RU Master 2.20 Type Library'
makepy_version = '0.5.01'
python_version = 0x30404f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{2C485B34-1437-4F59-8128-C387F8E8147B}')
MajorVersion = 1
MinorVersion = 0
LibraryFlags = 8
LCID = 0x0

class constants:
	CH1_ASC                       =0          # from enum AscEcp
	CH1_ECP                       =1          # from enum AscEcp
	CH2_ASC                       =2          # from enum AscEcp
	CH2_ECP                       =3          # from enum AscEcp
	CH3_ASC                       =4          # from enum AscEcp
	CH3_ECP                       =5          # from enum AscEcp
	CH4_ASC                       =6          # from enum AscEcp
	CH4_ECP                       =7          # from enum AscEcp
	AUX_BR_1152                   =2          # from enum AuxBitR
	AUX_BR_384                    =1          # from enum AuxBitR
	AUX_BR_96                     =0          # from enum AuxBitR
	AUX_SOURCE_EXT_CONN           =0          # from enum AuxSource
	AUX_SOURCE_USB                =1          # from enum AuxSource
	EVENT_1                       =1          # from enum BBclkEvent
	EVENT_2                       =2          # from enum BBclkEvent
	EVENT_3                       =3          # from enum BBclkEvent
	EVENT_4                       =4          # from enum BBclkEvent
	NO_EVENT                      =0          # from enum BBclkEvent
	DISABLED                      =0          # from enum BfnControl
	FIRST_EVENT                   =1          # from enum BfnControl
	EVENT1                        =0          # from enum BfnEvent
	EVENT2                        =1          # from enum BfnEvent
	EVENT3                        =2          # from enum BfnEvent
	EVENT4                        =3          # from enum BfnEvent
	EXCEPTIONAL                   =1          # from enum BfnMode
	normal                        =0          # from enum BfnMode
	DISABLE                       =0          # from enum BfnUpdate
	EVERY_EV                      =2          # from enum BfnUpdate
	FIRST_EV                      =1          # from enum BfnUpdate
	IMMEDIATELY                   =3          # from enum BfnUpdate
	CdmaSource                    =4          # from enum CarrierSourceType
	GsmSource                     =1          # from enum CarrierSourceType
	LteSource                     =3          # from enum CarrierSourceType
	StaticSource                  =0          # from enum CarrierSourceType
	WcdmaSource                   =2          # from enum CarrierSourceType
	DCM_CLK                       =0          # from enum ClkSrc
	LTU_CLK                       =1          # from enum ClkSrc
	RET0                          =4          # from enum Com0ComPort
	TPF0                          =0          # from enum Com0ComPort
	TPF1                          =1          # from enum Com0ComPort
	TPF2                          =2          # from enum Com0ComPort
	TPF3                          =3          # from enum Com0ComPort
	CPC_FSM_DISABLE               =2          # from enum CpcFsmLoopMode
	CPC_FSM_START_OVER            =0          # from enum CpcFsmLoopMode
	CPC_FSM_STAY_IN_LAST          =1          # from enum CpcFsmLoopMode
	CNF_CASC_CW                   =1          # from enum CpriCWHandl
	CNF_L1_RES                    =2          # from enum CpriCWHandl
	CNF_REV_CW                    =0          # from enum CpriCWHandl
	CNF_Z144                      =3          # from enum CpriCWHandl
	HFNSYNC                       =4          # from enum CpriFrAlSt
	XACQ1                         =0          # from enum CpriFrAlSt
	XACQ2                         =1          # from enum CpriFrAlSt
	XSYNC1                        =2          # from enum CpriFrAlSt
	XSYNC2                        =3          # from enum CpriFrAlSt
	LINK_DIS                      =0          # from enum CpriLink
	RX_EN                         =1          # from enum CpriLink
	RX_TX_EN                      =3          # from enum CpriLink
	TX_EN                         =2          # from enum CpriLink
	LS_A                          =0          # from enum CpriLinkSt
	LS_B                          =1          # from enum CpriLinkSt
	LS_B_PL                       =2          # from enum CpriLinkSt
	LS_C                          =3          # from enum CpriLinkSt
	LS_C_PL                       =4          # from enum CpriLinkSt
	LS_D                          =5          # from enum CpriLinkSt
	LS_D_MIN                      =6          # from enum CpriLinkSt
	LS_F                          =7          # from enum CpriLinkSt
	LNK_ECP_EN                    =0          # from enum CpriLnkSrv
	LNK_ECP_IDL                   =1          # from enum CpriLnkSrv
	LNK_HDLC0_EN                  =4          # from enum CpriLnkSrv
	LNK_HDLC1_EN                  =5          # from enum CpriLnkSrv
	LNK_HDLC2_EN                  =6          # from enum CpriLnkSrv
	LNK_HDLC3_EN                  =7          # from enum CpriLnkSrv
	LNK_IQC_EN                    =2          # from enum CpriLnkSrv
	LNK_IQC_IDL                   =3          # from enum CpriLnkSrv
	MAST_RE                       =2          # from enum CpriMode
	MAST_REC                      =3          # from enum CpriMode
	PRIM_SLAVE                    =0          # from enum CpriMode
	SEC_SLAVE                     =1          # from enum CpriMode
	DECR                          =0          # from enum CpriPhShift
	INCR                          =1          # from enum CpriPhShift
	CHAN1_RECORD_PORT_A           =2          # from enum CpriRX
	CHAN1_RECORD_PORT_B           =3          # from enum CpriRX
	CHAN2_RECORD_PORT_A           =4          # from enum CpriRX
	CHAN2_RECORD_PORT_B           =5          # from enum CpriRX
	RX_CHA                        =0          # from enum CpriRX
	RX_CHB                        =1          # from enum CpriRX
	RATE_12                       =0          # from enum CpriRate
	RATE_25                       =1          # from enum CpriRate
	RATE_49                       =2          # from enum CpriRate
	RATE_98                       =3          # from enum CpriRate
	SR_096                        =1          # from enum CpriSampRate
	SR_1536                       =5          # from enum CpriSampRate
	SR_192                        =2          # from enum CpriSampRate
	SR_2304                       =6          # from enum CpriSampRate
	SR_3072                       =7          # from enum CpriSampRate
	SR_384                        =3          # from enum CpriSampRate
	SR_768                        =4          # from enum CpriSampRate
	SR_NONE                       =0          # from enum CpriSampRate
	SR_096DL                      =20         # from enum CpriSampRateDL
	SR_1536DL                     =3          # from enum CpriSampRateDL
	SR_192DL                      =21         # from enum CpriSampRateDL
	SR_2304DL                     =4          # from enum CpriSampRateDL
	SR_3072DL                     =5          # from enum CpriSampRateDL
	SR_384DL                      =1          # from enum CpriSampRateDL
	SR_768DL                      =2          # from enum CpriSampRateDL
	PACKED_CPRI                   =0          # from enum CpriSampleSrc
	RAW                           =3          # from enum CpriSampleSrc
	UL_CARRIER                    =1          # from enum CpriSampleSrc
	SY_AUTO_BFE                   =2          # from enum CpriSync
	SY_AUTO_TADV                  =3          # from enum CpriSync
	SY_INTERNAL                   =1          # from enum CpriSync
	SY_LINE                       =0          # from enum CpriSync
	TX_CHA                        =0          # from enum CpriTX
	TX_CHAB                       =2          # from enum CpriTX
	TX_CHB                        =1          # from enum CpriTX
	T_CDMA                        =4          # from enum CpriTech
	T_GSM                         =2          # from enum CpriTech
	T_LTE                         =0          # from enum CpriTech
	T_WCDMA                       =1          # from enum CpriTech
	T_WCDMA5bit                   =3          # from enum CpriTech
	TRIG1                         =0          # from enum CpriTrig
	TRIG2                         =1          # from enum CpriTrig
	CLK_122_0                     =3          # from enum CpriTrigSrc
	CLK_122_180                   =5          # from enum CpriTrigSrc
	CLK_122_270                   =6          # from enum CpriTrigSrc
	CLK_122_90                    =4          # from enum CpriTrigSrc
	CLK_122_SCAN                  =7          # from enum CpriTrigSrc
	CLK_31_SCAN                   =8          # from enum CpriTrigSrc
	CPC_TRIG                      =9          # from enum CpriTrigSrc
	CPRI_TIMING_TRIG              =12         # from enum CpriTrigSrc
	DYNAMIC_GAIN_TRIG             =11         # from enum CpriTrigSrc
	GSM_FRAMESYNC                 =10         # from enum CpriTrigSrc
	SCAN_DL_K                     =1          # from enum CpriTrigSrc
	SCAN_UL_K                     =2          # from enum CpriTrigSrc
	TRIG_INPUT                    =64         # from enum CpriTrigSrc
	TR_NONE                       =0          # from enum CpriTrigSrc
	NON_IDLE                      =0          # from enum CpriUlStart
	RADIO_FRAME                   =1          # from enum CpriUlStart
	EXTERNAL_TRIGGER              =3          # from enum CpriUlStartEx
	NON_IDLE_X                    =0          # from enum CpriUlStartEx
	RADIO_FRAME_X                 =1          # from enum CpriUlStartEx
	SAMPLE_0_STROBE               =2          # from enum CpriUlStartEx
	VERSION_1                     =1          # from enum CpriVersion
	VERSION_2                     =2          # from enum CpriVersion
	CPRI_LTE                      =1          # from enum DLFileType
	CPRI_LTE_UNPACKED             =4          # from enum DLFileType
	CPRI_TD_SCDMA                 =2          # from enum DLFileType
	CPRI_WCDMA                    =0          # from enum DLFileType
	CPRI_WCDMA_UNPACKED           =3          # from enum DLFileType
	GAMMA_DL                      =64         # from enum DLFileType
	NO_FILE                       =100        # from enum DLFileType
	UNKNOWN_DL                    =99         # from enum DLFileType
	DgcModeEdit                   =0          # from enum DgcMode
	DgcModeRun                    =1          # from enum DgcMode
	DgcModeRunActiveRow           =2          # from enum DgcMode
	WB_37_5                       =3          # from enum ElMixRatio
	WB_50                         =0          # from enum ElMixRatio
	WB_67_5                       =1          # from enum ElMixRatio
	WB_75                         =2          # from enum ElMixRatio
	POLE_0                        =0          # from enum ElPoleLoc
	POLE_M12_5                    =1          # from enum ElPoleLoc
	POLE_M25                      =2          # from enum ElPoleLoc
	POLE_M37_5                    =3          # from enum ElPoleLoc
	POLE_P12_5                    =4          # from enum ElPoleLoc
	POLE_P25                      =5          # from enum ElPoleLoc
	POLE_P37_5                    =6          # from enum ElPoleLoc
	POLE_P50                      =7          # from enum ElPoleLoc
	SWING_0                       =7          # from enum ElTxDiffSwing
	SWING_1000                    =2          # from enum ElTxDiffSwing
	SWING_1050                    =1          # from enum ElTxDiffSwing
	SWING_1100                    =0          # from enum ElTxDiffSwing
	SWING_400                     =6          # from enum ElTxDiffSwing
	SWING_600                     =5          # from enum ElTxDiffSwing
	SWING_800                     =4          # from enum ElTxDiffSwing
	SWING_900                     =3          # from enum ElTxDiffSwing
	PREEMPH_16                    =4          # from enum ElTxPreemph
	PREEMPH_2                     =0          # from enum ElTxPreemph
	PREEMPH_23                    =5          # from enum ElTxPreemph
	PREEMPH_2_5                   =1          # from enum ElTxPreemph
	PREEMPH_31                    =6          # from enum ElTxPreemph
	PREEMPH_4_5                   =2          # from enum ElTxPreemph
	PREEMPH_9_5                   =3          # from enum ElTxPreemph
	ALLWAYS                       =6          # from enum EvCondition
	BC                            =5          # from enum EvCondition
	BFN_TSC_SC_CC_BC              =1          # from enum EvCondition
	CC_BC                         =4          # from enum EvCondition
	NO_EV                         =0          # from enum EvCondition
	SC_CC_BC                      =3          # from enum EvCondition
	TSC_SC_CC_BC                  =2          # from enum EvCondition
	EVERYTIME                     =1          # from enum EvControl
	FIRST_TIME                    =0          # from enum EvControl
	EV1                           =1          # from enum EventNo
	EV2                           =2          # from enum EventNo
	EV3                           =3          # from enum EventNo
	EV4                           =4          # from enum EventNo
	FPGA1                         =0          # from enum Fpga
	FPGA2                         =1          # from enum Fpga
	Downlink                      =1          # from enum GSMTiming
	Even                          =2          # from enum GSMTiming
	AGC_A                         =0          # from enum GamAGC
	AGC_B                         =2          # from enum GamAGC
	ID_A                          =1          # from enum GamAGC
	ID_B                          =3          # from enum GamAGC
	FUNCGEN                       =2          # from enum GamDataSrc
	MEMORY                        =1          # from enum GamDataSrc
	PATTGEN                       =0          # from enum GamDataSrc
	EMPTY                         =1          # from enum GamStatus
	FULL                          =0          # from enum GamStatus
	PAR_ERR                       =2          # from enum GamStatus
	RX_CH1                        =0          # from enum GammaRX
	RX_CH2                        =1          # from enum GammaRX
	RX_CH3                        =2          # from enum GammaRX
	RX_CH4                        =3          # from enum GammaRX
	TX_CH1                        =0          # from enum GammaTX
	TX_CH1_3                      =2          # from enum GammaTX
	TX_CH3                        =1          # from enum GammaTX
	BITRATE_115_2                 =2          # from enum HDLCUart
	BITRATE_38_4                  =1          # from enum HDLCUart
	BITRATE_460_8                 =3          # from enum HDLCUart
	BITRATE_9_6                   =0          # from enum HDLCUart
	IQMAN_DISABLE                 =0          # from enum IQManCtrl
	IQ_EVERY_EV                   =2          # from enum IQManCtrl
	ONLY_FIRST_EV                 =1          # from enum IQManCtrl
	BFN                           =10         # from enum IQManPin
	IA_QA_0                       =4          # from enum IQManPin
	IA_QA_1                       =3          # from enum IQManPin
	IA_QA_2                       =2          # from enum IQManPin
	IA_QA_3                       =1          # from enum IQManPin
	IB_QB_0                       =8          # from enum IQManPin
	IB_QB_1                       =7          # from enum IQManPin
	IB_QB_2                       =6          # from enum IQManPin
	IB_QB_3                       =5          # from enum IQManPin
	ID_PARITY                     =0          # from enum IQManPin
	STB                           =9          # from enum IQManPin
	DONOTHING                     =0          # from enum IQManType
	INV_SEL_BIT                   =1          # from enum IQManType
	REPLACE                       =2          # from enum IQManType
	CPRI                          =1          # from enum IfType
	GAMMA                         =0          # from enum IfType
	LOG_ALL                       =4          # from enum OAndMLogType
	LOG_COM                       =0          # from enum OAndMLogType
	LOG_DEBUG                     =2          # from enum OAndMLogType
	LOG_EVENT                     =3          # from enum OAndMLogType
	LOG_SIGNAL                    =1          # from enum OAndMLogType
	PN_I                          =0          # from enum PN_Select
	PN_Q                          =1          # from enum PN_Select
	PARALLEL                      =0          # from enum PattGenCtrl
	TIME_MUX                      =1          # from enum PattGenCtrl
	FG_TO_SCB1                    =1          # from enum PgDest
	FG_TO_SCB1_SCB2               =3          # from enum PgDest
	FG_TO_SCB2                    =2          # from enum PgDest
	ZERO_TO_SCB1_SCB2             =0          # from enum PgDest
	IQ_CONSTANT                   =0          # from enum PgSource
	PN_GENERATOR                  =1          # from enum PgSource
	db_0_15                       =0          # from enum RX_EQUALIZER
	db_0_36                       =1          # from enum RX_EQUALIZER
	db_0_74                       =2          # from enum RX_EQUALIZER
	db_0_96                       =3          # from enum RX_EQUALIZER
	db_1_98                       =4          # from enum RX_EQUALIZER
	db_2_19                       =5          # from enum RX_EQUALIZER
	db_2_71                       =6          # from enum RX_EQUALIZER
	db_2_88                       =7          # from enum RX_EQUALIZER
	db_4_15                       =8          # from enum RX_EQUALIZER
	db_4_32                       =9          # from enum RX_EQUALIZER
	db_4_46                       =10         # from enum RX_EQUALIZER
	db_4_79                       =11         # from enum RX_EQUALIZER
	db_5_20                       =12         # from enum RX_EQUALIZER
	db_5_34                       =13         # from enum RX_EQUALIZER
	db_5_54                       =14         # from enum RX_EQUALIZER
	db_5_67                       =15         # from enum RX_EQUALIZER
	BER_LOS                       =22         # from enum ReAlarm
	BIT_ERR                       =29         # from enum ReAlarm
	BIT_ERR_IRQ                   =9          # from enum ReAlarm
	CASC_MIS                      =14         # from enum ReAlarm
	CASC_W                        =13         # from enum ReAlarm
	DELAY                         =28         # from enum ReAlarm
	DEL_CNT_IRQ                   =5          # from enum ReAlarm
	ECP_LINK                      =8          # from enum ReAlarm
	FA_FSM_LOF                    =23         # from enum ReAlarm
	FSM_N_F                       =19         # from enum ReAlarm
	HDLC_PFA                      =30         # from enum ReAlarm
	INB_L1_RES                    =0          # from enum ReAlarm
	INB_LOF                       =4          # from enum ReAlarm
	INB_LOS                       =3          # from enum ReAlarm
	INB_RAI                       =1          # from enum ReAlarm
	INB_SDI                       =2          # from enum ReAlarm
	IQC_LINK                      =7          # from enum ReAlarm
	IQ_LINK                       =6          # from enum ReAlarm
	JIT_BUF                       =25         # from enum ReAlarm
	L1_RES                        =26         # from enum ReAlarm
	LOF_SW                        =27         # from enum ReAlarm
	MAX_LR_CH                     =12         # from enum ReAlarm
	M_S_CONF                      =18         # from enum ReAlarm
	PORT_LOS                      =21         # from enum ReAlarm
	PORT_NC                       =20         # from enum ReAlarm
	REV_CH                        =17         # from enum ReAlarm
	REV_FAULT                     =16         # from enum ReAlarm
	RE_BFN                        =10         # from enum ReAlarm
	RE_HFN                        =11         # from enum ReAlarm
	SDI                           =24         # from enum ReAlarm
	VER_FAULT                     =15         # from enum ReAlarm
	EXTERNAL                      =1          # from enum RefSource
	INTERNAL                      =0          # from enum RefSource
	LTU_LOCK_AL                   =0          # from enum RumaHWAlarms
	SFP1                          =0          # from enum SFP
	SFP2                          =1          # from enum SFP
	SCAN1                         =0          # from enum Scan
	SCAN2                         =1          # from enum Scan
	NO_COMMA                      =2          # from enum ScanAlarm
	SC_LOS                        =0          # from enum ScanAlarm
	UNLOCK                        =1          # from enum ScanAlarm
	LB_NONE                       =0          # from enum ScanLB
	LINE                          =1          # from enum ScanLB
	LOCAL                         =2          # from enum ScanLB
	SPEC                          =3          # from enum ScanLB
	SRATE_06                      =1          # from enum ScanRate
	SRATE_12                      =2          # from enum ScanRate
	SRATE_25                      =3          # from enum ScanRate
	SRATE_MDIO                    =0          # from enum ScanRate
	LOW                           =1          # from enum ScanTxRx
	MAX                           =3          # from enum ScanTxRx
	MEDIUM                        =2          # from enum ScanTxRx
	NONE                          =0          # from enum ScanTxRx
	SFP_LOS                       =0          # from enum SfpAlarm
	TX_FAULT                      =1          # from enum SfpAlarm
	SUBFRAME_0                    =0          # from enum SubFrame_e
	SUBFRAME_1                    =1          # from enum SubFrame_e
	SUBFRAME_2                    =2          # from enum SubFrame_e
	SUBFRAME_3                    =3          # from enum SubFrame_e
	SUBFRAME_4                    =4          # from enum SubFrame_e
	SUBFRAME_5                    =5          # from enum SubFrame_e
	SUBFRAME_6                    =6          # from enum SubFrame_e
	SUBFRAME_7                    =7          # from enum SubFrame_e
	SUBFRAME_8                    =8          # from enum SubFrame_e
	SUBFRAME_9                    =9          # from enum SubFrame_e
	SUBFRAME_ALL                  =-1         # from enum SubFrame_e
	SWNG_1000                     =5          # from enum Swing_CT10
	SWNG_1100                     =6          # from enum Swing_CT10
	SWNG_1200                     =7          # from enum Swing_CT10
	SWNG_450                      =0          # from enum Swing_CT10
	SWNG_600                      =1          # from enum Swing_CT10
	SWNG_700                      =2          # from enum Swing_CT10
	SWNG_800                      =3          # from enum Swing_CT10
	SWNG_900                      =4          # from enum Swing_CT10
	db_0                          =0          # from enum TX_PRECURSOR_CT10
	db_0_75                       =1          # from enum TX_PRECURSOR_CT10
	db_1_5                        =2          # from enum TX_PRECURSOR_CT10
	db_2_5                        =3          # from enum TX_PRECURSOR_CT10
	db_3_5                        =4          # from enum TX_PRECURSOR_CT10
	db_4_5                        =5          # from enum TX_PRECURSOR_CT10
	db_6                          =6          # from enum TX_PRECURSOR_CT10
	MUX_0                         =3          # from enum TrigMux
	MUX_1                         =4          # from enum TrigMux
	MUX_31MHZ                     =5          # from enum TrigMux
	MUX_CPRI1                     =0          # from enum TrigMux
	MUX_CPRI2                     =1          # from enum TrigMux
	MUX_GAMMA                     =2          # from enum TrigMux
	BFNCH1                        =7          # from enum TrigSrc
	BFNCH3                        =8          # from enum TrigSrc
	RXCH1                         =1          # from enum TrigSrc
	RXCH2                         =2          # from enum TrigSrc
	RXCH3                         =3          # from enum TrigSrc
	RXCH4                         =4          # from enum TrigSrc
	TXCH1                         =5          # from enum TrigSrc
	TXCH3                         =6          # from enum TrigSrc
	LTE                           =1          # from enum UmtsType
	TD_SCDMA                      =2          # from enum UmtsType
	WCDMA                         =0          # from enum UmtsType
	BOTH                          =0          # from enum WcdmaSample
	SAMPLE_0                      =1          # from enum WcdmaSample
	SAMPLE_1                      =2          # from enum WcdmaSample

from win32com.client import DispatchBaseClass
class IRuControl1(DispatchBaseClass):
	'IRuControl1 Interface'
	CLSID = IID('{A3676616-2626-430A-ABAE-0186181F5846}')
	coclass_clsid = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')

	def BBCLK_Blanking(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, clk_cycles=defaultNamedNotOptArg, clock=defaultNamedNotOptArg):
		'method BBCLK_Blanking'
		return self._oleobj_.InvokeTypes(37, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, src, clk_cycles, clock)

	def BBCLK_Manipulation(self, chain=defaultNamedNotOptArg, clk_phase=defaultNamedNotOptArg):
		'method BBCLK_Manipulation'
		return self._oleobj_.InvokeTypes(36, LCID, 1, (17, 0), ((3, 1), (3, 1)),chain
			, clk_phase)

	def BFN_Offset(self, offset=defaultNamedNotOptArg):
		'method BFN_Offset'
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), ((3, 1),),offset
			)

	def DL_BFN_Control(self, chain=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, StartBit1=defaultNamedNotOptArg, StartBit2=defaultNamedNotOptArg):
		'method DL_BFN_Control'
		return self._oleobj_.InvokeTypes(25, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1), (11, 1)),chain
			, mode, StartBit1, StartBit2)

	def DL_BFN_OffsNum(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method DL_BFN_OffsNum'
		return self._oleobj_.InvokeTypes(23, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, TSC, SC, CC, BC)

	def DL_BFN_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, control=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_BFN_Offset'
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1), (3, 1)),chain
			, TSC, SC, CC, BC, control
			, event)

	def DL_BFN_Update(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg, Number=defaultNamedNotOptArg):
		'method DL_BFN_Update'
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (19, 1)),chain
			, src, ctrl, Number)

	def DL_EventControl(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, cond=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg):
		'method DL_EventControl'
		return self._oleobj_.InvokeTypes(28, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, ev, cond, ctrl)

	def DL_EventCount(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, BFN=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg
			, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg):
		'method DL_EventCount'
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((3, 1), (3, 1), (18, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, ev, BFN, TSC, SC, CC
			, BC)

	def DL_GammaCarrier(self, chain=defaultNamedNotOptArg, CarrierID_A=defaultNamedNotOptArg, CarrierID_B=defaultNamedNotOptArg):
		'method DL_GammaCarrier'
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, CarrierID_A, CarrierID_B)

	def DL_GammaDataSrc(self, chain=defaultNamedNotOptArg, Source=defaultNamedNotOptArg):
		'method DL_GammaDataSrc'
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((3, 1), (3, 1)),chain
			, Source)

	def DL_GammaScale(self, chain=defaultNamedNotOptArg, factor_A_dB=defaultNamedNotOptArg, factor_B_dB=defaultNamedNotOptArg):
		'method DL_GammaScale'
		return self._oleobj_.InvokeTypes(29, LCID, 1, (24, 0), ((3, 1), (5, 1), (5, 1)),chain
			, factor_A_dB, factor_B_dB)

	def DL_Strobe_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_Strobe_Offset'
		return self._oleobj_.InvokeTypes(38, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1)),chain
			, TSC, SC, CC, BC, event
			)

	def ExtReferenceLock(self):
		'method ExtReferenceLock'
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), (),)

	def ExternalPatternGen(self, input=defaultNamedNotOptArg):
		'method ExternalPatternGen'
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), ((3, 1),),input
			)

	def ExternalTrig(self, dstrb_src=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method ExternalTrig'
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),dstrb_src
			, TSC, SC, CC, BC)

	def FG_GammaConstant(self, chain=defaultNamedNotOptArg, Constant_I=defaultNamedNotOptArg, Constant_Q=defaultNamedNotOptArg):
		'method FG_GammaConstant'
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, Constant_I, Constant_Q)

	def FG_GammaGenCtrl(self, chain=defaultNamedNotOptArg, dst=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method FG_GammaGenCtrl'
		return self._oleobj_.InvokeTypes(30, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1)),chain
			, dst, src)

	def FG_GammaPnGenSeed(self, chain=defaultNamedNotOptArg, SeedReg_I_Hi=defaultNamedNotOptArg, SeedReg_I_Lo=defaultNamedNotOptArg, SeedReg_Q_Hi=defaultNamedNotOptArg
			, SeedReg_Q_Lo=defaultNamedNotOptArg):
		'method FG_GammaPnGenSeed'
		return self._oleobj_.InvokeTypes(33, LCID, 1, (24, 0), ((3, 1), (19, 1), (19, 1), (19, 1), (19, 1)),chain
			, SeedReg_I_Hi, SeedReg_I_Lo, SeedReg_Q_Hi, SeedReg_Q_Lo)

	def FG_GammaPnGenStat(self, chain=defaultNamedNotOptArg, pn=defaultNamedNotOptArg):
		'method FG_GammaPnGenStat'
		return self._oleobj_.InvokeTypes(32, LCID, 1, (11, 0), ((3, 1), (3, 1)),chain
			, pn)

	def GetDeviceInfo(self):
		'method GetDeviceInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 1, (8, 0), (),)

	def GetFPGAVersion(self):
		'method GetFPGAVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 1, (8, 0), (),)

	def GetIOBoardInfo(self):
		'method GetIOBoardInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 1, (8, 0), (),)

	def IQ_ManipulationCtrl(self, chain=defaultNamedNotOptArg, man_type=defaultNamedNotOptArg, man_ctrl=defaultNamedNotOptArg, event=defaultNamedNotOptArg
			, num_bits=defaultNamedNotOptArg, man_pin=defaultNamedNotOptArg, manipulation_seq=defaultNamedNotOptArg):
		'method IQ_ManipulationCtrl'
		return self._oleobj_.InvokeTypes(34, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (17, 1), (3, 1), (18, 1)),chain
			, man_type, man_ctrl, event, num_bits, man_pin
			, manipulation_seq)

	def IQ_ManipulationMask(self, chain=defaultNamedNotOptArg, mstb=defaultNamedNotOptArg, ma0=defaultNamedNotOptArg, ma1=defaultNamedNotOptArg
			, ma2=defaultNamedNotOptArg, ma3=defaultNamedNotOptArg, mb0=defaultNamedNotOptArg, mb1=defaultNamedNotOptArg, mb2=defaultNamedNotOptArg
			, mb3=defaultNamedNotOptArg, mid_par=defaultNamedNotOptArg):
		'method IQ_ManipulationMask'
		return self._oleobj_.InvokeTypes(35, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1)),chain
			, mstb, ma0, ma1, ma2, ma3
			, mb0, mb1, mb2, mb3, mid_par
			)

	def LAPodOutput(self, enable=defaultNamedNotOptArg):
		'method LAPodOutput'
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((11, 0),),enable
			)

	def LA_ReadData(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method LA_ReadData'
		return self._ApplyTypes_(18, 1, (12, 0), ((3, 1), (19, 1)), 'LA_ReadData', None,chain
			, num_kB)

	def LA_SaveToFile(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LA_SaveToFile'
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((3, 1), (19, 1), (8, 1)),chain
			, num_kB, filename)

	def LA_StartTrig(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg
			, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg, ext_trig_on=defaultNamedNotOptArg, int_trig_off=defaultNamedNotOptArg):
		'method LA_StartTrig'
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((3, 1), (19, 1), (17, 1), (17, 1), (18, 1), (17, 1), (11, 1), (11, 1)),chain
			, num_kB, TSC, SC, CC, BC
			, ext_trig_on, int_trig_off)

	def LoadPatternFile(self, chain=defaultNamedNotOptArg, index=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LoadPatternFile'
		return self._oleobj_.InvokeTypes(12, LCID, 1, (19, 0), ((3, 1), (17, 1), (8, 1)),chain
			, index, filename)

	def MasterReady(self):
		'method MasterReady'
		return self._oleobj_.InvokeTypes(6, LCID, 1, (11, 0), (),)

	def PatternFileSelect(self, chain=defaultNamedNotOptArg, fileindx1=defaultNamedNotOptArg, fileindx2=defaultNamedNotOptArg, on=defaultNamedNotOptArg):
		'method PatternFileSelect'
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (11, 1)),chain
			, fileindx1, fileindx2, on)

	def ReceExtI2C(self, I2C_Address=defaultNamedNotOptArg, size=defaultNamedNotOptArg):
		'method ReceExtI2C'
		return self._ApplyTypes_(9, 1, (12, 0), ((17, 1), (17, 1)), 'ReceExtI2C', None,I2C_Address
			, size)

	def SendExtI2C(self, I2C_Address=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method SendExtI2C'
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((17, 1), (12, 1)),I2C_Address
			, data)

	def ShowWindow(self, normal=defaultNamedNotOptArg):
		'method ShowWindow'
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((11, 1),),normal
			)

	def ShutDown(self):
		'method ShutDown'
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def UL_GammaAGC(self, chain=defaultNamedNotOptArg, ga=defaultNamedNotOptArg):
		'method UL_GammaAGC'
		return self._oleobj_.InvokeTypes(20, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, ga)

	def UL_GammaStatus(self, chain=defaultNamedNotOptArg, gs=defaultNamedNotOptArg):
		'method UL_GammaStatus'
		return self._oleobj_.InvokeTypes(19, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, gs)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

class IRuControl2(DispatchBaseClass):
	'IRuControl2 Interface'
	CLSID = IID('{5C05EA90-661D-11D5-A4BC-00608CF21B5F}')
	coclass_clsid = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')

	def BBCLK_Blanking(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, clk_cycles=defaultNamedNotOptArg, clock=defaultNamedNotOptArg):
		'method BBCLK_Blanking'
		return self._oleobj_.InvokeTypes(37, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, src, clk_cycles, clock)

	def BBCLK_Manipulation(self, chain=defaultNamedNotOptArg, clk_phase=defaultNamedNotOptArg):
		'method BBCLK_Manipulation'
		return self._oleobj_.InvokeTypes(36, LCID, 1, (17, 0), ((3, 1), (3, 1)),chain
			, clk_phase)

	def BFN_Offset(self, offset=defaultNamedNotOptArg):
		'method BFN_Offset'
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), ((3, 1),),offset
			)

	def DL_BFN_Control(self, chain=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, StartBit1=defaultNamedNotOptArg, StartBit2=defaultNamedNotOptArg):
		'method DL_BFN_Control'
		return self._oleobj_.InvokeTypes(25, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1), (11, 1)),chain
			, mode, StartBit1, StartBit2)

	def DL_BFN_OffsNum(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method DL_BFN_OffsNum'
		return self._oleobj_.InvokeTypes(23, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, TSC, SC, CC, BC)

	def DL_BFN_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, control=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_BFN_Offset'
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1), (3, 1)),chain
			, TSC, SC, CC, BC, control
			, event)

	def DL_BFN_Update(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg, Number=defaultNamedNotOptArg):
		'method DL_BFN_Update'
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (19, 1)),chain
			, src, ctrl, Number)

	def DL_EventControl(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, cond=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg):
		'method DL_EventControl'
		return self._oleobj_.InvokeTypes(28, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, ev, cond, ctrl)

	def DL_EventCount(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, BFN=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg
			, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg):
		'method DL_EventCount'
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((3, 1), (3, 1), (18, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, ev, BFN, TSC, SC, CC
			, BC)

	def DL_GammaCarrier(self, chain=defaultNamedNotOptArg, CarrierID_A=defaultNamedNotOptArg, CarrierID_B=defaultNamedNotOptArg):
		'method DL_GammaCarrier'
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, CarrierID_A, CarrierID_B)

	def DL_GammaDataSrc(self, chain=defaultNamedNotOptArg, Source=defaultNamedNotOptArg):
		'method DL_GammaDataSrc'
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((3, 1), (3, 1)),chain
			, Source)

	def DL_GammaScale(self, chain=defaultNamedNotOptArg, factor_A_dB=defaultNamedNotOptArg, factor_B_dB=defaultNamedNotOptArg):
		'method DL_GammaScale'
		return self._oleobj_.InvokeTypes(29, LCID, 1, (24, 0), ((3, 1), (5, 1), (5, 1)),chain
			, factor_A_dB, factor_B_dB)

	def DL_Strobe_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_Strobe_Offset'
		return self._oleobj_.InvokeTypes(38, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1)),chain
			, TSC, SC, CC, BC, event
			)

	def EnableRuFPGA(self, enable=defaultNamedNotOptArg):
		'method EnableRuFPGA'
		return self._oleobj_.InvokeTypes(42, LCID, 1, (24, 0), ((11, 1),),enable
			)

	def ExtReferenceLock(self):
		'method ExtReferenceLock'
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), (),)

	def ExternalPatternGen(self, input=defaultNamedNotOptArg):
		'method ExternalPatternGen'
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), ((3, 1),),input
			)

	def ExternalTrig(self, dstrb_src=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method ExternalTrig'
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),dstrb_src
			, TSC, SC, CC, BC)

	def FG_GammaConstant(self, chain=defaultNamedNotOptArg, Constant_I=defaultNamedNotOptArg, Constant_Q=defaultNamedNotOptArg):
		'method FG_GammaConstant'
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, Constant_I, Constant_Q)

	def FG_GammaGenCtrl(self, chain=defaultNamedNotOptArg, dst=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method FG_GammaGenCtrl'
		return self._oleobj_.InvokeTypes(30, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1)),chain
			, dst, src)

	def FG_GammaPnGenSeed(self, chain=defaultNamedNotOptArg, SeedReg_I_Hi=defaultNamedNotOptArg, SeedReg_I_Lo=defaultNamedNotOptArg, SeedReg_Q_Hi=defaultNamedNotOptArg
			, SeedReg_Q_Lo=defaultNamedNotOptArg):
		'method FG_GammaPnGenSeed'
		return self._oleobj_.InvokeTypes(33, LCID, 1, (24, 0), ((3, 1), (19, 1), (19, 1), (19, 1), (19, 1)),chain
			, SeedReg_I_Hi, SeedReg_I_Lo, SeedReg_Q_Hi, SeedReg_Q_Lo)

	def FG_GammaPnGenStat(self, chain=defaultNamedNotOptArg, pn=defaultNamedNotOptArg):
		'method FG_GammaPnGenStat'
		return self._oleobj_.InvokeTypes(32, LCID, 1, (11, 0), ((3, 1), (3, 1)),chain
			, pn)

	def GammaScaleFile(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method GammaScaleFile'
		return self._oleobj_.InvokeTypes(40, LCID, 1, (24, 0), ((3, 1), (8, 1)),chain
			, filename)

	def GammaScaleRemoveAll(self, chain=defaultNamedNotOptArg):
		'method GammaScaleRemoveAll'
		return self._oleobj_.InvokeTypes(41, LCID, 1, (24, 0), ((3, 1),),chain
			)

	def GetDeviceInfo(self):
		'method GetDeviceInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 1, (8, 0), (),)

	def GetFPGAVersion(self):
		'method GetFPGAVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 1, (8, 0), (),)

	def GetIOBoardInfo(self):
		'method GetIOBoardInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 1, (8, 0), (),)

	def IQ_ManipulationCtrl(self, chain=defaultNamedNotOptArg, man_type=defaultNamedNotOptArg, man_ctrl=defaultNamedNotOptArg, event=defaultNamedNotOptArg
			, num_bits=defaultNamedNotOptArg, man_pin=defaultNamedNotOptArg, manipulation_seq=defaultNamedNotOptArg):
		'method IQ_ManipulationCtrl'
		return self._oleobj_.InvokeTypes(34, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (17, 1), (3, 1), (18, 1)),chain
			, man_type, man_ctrl, event, num_bits, man_pin
			, manipulation_seq)

	def IQ_ManipulationMask(self, chain=defaultNamedNotOptArg, mstb=defaultNamedNotOptArg, ma0=defaultNamedNotOptArg, ma1=defaultNamedNotOptArg
			, ma2=defaultNamedNotOptArg, ma3=defaultNamedNotOptArg, mb0=defaultNamedNotOptArg, mb1=defaultNamedNotOptArg, mb2=defaultNamedNotOptArg
			, mb3=defaultNamedNotOptArg, mid_par=defaultNamedNotOptArg):
		'method IQ_ManipulationMask'
		return self._oleobj_.InvokeTypes(35, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1)),chain
			, mstb, ma0, ma1, ma2, ma3
			, mb0, mb1, mb2, mb3, mid_par
			)

	def LAPodOutput(self, enable=defaultNamedNotOptArg):
		'method LAPodOutput'
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((11, 0),),enable
			)

	def LA_ReadData(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method LA_ReadData'
		return self._ApplyTypes_(18, 1, (12, 0), ((3, 1), (19, 1)), 'LA_ReadData', None,chain
			, num_kB)

	def LA_SaveToFile(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LA_SaveToFile'
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((3, 1), (19, 1), (8, 1)),chain
			, num_kB, filename)

	def LA_StartTrig(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg
			, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg, ext_trig_on=defaultNamedNotOptArg, int_trig_off=defaultNamedNotOptArg):
		'method LA_StartTrig'
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((3, 1), (19, 1), (17, 1), (17, 1), (18, 1), (17, 1), (11, 1), (11, 1)),chain
			, num_kB, TSC, SC, CC, BC
			, ext_trig_on, int_trig_off)

	def LoadPatternFile(self, chain=defaultNamedNotOptArg, index=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LoadPatternFile'
		return self._oleobj_.InvokeTypes(12, LCID, 1, (19, 0), ((3, 1), (17, 1), (8, 1)),chain
			, index, filename)

	def MasterReady(self):
		'method MasterReady'
		return self._oleobj_.InvokeTypes(6, LCID, 1, (11, 0), (),)

	def PatternFileList(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method PatternFileList'
		return self._oleobj_.InvokeTypes(39, LCID, 1, (19, 0), ((3, 1), (8, 1)),chain
			, filename)

	def PatternFileSelect(self, chain=defaultNamedNotOptArg, fileindx1=defaultNamedNotOptArg, fileindx2=defaultNamedNotOptArg, on=defaultNamedNotOptArg):
		'method PatternFileSelect'
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (11, 1)),chain
			, fileindx1, fileindx2, on)

	def ReceExtI2C(self, I2C_Address=defaultNamedNotOptArg, size=defaultNamedNotOptArg):
		'method ReceExtI2C'
		return self._ApplyTypes_(9, 1, (12, 0), ((17, 1), (17, 1)), 'ReceExtI2C', None,I2C_Address
			, size)

	def RuFPGA(self, cmd=defaultNamedNotOptArg, size=defaultNamedNotOptArg, address=defaultNamedNotOptArg, dwnlink=defaultNamedNotOptArg):
		'method RuFPGA'
		return self._ApplyTypes_(43, 1, (12, 0), ((17, 1), (18, 1), (19, 1), (12, 1)), 'RuFPGA', None,cmd
			, size, address, dwnlink)

	def SendExtI2C(self, I2C_Address=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method SendExtI2C'
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((17, 1), (12, 1)),I2C_Address
			, data)

	def ShowWindow(self, normal=defaultNamedNotOptArg):
		'method ShowWindow'
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((11, 1),),normal
			)

	def ShutDown(self):
		'method ShutDown'
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def UL_GammaAGC(self, chain=defaultNamedNotOptArg, ga=defaultNamedNotOptArg):
		'method UL_GammaAGC'
		return self._oleobj_.InvokeTypes(20, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, ga)

	def UL_GammaStatus(self, chain=defaultNamedNotOptArg, gs=defaultNamedNotOptArg):
		'method UL_GammaStatus'
		return self._oleobj_.InvokeTypes(19, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, gs)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

class IRuControl3(DispatchBaseClass):
	'IRuControl3 Interface'
	CLSID = IID('{03589009-265A-4FA8-A3F7-081CDB2DD05A}')
	coclass_clsid = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')

	def BBCLK_Blanking(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, clk_cycles=defaultNamedNotOptArg, clock=defaultNamedNotOptArg):
		'method BBCLK_Blanking'
		return self._oleobj_.InvokeTypes(37, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, src, clk_cycles, clock)

	def BBCLK_Manipulation(self, chain=defaultNamedNotOptArg, clk_phase=defaultNamedNotOptArg):
		'method BBCLK_Manipulation'
		return self._oleobj_.InvokeTypes(36, LCID, 1, (17, 0), ((3, 1), (3, 1)),chain
			, clk_phase)

	def BFN_Offset(self, offset=defaultNamedNotOptArg):
		'method BFN_Offset'
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), ((3, 1),),offset
			)

	def CI_DL_LoadFile(self, chan=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method CI_DL_LoadFile'
		return self._oleobj_.InvokeTypes(44, LCID, 1, (24, 0), ((3, 1), (8, 1)),chan
			, filename)

	def CI_DL_StartTransfer(self, chan=defaultNamedNotOptArg):
		'method CI_DL_StartTransfer'
		return self._oleobj_.InvokeTypes(45, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_DL_StopTransfer(self, chan=defaultNamedNotOptArg):
		'method CI_DL_StopTransfer'
		return self._oleobj_.InvokeTypes(46, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_FPGA_GetCtrlBuild(self):
		'method CI_FPGA_GetCtrlBuild'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(90, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetCtrlVersion(self):
		'method CI_FPGA_GetCtrlVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(89, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetIoBuild(self):
		'method CI_FPGA_GetIoBuild'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(92, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetIoVersion(self):
		'method CI_FPGA_GetIoVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(91, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetPll(self):
		'method CI_FPGA_GetPll'
		return self._oleobj_.InvokeTypes(93, LCID, 1, (11, 0), (),)

	def CI_FPGA_GetPs1(self):
		'method CI_FPGA_GetPs1'
		return self._oleobj_.InvokeTypes(94, LCID, 1, (11, 0), (),)

	def CI_FPGA_GetPs2(self):
		'method CI_FPGA_GetPs2'
		return self._oleobj_.InvokeTypes(95, LCID, 1, (11, 0), (),)

	def CI_FPGA_Reset(self):
		'method CI_FPGA_Reset'
		return self._oleobj_.InvokeTypes(96, LCID, 1, (24, 0), (),)

	def CI_REUSE_ClearAlarm(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_ClearAlarm'
		return self._oleobj_.InvokeTypes(51, LCID, 1, (24, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_EnableCwHandl(self, chan=defaultNamedNotOptArg, handl=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method CI_REUSE_EnableCwHandl'
		return self._oleobj_.InvokeTypes(56, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1)),chan
			, handl, enable)

	def CI_REUSE_GetAlarm(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_GetAlarm'
		return self._oleobj_.InvokeTypes(49, LCID, 1, (11, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_GetAlarmCnt(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(50, LCID, 1, (18, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_GetCWDetect(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetect'
		return self._oleobj_.InvokeTypes(62, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectAdd(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectAdd'
		return self._oleobj_.InvokeTypes(63, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectMast(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectMast'
		return self._oleobj_.InvokeTypes(66, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectRev(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectRev'
		return self._oleobj_.InvokeTypes(65, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectTim(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectTim'
		return self._oleobj_.InvokeTypes(64, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetFsmStatFa(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetFsmStatFa'
		return self._oleobj_.InvokeTypes(58, LCID, 1, (3, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetFsmStatLink(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetFsmStatLink'
		return self._oleobj_.InvokeTypes(59, LCID, 1, (3, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetHsbCw(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetHsbCw'
		return self._oleobj_.InvokeTypes(70, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetJitBufDelCntC(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetJitBufDelCntC'
		return self._oleobj_.InvokeTypes(60, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetJitBufDelCntF(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetJitBufDelCntF'
		return self._oleobj_.InvokeTypes(61, LCID, 1, (18, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetTADetectBfr(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectBfr'
		return self._oleobj_.InvokeTypes(68, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetTADetectHfr(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectHfr'
		return self._oleobj_.InvokeTypes(69, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetTADetectW(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectW'
		return self._oleobj_.InvokeTypes(67, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetVersion(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetVersion'
		return self._oleobj_.InvokeTypes(71, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_SetDelCntThr(self, chan=defaultNamedNotOptArg, delCntThr=defaultNamedNotOptArg):
		'method CI_REUSE_SetDelCntThr'
		return self._oleobj_.InvokeTypes(54, LCID, 1, (24, 0), ((3, 1), (17, 1)),chan
			, delCntThr)

	def CI_REUSE_SetLinkConf(self, chan=defaultNamedNotOptArg, lineRate=defaultNamedNotOptArg, enLink=defaultNamedNotOptArg, mode=defaultNamedNotOptArg
			, l1Res=defaultNamedNotOptArg):
		'method CI_REUSE_SetLinkConf'
		return self._oleobj_.InvokeTypes(52, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (11, 1)),chan
			, lineRate, enLink, mode, l1Res)

	def CI_REUSE_SetLnkSrvCtrl(self, chan=defaultNamedNotOptArg, srv=defaultNamedNotOptArg, setSrv=defaultNamedNotOptArg):
		'method CI_REUSE_SetLnkSrvCtrl '
		return self._oleobj_.InvokeTypes(57, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1)),chan
			, srv, setSrv)

	def CI_REUSE_SetSyncSrc(self, chan=defaultNamedNotOptArg, syncSrc=defaultNamedNotOptArg):
		'method CI_REUSE_SetSyncSrc'
		return self._oleobj_.InvokeTypes(55, LCID, 1, (24, 0), ((3, 1), (3, 1)),chan
			, syncSrc)

	def CI_REUSE_SetupCW(self, chan=defaultNamedNotOptArg, cascW=defaultNamedNotOptArg, timSyncOn=defaultNamedNotOptArg, protRev=defaultNamedNotOptArg
			, enChain=defaultNamedNotOptArg):
		'method CI_REUSE_SetupCW'
		return self._oleobj_.InvokeTypes(53, LCID, 1, (24, 0), ((3, 1), (17, 1), (11, 1), (17, 1), (11, 1)),chan
			, cascW, timSyncOn, protRev, enChain)

	def CI_SCAN_ClearAlarm(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_ClearAlarm'
		return self._oleobj_.InvokeTypes(82, LCID, 1, (24, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_GetAlarm(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_GetAlarm'
		return self._oleobj_.InvokeTypes(80, LCID, 1, (11, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_GetAlarmCnt(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(81, LCID, 1, (18, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_ReadReg(self, Scan=defaultNamedNotOptArg, reg=defaultNamedNotOptArg):
		'method CI_SCAN_ReadReg'
		return self._oleobj_.InvokeTypes(79, LCID, 1, (18, 0), ((3, 1), (18, 1)),Scan
			, reg)

	def CI_SCAN_Reset(self, Scan=defaultNamedNotOptArg):
		'method CI_SCAN_Reset'
		return self._oleobj_.InvokeTypes(77, LCID, 1, (24, 0), ((3, 1),),Scan
			)

	def CI_SCAN_SetCmd(self, Scan=defaultNamedNotOptArg, txPwD=defaultNamedNotOptArg, rxPwD=defaultNamedNotOptArg, enComAl=defaultNamedNotOptArg):
		'method CI_SCAN_SetCmd'
		return self._oleobj_.InvokeTypes(75, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1)),Scan
			, txPwD, rxPwD, enComAl)

	def CI_SCAN_SetModes(self, Scan=defaultNamedNotOptArg, txDeEmph=defaultNamedNotOptArg, rxEqu=defaultNamedNotOptArg, bRate=defaultNamedNotOptArg
			, lbMode=defaultNamedNotOptArg):
		'method CI_SCAN_SetModes'
		return self._oleobj_.InvokeTypes(76, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),Scan
			, txDeEmph, rxEqu, bRate, lbMode)

	def CI_SCAN_WriteReg(self, Scan=defaultNamedNotOptArg, reg=defaultNamedNotOptArg, Value=defaultNamedNotOptArg):
		'method CI_SCAN_WriteReg'
		return self._oleobj_.InvokeTypes(78, LCID, 1, (24, 0), ((3, 1), (18, 1), (18, 1)),Scan
			, reg, Value)

	def CI_SFP_ClearAlarm(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_ClearAlarm'
		return self._oleobj_.InvokeTypes(88, LCID, 1, (24, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_Disable(self, SFP=defaultNamedNotOptArg, DISABLE=defaultNamedNotOptArg):
		'method CI_SFP_Disable'
		return self._oleobj_.InvokeTypes(85, LCID, 1, (24, 0), ((3, 1), (11, 1)),SFP
			, DISABLE)

	def CI_SFP_GetAlarm(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_GetAlarm'
		return self._oleobj_.InvokeTypes(86, LCID, 1, (11, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_GetAlarmCnt(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(87, LCID, 1, (18, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_GetDisabled(self, SFP=defaultNamedNotOptArg):
		'method CI_SFP_GetDisabled'
		return self._oleobj_.InvokeTypes(84, LCID, 1, (11, 0), ((3, 1),),SFP
			)

	def CI_SFP_GetPresent(self, SFP=defaultNamedNotOptArg):
		'method CI_SFP_GetPresent'
		return self._oleobj_.InvokeTypes(83, LCID, 1, (11, 0), ((3, 1),),SFP
			)

	def CI_TD_GetGain(self, subframe=defaultNamedNotOptArg, slot=defaultNamedNotOptArg, axc=defaultNamedNotOptArg, pattfilename=defaultNamedNotOptArg
			, KUbpDbm=defaultNamedNotOptArg, skipAgcBit=defaultNamedNotOptArg, useAllChips=defaultNamedNotOptArg):
		'method CI_TD_GetGain'
		return self._oleobj_.InvokeTypes(98, LCID, 1, (5, 0), ((17, 1), (17, 1), (17, 1), (8, 1), (5, 1), (11, 1), (11, 1)),subframe
			, slot, axc, pattfilename, KUbpDbm, skipAgcBit
			, useAllChips)

	def CI_TD_SaveIQ(self, subframe=defaultNamedNotOptArg, slot=defaultNamedNotOptArg, axc=defaultNamedNotOptArg, pattfilename=defaultNamedNotOptArg
			, iqfilename=defaultNamedNotOptArg):
		'method CI_TD_SaveIQ'
		return self._oleobj_.InvokeTypes(97, LCID, 1, (24, 0), ((17, 1), (17, 1), (17, 1), (8, 1), (8, 1)),subframe
			, slot, axc, pattfilename, iqfilename)

	def CI_TRIG_ResetPhase(self, trig=defaultNamedNotOptArg):
		'method CI_TRIG_ResetPhase'
		return self._oleobj_.InvokeTypes(73, LCID, 1, (24, 0), ((3, 1),),trig
			)

	def CI_TRIG_SetTrigSrc(self, trig=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method CI_TRIG_SetTrigSrc'
		return self._oleobj_.InvokeTypes(74, LCID, 1, (24, 0), ((3, 1), (3, 1)),trig
			, src)

	def CI_TRIG_ShiftPhase(self, trig=defaultNamedNotOptArg, shift=defaultNamedNotOptArg):
		'method CI_TRIG_ShiftPhase'
		return self._oleobj_.InvokeTypes(72, LCID, 1, (24, 0), ((3, 1), (3, 1)),trig
			, shift)

	def CI_UL_Save(self, chan=defaultNamedNotOptArg, comment=defaultNamedNotOptArg, type=defaultNamedNotOptArg, lineRate=defaultNamedNotOptArg
			, wordLength=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method CI_UL_Save'
		return self._oleobj_.InvokeTypes(48, LCID, 1, (24, 0), ((3, 1), (8, 1), (3, 1), (3, 1), (17, 1), (8, 1)),chan
			, comment, type, lineRate, wordLength, filename
			)

	def CI_UL_StartSampling(self, chan=defaultNamedNotOptArg, crit=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method CI_UL_StartSampling'
		return self._oleobj_.InvokeTypes(47, LCID, 1, (24, 0), ((3, 1), (3, 1), (19, 1)),chan
			, crit, num_kB)

	def DL_BFN_Control(self, chain=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, StartBit1=defaultNamedNotOptArg, StartBit2=defaultNamedNotOptArg):
		'method DL_BFN_Control'
		return self._oleobj_.InvokeTypes(25, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1), (11, 1)),chain
			, mode, StartBit1, StartBit2)

	def DL_BFN_OffsNum(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method DL_BFN_OffsNum'
		return self._oleobj_.InvokeTypes(23, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, TSC, SC, CC, BC)

	def DL_BFN_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, control=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_BFN_Offset'
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1), (3, 1)),chain
			, TSC, SC, CC, BC, control
			, event)

	def DL_BFN_Update(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg, Number=defaultNamedNotOptArg):
		'method DL_BFN_Update'
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (19, 1)),chain
			, src, ctrl, Number)

	def DL_EventControl(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, cond=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg):
		'method DL_EventControl'
		return self._oleobj_.InvokeTypes(28, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, ev, cond, ctrl)

	def DL_EventCount(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, BFN=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg
			, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg):
		'method DL_EventCount'
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((3, 1), (3, 1), (18, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, ev, BFN, TSC, SC, CC
			, BC)

	def DL_GammaCarrier(self, chain=defaultNamedNotOptArg, CarrierID_A=defaultNamedNotOptArg, CarrierID_B=defaultNamedNotOptArg):
		'method DL_GammaCarrier'
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, CarrierID_A, CarrierID_B)

	def DL_GammaDataSrc(self, chain=defaultNamedNotOptArg, Source=defaultNamedNotOptArg):
		'method DL_GammaDataSrc'
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((3, 1), (3, 1)),chain
			, Source)

	def DL_GammaScale(self, chain=defaultNamedNotOptArg, factor_A_dB=defaultNamedNotOptArg, factor_B_dB=defaultNamedNotOptArg):
		'method DL_GammaScale'
		return self._oleobj_.InvokeTypes(29, LCID, 1, (24, 0), ((3, 1), (5, 1), (5, 1)),chain
			, factor_A_dB, factor_B_dB)

	def DL_Strobe_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_Strobe_Offset'
		return self._oleobj_.InvokeTypes(38, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1)),chain
			, TSC, SC, CC, BC, event
			)

	def EnableRuFPGA(self, enable=defaultNamedNotOptArg):
		'method EnableRuFPGA'
		return self._oleobj_.InvokeTypes(42, LCID, 1, (24, 0), ((11, 1),),enable
			)

	def ExtReferenceLock(self):
		'method ExtReferenceLock'
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), (),)

	def ExternalPatternGen(self, input=defaultNamedNotOptArg):
		'method ExternalPatternGen'
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), ((3, 1),),input
			)

	def ExternalTrig(self, dstrb_src=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method ExternalTrig'
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),dstrb_src
			, TSC, SC, CC, BC)

	def FG_GammaConstant(self, chain=defaultNamedNotOptArg, Constant_I=defaultNamedNotOptArg, Constant_Q=defaultNamedNotOptArg):
		'method FG_GammaConstant'
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, Constant_I, Constant_Q)

	def FG_GammaGenCtrl(self, chain=defaultNamedNotOptArg, dst=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method FG_GammaGenCtrl'
		return self._oleobj_.InvokeTypes(30, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1)),chain
			, dst, src)

	def FG_GammaPnGenSeed(self, chain=defaultNamedNotOptArg, SeedReg_I_Hi=defaultNamedNotOptArg, SeedReg_I_Lo=defaultNamedNotOptArg, SeedReg_Q_Hi=defaultNamedNotOptArg
			, SeedReg_Q_Lo=defaultNamedNotOptArg):
		'method FG_GammaPnGenSeed'
		return self._oleobj_.InvokeTypes(33, LCID, 1, (24, 0), ((3, 1), (19, 1), (19, 1), (19, 1), (19, 1)),chain
			, SeedReg_I_Hi, SeedReg_I_Lo, SeedReg_Q_Hi, SeedReg_Q_Lo)

	def FG_GammaPnGenStat(self, chain=defaultNamedNotOptArg, pn=defaultNamedNotOptArg):
		'method FG_GammaPnGenStat'
		return self._oleobj_.InvokeTypes(32, LCID, 1, (11, 0), ((3, 1), (3, 1)),chain
			, pn)

	def GammaScaleFile(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method GammaScaleFile'
		return self._oleobj_.InvokeTypes(40, LCID, 1, (24, 0), ((3, 1), (8, 1)),chain
			, filename)

	def GammaScaleRemoveAll(self, chain=defaultNamedNotOptArg):
		'method GammaScaleRemoveAll'
		return self._oleobj_.InvokeTypes(41, LCID, 1, (24, 0), ((3, 1),),chain
			)

	def GetDeviceInfo(self):
		'method GetDeviceInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 1, (8, 0), (),)

	def GetFPGAVersion(self):
		'method GetFPGAVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 1, (8, 0), (),)

	def GetIOBoardInfo(self):
		'method GetIOBoardInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 1, (8, 0), (),)

	def IQ_ManipulationCtrl(self, chain=defaultNamedNotOptArg, man_type=defaultNamedNotOptArg, man_ctrl=defaultNamedNotOptArg, event=defaultNamedNotOptArg
			, num_bits=defaultNamedNotOptArg, man_pin=defaultNamedNotOptArg, manipulation_seq=defaultNamedNotOptArg):
		'method IQ_ManipulationCtrl'
		return self._oleobj_.InvokeTypes(34, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (17, 1), (3, 1), (18, 1)),chain
			, man_type, man_ctrl, event, num_bits, man_pin
			, manipulation_seq)

	def IQ_ManipulationMask(self, chain=defaultNamedNotOptArg, mstb=defaultNamedNotOptArg, ma0=defaultNamedNotOptArg, ma1=defaultNamedNotOptArg
			, ma2=defaultNamedNotOptArg, ma3=defaultNamedNotOptArg, mb0=defaultNamedNotOptArg, mb1=defaultNamedNotOptArg, mb2=defaultNamedNotOptArg
			, mb3=defaultNamedNotOptArg, mid_par=defaultNamedNotOptArg):
		'method IQ_ManipulationMask'
		return self._oleobj_.InvokeTypes(35, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1)),chain
			, mstb, ma0, ma1, ma2, ma3
			, mb0, mb1, mb2, mb3, mid_par
			)

	def LAPodOutput(self, enable=defaultNamedNotOptArg):
		'method LAPodOutput'
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((11, 0),),enable
			)

	def LA_ReadData(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method LA_ReadData'
		return self._ApplyTypes_(18, 1, (12, 0), ((3, 1), (19, 1)), 'LA_ReadData', None,chain
			, num_kB)

	def LA_SaveToFile(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LA_SaveToFile'
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((3, 1), (19, 1), (8, 1)),chain
			, num_kB, filename)

	def LA_StartTrig(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg
			, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg, ext_trig_on=defaultNamedNotOptArg, int_trig_off=defaultNamedNotOptArg):
		'method LA_StartTrig'
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((3, 1), (19, 1), (17, 1), (17, 1), (18, 1), (17, 1), (11, 1), (11, 1)),chain
			, num_kB, TSC, SC, CC, BC
			, ext_trig_on, int_trig_off)

	def LoadPatternFile(self, chain=defaultNamedNotOptArg, index=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LoadPatternFile'
		return self._oleobj_.InvokeTypes(12, LCID, 1, (19, 0), ((3, 1), (17, 1), (8, 1)),chain
			, index, filename)

	def MasterReady(self):
		'method MasterReady'
		return self._oleobj_.InvokeTypes(6, LCID, 1, (11, 0), (),)

	def PatternFileList(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method PatternFileList'
		return self._oleobj_.InvokeTypes(39, LCID, 1, (19, 0), ((3, 1), (8, 1)),chain
			, filename)

	def PatternFileSelect(self, chain=defaultNamedNotOptArg, fileindx1=defaultNamedNotOptArg, fileindx2=defaultNamedNotOptArg, on=defaultNamedNotOptArg):
		'method PatternFileSelect'
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (11, 1)),chain
			, fileindx1, fileindx2, on)

	def ReceExtI2C(self, I2C_Address=defaultNamedNotOptArg, size=defaultNamedNotOptArg):
		'method ReceExtI2C'
		return self._ApplyTypes_(9, 1, (12, 0), ((17, 1), (17, 1)), 'ReceExtI2C', None,I2C_Address
			, size)

	def RuFPGA(self, cmd=defaultNamedNotOptArg, size=defaultNamedNotOptArg, address=defaultNamedNotOptArg, dwnlink=defaultNamedNotOptArg):
		'method RuFPGA'
		return self._ApplyTypes_(43, 1, (12, 0), ((17, 1), (18, 1), (19, 1), (12, 1)), 'RuFPGA', None,cmd
			, size, address, dwnlink)

	def SendExtI2C(self, I2C_Address=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method SendExtI2C'
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((17, 1), (12, 1)),I2C_Address
			, data)

	def ShowWindow(self, normal=defaultNamedNotOptArg):
		'method ShowWindow'
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((11, 1),),normal
			)

	def ShutDown(self):
		'method ShutDown'
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def UL_GammaAGC(self, chain=defaultNamedNotOptArg, ga=defaultNamedNotOptArg):
		'method UL_GammaAGC'
		return self._oleobj_.InvokeTypes(20, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, ga)

	def UL_GammaStatus(self, chain=defaultNamedNotOptArg, gs=defaultNamedNotOptArg):
		'method UL_GammaStatus'
		return self._oleobj_.InvokeTypes(19, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, gs)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

class IRuControl4(DispatchBaseClass):
	'IRuControl4 Interface'
	CLSID = IID('{982D3D23-1181-4907-A770-1DAF6979883F}')
	coclass_clsid = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')

	def AUX_SetAuxMux(self, ift=defaultNamedNotOptArg):
		'method AUX_SetAuxMux'
		return self._oleobj_.InvokeTypes(141, LCID, 1, (24, 0), ((3, 1),),ift
			)

	def AUX_SetAuxSourceMux(self, AuxSource=defaultNamedNotOptArg):
		'method AUX_SetAuxSourceMux'
		return self._oleobj_.InvokeTypes(169, LCID, 1, (24, 0), ((3, 1),),AuxSource
			)

	def AUX_SetCpriMode(self, hdlcCh=defaultNamedNotOptArg, br=defaultNamedNotOptArg):
		'method AUX_SetCpriMode'
		return self._oleobj_.InvokeTypes(142, LCID, 1, (24, 0), ((17, 1), (3, 1)),hdlcCh
			, br)

	def BBCLK_Blanking(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, clk_cycles=defaultNamedNotOptArg, clock=defaultNamedNotOptArg):
		'method BBCLK_Blanking'
		return self._oleobj_.InvokeTypes(37, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, src, clk_cycles, clock)

	def BBCLK_Manipulation(self, chain=defaultNamedNotOptArg, clk_phase=defaultNamedNotOptArg):
		'method BBCLK_Manipulation'
		return self._oleobj_.InvokeTypes(36, LCID, 1, (17, 0), ((3, 1), (3, 1)),chain
			, clk_phase)

	def BBCLK_Out(self, chain=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method BBCLK_Out'
		return self._oleobj_.InvokeTypes(103, LCID, 1, (24, 0), ((3, 1), (11, 1)),chain
			, enable)

	def BFN_Offset(self, offset=defaultNamedNotOptArg):
		'method BFN_Offset'
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), ((3, 1),),offset
			)

	def CI_DL_CarrAxcAddr(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, addr=defaultNamedNotOptArg):
		'method CI_DL_CarrAxcAddr'
		return self._oleobj_.InvokeTypes(116, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chan
			, carrier, addr)

	def CI_DL_CarrFsinfo(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, hf=defaultNamedNotOptArg, bf=defaultNamedNotOptArg):
		'method CI_DL_CarrFsinfo'
		return self._oleobj_.InvokeTypes(117, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (17, 1)),chan
			, carrier, hf, bf)

	def CI_DL_CarrId(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, id=defaultNamedNotOptArg):
		'method CI_DL_CarrId'
		return self._oleobj_.InvokeTypes(115, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chan
			, carrier, id)

	def CI_DL_CarrierNumberForFSMStart(self, carrierNumber=defaultNamedNotOptArg):
		'method CI_DL_CarrierNumberForFSMStart'
		return self._oleobj_.InvokeTypes(181, LCID, 1, (24, 0), ((3, 1),),carrierNumber
			)

	def CI_DL_Combine_CopyContainers(self, filename=defaultNamedNotOptArg, StartSample_Source=defaultNamedNotOptArg, NumberOfSamples_Source=defaultNamedNotOptArg, NewStructure=defaultNamedNotOptArg
			, StartSample_Dest=defaultNamedNotOptArg, NumberOfSamples_Dest=defaultNamedNotOptArg, Frame=defaultNamedNotOptArg, hf=defaultNamedNotOptArg, bf=defaultNamedNotOptArg):
		'method CI_DL_Combine_CopyContainers'
		return self._oleobj_.InvokeTypes(185, LCID, 1, (24, 0), ((8, 1), (3, 0), (3, 0), (11, 0), (3, 0), (3, 0), (3, 0), (3, 0), (3, 0)),filename
			, StartSample_Source, NumberOfSamples_Source, NewStructure, StartSample_Dest, NumberOfSamples_Dest
			, Frame, hf, bf)

	def CI_DL_Combine_DeleteAll(self):
		'method CI_DL_Combine_DeleteAll'
		return self._oleobj_.InvokeTypes(188, LCID, 1, (24, 0), (),)

	def CI_DL_Combine_GetUsedContainers(self):
		'method CI_DL_Combine_GetUsedContainers'
		return self._ApplyTypes_(184, 1, (12, 0), (), 'CI_DL_Combine_GetUsedContainers', None,)

	def CI_DL_Combine_LoadFile(self, filename=defaultNamedNotOptArg):
		'method CI_DL_Combine_LoadFile'
		return self._oleobj_.InvokeTypes(183, LCID, 1, (24, 0), ((8, 1),),filename
			)

	def CI_DL_Combine_LoadToMemory(self):
		'method CI_DL_Combine_LoadToMemory'
		return self._oleobj_.InvokeTypes(186, LCID, 1, (24, 0), (),)

	def CI_DL_Combine_SaveToFile(self, filename=defaultNamedNotOptArg):
		'method CI_DL_Combine_SaveToFile'
		return self._oleobj_.InvokeTypes(187, LCID, 1, (24, 0), ((8, 1),),filename
			)

	def CI_DL_GainDeleteAllCarriers(self):
		'method CI_DL_GainDeleteAllCarriers'
		return self._oleobj_.InvokeTypes(176, LCID, 1, (24, 0), (),)

	def CI_DL_GainEnableCarrier(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method CI_DL_GainEnableCarrier'
		return self._oleobj_.InvokeTypes(113, LCID, 1, (24, 0), ((3, 1), (17, 1), (11, 1)),chan
			, carrier, enable)

	def CI_DL_GainLoadFile(self, chan=defaultNamedNotOptArg, filename=defaultNamedNotOptArg, unpacked=defaultNamedNotOptArg):
		'method CI_DL_GainLoadFile'
		return self._oleobj_.InvokeTypes(111, LCID, 1, (24, 0), ((3, 1), (8, 1), (11, 1)),chan
			, filename, unpacked)

	def CI_DL_GainSetup(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, start=defaultNamedNotOptArg, rate=defaultNamedNotOptArg
			, enGain=defaultNamedNotOptArg, gain=defaultNamedNotOptArg):
		'method CI_DL_GainSetup'
		return self._oleobj_.InvokeTypes(112, LCID, 1, (5, 0), ((3, 1), (17, 1), (17, 0), (3, 1), (11, 1), (5, 1)),chan
			, carrier, start, rate, enGain, gain
			)

	def CI_DL_GainSetupOneCarrier(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, start=defaultNamedNotOptArg, rate=defaultNamedNotOptArg
			, enGain=defaultNamedNotOptArg, gain=defaultNamedNotOptArg, enable=defaultNamedNotOptArg, id=defaultNamedNotOptArg, addr=defaultNamedNotOptArg
			, hf=defaultNamedNotOptArg, bf=defaultNamedNotOptArg, type=defaultNamedNotOptArg):
		'method CI_DL_GainSetupOneCarrier'
		return self._oleobj_.InvokeTypes(177, LCID, 1, (5, 0), ((3, 1), (17, 1), (17, 1), (3, 1), (11, 1), (5, 1), (11, 1), (17, 1), (17, 1), (17, 1), (17, 1), (3, 1)),chan
			, carrier, start, rate, enGain, gain
			, enable, id, addr, hf, bf
			, type)

	def CI_DL_LoadFile(self, chan=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method CI_DL_LoadFile'
		return self._oleobj_.InvokeTypes(44, LCID, 1, (24, 0), ((3, 1), (8, 1)),chan
			, filename)

	def CI_DL_LoadFile_Raw(self, chan=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method CI_DL_LoadFile_Raw'
		return self._oleobj_.InvokeTypes(241, LCID, 1, (24, 0), ((3, 1), (8, 1)),chan
			, filename)

	def CI_DL_LoadFile_TimeAdvanced(self, chan=defaultNamedNotOptArg, filename=defaultNamedNotOptArg, hf=defaultNamedNotOptArg, bf=defaultNamedNotOptArg):
		'method CI_DL_LoadFile_TimeAdvanced'
		return self._oleobj_.InvokeTypes(178, LCID, 1, (24, 0), ((3, 1), (8, 1), (2, 1), (2, 1)),chan
			, filename, hf, bf)

	def CI_DL_StartTransfer(self, chan=defaultNamedNotOptArg):
		'method CI_DL_StartTransfer'
		return self._oleobj_.InvokeTypes(45, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_DL_StopTransfer(self, chan=defaultNamedNotOptArg):
		'method CI_DL_StopTransfer'
		return self._oleobj_.InvokeTypes(46, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_FPGA_GetCtrlBuild(self):
		'method CI_FPGA_GetCtrlBuild'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(90, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetCtrlVersion(self):
		'method CI_FPGA_GetCtrlVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(89, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetIoBuild(self):
		'method CI_FPGA_GetIoBuild'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(92, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetIoVersion(self):
		'method CI_FPGA_GetIoVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(91, LCID, 1, (8, 0), (),)

	def CI_FPGA_GetPll(self):
		'method CI_FPGA_GetPll'
		return self._oleobj_.InvokeTypes(93, LCID, 1, (11, 0), (),)

	def CI_FPGA_GetPs1(self):
		'method CI_FPGA_GetPs1'
		return self._oleobj_.InvokeTypes(94, LCID, 1, (11, 0), (),)

	def CI_FPGA_GetPs2(self):
		'method CI_FPGA_GetPs2'
		return self._oleobj_.InvokeTypes(95, LCID, 1, (11, 0), (),)

	def CI_FPGA_Reset(self):
		'method CI_FPGA_Reset'
		return self._oleobj_.InvokeTypes(96, LCID, 1, (24, 0), (),)

	def CI_GetAdditionalCWHandling(self, chan=defaultNamedNotOptArg, disableProtocolHand=pythoncom.Missing, enableVersionHand=pythoncom.Missing, enableHdlcBitRateHand=pythoncom.Missing
			, enableEthPtrHand=pythoncom.Missing):
		'method CI_GetAdditionalCWHandling'
		return self._ApplyTypes_(236, 1, (24, 0), ((3, 1), (16395, 2), (16395, 2), (16395, 2), (16395, 2)), 'CI_GetAdditionalCWHandling', None,chan
			, disableProtocolHand, enableVersionHand, enableHdlcBitRateHand, enableEthPtrHand)

	def CI_GetCpriVersion(self, chan=defaultNamedNotOptArg):
		'method CI_GetCpriVersion'
		return self._oleobj_.InvokeTypes(238, LCID, 1, (3, 0), ((3, 1),),chan
			)

	def CI_GetScramblingSeed(self, chan=defaultNamedNotOptArg):
		'method CI_GetScramblingSeed'
		return self._oleobj_.InvokeTypes(240, LCID, 1, (19, 0), ((3, 1),),chan
			)

	def CI_REUSE_AlarmMonitor(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_AlarmMonitor'
		return self._oleobj_.InvokeTypes(100, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_ClearAlarm(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_ClearAlarm'
		return self._oleobj_.InvokeTypes(51, LCID, 1, (24, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_ClearAllAlarms(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_ClearAllAlarms'
		return self._oleobj_.InvokeTypes(99, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_EnableCwHandl(self, chan=defaultNamedNotOptArg, handl=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method CI_REUSE_EnableCwHandl'
		return self._oleobj_.InvokeTypes(56, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1)),chan
			, handl, enable)

	def CI_REUSE_GetAlarm(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_GetAlarm'
		return self._oleobj_.InvokeTypes(49, LCID, 1, (11, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_GetAlarmCnt(self, chan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_REUSE_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(50, LCID, 1, (18, 0), ((3, 1), (3, 1)),chan
			, alType)

	def CI_REUSE_GetCWDetect(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetect'
		return self._oleobj_.InvokeTypes(62, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectAdd(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectAdd'
		return self._oleobj_.InvokeTypes(63, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectMast(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectMast'
		return self._oleobj_.InvokeTypes(66, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectRev(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectRev'
		return self._oleobj_.InvokeTypes(65, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetCWDetectTim(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetCWDetectTim'
		return self._oleobj_.InvokeTypes(64, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetFsmStatFa(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetFsmStatFa'
		return self._oleobj_.InvokeTypes(58, LCID, 1, (3, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetFsmStatLink(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetFsmStatLink'
		return self._oleobj_.InvokeTypes(59, LCID, 1, (3, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetHsbCw(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetHsbCw'
		return self._oleobj_.InvokeTypes(70, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetJitBufDelCntC(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetJitBufDelCntC'
		return self._oleobj_.InvokeTypes(60, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetJitBufDelCntF(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetJitBufDelCntF'
		return self._oleobj_.InvokeTypes(61, LCID, 1, (18, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetPFAAlarms(self, hasAlarm=pythoncom.Missing):
		'method CI_REUSE_GetPFAChannel'
		return self._ApplyTypes_(157, 1, (12, 0), ((16395, 2),), 'CI_REUSE_GetPFAAlarms', None,hasAlarm
			)

	def CI_REUSE_GetTADetectBfr(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectBfr'
		return self._oleobj_.InvokeTypes(68, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetTADetectHfr(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectHfr'
		return self._oleobj_.InvokeTypes(69, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetTADetectW(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetTADetectW'
		return self._oleobj_.InvokeTypes(67, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_GetUartBitrate(self):
		'method CI_REUSE_GetUartBitrate'
		return self._oleobj_.InvokeTypes(156, LCID, 1, (3, 0), (),)

	def CI_REUSE_GetVersion(self, chan=defaultNamedNotOptArg):
		'method CI_REUSE_GetVersion'
		return self._oleobj_.InvokeTypes(71, LCID, 1, (17, 0), ((3, 1),),chan
			)

	def CI_REUSE_SetDelCntThr(self, chan=defaultNamedNotOptArg, delCntThr=defaultNamedNotOptArg):
		'method CI_REUSE_SetDelCntThr'
		return self._oleobj_.InvokeTypes(54, LCID, 1, (24, 0), ((3, 1), (17, 1)),chan
			, delCntThr)

	def CI_REUSE_SetLinkConf(self, chan=defaultNamedNotOptArg, lineRate=defaultNamedNotOptArg, enLink=defaultNamedNotOptArg, mode=defaultNamedNotOptArg
			, l1Res=defaultNamedNotOptArg):
		'method CI_REUSE_SetLinkConf'
		return self._oleobj_.InvokeTypes(52, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (11, 1)),chan
			, lineRate, enLink, mode, l1Res)

	def CI_REUSE_SetLnkSrvCtrl(self, chan=defaultNamedNotOptArg, srv=defaultNamedNotOptArg, setSrv=defaultNamedNotOptArg):
		'method CI_REUSE_SetLnkSrvCtrl '
		return self._oleobj_.InvokeTypes(57, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1)),chan
			, srv, setSrv)

	def CI_REUSE_SetSyncSrc(self, chan=defaultNamedNotOptArg, syncSrc=defaultNamedNotOptArg):
		'method CI_REUSE_SetSyncSrc'
		return self._oleobj_.InvokeTypes(55, LCID, 1, (24, 0), ((3, 1), (3, 1)),chan
			, syncSrc)

	def CI_REUSE_SetupCW(self, chan=defaultNamedNotOptArg, cascW=defaultNamedNotOptArg, timSyncOn=defaultNamedNotOptArg, protRev=defaultNamedNotOptArg
			, enChain=defaultNamedNotOptArg):
		'method CI_REUSE_SetupCW'
		return self._oleobj_.InvokeTypes(53, LCID, 1, (24, 0), ((3, 1), (17, 1), (11, 1), (17, 1), (11, 1)),chan
			, cascW, timSyncOn, protRev, enChain)

	def CI_SCAN_ClearAlarm(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_ClearAlarm'
		return self._oleobj_.InvokeTypes(82, LCID, 1, (24, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_GetAlarm(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_GetAlarm'
		return self._oleobj_.InvokeTypes(80, LCID, 1, (11, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_GetAlarmCnt(self, Scan=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SCAN_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(81, LCID, 1, (18, 0), ((3, 1), (3, 1)),Scan
			, alType)

	def CI_SCAN_ReadReg(self, Scan=defaultNamedNotOptArg, reg=defaultNamedNotOptArg):
		'method CI_SCAN_ReadReg'
		return self._oleobj_.InvokeTypes(79, LCID, 1, (18, 0), ((3, 1), (18, 1)),Scan
			, reg)

	def CI_SCAN_Reset(self, Scan=defaultNamedNotOptArg):
		'method CI_SCAN_Reset'
		return self._oleobj_.InvokeTypes(77, LCID, 1, (24, 0), ((3, 1),),Scan
			)

	def CI_SCAN_SetCmd(self, Scan=defaultNamedNotOptArg, txPwD=defaultNamedNotOptArg, rxPwD=defaultNamedNotOptArg, enComAl=defaultNamedNotOptArg):
		'method CI_SCAN_SetCmd'
		return self._oleobj_.InvokeTypes(75, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1)),Scan
			, txPwD, rxPwD, enComAl)

	def CI_SCAN_SetModes(self, Scan=defaultNamedNotOptArg, txDeEmph=defaultNamedNotOptArg, rxEqu=defaultNamedNotOptArg, bRate=defaultNamedNotOptArg
			, lbMode=defaultNamedNotOptArg):
		'method CI_SCAN_SetModes'
		return self._oleobj_.InvokeTypes(76, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),Scan
			, txDeEmph, rxEqu, bRate, lbMode)

	def CI_SCAN_WriteReg(self, Scan=defaultNamedNotOptArg, reg=defaultNamedNotOptArg, Value=defaultNamedNotOptArg):
		'method CI_SCAN_WriteReg'
		return self._oleobj_.InvokeTypes(78, LCID, 1, (24, 0), ((3, 1), (18, 1), (18, 1)),Scan
			, reg, Value)

	def CI_SFP_ClearAlarm(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_ClearAlarm'
		return self._oleobj_.InvokeTypes(88, LCID, 1, (24, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_Disable(self, SFP=defaultNamedNotOptArg, DISABLE=defaultNamedNotOptArg):
		'method CI_SFP_Disable'
		return self._oleobj_.InvokeTypes(85, LCID, 1, (24, 0), ((3, 1), (11, 1)),SFP
			, DISABLE)

	def CI_SFP_GetAlarm(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_GetAlarm'
		return self._oleobj_.InvokeTypes(86, LCID, 1, (11, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_GetAlarmCnt(self, SFP=defaultNamedNotOptArg, alType=defaultNamedNotOptArg):
		'method CI_SFP_GetAlarmCnt'
		return self._oleobj_.InvokeTypes(87, LCID, 1, (18, 0), ((3, 1), (3, 1)),SFP
			, alType)

	def CI_SFP_GetDisabled(self, SFP=defaultNamedNotOptArg):
		'method CI_SFP_GetDisabled'
		return self._oleobj_.InvokeTypes(84, LCID, 1, (11, 0), ((3, 1),),SFP
			)

	def CI_SFP_GetPresent(self, SFP=defaultNamedNotOptArg):
		'method CI_SFP_GetPresent'
		return self._oleobj_.InvokeTypes(83, LCID, 1, (11, 0), ((3, 1),),SFP
			)

	def CI_SetAdditionalCWHandling(self, chan=defaultNamedNotOptArg, disableProtocolHand=defaultNamedNotOptArg, enableVersionHand=defaultNamedNotOptArg, enableHdlcBitRateHand=defaultNamedNotOptArg
			, enableEthPtrHand=defaultNamedNotOptArg):
		'method CI_SetAdditionalCWHandling'
		return self._oleobj_.InvokeTypes(235, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1), (11, 1)),chan
			, disableProtocolHand, enableVersionHand, enableHdlcBitRateHand, enableEthPtrHand)

	def CI_SetCpriMux(self, chan=defaultNamedNotOptArg):
		'method CI_SetCpriMux'
		return self._oleobj_.InvokeTypes(144, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_SetCpriVersion(self, chan=defaultNamedNotOptArg, ver=defaultNamedNotOptArg):
		'method CI_SetCpriVersion'
		return self._oleobj_.InvokeTypes(237, LCID, 1, (24, 0), ((3, 1), (3, 1)),chan
			, ver)

	def CI_SetScramblingSeed(self, chan=defaultNamedNotOptArg, seed=defaultNamedNotOptArg):
		'method CI_SetScramblingSeed'
		return self._oleobj_.InvokeTypes(239, LCID, 1, (24, 0), ((3, 1), (19, 1)),chan
			, seed)

	def CI_TD_GetGain(self, subframe=defaultNamedNotOptArg, slot=defaultNamedNotOptArg, axc=defaultNamedNotOptArg, pattfilename=defaultNamedNotOptArg
			, KUbpDbm=defaultNamedNotOptArg, skipAgcBit=defaultNamedNotOptArg, useAllChips=defaultNamedNotOptArg):
		'method CI_TD_GetGain'
		return self._oleobj_.InvokeTypes(98, LCID, 1, (5, 0), ((17, 1), (17, 1), (17, 1), (8, 1), (5, 1), (11, 1), (11, 1)),subframe
			, slot, axc, pattfilename, KUbpDbm, skipAgcBit
			, useAllChips)

	def CI_TD_SaveIQ(self, subframe=defaultNamedNotOptArg, slot=defaultNamedNotOptArg, axc=defaultNamedNotOptArg, pattfilename=defaultNamedNotOptArg
			, iqfilename=defaultNamedNotOptArg):
		'method CI_TD_SaveIQ'
		return self._oleobj_.InvokeTypes(97, LCID, 1, (24, 0), ((17, 1), (17, 1), (17, 1), (8, 1), (8, 1)),subframe
			, slot, axc, pattfilename, iqfilename)

	def CI_TRIG_GsmFramesync(self, chan=defaultNamedNotOptArg, offset=defaultNamedNotOptArg, length=defaultNamedNotOptArg, hyperframe=defaultNamedNotOptArg
			, basicframe=defaultNamedNotOptArg):
		'method CI_TRIG_GsmFramesync'
		return self._oleobj_.InvokeTypes(143, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),chan
			, offset, length, hyperframe, basicframe)

	def CI_TRIG_K285Setup(self, chan=defaultNamedNotOptArg, offsetTx=defaultNamedNotOptArg, offsetRx=defaultNamedNotOptArg, lenTx=defaultNamedNotOptArg
			, lenRx=defaultNamedNotOptArg):
		'method CI_TRIG_K285Setup'
		return self._oleobj_.InvokeTypes(134, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (3, 1)),chan
			, offsetTx, offsetRx, lenTx, lenRx)

	def CI_TRIG_LoadTimingTriggersConfig(self, filename=defaultNamedNotOptArg):
		'method CI_TRIG_LoadTimingTriggersConfig'
		return self._oleobj_.InvokeTypes(171, LCID, 1, (24, 0), ((8, 1),),filename
			)

	def CI_TRIG_ResetPhase(self, trig=defaultNamedNotOptArg):
		'method CI_TRIG_ResetPhase'
		return self._oleobj_.InvokeTypes(73, LCID, 1, (24, 0), ((3, 1),),trig
			)

	def CI_TRIG_SetTrigSrc(self, trig=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method CI_TRIG_SetTrigSrc'
		return self._oleobj_.InvokeTypes(74, LCID, 1, (24, 0), ((3, 1), (3, 1)),trig
			, src)

	def CI_TRIG_ShiftPhase(self, trig=defaultNamedNotOptArg, shift=defaultNamedNotOptArg):
		'method CI_TRIG_ShiftPhase'
		return self._oleobj_.InvokeTypes(72, LCID, 1, (24, 0), ((3, 1), (3, 1)),trig
			, shift)

	def CI_TRIG_TimingTriggersEnableTrigger(self, triggerNr=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method CI_TRIG_TimingTriggersEnableTrigger'
		return self._oleobj_.InvokeTypes(172, LCID, 1, (24, 0), ((17, 1), (3, 1)),triggerNr
			, enable)

	def CI_UL_CarrierStartSampling(self, chan=defaultNamedNotOptArg, crit=defaultNamedNotOptArg, src=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method CI_UL_CarrierStartSampling'
		return self._oleobj_.InvokeTypes(110, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (19, 1)),chan
			, crit, src, num_kB)

	def CI_UL_ClearAxc(self, chan=defaultNamedNotOptArg):
		'method CI_UL_ClearAxc'
		return self._oleobj_.InvokeTypes(120, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def CI_UL_EnableCarrier(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, enable=defaultNamedNotOptArg):
		'method CI_UL_EnableCarrier'
		return self._oleobj_.InvokeTypes(109, LCID, 1, (24, 0), ((3, 1), (17, 1), (11, 1)),chan
			, carrier, enable)

	def CI_UL_GetCarrierAgc(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg):
		'method CI_UL_GetCarrierAgc'
		return self._ApplyTypes_(119, 1, (12, 0), ((3, 1), (17, 1)), 'CI_UL_GetCarrierAgc', None,chan
			, carrier)

	def CI_UL_GetCarrierFsinfo(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg):
		'method CI_UL_GetCarrierFsinfo'
		return self._ApplyTypes_(118, 1, (12, 0), ((3, 1), (17, 1)), 'CI_UL_GetCarrierFsinfo', None,chan
			, carrier)

	def CI_UL_GetCarrierSamples(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, numSamples=defaultNamedNotOptArg, smpl=defaultNamedNotOptArg):
		'method CI_UL_GetCarrierSamples'
		return self._ApplyTypes_(107, 1, (12, 0), ((3, 1), (17, 1), (19, 1), (3, 1)), 'CI_UL_GetCarrierSamples', None,chan
			, carrier, numSamples, smpl)

	def CI_UL_SamplingReady(self, chan=defaultNamedNotOptArg):
		'method CI_UL_SamplingReady'
		return self._oleobj_.InvokeTypes(133, LCID, 1, (11, 0), ((3, 1),),chan
			)

	def CI_UL_Save(self, chan=defaultNamedNotOptArg, comment=defaultNamedNotOptArg, type=defaultNamedNotOptArg, lineRate=defaultNamedNotOptArg
			, wordLength=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method CI_UL_Save'
		return self._oleobj_.InvokeTypes(48, LCID, 1, (24, 0), ((3, 1), (8, 1), (3, 1), (3, 1), (17, 1), (8, 1)),chan
			, comment, type, lineRate, wordLength, filename
			)

	def CI_UL_SetupCarrier(self, chan=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, idt=defaultNamedNotOptArg, start=defaultNamedNotOptArg
			, rate=defaultNamedNotOptArg, tech=defaultNamedNotOptArg, s0=defaultNamedNotOptArg, manByp=defaultNamedNotOptArg, manDbgExp=defaultNamedNotOptArg
			, manDbgAagc=defaultNamedNotOptArg, enManDbg=defaultNamedNotOptArg, fsinfoDbgHf=defaultNamedNotOptArg, fsinfoDbgBf=defaultNamedNotOptArg, enFsinfoDbg=defaultNamedNotOptArg):
		'method CI_UL_SetupCarrier'
		return self._oleobj_.InvokeTypes(108, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (17, 1), (3, 1), (3, 1), (11, 1), (11, 1), (17, 1), (17, 1), (11, 1), (17, 1), (17, 1), (11, 1)),chan
			, carrier, idt, start, rate, tech
			, s0, manByp, manDbgExp, manDbgAagc, enManDbg
			, fsinfoDbgHf, fsinfoDbgBf, enFsinfoDbg)

	def CI_UL_StartSampling(self, chan=defaultNamedNotOptArg, crit=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method CI_UL_StartSampling'
		return self._oleobj_.InvokeTypes(47, LCID, 1, (24, 0), ((3, 1), (3, 1), (19, 1)),chan
			, crit, num_kB)

	def CI_UL_SubFrameExtr(self, CarrierID=defaultNamedNotOptArg, subframeStart=defaultNamedNotOptArg, nrOfSubframe=defaultNamedNotOptArg, offset=defaultNamedNotOptArg
			, ULSubFrameEnable=defaultNamedNotOptArg):
		'method CI_UL_SubFrameExtr'
		return self._oleobj_.InvokeTypes(182, LCID, 1, (24, 0), ((17, 1), (3, 1), (17, 1), (3, 1), (11, 1)),CarrierID
			, subframeStart, nrOfSubframe, offset, ULSubFrameEnable)

	def CNC_GetPortName(self, port=defaultNamedNotOptArg):
		'method CNC_GetPortName'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(233, LCID, 1, (8, 0), ((3, 1),),port
			)

	def CPC_FSM_AddCpcFile(self, filename=defaultNamedNotOptArg):
		'method CPC_FSM_AddCpcFile'
		return self._oleobj_.InvokeTypes(219, LCID, 1, (3, 0), ((8, 1),),filename
			)

	def CPC_FSM_AddCpcState(self, filename=defaultNamedNotOptArg, nrIterations=defaultNamedNotOptArg):
		'method CPC_FSM_AddCpcState'
		return self._oleobj_.InvokeTypes(222, LCID, 1, (24, 0), ((8, 1), (19, 1)),filename
			, nrIterations)

	def CPC_FSM_ClearAll(self):
		'method CPC_FSM_ClearAll'
		return self._oleobj_.InvokeTypes(218, LCID, 1, (24, 0), (),)

	def CPC_FSM_ClearAllCpcStates(self):
		'method CPC_FSM_ClearAllCpcStates'
		return self._oleobj_.InvokeTypes(221, LCID, 1, (24, 0), (),)

	def CPC_FSM_DisableCpcFsm(self):
		'method CPC_FSM_DisableCpcFsm'
		return self._oleobj_.InvokeTypes(224, LCID, 1, (24, 0), (),)

	def CPC_FSM_EnableCpcFsm(self, loopMode=defaultNamedNotOptArg):
		'method CPC_FSM_EnableCpcFsm'
		return self._oleobj_.InvokeTypes(223, LCID, 1, (24, 0), ((3, 1),),loopMode
			)

	def CPC_FSM_SendCpcToHw_Cdl2FileIndex(self, cpcFileIndex=defaultNamedNotOptArg, cdl2FileIndex=defaultNamedNotOptArg):
		'method CPC_FSM_SendCpcToHw_Cdl2FileIndex'
		return self._oleobj_.InvokeTypes(225, LCID, 1, (24, 0), ((3, 1), (3, 1)),cpcFileIndex
			, cdl2FileIndex)

	def CPC_FSM_SendCpcToHw_LoopLenght(self, cpcFileIndex=defaultNamedNotOptArg, loopLength=defaultNamedNotOptArg):
		'method CPC_FSM_SendCpcToHw_LoopLenght'
		return self._oleobj_.InvokeTypes(226, LCID, 1, (24, 0), ((3, 1), (3, 1)),cpcFileIndex
			, loopLength)

	def CPC_FSM_UseCpcFile(self, filename=defaultNamedNotOptArg):
		'method CPC_FSM_UseCpcFile'
		return self._oleobj_.InvokeTypes(220, LCID, 1, (24, 0), ((8, 1),),filename
			)

	def CPC_FSM_UseCpcFile_Index(self, cpcFileIndex=defaultNamedNotOptArg):
		'method CPC_FSM_UseCpcFile'
		return self._oleobj_.InvokeTypes(227, LCID, 1, (24, 0), ((3, 1),),cpcFileIndex
			)

	def CPC_SetLoopLength(self, loopLength=defaultNamedNotOptArg):
		'Setup CPC loop length method'
		return self._oleobj_.InvokeTypes(140, LCID, 1, (19, 0), ((19, 1),),loopLength
			)

	def CPC_Setup(self, filename=defaultNamedNotOptArg, enable=defaultNamedNotOptArg, useCdlFileName=defaultNamedNotOptArg):
		'Setup CPC file method'
		return self._oleobj_.InvokeTypes(139, LCID, 1, (19, 0), ((8, 1), (11, 0), (11, 0)),filename
			, enable, useCdlFileName)

	def DGC_ChangeToGsmMode(self, timing=defaultNamedNotOptArg):
		'method DGC_ChangeToGsmMode'
		return self._oleobj_.InvokeTypes(154, LCID, 1, (24, 0), ((3, 1),),timing
			)

	def DGC_ChangeTo_LTE_WCDMAMode(self):
		'method DGC_ChangeTo_LTE_WCDMAMode'
		return self._oleobj_.InvokeTypes(155, LCID, 1, (24, 0), (),)

	def DGC_Change_Gain_Gsm(self, rowNr=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method DGC_Change_Gain_Gsm'
		return self._oleobj_.InvokeTypes(175, LCID, 1, (24, 0), ((17, 1), (17, 1), (12, 1)),rowNr
			, stateIndex, data)

	def DGC_Change_Gain_Gsm_db(self, rowNr=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method DGC_Change_Gain_Gsm_db'
		return self._oleobj_.InvokeTypes(199, LCID, 1, (24, 0), ((17, 1), (17, 1), (12, 1)),rowNr
			, stateIndex, data)

	def DGC_Change_Gain_NonGsm(self, rowNr=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method DGC_Change_Gain_NonGsm'
		return self._oleobj_.InvokeTypes(246, LCID, 1, (24, 0), ((17, 1), (17, 1), (12, 1)),rowNr
			, stateIndex, data)

	def DGC_Change_Gain_NonGsm_db(self, rowNr=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method DGC_Change_Gain_NonGsm_db'
		return self._oleobj_.InvokeTypes(247, LCID, 1, (24, 0), ((17, 1), (17, 1), (12, 1)),rowNr
			, stateIndex, data)

	def DGC_DeleteAll(self):
		'method DGC_DeleteAll'
		return self._oleobj_.InvokeTypes(248, LCID, 1, (24, 0), (),)

	def DGC_GetActiveGsmRow(self):
		'method DGC_GetActiveGsmRow'
		return self._oleobj_.InvokeTypes(160, LCID, 1, (2, 0), (),)

	def DGC_GetCurrentFsmState(self, rowIndex=pythoncom.Missing, stateIndex=pythoncom.Missing):
		'method DGC_GetCurrentFsmState OBSOLETE Use DGC_GetCurrentFsmState_WCDMA'
		return self._ApplyTypes_(180, 1, (24, 0), ((16387, 2), (16387, 2)), 'DGC_GetCurrentFsmState', None,rowIndex
			, stateIndex)

	def DGC_GetCurrentFsmState_Wcdma(self, rowIndex=pythoncom.Missing, stateIndex=pythoncom.Missing):
		'method DGC_GetCurrentFsmState'
		return self._ApplyTypes_(150, 1, (24, 0), ((16387, 2), (16387, 2)), 'DGC_GetCurrentFsmState_Wcdma', None,rowIndex
			, stateIndex)

	def DGC_GetDLCarrierSourceType(self, carrier=defaultNamedNotOptArg):
		'method DGC_GetDLCarrierSourceType'
		return self._oleobj_.InvokeTypes(174, LCID, 1, (3, 0), ((3, 1),),carrier
			)

	def DGC_GetFsmMode(self):
		'method DGC_GetFsmMode'
		return self._oleobj_.InvokeTypes(146, LCID, 1, (3, 0), (),)

	def DGC_GetStartOffset(self, rf=pythoncom.Missing, hf=pythoncom.Missing, bf=pythoncom.Missing):
		'method DGC_GetStartOffset'
		return self._ApplyTypes_(153, 1, (24, 0), ((16387, 2), (16387, 2), (16387, 2)), 'DGC_GetStartOffset', None,rf
			, hf, bf)

	def DGC_LoadFsmConfig(self, filename=defaultNamedNotOptArg):
		'method DGC_LoadFsmConfig'
		return self._oleobj_.InvokeTypes(147, LCID, 1, (24, 0), ((8, 1),),filename
			)

	def DGC_Scale_Row_Gsm_db(self, rowNr=defaultNamedNotOptArg, carrier=defaultNamedNotOptArg, gain=defaultNamedNotOptArg):
		'method DGC_Scale_Row_Gsm_db'
		return self._oleobj_.InvokeTypes(200, LCID, 1, (24, 0), ((17, 1), (17, 1), (5, 1)),rowNr
			, carrier, gain)

	def DGC_SendFsmToHw(self):
		'method DGC_SendFsmToHw'
		return self._oleobj_.InvokeTypes(148, LCID, 1, (24, 0), (),)

	def DGC_SetActiveFsmRow(self, rowIndex=defaultNamedNotOptArg):
		'method DGC_SetActiveFsmRow'
		return self._oleobj_.InvokeTypes(151, LCID, 1, (24, 0), ((3, 1),),rowIndex
			)

	def DGC_SetActiveGsmRow(self, row=defaultNamedNotOptArg):
		'method DGC_SetActiveGsmRow'
		return self._oleobj_.InvokeTypes(159, LCID, 1, (24, 0), ((2, 1),),row
			)

	def DGC_SetCurrentFsmState(self, rowIndex=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg):
		'method DGC_SetCurrentFsmState OBSOLETE Use DGC_SetCurrentFsmState_WCDMA'
		return self._oleobj_.InvokeTypes(179, LCID, 1, (24, 0), ((3, 1), (3, 1)),rowIndex
			, stateIndex)

	def DGC_SetCurrentFsmState_Wcdma(self, rowIndex=defaultNamedNotOptArg, stateIndex=defaultNamedNotOptArg):
		'method DGC_SetCurrentFsmState'
		return self._oleobj_.InvokeTypes(149, LCID, 1, (24, 0), ((3, 1), (3, 1)),rowIndex
			, stateIndex)

	def DGC_SetDLCarrierSourceType(self, carrier=defaultNamedNotOptArg, type=defaultNamedNotOptArg):
		'method DGC_SetDLCarrierSourceType'
		return self._oleobj_.InvokeTypes(173, LCID, 1, (24, 0), ((3, 1), (3, 1)),carrier
			, type)

	def DGC_SetFsmMode(self, mode=defaultNamedNotOptArg):
		'method DGC_SetFsmMode'
		return self._oleobj_.InvokeTypes(145, LCID, 1, (24, 0), ((3, 1),),mode
			)

	def DGC_SetStartOffset(self, rf=defaultNamedNotOptArg, hf=defaultNamedNotOptArg, bf=defaultNamedNotOptArg):
		'method DGC_SetStartOffset'
		return self._oleobj_.InvokeTypes(152, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1)),rf
			, hf, bf)

	def DL_BFN_Control(self, chain=defaultNamedNotOptArg, mode=defaultNamedNotOptArg, StartBit1=defaultNamedNotOptArg, StartBit2=defaultNamedNotOptArg):
		'method DL_BFN_Control'
		return self._oleobj_.InvokeTypes(25, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1), (11, 1)),chain
			, mode, StartBit1, StartBit2)

	def DL_BFN_OffsNum(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method DL_BFN_OffsNum'
		return self._oleobj_.InvokeTypes(23, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, TSC, SC, CC, BC)

	def DL_BFN_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, control=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_BFN_Offset'
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1), (3, 1)),chain
			, TSC, SC, CC, BC, control
			, event)

	def DL_BFN_Update(self, chain=defaultNamedNotOptArg, src=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg, Number=defaultNamedNotOptArg):
		'method DL_BFN_Update'
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (19, 1)),chain
			, src, ctrl, Number)

	def DL_EventControl(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, cond=defaultNamedNotOptArg, ctrl=defaultNamedNotOptArg):
		'method DL_EventControl'
		return self._oleobj_.InvokeTypes(28, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chain
			, ev, cond, ctrl)

	def DL_EventCount(self, chain=defaultNamedNotOptArg, ev=defaultNamedNotOptArg, BFN=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg
			, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg):
		'method DL_EventCount'
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((3, 1), (3, 1), (18, 1), (17, 1), (17, 1), (18, 1), (17, 1)),chain
			, ev, BFN, TSC, SC, CC
			, BC)

	def DL_GammaActivate(self, chain=defaultNamedNotOptArg, on=defaultNamedNotOptArg):
		'method DL_GammaActivate'
		return self._oleobj_.InvokeTypes(114, LCID, 1, (24, 0), ((3, 1), (11, 1)),chain
			, on)

	def DL_GammaCarrier(self, chain=defaultNamedNotOptArg, CarrierID_A=defaultNamedNotOptArg, CarrierID_B=defaultNamedNotOptArg):
		'method DL_GammaCarrier'
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, CarrierID_A, CarrierID_B)

	def DL_GammaDataSrc(self, chain=defaultNamedNotOptArg, Source=defaultNamedNotOptArg):
		'method DL_GammaDataSrc'
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((3, 1), (3, 1)),chain
			, Source)

	def DL_GammaScale(self, chain=defaultNamedNotOptArg, factor_A_dB=defaultNamedNotOptArg, factor_B_dB=defaultNamedNotOptArg):
		'method DL_GammaScale'
		return self._oleobj_.InvokeTypes(29, LCID, 1, (24, 0), ((3, 1), (5, 1), (5, 1)),chain
			, factor_A_dB, factor_B_dB)

	def DL_Strobe_Offset(self, chain=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg, event=defaultNamedNotOptArg):
		'method DL_Strobe_Offset'
		return self._oleobj_.InvokeTypes(38, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1), (3, 1)),chain
			, TSC, SC, CC, BC, event
			)

	def DebugGetRegister(self, address=defaultNamedNotOptArg):
		'method DebugGetRegister'
		return self._oleobj_.InvokeTypes(128, LCID, 1, (19, 0), ((18, 1),),address
			)

	def DebugReadMemory(self, address=defaultNamedNotOptArg, size=defaultNamedNotOptArg, MEMORY=defaultNamedNotOptArg):
		'method DebugReadMemory'
		return self._ApplyTypes_(130, 1, (12, 0), ((19, 1), (19, 1), (3, 1)), 'DebugReadMemory', None,address
			, size, MEMORY)

	def DebugSetRegister(self, address=defaultNamedNotOptArg, Value=defaultNamedNotOptArg):
		'method DebugSetRegister'
		return self._oleobj_.InvokeTypes(129, LCID, 1, (24, 0), ((18, 1), (19, 1)),address
			, Value)

	def DebugWriteMemory(self, address=defaultNamedNotOptArg, size=defaultNamedNotOptArg, MEMORY=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method DebugWriteMemory'
		return self._oleobj_.InvokeTypes(131, LCID, 1, (24, 0), ((19, 1), (3, 1), (3, 1), (12, 1)),address
			, size, MEMORY, data)

	def EnableRuFPGA(self, enable=defaultNamedNotOptArg):
		'method EnableRuFPGA'
		return self._oleobj_.InvokeTypes(42, LCID, 1, (24, 0), ((11, 1),),enable
			)

	def ExtReferenceLock(self):
		'method ExtReferenceLock'
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), (),)

	def ExternalPatternGen(self, input=defaultNamedNotOptArg):
		'method ExternalPatternGen'
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), ((3, 1),),input
			)

	def ExternalTrig(self, dstrb_src=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg, CC=defaultNamedNotOptArg
			, BC=defaultNamedNotOptArg):
		'method ExternalTrig'
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (18, 1), (17, 1)),dstrb_src
			, TSC, SC, CC, BC)

	def FG_GammaConstant(self, chain=defaultNamedNotOptArg, Constant_I=defaultNamedNotOptArg, Constant_Q=defaultNamedNotOptArg):
		'method FG_GammaConstant'
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1)),chain
			, Constant_I, Constant_Q)

	def FG_GammaGenCtrl(self, chain=defaultNamedNotOptArg, dst=defaultNamedNotOptArg, src=defaultNamedNotOptArg):
		'method FG_GammaGenCtrl'
		return self._oleobj_.InvokeTypes(30, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1)),chain
			, dst, src)

	def FG_GammaPnGenSeed(self, chain=defaultNamedNotOptArg, SeedReg_I_Hi=defaultNamedNotOptArg, SeedReg_I_Lo=defaultNamedNotOptArg, SeedReg_Q_Hi=defaultNamedNotOptArg
			, SeedReg_Q_Lo=defaultNamedNotOptArg):
		'method FG_GammaPnGenSeed'
		return self._oleobj_.InvokeTypes(33, LCID, 1, (24, 0), ((3, 1), (19, 1), (19, 1), (19, 1), (19, 1)),chain
			, SeedReg_I_Hi, SeedReg_I_Lo, SeedReg_Q_Hi, SeedReg_Q_Lo)

	def FG_GammaPnGenStat(self, chain=defaultNamedNotOptArg, pn=defaultNamedNotOptArg):
		'method FG_GammaPnGenStat'
		return self._oleobj_.InvokeTypes(32, LCID, 1, (11, 0), ((3, 1), (3, 1)),chain
			, pn)

	def GammaScaleFile(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method GammaScaleFile'
		return self._oleobj_.InvokeTypes(40, LCID, 1, (24, 0), ((3, 1), (8, 1)),chain
			, filename)

	def GammaScaleRemoveAll(self, chain=defaultNamedNotOptArg):
		'method GammaScaleRemoveAll'
		return self._oleobj_.InvokeTypes(41, LCID, 1, (24, 0), ((3, 1),),chain
			)

	def GetDeviceInfo(self):
		'method GetDeviceInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 1, (8, 0), (),)

	def GetFPGAVersion(self):
		'method GetFPGAVersion'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 1, (8, 0), (),)

	def GetFPGAVersionEx(self, Fpga=defaultNamedNotOptArg):
		'method GetFPGAVersionEx'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(104, LCID, 1, (8, 0), ((3, 1),),Fpga
			)

	def GetIOBoardInfo(self):
		'method GetIOBoardInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 1, (8, 0), (),)

	def GetPlatformParameter(self, key=defaultNamedNotOptArg):
		'method GetPlatformParameter'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(158, LCID, 1, (8, 0), ((8, 1),),key
			)

	def HW_AlarmClear(self, alarm=defaultNamedNotOptArg):
		'method HW_AlarmClear'
		return self._oleobj_.InvokeTypes(136, LCID, 1, (24, 0), ((3, 1),),alarm
			)

	def HW_AlarmGet(self, alarm=defaultNamedNotOptArg):
		'method HW_AlarmGet'
		return self._oleobj_.InvokeTypes(135, LCID, 1, (11, 0), ((3, 1),),alarm
			)

	def HW_ConfCpriElIf(self, chan=defaultNamedNotOptArg, lev=defaultNamedNotOptArg, preBoost=defaultNamedNotOptArg, swing=defaultNamedNotOptArg
			, ratio=defaultNamedNotOptArg, location=defaultNamedNotOptArg):
		'method HW_ConfCpriElIf'
		return self._oleobj_.InvokeTypes(138, LCID, 1, (24, 0), ((3, 1), (3, 1), (11, 1), (3, 1), (3, 1), (3, 1)),chan
			, lev, preBoost, swing, ratio, location
			)

	def HW_GetConfCpriEl_CT10(self, chan=defaultNamedNotOptArg, swing=pythoncom.Missing, precursor=pythoncom.Missing, equalizer=pythoncom.Missing):
		'method HW_GetConfCpriEl_CT10'
		return self._ApplyTypes_(243, 1, (24, 0), ((3, 1), (16387, 2), (16387, 2), (16387, 2)), 'HW_GetConfCpriEl_CT10', None,chan
			, swing, precursor, equalizer)

	def HW_SetConfCpriEl_CT10(self, chan=defaultNamedNotOptArg, swing=defaultNamedNotOptArg, precursor=defaultNamedNotOptArg, equalizer=defaultNamedNotOptArg):
		'method HW_SetConfCpriEl_CT10'
		return self._oleobj_.InvokeTypes(242, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),chan
			, swing, precursor, equalizer)

	def HW_SwapCpriAtoB(self, swap=defaultNamedNotOptArg):
		'method HW_SwapCpriAtoB'
		return self._oleobj_.InvokeTypes(137, LCID, 1, (24, 0), ((11, 1),),swap
			)

	def IQ_ManipulationCtrl(self, chain=defaultNamedNotOptArg, man_type=defaultNamedNotOptArg, man_ctrl=defaultNamedNotOptArg, event=defaultNamedNotOptArg
			, num_bits=defaultNamedNotOptArg, man_pin=defaultNamedNotOptArg, manipulation_seq=defaultNamedNotOptArg):
		'method IQ_ManipulationCtrl'
		return self._oleobj_.InvokeTypes(34, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (17, 1), (3, 1), (18, 1)),chain
			, man_type, man_ctrl, event, num_bits, man_pin
			, manipulation_seq)

	def IQ_ManipulationMask(self, chain=defaultNamedNotOptArg, mstb=defaultNamedNotOptArg, ma0=defaultNamedNotOptArg, ma1=defaultNamedNotOptArg
			, ma2=defaultNamedNotOptArg, ma3=defaultNamedNotOptArg, mb0=defaultNamedNotOptArg, mb1=defaultNamedNotOptArg, mb2=defaultNamedNotOptArg
			, mb3=defaultNamedNotOptArg, mid_par=defaultNamedNotOptArg):
		'method IQ_ManipulationMask'
		return self._oleobj_.InvokeTypes(35, LCID, 1, (24, 0), ((3, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1), (11, 1)),chain
			, mstb, ma0, ma1, ma2, ma3
			, mb0, mb1, mb2, mb3, mid_par
			)

	def LAPodOutput(self, enable=defaultNamedNotOptArg):
		'method LAPodOutput'
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((11, 0),),enable
			)

	def LA_ReadData(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg):
		'method LA_ReadData'
		return self._ApplyTypes_(18, 1, (12, 0), ((3, 1), (19, 1)), 'LA_ReadData', None,chain
			, num_kB)

	def LA_SaveToFile(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LA_SaveToFile'
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((3, 1), (19, 1), (8, 1)),chain
			, num_kB, filename)

	def LA_StartTrig(self, chain=defaultNamedNotOptArg, num_kB=defaultNamedNotOptArg, TSC=defaultNamedNotOptArg, SC=defaultNamedNotOptArg
			, CC=defaultNamedNotOptArg, BC=defaultNamedNotOptArg, ext_trig_on=defaultNamedNotOptArg, int_trig_off=defaultNamedNotOptArg):
		'method LA_StartTrig'
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((3, 1), (19, 1), (17, 1), (17, 1), (18, 1), (17, 1), (11, 1), (11, 1)),chain
			, num_kB, TSC, SC, CC, BC
			, ext_trig_on, int_trig_off)

	def LoadFpga(self, Fpga=defaultNamedNotOptArg, FilePath=defaultNamedNotOptArg):
		'method LoadFpga'
		return self._oleobj_.InvokeTypes(106, LCID, 1, (11, 0), ((3, 1), (8, 1)),Fpga
			, FilePath)

	def LoadPatternFile(self, chain=defaultNamedNotOptArg, index=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method LoadPatternFile'
		return self._oleobj_.InvokeTypes(12, LCID, 1, (19, 0), ((3, 1), (17, 1), (8, 1)),chain
			, index, filename)

	def Log_ClearLogs(self):
		'method Log_ClearLogs'
		return self._oleobj_.InvokeTypes(163, LCID, 1, (24, 0), (),)

	def Log_DisableLogging(self):
		'method Log_DisableLogging'
		return self._oleobj_.InvokeTypes(162, LCID, 1, (24, 0), (),)

	def Log_EnableLogging(self):
		'method Log_EnableLogging'
		return self._oleobj_.InvokeTypes(161, LCID, 1, (24, 0), (),)

	def Log_SaveLogs(self, path=defaultNamedNotOptArg):
		'method Log_SaveLogs'
		return self._oleobj_.InvokeTypes(164, LCID, 1, (24, 0), ((8, 1),),path
			)

	def MasterReady(self):
		'method MasterReady'
		return self._oleobj_.InvokeTypes(6, LCID, 1, (11, 0), (),)

	def MultiActivateFile(self, index=defaultNamedNotOptArg, activate=defaultNamedNotOptArg):
		'method MultiActivateFile'
		return self._oleobj_.InvokeTypes(123, LCID, 1, (24, 0), ((18, 1), (11, 1)),index
			, activate)

	def MultiActivateFileWithCpc(self, index=defaultNamedNotOptArg):
		'method MultiActivateFileWithCpc'
		return self._oleobj_.InvokeTypes(228, LCID, 1, (24, 0), ((18, 1),),index
			)

	def MultiAddLoadFile(self, filename=defaultNamedNotOptArg, type=defaultNamedNotOptArg, enCh1=defaultNamedNotOptArg, enCh2=defaultNamedNotOptArg
			, enCh3=defaultNamedNotOptArg, enCh4=defaultNamedNotOptArg):
		'method MultiAddLoadFile'
		return self._oleobj_.InvokeTypes(121, LCID, 1, (18, 0), ((8, 1), (3, 1), (11, 1), (11, 1), (11, 1), (11, 1)),filename
			, type, enCh1, enCh2, enCh3, enCh4
			)

	def MultiChannelEnable(self, enCh1=defaultNamedNotOptArg, enCh2=defaultNamedNotOptArg, enCh3=defaultNamedNotOptArg, enCh4=defaultNamedNotOptArg
			, index=defaultNamedNotOptArg):
		'method MultiChannelEnable'
		return self._oleobj_.InvokeTypes(122, LCID, 1, (24, 0), ((11, 1), (11, 1), (11, 1), (11, 1), (18, 1)),enCh1
			, enCh2, enCh3, enCh4, index)

	def MultiClearAll(self):
		'method MultiClearAll'
		return self._oleobj_.InvokeTypes(124, LCID, 1, (24, 0), (),)

	def MultiDeactivateAll(self):
		'method MultiDeactivateAll'
		return self._oleobj_.InvokeTypes(127, LCID, 1, (24, 0), (),)

	def MultiDeactivateFileWithCpc(self, index=defaultNamedNotOptArg):
		'method MultiDeactivateFileWithCpc'
		return self._oleobj_.InvokeTypes(229, LCID, 1, (24, 0), ((18, 1),),index
			)

	def MultiGetAllFileData(self):
		'method MultiGetAllFileData'
		return self._ApplyTypes_(125, 1, (12, 0), (), 'MultiGetAllFileData', None,)

	def MultiGetFileType(self, index=defaultNamedNotOptArg):
		'method MultiGetFileType'
		return self._oleobj_.InvokeTypes(126, LCID, 1, (3, 0), ((18, 1),),index
			)

	def PatternFileList(self, chain=defaultNamedNotOptArg, filename=defaultNamedNotOptArg):
		'method PatternFileList'
		return self._oleobj_.InvokeTypes(39, LCID, 1, (19, 0), ((3, 1), (8, 1)),chain
			, filename)

	def PatternFileSelect(self, chain=defaultNamedNotOptArg, fileindx1=defaultNamedNotOptArg, fileindx2=defaultNamedNotOptArg, on=defaultNamedNotOptArg):
		'method PatternFileSelect'
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((3, 1), (17, 1), (17, 1), (11, 1)),chain
			, fileindx1, fileindx2, on)

	def RPX_AddDeviceToCOMPort(self, comPort=defaultNamedNotOptArg, rpxUniqueID=defaultNamedNotOptArg, rpxHdlcAddress=defaultNamedNotOptArg, id=defaultNamedNotOptArg):
		'method RPX_AddDeviceToCOMPort'
		return self._oleobj_.InvokeTypes(250, LCID, 1, (24, 0), ((8, 1), (8, 1), (3, 1), (3, 1)),comPort
			, rpxUniqueID, rpxHdlcAddress, id)

	def RPX_CreateRpxCOMPort(self, comPort=defaultNamedNotOptArg, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, baudRate=defaultNamedNotOptArg
			, physicalPosition=defaultNamedNotOptArg, ecpAddress=defaultNamedNotOptArg, rpxHdlcAddress=defaultNamedNotOptArg, objectID=pythoncom.Missing):
		'method RPX_CreateRpxCOMPort'
		return self._ApplyTypes_(230, 1, (24, 0), ((8, 1), (3, 1), (3, 1), (3, 1), (3, 1), (3, 1), (3, 1), (16387, 2)), 'RPX_CreateRpxCOMPort', None,comPort
			, portNo, radioType, baudRate, physicalPosition, ecpAddress
			, rpxHdlcAddress, objectID)

	def RPX_CreateRpxCOMPort2(self, comPort=defaultNamedNotOptArg, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, baudRate=defaultNamedNotOptArg
			, physicalPosition=defaultNamedNotOptArg, ecpAddress=defaultNamedNotOptArg, id=pythoncom.Missing):
		'method RPX_CreateRpxCOMPort2'
		return self._ApplyTypes_(249, 1, (24, 0), ((8, 1), (3, 1), (3, 1), (3, 1), (3, 1), (3, 1), (16387, 2)), 'RPX_CreateRpxCOMPort2', None,comPort
			, portNo, radioType, baudRate, physicalPosition, ecpAddress
			, id)

	def RPX_DestroyRpxCOMPort(self, id=defaultNamedNotOptArg):
		'method RPX_DestroyRpxCOMPort'
		return self._oleobj_.InvokeTypes(231, LCID, 1, (24, 0), ((3, 1),),id
			)

	def RPX_GetDeviceLinkStatus(self, rpxHdlcAddress=defaultNamedNotOptArg, id=defaultNamedNotOptArg):
		'method RPX_GetDeviceLinkStatus'
		return self._oleobj_.InvokeTypes(252, LCID, 1, (3, 0), ((3, 1), (3, 1)),rpxHdlcAddress
			, id)

	def RPX_GetStatus(self, id=defaultNamedNotOptArg, ok=pythoncom.Missing):
		'method RPX_GetStatus'
		return self._ApplyTypes_(232, 1, (24, 0), ((3, 1), (16387, 2)), 'RPX_GetStatus', None,id
			, ok)

	def RPX_ResetHDLCAddress(self, rpxHdlcAddress=defaultNamedNotOptArg, id=defaultNamedNotOptArg):
		'method RPX_ResetHDLCAddress'
		return self._oleobj_.InvokeTypes(251, LCID, 1, (24, 0), ((3, 1), (3, 1)),rpxHdlcAddress
			, id)

	def RT_AsynchronousUpgradeRU(self, filename=defaultNamedNotOptArg, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg, restart=defaultNamedNotOptArg):
		'method RT_AsynchronousUpgradeRU'
		return self._oleobj_.InvokeTypes(190, LCID, 1, (19, 0), ((8, 1), (19, 1), (19, 1), (11, 1)),filename
			, port, physPos, restart)

	def RT_DeleteRuSector(self, radioPid=defaultNamedNotOptArg, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg):
		'method RT_DeleteRuSector'
		return self._oleobj_.InvokeTypes(198, LCID, 1, (19, 0), ((8, 1), (19, 1), (19, 1)),radioPid
			, port, physPos)

	def RT_EnablePq2(self, enable=defaultNamedNotOptArg):
		'method RT_EnablePq2'
		return self._oleobj_.InvokeTypes(197, LCID, 1, (24, 0), ((11, 1),),enable
			)

	def RT_IsLinkRuUP(self, port=defaultNamedNotOptArg):
		'method RT_RestartRU'
		return self._oleobj_.InvokeTypes(194, LCID, 1, (19, 0), ((19, 1),),port
			)

	def RT_IsLinkRuUP2(self, port=defaultNamedNotOptArg, physicalPosition=defaultNamedNotOptArg):
		'method RT_RestartRU'
		return self._oleobj_.InvokeTypes(234, LCID, 1, (19, 0), ((19, 1), (19, 1)),port
			, physicalPosition)

	def RT_IsPQ2Enabled(self):
		'method RT_IsPQ2Enabled'
		return self._oleobj_.InvokeTypes(196, LCID, 1, (11, 0), (),)

	def RT_RestartRU(self, radioPid=defaultNamedNotOptArg, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg):
		'method RT_RestartRU'
		return self._oleobj_.InvokeTypes(192, LCID, 1, (19, 0), ((8, 1), (19, 1), (19, 1)),radioPid
			, port, physPos)

	def RT_RuHwInfo(self, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg):
		'method RT_RuHwInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(195, LCID, 1, (8, 0), ((19, 1), (19, 1)),port
			, physPos)

	def RT_RuSwInfo(self, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg):
		'method RT_RuSwInfo'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(193, LCID, 1, (8, 0), ((19, 1), (19, 1)),port
			, physPos)

	def RT_UpgradeRU(self, filename=defaultNamedNotOptArg, port=defaultNamedNotOptArg, physPos=defaultNamedNotOptArg, restart=defaultNamedNotOptArg):
		'method RT_UpgradeRU'
		return self._oleobj_.InvokeTypes(189, LCID, 1, (19, 0), ((8, 1), (19, 1), (19, 1), (11, 1)),filename
			, port, physPos, restart)

	def RT_UpgradeRUStatus(self, totPercent=pythoncom.Missing, statePercent=pythoncom.Missing):
		'method RT_UpgradeRUStatus'
		return self._ApplyTypes_(191, 1, (19, 0), ((16387, 2), (16387, 2)), 'RT_UpgradeRUStatus', None,totPercent
			, statePercent)

	def ReceExtI2C(self, I2C_Address=defaultNamedNotOptArg, size=defaultNamedNotOptArg):
		'method ReceExtI2C'
		return self._ApplyTypes_(9, 1, (12, 0), ((17, 1), (17, 1)), 'ReceExtI2C', None,I2C_Address
			, size)

	def RuFPGA(self, cmd=defaultNamedNotOptArg, size=defaultNamedNotOptArg, address=defaultNamedNotOptArg, dwnlink=defaultNamedNotOptArg):
		'method RuFPGA'
		return self._ApplyTypes_(43, 1, (12, 0), ((17, 1), (18, 1), (19, 1), (12, 1)), 'RuFPGA', None,cmd
			, size, address, dwnlink)

	def ScanCircuitReinitialisation(self, chan=defaultNamedNotOptArg):
		'method SetScanCircuitLoopMode'
		return self._oleobj_.InvokeTypes(170, LCID, 1, (24, 0), ((3, 1),),chan
			)

	def SendExtI2C(self, I2C_Address=defaultNamedNotOptArg, data=defaultNamedNotOptArg):
		'method SendExtI2C'
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((17, 1), (12, 1)),I2C_Address
			, data)

	def SetTrigMux(self, port1=defaultNamedNotOptArg, port2=defaultNamedNotOptArg, port3=defaultNamedNotOptArg, port4=defaultNamedNotOptArg):
		'method SetTrigMux'
		return self._oleobj_.InvokeTypes(132, LCID, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1)),port1
			, port2, port3, port4)

	def Set_ASC_ECP(self, AscEcp=defaultNamedNotOptArg):
		'method Set_ASC_ECP'
		return self._oleobj_.InvokeTypes(101, LCID, 1, (24, 0), ((3, 1),),AscEcp
			)

	def Set_Gamma_Cpri(self, ifSel=defaultNamedNotOptArg):
		'method Set_Gamma_Cpri'
		return self._oleobj_.InvokeTypes(102, LCID, 1, (24, 0), ((3, 1),),ifSel
			)

	def Set_Timing_Ref(self, Source=defaultNamedNotOptArg):
		'method Set_Timing_Ref'
		return self._oleobj_.InvokeTypes(105, LCID, 1, (24, 0), ((3, 1),),Source
			)

	def ShowWindow(self, normal=defaultNamedNotOptArg):
		'method ShowWindow'
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((11, 1),),normal
			)

	def ShutDown(self):
		'method ShutDown'
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def TPF_ActiveX_ReadCmd(self, objectID=defaultNamedNotOptArg, freeString=pythoncom.Missing, done=pythoncom.Missing, MessageFound=pythoncom.Missing):
		'method TPF_ActiveX_ReadCmd'
		return self._ApplyTypes_(213, 1, (24, 0), ((3, 1), (16392, 2), (16395, 2), (16395, 2)), 'TPF_ActiveX_ReadCmd', None,objectID
			, freeString, done, MessageFound)

	def TPF_ActiveX_SendCmd(self, objectID=defaultNamedNotOptArg, freeString=defaultNamedNotOptArg):
		'method TPF_ActiveX_SendCmd'
		return self._oleobj_.InvokeTypes(212, LCID, 1, (24, 0), ((3, 1), (8, 1)),objectID
			, freeString)

	def TPF_ClearAll(self):
		'method TPF_ClearAll'
		return self._oleobj_.InvokeTypes(214, LCID, 1, (24, 0), (),)

	def TPF_ClearLog(self, logType=defaultNamedNotOptArg):
		'method TPF_ClearLog'
		return self._oleobj_.InvokeTypes(244, LCID, 1, (24, 0), ((3, 1),),logType
			)

	def TPF_CreateActiveX(self, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, objectID=pythoncom.Missing):
		'method TPF_CreateActiveX'
		return self._ApplyTypes_(205, 1, (24, 0), ((3, 1), (3, 1), (16387, 2)), 'TPF_CreateActiveX', None,portNo
			, radioType, objectID)

	def TPF_CreateActiveX2(self, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, physicalPosition=defaultNamedNotOptArg, ecpAddress=defaultNamedNotOptArg
			, objectID=pythoncom.Missing):
		'method TPF_CreateActiveX2'
		return self._ApplyTypes_(216, 1, (24, 0), ((3, 1), (3, 1), (3, 1), (3, 1), (16387, 2)), 'TPF_CreateActiveX2', None,portNo
			, radioType, physicalPosition, ecpAddress, objectID)

	def TPF_CreateCOMPort(self, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, comPort=defaultNamedNotOptArg, baudRate=defaultNamedNotOptArg
			, echo=defaultNamedNotOptArg, objectID=pythoncom.Missing):
		'method TPF_CreateCOMPort'
		return self._ApplyTypes_(203, 1, (24, 0), ((3, 1), (3, 1), (8, 1), (3, 1), (3, 1), (16387, 2)), 'TPF_CreateCOMPort', None,portNo
			, radioType, comPort, baudRate, echo, objectID
			)

	def TPF_CreateCOMPort2(self, portNo=defaultNamedNotOptArg, radioType=defaultNamedNotOptArg, comPort=defaultNamedNotOptArg, baudRate=defaultNamedNotOptArg
			, echo=defaultNamedNotOptArg, physicalPosition=defaultNamedNotOptArg, ecpAddress=defaultNamedNotOptArg, objectID=pythoncom.Missing):
		'method TPF_CreateCOMPort2'
		return self._ApplyTypes_(215, 1, (24, 0), ((3, 1), (3, 1), (8, 1), (3, 1), (3, 1), (3, 1), (3, 1), (16387, 2)), 'TPF_CreateCOMPort2', None,portNo
			, radioType, comPort, baudRate, echo, physicalPosition
			, ecpAddress, objectID)

	def TPF_DestroyActiveX(self, objectID=defaultNamedNotOptArg):
		'method TPF_DestroyActiveX'
		return self._oleobj_.InvokeTypes(206, LCID, 1, (24, 0), ((3, 1),),objectID
			)

	def TPF_DestroyCOMPort(self, objectID=defaultNamedNotOptArg):
		'method TPF_DestroyCOMPort'
		return self._oleobj_.InvokeTypes(204, LCID, 1, (24, 0), ((3, 1),),objectID
			)

	def TPF_Exit(self):
		'method TPF_Exit'
		return self._oleobj_.InvokeTypes(202, LCID, 1, (24, 0), (),)

	def TPF_GetNrOfRULinks(self, nrOfLinks=pythoncom.Missing):
		'method TPF_GetNrOfRULinks'
		return self._ApplyTypes_(207, 1, (24, 0), ((16387, 2),), 'TPF_GetNrOfRULinks', None,nrOfLinks
			)

	def TPF_GetRULinkAt(self, linkIndex=defaultNamedNotOptArg, objectID=pythoncom.Missing, status=pythoncom.Missing, object_type=pythoncom.Missing
			, rulink=pythoncom.Missing, info=pythoncom.Missing):
		'method TPF_GetRULinkAt'
		return self._ApplyTypes_(208, 1, (24, 0), ((3, 1), (16387, 2), (16387, 2), (16392, 2), (16392, 2), (16392, 2)), 'TPF_GetRULinkAt', None,linkIndex
			, objectID, status, object_type, rulink, info
			)

	def TPF_GetRULinkAt2(self, linkIndex=defaultNamedNotOptArg, objectID=pythoncom.Missing, status=pythoncom.Missing, object_type=pythoncom.Missing
			, rulink=pythoncom.Missing, info=pythoncom.Missing, COM_PORT=pythoncom.Missing, rate=pythoncom.Missing, ECP=pythoncom.Missing
			, PHYS_ADD=pythoncom.Missing, port=pythoncom.Missing, radioType=pythoncom.Missing):
		'method TPF_GetRULinkAt2'
		return self._ApplyTypes_(217, 1, (24, 0), ((3, 1), (16387, 2), (16387, 2), (16392, 2), (16392, 2), (16392, 2), (16392, 2), (16387, 2), (16387, 2), (16387, 2), (16387, 2), (16387, 2)), 'TPF_GetRULinkAt2', None,linkIndex
			, objectID, status, object_type, rulink, info
			, COM_PORT, rate, ECP, PHYS_ADD, port
			, radioType)

	def TPF_GetStatus(self, objectID=defaultNamedNotOptArg, ok=pythoncom.Missing):
		'method TPF_GetStatus'
		return self._ApplyTypes_(209, 1, (24, 0), ((3, 1), (16387, 2)), 'TPF_GetStatus', None,objectID
			, ok)

	def TPF_Init(self):
		'method TPF_Init'
		return self._oleobj_.InvokeTypes(201, LCID, 1, (24, 0), (),)

	def TPF_SaveLog(self, path=defaultNamedNotOptArg, logType=defaultNamedNotOptArg):
		'method TPF_SaveLog'
		return self._oleobj_.InvokeTypes(245, LCID, 1, (24, 0), ((8, 1), (3, 1)),path
			, logType)

	def TPF_StartLog(self):
		'method TPF_StartLog'
		return self._oleobj_.InvokeTypes(210, LCID, 1, (24, 0), (),)

	def TPF_StopLog(self):
		'method TPF_StopLog'
		return self._oleobj_.InvokeTypes(211, LCID, 1, (24, 0), (),)

	def UL_GammaAGC(self, chain=defaultNamedNotOptArg, ga=defaultNamedNotOptArg):
		'method UL_GammaAGC'
		return self._oleobj_.InvokeTypes(20, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, ga)

	def UL_GammaStatus(self, chain=defaultNamedNotOptArg, gs=defaultNamedNotOptArg):
		'method UL_GammaStatus'
		return self._oleobj_.InvokeTypes(19, LCID, 1, (19, 0), ((3, 1), (3, 1)),chain
			, gs)

	def USB_AUX_FlushRX(self):
		'method USB_AUX_FlushRX'
		return self._oleobj_.InvokeTypes(168, LCID, 1, (24, 0), (),)

	def USB_AUX_FlushTX(self):
		'method USB_AUX_FlushTX'
		return self._oleobj_.InvokeTypes(167, LCID, 1, (24, 0), (),)

	def USB_AUX_Read(self):
		'method USB_AUX_Read'
		return self._ApplyTypes_(166, 1, (12, 0), (), 'USB_AUX_Read', None,)

	def USB_AUX_Write(self, data=defaultNamedNotOptArg):
		'method USB_AUX_Write'
		return self._oleobj_.InvokeTypes(165, LCID, 1, (24, 0), ((12, 1),),data
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

class _Object(DispatchBaseClass):
	CLSID = IID('{65074F7F-63C0-304E-AF0A-D51741CB4A8D}')
	coclass_clsid = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')

	def Equals(self, obj=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(1610743809, LCID, 1, (11, 0), ((12, 1),),obj
			)

	def GetHashCode(self):
		return self._oleobj_.InvokeTypes(1610743810, LCID, 1, (3, 0), (),)

	# Result is of type _Type
	def GetType(self):
		ret = self._oleobj_.InvokeTypes(1610743811, LCID, 1, (13, 0), (),)
		if ret is not None:
			# See if this IUnknown is really an IDispatch
			try:
				ret = ret.QueryInterface(pythoncom.IID_IDispatch)
			except pythoncom.error:
				return ret
			ret = Dispatch(ret, 'GetType', '{BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2}')
		return ret

	_prop_map_get_ = {
		"ToString": (0, 2, (8, 0), (), "ToString", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'ToString'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "ToString", None))
	def __str__(self, *args):
		return str(self.__call__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

from win32com.client import CoClassBaseClass
# This CoClass is known by the name 'RuMaster.RuControl'
class RuControl(CoClassBaseClass): # A CoClass
	# RuControl Class
	CLSID = IID('{75BF5D19-5AB0-4004-97EE-F039164681A3}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRuControl3,
		IRuControl1,
		IRuControl4,
		IRuControl2,
		_Object,
	]
	default_interface = IRuControl4

IRuControl1_vtables_dispatch_ = 1
IRuControl1_vtables_ = [
	(( 'ShutDown' , ), 1, (1, (), [ ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( 'GetDeviceInfo' , 'DeviceInfo' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( 'ShowWindow' , 'normal' , ), 3, (3, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( 'GetFPGAVersion' , 'FpgaInfo' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( 'GetIOBoardInfo' , 'IOBInfo' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( 'MasterReady' , 'ready' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( 'ExtReferenceLock' , 'lock' , ), 7, (7, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( 'SendExtI2C' , 'I2C_Address' , 'data' , ), 8, (8, (), [ (17, 1, None, None) , 
			 (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( 'ReceExtI2C' , 'I2C_Address' , 'size' , 'data' , ), 9, (9, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( 'ExternalPatternGen' , 'input' , ), 10, (10, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( 'LAPodOutput' , 'enable' , ), 11, (11, (), [ (11, 0, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( 'LoadPatternFile' , 'chain' , 'index' , 'filename' , 'nBytes' , 
			 ), 12, (12, (), [ (3, 1, None, None) , (17, 1, None, None) , (8, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( 'PatternFileSelect' , 'chain' , 'fileindx1' , 'fileindx2' , 'on' , 
			 ), 13, (13, (), [ (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( 'ExternalTrig' , 'dstrb_src' , 'TSC' , 'SC' , 'CC' , 
			 'BC' , ), 14, (14, (), [ (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (18, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( 'BFN_Offset' , 'offset' , ), 15, (15, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( 'LA_StartTrig' , 'chain' , 'num_kB' , 'TSC' , 'SC' , 
			 'CC' , 'BC' , 'ext_trig_on' , 'int_trig_off' , ), 16, (16, (), [ 
			 (3, 1, None, None) , (19, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (18, 1, None, None) , 
			 (17, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( 'LA_SaveToFile' , 'chain' , 'num_kB' , 'filename' , ), 17, (17, (), [ 
			 (3, 1, None, None) , (19, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( 'LA_ReadData' , 'chain' , 'num_kB' , 'data' , ), 18, (18, (), [ 
			 (3, 1, None, None) , (19, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( 'UL_GammaStatus' , 'chain' , 'gs' , 'Value' , ), 19, (19, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( 'UL_GammaAGC' , 'chain' , 'ga' , 'Value' , ), 20, (20, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( 'DL_GammaDataSrc' , 'chain' , 'Source' , ), 21, (21, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( 'DL_GammaCarrier' , 'chain' , 'CarrierID_A' , 'CarrierID_B' , ), 22, (22, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( 'DL_BFN_OffsNum' , 'chain' , 'TSC' , 'SC' , 'CC' , 
			 'BC' , ), 23, (23, (), [ (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (18, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( 'DL_BFN_Update' , 'chain' , 'src' , 'ctrl' , 'Number' , 
			 ), 24, (24, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( 'DL_BFN_Control' , 'chain' , 'mode' , 'StartBit1' , 'StartBit2' , 
			 ), 25, (25, (), [ (3, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( 'DL_BFN_Offset' , 'chain' , 'TSC' , 'SC' , 'CC' , 
			 'BC' , 'control' , 'event' , ), 26, (26, (), [ (3, 1, None, None) , 
			 (17, 1, None, None) , (17, 1, None, None) , (18, 1, None, None) , (17, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( 'DL_EventCount' , 'chain' , 'ev' , 'BFN' , 'TSC' , 
			 'SC' , 'CC' , 'BC' , ), 27, (27, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , (18, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (18, 1, None, None) , 
			 (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( 'DL_EventControl' , 'chain' , 'ev' , 'cond' , 'ctrl' , 
			 ), 28, (28, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( 'DL_GammaScale' , 'chain' , 'factor_A_dB' , 'factor_B_dB' , ), 29, (29, (), [ 
			 (3, 1, None, None) , (5, 1, None, None) , (5, 1, None, None) , ], 1 , 1 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( 'FG_GammaGenCtrl' , 'chain' , 'dst' , 'src' , ), 30, (30, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( 'FG_GammaConstant' , 'chain' , 'Constant_I' , 'Constant_Q' , ), 31, (31, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( 'FG_GammaPnGenStat' , 'chain' , 'pn' , 'status' , ), 32, (32, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( 'FG_GammaPnGenSeed' , 'chain' , 'SeedReg_I_Hi' , 'SeedReg_I_Lo' , 'SeedReg_Q_Hi' , 
			 'SeedReg_Q_Lo' , ), 33, (33, (), [ (3, 1, None, None) , (19, 1, None, None) , (19, 1, None, None) , 
			 (19, 1, None, None) , (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( 'IQ_ManipulationCtrl' , 'chain' , 'man_type' , 'man_ctrl' , 'event' , 
			 'num_bits' , 'man_pin' , 'manipulation_seq' , ), 34, (34, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (17, 1, None, None) , (3, 1, None, None) , 
			 (18, 1, None, None) , ], 1 , 1 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( 'IQ_ManipulationMask' , 'chain' , 'mstb' , 'ma0' , 'ma1' , 
			 'ma2' , 'ma3' , 'mb0' , 'mb1' , 'mb2' , 
			 'mb3' , 'mid_par' , ), 35, (35, (), [ (3, 1, None, None) , (11, 1, None, None) , 
			 (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , 
			 (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( 'BBCLK_Manipulation' , 'chain' , 'clk_phase' , 'status' , ), 36, (36, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
	(( 'BBCLK_Blanking' , 'chain' , 'src' , 'clk_cycles' , 'clock' , 
			 ), 37, (37, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( 'DL_Strobe_Offset' , 'chain' , 'TSC' , 'SC' , 'CC' , 
			 'BC' , 'event' , ), 38, (38, (), [ (3, 1, None, None) , (17, 1, None, None) , 
			 (17, 1, None, None) , (18, 1, None, None) , (17, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
]

IRuControl2_vtables_dispatch_ = 1
IRuControl2_vtables_ = [
	(( 'PatternFileList' , 'chain' , 'filename' , 'nBytes' , ), 39, (39, (), [ 
			 (3, 1, None, None) , (8, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 180 , (3, 0, None, None) , 0 , )),
	(( 'GammaScaleFile' , 'chain' , 'filename' , ), 40, (40, (), [ (3, 1, None, None) , 
			 (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( 'GammaScaleRemoveAll' , 'chain' , ), 41, (41, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 188 , (3, 0, None, None) , 0 , )),
	(( 'EnableRuFPGA' , 'enable' , ), 42, (42, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 192 , (3, 0, None, None) , 0 , )),
	(( 'RuFPGA' , 'cmd' , 'size' , 'address' , 'dwnlink' , 
			 'uplink' , ), 43, (43, (), [ (17, 1, None, None) , (18, 1, None, None) , (19, 1, None, None) , 
			 (12, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 196 , (3, 0, None, None) , 0 , )),
]

IRuControl3_vtables_dispatch_ = 1
IRuControl3_vtables_ = [
	(( 'CI_DL_LoadFile' , 'chan' , 'filename' , ), 44, (44, (), [ (3, 1, None, None) , 
			 (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 200 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_StartTransfer' , 'chan' , ), 45, (45, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 204 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_StopTransfer' , 'chan' , ), 46, (46, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_StartSampling' , 'chan' , 'crit' , 'num_kB' , ), 47, (47, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_Save' , 'chan' , 'comment' , 'type' , 'lineRate' , 
			 'wordLength' , 'filename' , ), 48, (48, (), [ (3, 1, None, None) , (8, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (17, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 216 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetAlarm' , 'chan' , 'alType' , 'alOn' , ), 49, (49, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 220 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetAlarmCnt' , 'chan' , 'alType' , 'alCnt' , ), 50, (50, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 224 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_ClearAlarm' , 'chan' , 'alType' , ), 51, (51, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 228 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_SetLinkConf' , 'chan' , 'lineRate' , 'enLink' , 'mode' , 
			 'l1Res' , ), 52, (52, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 232 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_SetupCW' , 'chan' , 'cascW' , 'timSyncOn' , 'protRev' , 
			 'enChain' , ), 53, (53, (), [ (3, 1, None, None) , (17, 1, None, None) , (11, 1, None, None) , 
			 (17, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 236 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_SetDelCntThr' , 'chan' , 'delCntThr' , ), 54, (54, (), [ (3, 1, None, None) , 
			 (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 240 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_SetSyncSrc' , 'chan' , 'syncSrc' , ), 55, (55, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 244 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_EnableCwHandl' , 'chan' , 'handl' , 'enable' , ), 56, (56, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 248 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_SetLnkSrvCtrl' , 'chan' , 'srv' , 'setSrv' , ), 57, (57, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 252 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetFsmStatFa' , 'chan' , 'faState' , ), 58, (58, (), [ (3, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 256 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetFsmStatLink' , 'chan' , 'linkState' , ), 59, (59, (), [ (3, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 260 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetJitBufDelCntC' , 'chan' , 'cCnt' , ), 60, (60, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 264 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetJitBufDelCntF' , 'chan' , 'fCnt' , ), 61, (61, (), [ (3, 1, None, None) , 
			 (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 268 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetCWDetect' , 'chan' , 'cascW' , ), 62, (62, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 272 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetCWDetectAdd' , 'chan' , 'cascWAdd' , ), 63, (63, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 276 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetCWDetectTim' , 'chan' , 'timSyncOn' , ), 64, (64, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 280 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetCWDetectRev' , 'chan' , 'protRev' , ), 65, (65, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 284 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetCWDetectMast' , 'chan' , 'isMaster' , ), 66, (66, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 288 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetTADetectW' , 'chan' , 'words' , ), 67, (67, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 292 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetTADetectBfr' , 'chan' , 'bFrames' , ), 68, (68, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 296 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetTADetectHfr' , 'chan' , 'hFrames' , ), 69, (69, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 300 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetHsbCw' , 'chan' , 'hsbW' , ), 70, (70, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 304 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetVersion' , 'chan' , 'version' , ), 71, (71, (), [ (3, 1, None, None) , 
			 (16401, 10, None, None) , ], 1 , 1 , 4 , 0 , 308 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_ShiftPhase' , 'trig' , 'shift' , ), 72, (72, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 312 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_ResetPhase' , 'trig' , ), 73, (73, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 316 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_SetTrigSrc' , 'trig' , 'src' , ), 74, (74, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 320 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_SetCmd' , 'Scan' , 'txPwD' , 'rxPwD' , 'enComAl' , 
			 ), 75, (75, (), [ (3, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 324 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_SetModes' , 'Scan' , 'txDeEmph' , 'rxEqu' , 'bRate' , 
			 'lbMode' , ), 76, (76, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 328 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_Reset' , 'Scan' , ), 77, (77, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 332 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_WriteReg' , 'Scan' , 'reg' , 'Value' , ), 78, (78, (), [ 
			 (3, 1, None, None) , (18, 1, None, None) , (18, 1, None, None) , ], 1 , 1 , 4 , 0 , 336 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_ReadReg' , 'Scan' , 'reg' , 'Value' , ), 79, (79, (), [ 
			 (3, 1, None, None) , (18, 1, None, None) , (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 340 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_GetAlarm' , 'Scan' , 'alType' , 'alOn' , ), 80, (80, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 344 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_GetAlarmCnt' , 'Scan' , 'alType' , 'alCnt' , ), 81, (81, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 348 , (3, 0, None, None) , 0 , )),
	(( 'CI_SCAN_ClearAlarm' , 'Scan' , 'alType' , ), 82, (82, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 352 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_GetPresent' , 'SFP' , 'sfpPresent' , ), 83, (83, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 356 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_GetDisabled' , 'SFP' , 'DISABLED' , ), 84, (84, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 360 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_Disable' , 'SFP' , 'DISABLE' , ), 85, (85, (), [ (3, 1, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 364 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_GetAlarm' , 'SFP' , 'alType' , 'alOn' , ), 86, (86, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 368 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_GetAlarmCnt' , 'SFP' , 'alType' , 'alCnt' , ), 87, (87, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 372 , (3, 0, None, None) , 0 , )),
	(( 'CI_SFP_ClearAlarm' , 'SFP' , 'alType' , ), 88, (88, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 376 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetCtrlVersion' , 'ver' , ), 89, (89, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 380 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetCtrlBuild' , 'build' , ), 90, (90, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 384 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetIoVersion' , 'ver' , ), 91, (91, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 388 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetIoBuild' , 'build' , ), 92, (92, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 392 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetPll' , 'pllLocked' , ), 93, (93, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 396 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetPs1' , 'psLocked1' , ), 94, (94, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 400 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_GetPs2' , 'psLocked2' , ), 95, (95, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 404 , (3, 0, None, None) , 0 , )),
	(( 'CI_FPGA_Reset' , ), 96, (96, (), [ ], 1 , 1 , 4 , 0 , 408 , (3, 0, None, None) , 0 , )),
	(( 'CI_TD_SaveIQ' , 'subframe' , 'slot' , 'axc' , 'pattfilename' , 
			 'iqfilename' , ), 97, (97, (), [ (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (8, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 412 , (3, 0, None, None) , 0 , )),
	(( 'CI_TD_GetGain' , 'subframe' , 'slot' , 'axc' , 'pattfilename' , 
			 'KUbpDbm' , 'skipAgcBit' , 'useAllChips' , 'agcValue' , ), 98, (98, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (8, 1, None, None) , (5, 1, None, None) , 
			 (11, 1, None, None) , (11, 1, None, None) , (16389, 10, None, None) , ], 1 , 1 , 4 , 0 , 416 , (3, 0, None, None) , 0 , )),
]

IRuControl4_vtables_dispatch_ = 1
IRuControl4_vtables_ = [
	(( 'CI_REUSE_ClearAllAlarms' , 'chan' , 'noAlarms' , ), 99, (99, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 420 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_AlarmMonitor' , 'chan' , 'alarms' , ), 100, (100, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 424 , (3, 0, None, None) , 0 , )),
	(( 'Set_ASC_ECP' , 'AscEcp' , ), 101, (101, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 428 , (3, 0, None, None) , 0 , )),
	(( 'Set_Gamma_Cpri' , 'ifSel' , ), 102, (102, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 432 , (3, 0, None, None) , 0 , )),
	(( 'BBCLK_Out' , 'chain' , 'enable' , ), 103, (103, (), [ (3, 1, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 436 , (3, 0, None, None) , 0 , )),
	(( 'GetFPGAVersionEx' , 'Fpga' , 'FpgaInfo' , ), 104, (104, (), [ (3, 1, None, None) , 
			 (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 440 , (3, 0, None, None) , 0 , )),
	(( 'Set_Timing_Ref' , 'Source' , ), 105, (105, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 444 , (3, 0, None, None) , 0 , )),
	(( 'LoadFpga' , 'Fpga' , 'FilePath' , 'loaded' , ), 106, (106, (), [ 
			 (3, 1, None, None) , (8, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 448 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_GetCarrierSamples' , 'chan' , 'carrier' , 'numSamples' , 'smpl' , 
			 'data' , ), 107, (107, (), [ (3, 1, None, None) , (17, 1, None, None) , (19, 1, None, None) , 
			 (3, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 452 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_SetupCarrier' , 'chan' , 'carrier' , 'idt' , 'start' , 
			 'rate' , 'tech' , 's0' , 'manByp' , 'manDbgExp' , 
			 'manDbgAagc' , 'enManDbg' , 'fsinfoDbgHf' , 'fsinfoDbgBf' , 'enFsinfoDbg' , 
			 ), 108, (108, (), [ (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (17, 1, None, None) , 
			 (17, 1, None, None) , (11, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 456 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_EnableCarrier' , 'chan' , 'carrier' , 'enable' , ), 109, (109, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 460 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_CarrierStartSampling' , 'chan' , 'crit' , 'src' , 'num_kB' , 
			 ), 110, (110, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 464 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_GainLoadFile' , 'chan' , 'filename' , 'unpacked' , ), 111, (111, (), [ 
			 (3, 1, None, None) , (8, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 468 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_GainSetup' , 'chan' , 'carrier' , 'start' , 'rate' , 
			 'enGain' , 'gain' , 'gainFactor' , ), 112, (112, (), [ (3, 1, None, None) , 
			 (17, 1, None, None) , (17, 0, None, None) , (3, 1, None, None) , (11, 1, None, None) , (5, 1, None, None) , 
			 (16389, 10, None, None) , ], 1 , 1 , 4 , 0 , 472 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_GainEnableCarrier' , 'chan' , 'carrier' , 'enable' , ), 113, (113, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 476 , (3, 0, None, None) , 0 , )),
	(( 'DL_GammaActivate' , 'chain' , 'on' , ), 114, (114, (), [ (3, 1, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 480 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_CarrId' , 'chan' , 'carrier' , 'id' , ), 115, (115, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 484 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_CarrAxcAddr' , 'chan' , 'carrier' , 'addr' , ), 116, (116, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 488 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_CarrFsinfo' , 'chan' , 'carrier' , 'hf' , 'bf' , 
			 ), 117, (117, (), [ (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , ], 1 , 1 , 4 , 0 , 492 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_GetCarrierFsinfo' , 'chan' , 'carrier' , 'data' , ), 118, (118, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 496 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_GetCarrierAgc' , 'chan' , 'carrier' , 'data' , ), 119, (119, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 500 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_ClearAxc' , 'chan' , ), 120, (120, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 504 , (3, 0, None, None) , 0 , )),
	(( 'MultiAddLoadFile' , 'filename' , 'type' , 'enCh1' , 'enCh2' , 
			 'enCh3' , 'enCh4' , 'index' , ), 121, (121, (), [ (8, 1, None, None) , 
			 (3, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , 
			 (16402, 10, None, None) , ], 1 , 1 , 4 , 0 , 508 , (3, 0, None, None) , 0 , )),
	(( 'MultiChannelEnable' , 'enCh1' , 'enCh2' , 'enCh3' , 'enCh4' , 
			 'index' , ), 122, (122, (), [ (11, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , 
			 (11, 1, None, None) , (18, 1, None, None) , ], 1 , 1 , 4 , 0 , 512 , (3, 0, None, None) , 0 , )),
	(( 'MultiActivateFile' , 'index' , 'activate' , ), 123, (123, (), [ (18, 1, None, None) , 
			 (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 516 , (3, 0, None, None) , 0 , )),
	(( 'MultiClearAll' , ), 124, (124, (), [ ], 1 , 1 , 4 , 0 , 520 , (3, 0, None, None) , 0 , )),
	(( 'MultiGetAllFileData' , 'data' , ), 125, (125, (), [ (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 524 , (3, 0, None, None) , 0 , )),
	(( 'MultiGetFileType' , 'index' , 'type' , ), 126, (126, (), [ (18, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 528 , (3, 0, None, None) , 0 , )),
	(( 'MultiDeactivateAll' , ), 127, (127, (), [ ], 1 , 1 , 4 , 0 , 532 , (3, 0, None, None) , 0 , )),
	(( 'DebugGetRegister' , 'address' , 'Value' , ), 128, (128, (), [ (18, 1, None, None) , 
			 (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 536 , (3, 0, None, None) , 0 , )),
	(( 'DebugSetRegister' , 'address' , 'Value' , ), 129, (129, (), [ (18, 1, None, None) , 
			 (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 540 , (3, 0, None, None) , 0 , )),
	(( 'DebugReadMemory' , 'address' , 'size' , 'MEMORY' , 'data' , 
			 ), 130, (130, (), [ (19, 1, None, None) , (19, 1, None, None) , (3, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 544 , (3, 0, None, None) , 0 , )),
	(( 'DebugWriteMemory' , 'address' , 'size' , 'MEMORY' , 'data' , 
			 ), 131, (131, (), [ (19, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 548 , (3, 0, None, None) , 0 , )),
	(( 'SetTrigMux' , 'port1' , 'port2' , 'port3' , 'port4' , 
			 ), 132, (132, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 552 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_SamplingReady' , 'chan' , 'ready' , ), 133, (133, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 556 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_K285Setup' , 'chan' , 'offsetTx' , 'offsetRx' , 'lenTx' , 
			 'lenRx' , ), 134, (134, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 560 , (3, 0, None, None) , 0 , )),
	(( 'HW_AlarmGet' , 'alarm' , 'status' , ), 135, (135, (), [ (3, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 564 , (3, 0, None, None) , 0 , )),
	(( 'HW_AlarmClear' , 'alarm' , ), 136, (136, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 568 , (3, 0, None, None) , 0 , )),
	(( 'HW_SwapCpriAtoB' , 'swap' , ), 137, (137, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 572 , (3, 0, None, None) , 0 , )),
	(( 'HW_ConfCpriElIf' , 'chan' , 'lev' , 'preBoost' , 'swing' , 
			 'ratio' , 'location' , ), 138, (138, (), [ (3, 1, None, None) , (3, 1, None, None) , 
			 (11, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 576 , (3, 0, None, None) , 0 , )),
	(( 'CPC_Setup' , 'filename' , 'enable' , 'useCdlFileName' , 'ready' , 
			 ), 139, (139, (), [ (8, 1, None, None) , (11, 0, None, None) , (11, 0, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 580 , (3, 0, None, None) , 0 , )),
	(( 'CPC_SetLoopLength' , 'loopLength' , 'ready' , ), 140, (140, (), [ (19, 1, None, None) , 
			 (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 584 , (3, 0, None, None) , 0 , )),
	(( 'AUX_SetAuxMux' , 'ift' , ), 141, (141, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 588 , (3, 0, None, None) , 0 , )),
	(( 'AUX_SetCpriMode' , 'hdlcCh' , 'br' , ), 142, (142, (), [ (17, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 592 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_GsmFramesync' , 'chan' , 'offset' , 'length' , 'hyperframe' , 
			 'basicframe' , ), 143, (143, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 596 , (3, 0, None, None) , 0 , )),
	(( 'CI_SetCpriMux' , 'chan' , ), 144, (144, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 600 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetFsmMode' , 'mode' , ), 145, (145, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 604 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetFsmMode' , 'mode' , ), 146, (146, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 608 , (3, 0, None, None) , 0 , )),
	(( 'DGC_LoadFsmConfig' , 'filename' , ), 147, (147, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 612 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SendFsmToHw' , ), 148, (148, (), [ ], 1 , 1 , 4 , 0 , 616 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetCurrentFsmState_Wcdma' , 'rowIndex' , 'stateIndex' , ), 149, (149, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 620 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetCurrentFsmState_Wcdma' , 'rowIndex' , 'stateIndex' , ), 150, (150, (), [ (16387, 2, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 624 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetActiveFsmRow' , 'rowIndex' , ), 151, (151, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 628 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetStartOffset' , 'rf' , 'hf' , 'bf' , ), 152, (152, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 632 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetStartOffset' , 'rf' , 'hf' , 'bf' , ), 153, (153, (), [ 
			 (16387, 2, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 636 , (3, 0, None, None) , 0 , )),
	(( 'DGC_ChangeToGsmMode' , 'timing' , ), 154, (154, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 640 , (3, 0, None, None) , 0 , )),
	(( 'DGC_ChangeTo_LTE_WCDMAMode' , ), 155, (155, (), [ ], 1 , 1 , 4 , 0 , 644 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetUartBitrate' , 'bitRate' , ), 156, (156, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 648 , (3, 0, None, None) , 0 , )),
	(( 'CI_REUSE_GetPFAAlarms' , 'hasAlarm' , 'channels' , ), 157, (157, (), [ (16395, 2, None, None) , 
			 (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 652 , (3, 0, None, None) , 0 , )),
	(( 'GetPlatformParameter' , 'key' , 'result' , ), 158, (158, (), [ (8, 1, None, None) , 
			 (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 656 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetActiveGsmRow' , 'row' , ), 159, (159, (), [ (2, 1, None, None) , ], 1 , 1 , 4 , 0 , 660 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetActiveGsmRow' , 'row' , ), 160, (160, (), [ (16386, 10, None, None) , ], 1 , 1 , 4 , 0 , 664 , (3, 0, None, None) , 0 , )),
	(( 'Log_EnableLogging' , ), 161, (161, (), [ ], 1 , 1 , 4 , 0 , 668 , (3, 0, None, None) , 0 , )),
	(( 'Log_DisableLogging' , ), 162, (162, (), [ ], 1 , 1 , 4 , 0 , 672 , (3, 0, None, None) , 0 , )),
	(( 'Log_ClearLogs' , ), 163, (163, (), [ ], 1 , 1 , 4 , 0 , 676 , (3, 0, None, None) , 0 , )),
	(( 'Log_SaveLogs' , 'path' , ), 164, (164, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 680 , (3, 0, None, None) , 0 , )),
	(( 'USB_AUX_Write' , 'data' , ), 165, (165, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 684 , (3, 0, None, None) , 0 , )),
	(( 'USB_AUX_Read' , 'data' , ), 166, (166, (), [ (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 688 , (3, 0, None, None) , 0 , )),
	(( 'USB_AUX_FlushTX' , ), 167, (167, (), [ ], 1 , 1 , 4 , 0 , 692 , (3, 0, None, None) , 0 , )),
	(( 'USB_AUX_FlushRX' , ), 168, (168, (), [ ], 1 , 1 , 4 , 0 , 696 , (3, 0, None, None) , 0 , )),
	(( 'AUX_SetAuxSourceMux' , 'AuxSource' , ), 169, (169, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 700 , (3, 0, None, None) , 0 , )),
	(( 'ScanCircuitReinitialisation' , 'chan' , ), 170, (170, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 704 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_LoadTimingTriggersConfig' , 'filename' , ), 171, (171, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 708 , (3, 0, None, None) , 0 , )),
	(( 'CI_TRIG_TimingTriggersEnableTrigger' , 'triggerNr' , 'enable' , ), 172, (172, (), [ (17, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 712 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetDLCarrierSourceType' , 'carrier' , 'type' , ), 173, (173, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 716 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetDLCarrierSourceType' , 'carrier' , 'type' , ), 174, (174, (), [ (3, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 720 , (3, 0, None, None) , 0 , )),
	(( 'DGC_Change_Gain_Gsm' , 'rowNr' , 'stateIndex' , 'data' , ), 175, (175, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 724 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_GainDeleteAllCarriers' , ), 176, (176, (), [ ], 1 , 1 , 4 , 0 , 728 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_GainSetupOneCarrier' , 'chan' , 'carrier' , 'start' , 'rate' , 
			 'enGain' , 'gain' , 'enable' , 'id' , 'addr' , 
			 'hf' , 'bf' , 'type' , 'gainFactor' , ), 177, (177, (), [ 
			 (3, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , 
			 (5, 1, None, None) , (11, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , (17, 1, None, None) , 
			 (17, 1, None, None) , (3, 1, None, None) , (16389, 10, None, None) , ], 1 , 1 , 4 , 0 , 732 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_LoadFile_TimeAdvanced' , 'chan' , 'filename' , 'hf' , 'bf' , 
			 ), 178, (178, (), [ (3, 1, None, None) , (8, 1, None, None) , (2, 1, None, None) , (2, 1, None, None) , ], 1 , 1 , 4 , 0 , 736 , (3, 0, None, None) , 0 , )),
	(( 'DGC_SetCurrentFsmState' , 'rowIndex' , 'stateIndex' , ), 179, (179, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 740 , (3, 0, None, None) , 0 , )),
	(( 'DGC_GetCurrentFsmState' , 'rowIndex' , 'stateIndex' , ), 180, (180, (), [ (16387, 2, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 744 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_CarrierNumberForFSMStart' , 'carrierNumber' , ), 181, (181, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 748 , (3, 0, None, None) , 0 , )),
	(( 'CI_UL_SubFrameExtr' , 'CarrierID' , 'subframeStart' , 'nrOfSubframe' , 'offset' , 
			 'ULSubFrameEnable' , ), 182, (182, (), [ (17, 1, None, None) , (3, 1, None, None) , (17, 1, None, None) , 
			 (3, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 752 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_LoadFile' , 'filename' , ), 183, (183, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 756 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_GetUsedContainers' , 'containers' , ), 184, (184, (), [ (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 760 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_CopyContainers' , 'filename' , 'StartSample_Source' , 'NumberOfSamples_Source' , 'NewStructure' , 
			 'StartSample_Dest' , 'NumberOfSamples_Dest' , 'Frame' , 'hf' , 'bf' , 
			 ), 185, (185, (), [ (8, 1, None, None) , (3, 0, None, None) , (3, 0, None, None) , (11, 0, None, None) , 
			 (3, 0, None, None) , (3, 0, None, None) , (3, 0, None, None) , (3, 0, None, None) , (3, 0, None, None) , ], 1 , 1 , 4 , 0 , 764 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_LoadToMemory' , ), 186, (186, (), [ ], 1 , 1 , 4 , 0 , 768 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_SaveToFile' , 'filename' , ), 187, (187, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 772 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_Combine_DeleteAll' , ), 188, (188, (), [ ], 1 , 1 , 4 , 0 , 776 , (3, 0, None, None) , 0 , )),
	(( 'RT_UpgradeRU' , 'filename' , 'port' , 'physPos' , 'restart' , 
			 'ret_val' , ), 189, (189, (), [ (8, 1, None, None) , (19, 1, None, None) , (19, 1, None, None) , 
			 (11, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 780 , (3, 0, None, None) , 0 , )),
	(( 'RT_AsynchronousUpgradeRU' , 'filename' , 'port' , 'physPos' , 'restart' , 
			 'ret_val' , ), 190, (190, (), [ (8, 1, None, None) , (19, 1, None, None) , (19, 1, None, None) , 
			 (11, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 784 , (3, 0, None, None) , 0 , )),
	(( 'RT_UpgradeRUStatus' , 'totPercent' , 'statePercent' , 'ret_val' , ), 191, (191, (), [ 
			 (16387, 2, None, None) , (16387, 2, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 788 , (3, 0, None, None) , 0 , )),
	(( 'RT_RestartRU' , 'radioPid' , 'port' , 'physPos' , 'ret_val' , 
			 ), 192, (192, (), [ (8, 1, None, None) , (19, 1, None, None) , (19, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 792 , (3, 0, None, None) , 0 , )),
	(( 'RT_RuSwInfo' , 'port' , 'physPos' , 'SwInfo' , ), 193, (193, (), [ 
			 (19, 1, None, None) , (19, 1, None, None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 796 , (3, 0, None, None) , 0 , )),
	(( 'RT_IsLinkRuUP' , 'port' , 'ret_val' , ), 194, (194, (), [ (19, 1, None, None) , 
			 (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 800 , (3, 0, None, None) , 0 , )),
	(( 'RT_RuHwInfo' , 'port' , 'physPos' , 'HwInfo' , ), 195, (195, (), [ 
			 (19, 1, None, None) , (19, 1, None, None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 804 , (3, 0, None, None) , 0 , )),
	(( 'RT_IsPQ2Enabled' , 'enable' , ), 196, (196, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 808 , (3, 0, None, None) , 0 , )),
	(( 'RT_EnablePq2' , 'enable' , ), 197, (197, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 812 , (3, 0, None, None) , 0 , )),
	(( 'RT_DeleteRuSector' , 'radioPid' , 'port' , 'physPos' , 'ret_val' , 
			 ), 198, (198, (), [ (8, 1, None, None) , (19, 1, None, None) , (19, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 816 , (3, 0, None, None) , 0 , )),
	(( 'DGC_Change_Gain_Gsm_db' , 'rowNr' , 'stateIndex' , 'data' , ), 199, (199, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 820 , (3, 0, None, None) , 0 , )),
	(( 'DGC_Scale_Row_Gsm_db' , 'rowNr' , 'carrier' , 'gain' , ), 200, (200, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (5, 1, None, None) , ], 1 , 1 , 4 , 0 , 824 , (3, 0, None, None) , 0 , )),
	(( 'TPF_Init' , ), 201, (201, (), [ ], 1 , 1 , 4 , 0 , 828 , (3, 0, None, None) , 0 , )),
	(( 'TPF_Exit' , ), 202, (202, (), [ ], 1 , 1 , 4 , 0 , 832 , (3, 0, None, None) , 0 , )),
	(( 'TPF_CreateCOMPort' , 'portNo' , 'radioType' , 'comPort' , 'baudRate' , 
			 'echo' , 'objectID' , ), 203, (203, (), [ (3, 1, None, None) , (3, 1, None, None) , 
			 (8, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 836 , (3, 0, None, None) , 0 , )),
	(( 'TPF_DestroyCOMPort' , 'objectID' , ), 204, (204, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 840 , (3, 0, None, None) , 0 , )),
	(( 'TPF_CreateActiveX' , 'portNo' , 'radioType' , 'objectID' , ), 205, (205, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 844 , (3, 0, None, None) , 0 , )),
	(( 'TPF_DestroyActiveX' , 'objectID' , ), 206, (206, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 848 , (3, 0, None, None) , 0 , )),
	(( 'TPF_GetNrOfRULinks' , 'nrOfLinks' , ), 207, (207, (), [ (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 852 , (3, 0, None, None) , 0 , )),
	(( 'TPF_GetRULinkAt' , 'linkIndex' , 'objectID' , 'status' , 'object_type' , 
			 'rulink' , 'info' , ), 208, (208, (), [ (3, 1, None, None) , (16387, 2, None, None) , 
			 (16387, 2, None, None) , (16392, 2, None, None) , (16392, 2, None, None) , (16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 856 , (3, 0, None, None) , 0 , )),
	(( 'TPF_GetStatus' , 'objectID' , 'ok' , ), 209, (209, (), [ (3, 1, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 860 , (3, 0, None, None) , 0 , )),
	(( 'TPF_StartLog' , ), 210, (210, (), [ ], 1 , 1 , 4 , 0 , 864 , (3, 0, None, None) , 0 , )),
	(( 'TPF_StopLog' , ), 211, (211, (), [ ], 1 , 1 , 4 , 0 , 868 , (3, 0, None, None) , 0 , )),
	(( 'TPF_ActiveX_SendCmd' , 'objectID' , 'freeString' , ), 212, (212, (), [ (3, 1, None, None) , 
			 (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 872 , (3, 0, None, None) , 0 , )),
	(( 'TPF_ActiveX_ReadCmd' , 'objectID' , 'freeString' , 'done' , 'MessageFound' , 
			 ), 213, (213, (), [ (3, 1, None, None) , (16392, 2, None, None) , (16395, 2, None, None) , (16395, 2, None, None) , ], 1 , 1 , 4 , 0 , 876 , (3, 0, None, None) , 0 , )),
	(( 'TPF_ClearAll' , ), 214, (214, (), [ ], 1 , 1 , 4 , 0 , 880 , (3, 0, None, None) , 0 , )),
	(( 'TPF_CreateCOMPort2' , 'portNo' , 'radioType' , 'comPort' , 'baudRate' , 
			 'echo' , 'physicalPosition' , 'ecpAddress' , 'objectID' , ), 215, (215, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 884 , (3, 0, None, None) , 0 , )),
	(( 'TPF_CreateActiveX2' , 'portNo' , 'radioType' , 'physicalPosition' , 'ecpAddress' , 
			 'objectID' , ), 216, (216, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 888 , (3, 0, None, None) , 0 , )),
	(( 'TPF_GetRULinkAt2' , 'linkIndex' , 'objectID' , 'status' , 'object_type' , 
			 'rulink' , 'info' , 'COM_PORT' , 'rate' , 'ECP' , 
			 'PHYS_ADD' , 'port' , 'radioType' , ), 217, (217, (), [ (3, 1, None, None) , 
			 (16387, 2, None, None) , (16387, 2, None, None) , (16392, 2, None, None) , (16392, 2, None, None) , (16392, 2, None, None) , 
			 (16392, 2, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 892 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_ClearAll' , ), 218, (218, (), [ ], 1 , 1 , 4 , 0 , 896 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_AddCpcFile' , 'filename' , 'fileIndex' , ), 219, (219, (), [ (8, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 900 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_UseCpcFile' , 'filename' , ), 220, (220, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 904 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_ClearAllCpcStates' , ), 221, (221, (), [ ], 1 , 1 , 4 , 0 , 908 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_AddCpcState' , 'filename' , 'nrIterations' , ), 222, (222, (), [ (8, 1, None, None) , 
			 (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 912 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_EnableCpcFsm' , 'loopMode' , ), 223, (223, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 916 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_DisableCpcFsm' , ), 224, (224, (), [ ], 1 , 1 , 4 , 0 , 920 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_SendCpcToHw_Cdl2FileIndex' , 'cpcFileIndex' , 'cdl2FileIndex' , ), 225, (225, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 924 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_SendCpcToHw_LoopLenght' , 'cpcFileIndex' , 'loopLength' , ), 226, (226, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 928 , (3, 0, None, None) , 0 , )),
	(( 'CPC_FSM_UseCpcFile_Index' , 'cpcFileIndex' , ), 227, (227, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 932 , (3, 0, None, None) , 0 , )),
	(( 'MultiActivateFileWithCpc' , 'index' , ), 228, (228, (), [ (18, 1, None, None) , ], 1 , 1 , 4 , 0 , 936 , (3, 0, None, None) , 0 , )),
	(( 'MultiDeactivateFileWithCpc' , 'index' , ), 229, (229, (), [ (18, 1, None, None) , ], 1 , 1 , 4 , 0 , 940 , (3, 0, None, None) , 0 , )),
	(( 'RPX_CreateRpxCOMPort' , 'comPort' , 'portNo' , 'radioType' , 'baudRate' , 
			 'physicalPosition' , 'ecpAddress' , 'rpxHdlcAddress' , 'objectID' , ), 230, (230, (), [ 
			 (8, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 944 , (3, 0, None, None) , 0 , )),
	(( 'RPX_DestroyRpxCOMPort' , 'id' , ), 231, (231, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 948 , (3, 0, None, None) , 0 , )),
	(( 'RPX_GetStatus' , 'id' , 'ok' , ), 232, (232, (), [ (3, 1, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 952 , (3, 0, None, None) , 0 , )),
	(( 'CNC_GetPortName' , 'port' , 'portName' , ), 233, (233, (), [ (3, 1, None, None) , 
			 (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 956 , (3, 0, None, None) , 0 , )),
	(( 'RT_IsLinkRuUP2' , 'port' , 'physicalPosition' , 'ret_val' , ), 234, (234, (), [ 
			 (19, 1, None, None) , (19, 1, None, None) , (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 960 , (3, 0, None, None) , 0 , )),
	(( 'CI_SetAdditionalCWHandling' , 'chan' , 'disableProtocolHand' , 'enableVersionHand' , 'enableHdlcBitRateHand' , 
			 'enableEthPtrHand' , ), 235, (235, (), [ (3, 1, None, None) , (11, 1, None, None) , (11, 1, None, None) , 
			 (11, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 964 , (3, 0, None, None) , 0 , )),
	(( 'CI_GetAdditionalCWHandling' , 'chan' , 'disableProtocolHand' , 'enableVersionHand' , 'enableHdlcBitRateHand' , 
			 'enableEthPtrHand' , ), 236, (236, (), [ (3, 1, None, None) , (16395, 2, None, None) , (16395, 2, None, None) , 
			 (16395, 2, None, None) , (16395, 2, None, None) , ], 1 , 1 , 4 , 0 , 968 , (3, 0, None, None) , 0 , )),
	(( 'CI_SetCpriVersion' , 'chan' , 'ver' , ), 237, (237, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 972 , (3, 0, None, None) , 0 , )),
	(( 'CI_GetCpriVersion' , 'chan' , 'ver' , ), 238, (238, (), [ (3, 1, None, None) , 
			 (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 976 , (3, 0, None, None) , 0 , )),
	(( 'CI_SetScramblingSeed' , 'chan' , 'seed' , ), 239, (239, (), [ (3, 1, None, None) , 
			 (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 980 , (3, 0, None, None) , 0 , )),
	(( 'CI_GetScramblingSeed' , 'chan' , 'seed' , ), 240, (240, (), [ (3, 1, None, None) , 
			 (16403, 10, None, None) , ], 1 , 1 , 4 , 0 , 984 , (3, 0, None, None) , 0 , )),
	(( 'CI_DL_LoadFile_Raw' , 'chan' , 'filename' , ), 241, (241, (), [ (3, 1, None, None) , 
			 (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 988 , (3, 0, None, None) , 0 , )),
	(( 'HW_SetConfCpriEl_CT10' , 'chan' , 'swing' , 'precursor' , 'equalizer' , 
			 ), 242, (242, (), [ (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 992 , (3, 0, None, None) , 0 , )),
	(( 'HW_GetConfCpriEl_CT10' , 'chan' , 'swing' , 'precursor' , 'equalizer' , 
			 ), 243, (243, (), [ (3, 1, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 996 , (3, 0, None, None) , 0 , )),
	(( 'TPF_ClearLog' , 'logType' , ), 244, (244, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 1000 , (3, 0, None, None) , 0 , )),
	(( 'TPF_SaveLog' , 'path' , 'logType' , ), 245, (245, (), [ (8, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 1004 , (3, 0, None, None) , 0 , )),
	(( 'DGC_Change_Gain_NonGsm' , 'rowNr' , 'stateIndex' , 'data' , ), 246, (246, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 1008 , (3, 0, None, None) , 0 , )),
	(( 'DGC_Change_Gain_NonGsm_db' , 'rowNr' , 'stateIndex' , 'data' , ), 247, (247, (), [ 
			 (17, 1, None, None) , (17, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 1012 , (3, 0, None, None) , 0 , )),
	(( 'DGC_DeleteAll' , ), 248, (248, (), [ ], 1 , 1 , 4 , 0 , 1016 , (3, 0, None, None) , 0 , )),
	(( 'RPX_CreateRpxCOMPort2' , 'comPort' , 'portNo' , 'radioType' , 'baudRate' , 
			 'physicalPosition' , 'ecpAddress' , 'id' , ), 249, (249, (), [ (8, 1, None, None) , 
			 (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , 
			 (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 1020 , (3, 0, None, None) , 0 , )),
	(( 'RPX_AddDeviceToCOMPort' , 'comPort' , 'rpxUniqueID' , 'rpxHdlcAddress' , 'id' , 
			 ), 250, (250, (), [ (8, 1, None, None) , (8, 1, None, None) , (3, 1, None, None) , (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 1024 , (3, 0, None, None) , 0 , )),
	(( 'RPX_ResetHDLCAddress' , 'rpxHdlcAddress' , 'id' , ), 251, (251, (), [ (3, 1, None, None) , 
			 (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 1028 , (3, 0, None, None) , 0 , )),
	(( 'RPX_GetDeviceLinkStatus' , 'rpxHdlcAddress' , 'id' , 'ok' , ), 252, (252, (), [ 
			 (3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 1032 , (3, 0, None, None) , 0 , )),
]

_Object_vtables_dispatch_ = 1
_Object_vtables_ = [
	(( 'ToString' , 'pRetVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( 'Equals' , 'obj' , 'pRetVal' , ), 1610743809, (1610743809, (), [ (12, 1, None, None) , 
			 (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( 'GetHashCode' , 'pRetVal' , ), 1610743810, (1610743810, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( 'GetType' , 'pRetVal' , ), 1610743811, (1610743811, (), [ (16397, 10, None, "IID('{BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2}')") , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

RecordMap = {
}

CLSIDToClassMap = {
	'{03589009-265A-4FA8-A3F7-081CDB2DD05A}' : IRuControl3,
	'{982D3D23-1181-4907-A770-1DAF6979883F}' : IRuControl4,
	'{75BF5D19-5AB0-4004-97EE-F039164681A3}' : RuControl,
	'{A3676616-2626-430A-ABAE-0186181F5846}' : IRuControl1,
	'{5C05EA90-661D-11D5-A4BC-00608CF21B5F}' : IRuControl2,
	'{65074F7F-63C0-304E-AF0A-D51741CB4A8D}' : _Object,
}
CLSIDToPackageMap = {}
win32com.client.CLSIDToClass.RegisterCLSIDsFromDict( CLSIDToClassMap )
VTablesToPackageMap = {}
VTablesToClassMap = {
	'{03589009-265A-4FA8-A3F7-081CDB2DD05A}' : 'IRuControl3',
	'{65074F7F-63C0-304E-AF0A-D51741CB4A8D}' : '_Object',
	'{982D3D23-1181-4907-A770-1DAF6979883F}' : 'IRuControl4',
	'{5C05EA90-661D-11D5-A4BC-00608CF21B5F}' : 'IRuControl2',
	'{A3676616-2626-430A-ABAE-0186181F5846}' : 'IRuControl1',
}


NamesToIIDMap = {
	'IRuControl4' : '{982D3D23-1181-4907-A770-1DAF6979883F}',
	'_Object' : '{65074F7F-63C0-304E-AF0A-D51741CB4A8D}',
	'IRuControl2' : '{5C05EA90-661D-11D5-A4BC-00608CF21B5F}',
	'IRuControl3' : '{03589009-265A-4FA8-A3F7-081CDB2DD05A}',
	'IRuControl1' : '{A3676616-2626-430A-ABAE-0186181F5846}',
}

win32com.client.constants.__dicts__.append(constants.__dict__)

