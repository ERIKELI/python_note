from sqlalchemy import create_engine, exc, orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Table, ForeignKey, Column
from sqlalchemy.sql.sqltypes import Integer, VARCHAR
from sqlalchemy.dialects.mysql.base import TINYINT
from sqlalchemy.orm import relationship


# declarative_base function will return a class which using active record pattern
# It will combine object opeartion and data operation automatically
Base = declarative_base()

#dsn = 'mssql+pyodbc://ItpReadOnly:@reaedonlyENC@encitp.cn.ao.ericsson.se\itp:0/ITP'
dsn = 'mysql://root:root@localhost/test_db'

# Create table based on Base obj
class AccountStore(Base):
    __tablename__ = 'account_store'
    account_id = Column(Integer, primary_key=True)
    items = Column(VARCHAR(20))
    account = Column(VARCHAR(50))
    password = Column(VARCHAR(50))
print(type(AccountStore))
class SqlalchemyActor():
    def __init__(self, dsn):
        try:
            engine = create_engine(dsn)
        except ImportError:
            raise RuntimeError
        engine.connect()

        # sessionmaker is a factory obj, generate a Session instance, reload __call__ function
        # __call__ function will return a session class each time
        Session = orm.sessionmaker(bind=engine)
        # use Session() to create a class, and assign it to a attribute
        self.session = Session()
        # Assign costom table and engine to attribute
        self.account = AccountStore
        self.engine = engine
        # Bind engine and table
        # Method one: assign manually one by one
        self.account.metadata.bind = engine
        # Method two: use reflect to map all/partly Table schema
        #Base.metadata.reflect(engine)

ses = SqlalchemyActor(dsn)

def printf(items):
    for i in items:
        print(i)
    print(30*'-')

def printfx(items):
    for i in items:
        print(i.__dict__)
    print(30*'-')


# execute(SQL)
re = ses.session.execute('SELECT * FROM account_store')
printf(re)
# query(table_name)
# query function similar to Connection.query / Cursor.execute in SQL adaptor
# query(Account) equal to SELECT * FROM acount_store
re = ses.session.query(AccountStore)
print(type(re), re)
printfx(re)

# query(table_name.column_name)
# SELECT items FROM account_store
re = ses.session.query(AccountStore.items)
print(re)
printf(re)

sq = ses.session.query(AccountStore)

# all() / one() / first(): 
# source code does similar to: lambda x: list(x)
print(type(sq))
print(type(sq.all()))

# filter()
# SELECT * FROM account_store WHERE account_store.items = 'WeChat'
re = sq.filter(AccountStore.items=='WeChat')
print(re)
printfx(re)

# filter_by()
# SELECT * FROM account_store WHERE account_store.items = 'WeChat'
re = sq.filter_by(items='WeChat')
print(re)
printfx(re)

# order_by()
# SELECT * FROM account_store ORDER BY account_store.items
re = sq.order_by(AccountStore.items)
print(re)
printfx(re)

# desc()
# SELECT * FROM account_store ORDER BY account_store.items DESC
re = sq.order_by(AccountStore.items.desc())
print(re)
printfx(re)

# limit(): select next 3 values
# SELECT * FROM account_store LIMIT 3
re = sq.limit(3)
print(re)
printfx(re)

# offset(): select from 3rd values
# SELECT * FROM table_name OFFSET 2
re = sq.offset(2)
print(re)
printfx(re)

# select from 3rd and next 3 values
re = sq.limit(3).offset(2) # equal to re = sq.offset(2).limit(3)
print(re)
printfx(re)
re = sq[2:5] # equal to re = sq.offset(2).limit(3)
print(re)
printfx(re)

# DELETE value
re = sq.filter(AccountStore.account_id==8).delete()
print(re) # 0 fail, 1 success
re = sq.filter(AccountStore.account_id==9).delete()
print(re) # 0 fail, 1 success
re = sq.filter(AccountStore.account_id==10).delete()
print(re) # 0 fail, 1 success

# INSERT value
# Not ORM
ses.session.execute(AccountStore.__table__.insert(), {'items': 'KUBO', 'account': 'STAR', 'password': '1d2345', 'account_id': 10})
# Below code is an expression of SQL
sql = ses.account.__table__.insert([{'items': 'Ericsson', 'account': 'EKELIKE', 'password': '12345', 'account_id': 8}, {'items': 'MOMO', 'account': 'sad', 'password': '145', 'account_id': 9}])
ses.session.execute(sql)

# SELECT value
re = sq.get(9)
print(re)
# Modify value
re.password = 1293

# Count values
re = sq.count()
print(re)

ses.session.commit()
ses.session.close()
