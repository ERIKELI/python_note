from sqlalchemy.sql.schema import Table, Column
from sqlalchemy.sql.sqltypes import Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

# Method one:
class table_one(Base):
    __tablename__ = 'table_one'
    id = Column(Integer, primary_key=True)

# Method two:
class table_two(Base):
    __table__ = Table('table_two', Base.metadata, 
            Column('id', Integer, primary_key=True))

print(type(table_one), type(table_one.id), table_one.id, sep='\n')
print(type(table_two), type(table_two.id), table_two.id, sep='\n')

