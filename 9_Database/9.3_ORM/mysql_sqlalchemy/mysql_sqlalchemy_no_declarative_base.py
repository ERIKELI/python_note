from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Table, Column
from sqlalchemy.sql.sqltypes import Integer, VARCHAR
from sqlalchemy.orm import sessionmaker

# No need to create declarative_base
# Base = declarative_base()

# Set DSN
dsn = 'mysql+mysqldb://root:root@localhost/test_db'

# Create engine, get connection
eng = create_engine(dsn)
cnx = eng.connect()

# No need to create session
# Session = sessionmaker(bind=eng)
# session = Session()

# Generate metadata instance
metadata = MetaData()
# Bind engine to metadata
metadata.bind = eng

# Build up Table
test_tb = Table('test_tb', metadata, 
                Column('id', Integer),
                Column('name', VARCHAR(8))
                )
#print(type(test_tb.insert()))
# Insert value, .rowcount will return the row number affected
print(test_tb.insert().execute(id=6, name='Momo').rowcount)
# Delete value, note: '.c' attr should be added
print(test_tb.delete(test_tb.c.id==6).execute().rowcount)

print(test_tb.insert().execute([{'id': 7, 'name': 'Momo'}, {'id': 8, 'name': 'Kitkat'}]).rowcount)
# Update value, change value where id is 7 to 9
print(test_tb.update(test_tb.c.id==7).execute(id=9).rowcount)

print(test_tb.delete(test_tb.c.id==8).execute().rowcount)
print(test_tb.delete(test_tb.c.id==9).execute().rowcount)

cnx.close()
