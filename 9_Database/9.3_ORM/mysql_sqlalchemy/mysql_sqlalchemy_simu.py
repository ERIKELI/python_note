from sqlalchemy import create_engine, exc, orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Table, ForeignKey, Column
from sqlalchemy.sql.sqltypes import Integer, VARCHAR
from sqlalchemy.dialects.mysql.base import TINYINT
from sqlalchemy.orm import relationship


# declarative_base function will return a class which using active record pattern
# It will combine object opeartion and data operation automatically
Base = declarative_base()

# This is rel table
t_PersonAccount_Rel = Table('personaccount_rel', 
                            Base.metadata,
                            Column('name', VARCHAR(8), ForeignKey('person_info.name')),
                            Column('account_id', Integer, ForeignKey('account_store.account_id')))

# Create table based on Base obj
class PersonInfo(Base):
    __table__ = Table('person_info',
                        Base.metadata,
                        Column('id', TINYINT(4)),
                        Column('age', Integer),
                        Column('name', VARCHAR(8), primary_key=True))

    # Need to search via person --> account
    # So build up a relationship between person and account
    # relationship(class_name, class, class_name)
    AccountStore = relationship('AccountStore',
                                secondary=t_PersonAccount_Rel,
                                backref='PersonInfo')

class AccountStore(Base):
    __table__ = Table('account_store',
                        Base.metadata,
                        Column('account_id', Integer, primary_key=True),
                        Column('items', VARCHAR(20)),
                        Column('account', VARCHAR(50)),
                        Column('password', VARCHAR(50)))

    def __repr__(self):
        return 'Items: %s\nAccount: %s\nPassword: %s' % (self.items, self.account, self.password)

class SqlalchemyActor():
    def __init__(self, dsn):
        try:
            engine = create_engine(dsn, echo=False, max_overflow=5, encoding='utf-8')
        except ImportError:
            raise RuntimeError
        engine.connect()

        # sessionmaker is a factory obj, generate a Session instance, reload __call__ function
        # __call__ function will return a session class each time
        Session = orm.sessionmaker(bind=engine)
        # use Session() to create a class, and assign it to an attribute
        self.session = Session()
        # Assign costom table and engine to attribute
        self.account = AccountStore.__table__
        self.engine = engine
        # Bind engine and table
        # Method one: assign manually one by one
        self.account.metadata.bind = engine
        # Method two: use reflect to map all/partly Table schema
        #Base.metadata.reflect(engine)

class PersonInfoCriteria():
    """
    This is the criteria for PersonInfo
    Replace None with input value
    """
    def __init__(self, **kwargs):
        self.id = None
        self.age = None
        self.name = None
        self.result = None

        for field, argument in kwargs.items():
            if str(field) == 'id':
                self.id = argument
            if str(field) == 'age':
                self.age = argument
            if str(field) == 'name':
                self.name = argument

class PersonInfoService():
    """
    This is the service for PersonInfo
    Generate condition and filter out expression for filter(SQL) according to criteria value
    """

    # This function to build criteria(expression/clause) for filter(SQL)
    # Note: PersonInfo is based on declarative_base, 
    # so PersonInfo.attr == value is an condition expression(clause) for sqlalchemy function
    # also PersonInfo.attr.like(value) too, like function equal to "%" in SQL
    # finally return the list of clauses
    @staticmethod
    def _criteria_builder(person_info_criteria):
        clauses = []
        if person_info_criteria.id:
            clauses.append(PersonInfo.id == person_info_criteria.id)
        if person_info_criteria.age:
            clauses.append(PersonInfo.age == person_info_criteria.age)
        if person_info_criteria.name:
            if '%' in person_info_criteria.name:
                clauses.append(PersonInfo.name.like(person_info_criteria.name))
            else:
                clauses.append(PersonInfo.name == person_info_criteria.name)
        return clauses

    @staticmethod
    def find(person_info_criteria, session):
        # Build clauses for session filter
        clauses = PersonInfoService._criteria_builder(person_info_criteria)
        # Query PersonInfo and filter according to clauses, use all() function to return as list
        person_info_criteria.result = session.query(PersonInfo).filter(*clauses).all()
        return person_info_criteria.result

class AccountStoreCriteria():
    def __init__(self, **kwargs):
        self.items = None
        self.account = None
        self.password = None
        self.account_id = None
        self.person_info = None
        self.result = None

        for field, argument in kwargs.items():
            if field == 'items':
                self.items = argument
            if field == 'account':
                self.account = argument
            if field == 'password':
                self.password = argument
            if field == 'account_id':
                self.account_id = argument
            if field == 'person_info':
                self.person_info = argument

class AccountStoreService():
    
    @staticmethod
    def _criteria_builder(account_store_criteria):
        clauses = []
        if account_store_criteria.items:
            clauses.append(AccountStore.items == account_store_criteria.items)
        if account_store_criteria.account:
            if '%' in account_store_criteria.account:
                clauses.append(AccountStore.account.like(account_store_criteria.account))
            else:
                clauses.append(AccountStore.account == account_store_criteria.account)
        if account_store_criteria.password:
            clauses.append(AccountStore.password == account_store_criteria.password)
        if account_store_criteria.account_id:
            clauses.append(AccountStore.accout_id == account_store_criteria.account_id)

        # person_info from PersonInfoService filter 
        # Note: pnif.AccountStore is an instrumentedList type obj
        # sqlalchemy use instrumentedList to simulate one-to-many and many-to-many relationships
        # sqlalchemy does not support in_ many to many relationships yet
        # in_() function to filter out account id in range
        # SQL: SELECT * FROM account_store WHERE account_store.account_id in (...)
        if account_store_criteria.person_info:
            account_ids = []
            for pnif in account_store_criteria.person_info:
                for acid in pnif.AccountStore:
                    account_ids.append(acid.account_id)
            clauses.append(AccountStore.account_id.in_(account_ids))

        return clauses

    @staticmethod
    def find(account_store_criteria, session):
        clauses = AccountStoreService._criteria_builder(account_store_criteria)
        account_store_criteria.result = session.query(AccountStore).filter(*clauses).all()
        return account_store_criteria.result

if __name__ == '__main__':
    #dsn = 'mssql+pyodbc://ItpReadOnly:@reaedonlyENC@encitp.cn.ao.ericsson.se\itp:0/ITP'
    dsn = 'mysql+mysqldb://root:root@localhost/test_db'
    ses = SqlalchemyActor(dsn)
    session = ses.session

    # Filter out the person information according to id and age
    id, age = 2, 7
    clauses = PersonInfoCriteria(id=id, age=age)
    # re is an obj list of PersonInfo, use obj.attr to fetch value
    person_info = PersonInfoService.find(clauses, session)
    name = person_info[0].name
    print('Filter out user: %s' % name)

    # Filter out the account id according to name via relation table
    items = ['WeChat', 'Qq']
    for it in items:
        clauses = AccountStoreCriteria(items=it, person_info=person_info)
        account_info = AccountStoreService.find(clauses, session)
        for ac in account_info:
            print(30*'-'+'\n%s' % name)
            print(ac)

