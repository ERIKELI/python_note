from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Table, Column
from sqlalchemy.sql.sqltypes import Integer, VARCHAR
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

# Set DSN
dsn = 'mysql+mysqldb://root:root@localhost/test_db'

# Create engine, get connection
eng = create_engine(dsn)
cnx = eng.connect()

# Make a Session class binded with engine
Session = sessionmaker(bind=eng)
# Generate a session instance
session = Session()

# Build up Table class
class Test_tb(Base):
    __table__ = Table('test_tb', Base.metadata, 
                        Column('id', Integer, primary_key=True),
                        Column('name', VARCHAR(8))
                        )


# Insert value
# session.add_all([Test_tb(id=6, name='Momo')])
session.add(Test_tb(id=6, name='Momo'))
session.commit()

# Delete value
session.query(Test_tb).filter(Test_tb.id==6).delete()
session.commit()

session.add_all([Test_tb(id=7, name='Momo'), Test_tb(id=8, name='Kitkat')])
session.commit()
# Update value
fr = session.query(Test_tb).filter_by(id=7).all()
for f in fr:
    f.id = 9
session.query(Test_tb).filter(Test_tb.id==8).delete()
session.query(Test_tb).filter(Test_tb.id==9).delete()
session.commit()

re = session.query(Test_tb).all()
# Show value: Each r is a row in table
for r in re:
    print(r.__dict__)

cnx.close()

