from sqlalchemy import Column, String, create_engine, MetaData, Table, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = None
# Create base class
Base = declarative_base(engine)
# Create a database connection
# SQL connect method: mssql+pyodbc
# Username: ItpReadOnly
# Password: @Oneweeklive
# Database connection address: itp_db.kl.sw.ericsson.se
# Port: 1433
# Database name: ITP

def initDatabase(connection):
    # Create instance and connect to data base
    engine = create_engine(connection, echo=True, max_overflow=5,
                            encoding='utf-8', connect_args={'timeout': 2})
    # reflect can map all/partly Table schema
    Base.metadata.reflect(engine)
    # Bind instance and engine, return a class
    Session_class = sessionmaker(bind=engine)
    # Generate session instance
    session = Session_class()
    return session

if __name__ == '__main__':
    connection = 'mssql+pyodbc://ItpReadOnly:@readonlyENC@encitp.cn.ao.ericsson.se\itp:0/ITP'
    #connection = 'mssql+pyodbc://ItpReadOnly:@Oneweeklive@itp_db.kl.sw.ericsson.se:1433/ITP'
    initDatabase(connection)
