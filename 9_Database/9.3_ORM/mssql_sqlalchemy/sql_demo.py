import sql_base
from sql_test_cases import *
from sql_test_files import *


# For version 0.9.9 sqlalchemy, no need to specify driver, while after 1.0, driver specification is necessary
# connection = 'mssql+pymssql://ItpReadOnly:@readonlyENC@encitp.cn.ao.ericsson.se\itp:0/ITP'
connection = 'mssql+pyodbc://ItpReadOnly:@readonlyENC@encitp.cn.ao.ericsson.se\itp:0/ITP?Driver=SQL Server'
session = sql_base.initDatabase(connection)
product_number = 'ROA 128 6386/1'
product_rstate = 'R1D'
#product_number = 'KRC 161 490/2'
#product_rstate = 'R1C'

# test_cases_criteria is the instance of TestCasesCriteria obj
test_cases_criteria = TestCasesCriteria(product_number=product_number, revision=product_rstate, test_type='Prod', deleted=False)
# find function return a list that contains the test_cases obj
fetched_test_cases = TestCasesService.find(test_cases_criteria, session)

file = 'AUBOOT'
# file = 'SECONDBOOT'
# ["SECONDBOOT", "ETSW SYS", "ETSW APP", "PA DB", "BOARD PARAMETERS", "PROD DB", "_ETSW UBOOT ENV_", "TRXCTRL"]
test_files_criteria = TestFilesCriteria(description=None, test_cases=fetched_test_cases)
fetched_test_files = TestFilesService.find(test_files_criteria, session)
if fetched_test_files:
    for f in fetched_test_files:
        print('Filter out file id:', f.TestFileId, 'descirption:', f.Description)

# Extract file
# fetched_test_files[0].extract(destination_path='C:/Users/EKELIKE/Desktop/sqlalchemy/file_destination/', file_share=r'\\encitp.cn.ao.ericsson.se\server$\Release\Module\Radio 44xx')
