from sqlalchemy.sql.schema import Table, ForeignKey, Column
from sqlalchemy.sql.sqltypes import Integer, Unicode, DateTime
from sqlalchemy.orm import relationship
from sql_base import Base
import zipfile

# Association table
t_V_TestCaseTestFiles_Rel = Table(
        'V_TestCaseTestFiles_Rel', Base.metadata,
        Column('TestCaseId', Integer, ForeignKey('V_TestCases.TestCaseId')),
        Column('TestFileId', Integer, ForeignKey('V_TestFiles.TestFileId')),
        Column('PositionNumber', Integer, nullable=False),
        Column('LayoutFileId', Integer))

class TestFileTypes(Base):
    """TestFileTypes table class"""
    __table__ = Table('V_TestFileTypes',
                    Base.metadata,
                    Column('TestFileTypeId', Integer, nullable=False),
                    Column('Name', Unicode(30), primary_key=True),
                    Column('Description', Unicode(40))
                    )

class TestCases(Base):
    """TestCases table class"""
    __table__ = Table('V_TestCases',
                    Base.metadata,
                    Column('TestCaseId', Integer, primary_key=True),
                    Column('Name', Unicode(328), nullable=False),
                    Column('Description', Unicode(40), nullable=False),
                    Column('ProductNumber', Unicode(28), nullable=False),
                    Column('Revision', Unicode(7), nullable=False),
                    Column('TestType', Unicode(16), nullable=False),
                    Column('ProcessStepId', Integer, nullable=False),
                    Column('ConfigFilter', Unicode(255)),
                    Column('TagId', Integer),
                    Column('Modified', DateTime, nullable=False),
                    Column('ModifierId', Integer, nullable=False),
                    Column('FamilyId', Integer, nullable=False),
                    Column('Deleted', Integer, nullable=False)
                    )
    # Build a relationship between TestCases and TestFiles
    TestFiles = relationship('TestFiles',
                    secondary=t_V_TestCaseTestFiles_Rel,
                    backref='TestCases')
class TestFiles(Base):
    """TestFiles table class"""
    __table__ = Table('V_TestFiles',
                    Base.metadata,
                    Column('TestFileId', Integer, primary_key=True),
                    Column('TestFileTypeId', Integer, nullable=False),
                    Column('ProductNumber', Unicode(28), nullable=False),
                    Column('Revision', Unicode(7), nullable=False),
                    Column('Description', Unicode(40), nullable=False),
                    Column('EntryPoint', Unicode(128), nullable=False),
                    Column('TagId', Integer),
                    Column('Modified', DateTime, nullable=False),
                    Column('ModifierId', Integer, nullable=False),
                    Column('FamilyId', Integer, nullable=False),
                    Column('Deleted', Integer, nullable=False)
                    )

    def extract(self, destination_path, file_share):
        """Extract files from shared network location"""
        my_zip = (file_share + '/' + str(self.TestFileId) + '.zip')
        zip_file = zipfile.ZipFile(my_zip, 'r')

        for files in zip_file.namelist():
            zip_file.extract(files, destination_path + str(self.TestFileId))
        zip_file.close()

if __name__=='__main__':
    print(TestCases.Name=='C')
    print(type(TestCases.Name=='C'))
    #print(TestCases.Description=='CC')
    # TestCases.Description is the InstrumentedAttribute
   # print(type(TestCases.Description))