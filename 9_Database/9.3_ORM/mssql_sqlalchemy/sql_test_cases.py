from sql_model import TestCases 

class TestCasesCriteria():
    """TestCasesCriteria class"""

    def __init__(self,**kwargs):
        self.id = None
        self.name = None
        self.description = None
        self.product_number = None
        self.revision = None
        self.test_type = None
        self.modified_from = None
        self.modified_to = None
        self.deleted = None
        self.result = None

        for flag, argument in kwargs.items():
            if str(flag) == 'id':
                self.id = argument
            if str(flag) == 'name':
                self.name = argument
            if str(flag) == 'description':
                self.description = argument
            if str(flag) == 'product_number':
                self.product_number = argument
            if str(flag) == 'revision':
                self.revision = argument
            if str(flag) == 'test_type':
                self.test_type = argument
            if str(flag) == 'modified_from':
                self.modified_from = argument
            if str(flag) == 'modified_to':
                self.modified_to = argument
            if str(flag) == 'deleted':
                self.deleted = argument

class TestCasesService():
    """TestCasesService class"""

    @staticmethod
    def _criteria_builder(test_cases_criteria):
        clauses = []
        # Id
        if test_cases_criteria.id:
            clauses.append(TestCases.TestCaseId == test_cases_criteria.id)
        # CriteriaName
        if test_cases_criteria.name:
            if '%' in test_cases_criteria.name:
                clauses.append(TestCases.Name.like(test_cases_criteria.name))
            else:
                clauses.append(TestCases.Name == test_cases_criteria.name)
        # Description
        if test_cases_criteria.description:
            if '%' in test_cases_criteria.description:
                clauses.append(TestCases.Description.like(test_cases_criteria.description))
            else:
                clauses.append(TestCases.Description == test_cases_criteria.description)
        # Product Number
        if test_cases_criteria.product_number:
            if '%' in test_cases_criteria.product_number:
                clauses.append(TestCases.ProductNumber.like(test_cases_criteria.product_number))
            else:
                clauses.append(TestCases.ProductNumber == test_cases_criteria.product_number)
        # Revision
        if test_cases_criteria.revision:
            clauses.append(TestCases.Revision == test_cases_criteria.revision)
        # TestType
        if test_cases_criteria.test_type:
            clauses.append(TestCases.TestType == test_cases_criteria.test_type)
        # Modified from
        if test_cases_criteria.modified_from:
            clauses.append(TestCases.Modified >= test_cases_criteria.modified_from)
        # Modified to
        if test_cases_criteria.modified_to:
            clauses.append(TestCases.Modified <= test_cases_criteria.modified_to)
        # Deleted
        if test_cases_criteria.deleted is True:
            clauses.append(TestCases.Deleted == True)
        elif test_cases_criteria.deleted is False:
            clauses.append(TestCases.Deleted == False)
        return clauses

    # test_cases_criteria.result is a list which contains the test cases fetched
    @staticmethod
    def find(test_cases_criteria, session):
        clauses = TestCasesService._criteria_builder(test_cases_criteria)
        # test_cases_criteria.result is a list contains the obj after filter
        # .all() will return a list
        test_cases_criteria.result = session.query(TestCases).filter(*clauses).all()
        return test_cases_criteria.result

    @staticmethod
    def count(test_cases_criteria, session):
        clauses = TestCasesService._criteria_builder(test_cases_criteria)
        test_cases_criteria.result = session.query(TestCases).filter(*clauses).count()
        return test_cases_criteria.result
        

