from sql_model import TestFiles 

class TestFilesCriteria():
    """TestFilesCriteria class"""

    def __init__(self,**kwargs):
        self.id = None
        self.type_id = None
        self.description = None
        self.product_number = None
        self.revision = None
        self.entry_point = None
        self.tag_id = None
        self.modified_from = None
        self.modified_to = None
        self.modified_id = None
        self.family_id = None
        self.deleted = None
        self.test_cases = None
        self.result = None

        for flag, argument in kwargs.items():
            if str(flag) == 'id':
                self.id = argument
            if str(flag) == 'type_id':
                self.name = argument
            if str(flag) == 'description':
                self.description = argument
            if str(flag) == 'product_number':
                self.product_number = argument
            if str(flag) == 'revision':
                self.revision = argument
            if str(flag) == 'entry_point':
                self.entry_point = argument
            if str(flag) == 'tag_id':
                self.tag_id= argument
            if str(flag) == 'modified_from':
                self.modified_from = argument
            if str(flag) == 'modified_to':
                self.modified_to = argument
            if str(flag) == 'modified_id':
                self.modified_id = argument
            if str(flag) == 'family_id':
                self.family_id = argument
            if str(flag) == 'deleted':
                self.deleted = argument
            if str(flag) == 'test_cases':
                self.test_cases = argument
            if str(flag) == 'result':
                self.result = argument

class TestFilesService():
    """This is the service for Port object"""

    @staticmethod
    def _criteria_builder(test_files_criteria):
        clauses = []
        # Id
        if test_files_criteria.id:
            clauses.append(TestFiles.TestFileId == test_files_criteria.id)
        # TestFileTypeId
        if test_files_criteria.type_id:
            clauses.append(TestFiles.TestFileTypeId == test_files_criteria.type_id) 
        # Product Number
        if test_files_criteria.product_number:
            if '%' in test_files_criteria.product_number:
                clauses.append(TestFiles.ProductNumber.like(test_files_criteria.product_number))
            else:
                clauses.append(TestFiles.ProductNumber == test_files_criteria.product_number)
        # Revision
        if test_files_criteria.revision:
            clauses.append(TestFiles.Revision == test_files_criteria.revision)
        # Description
        if test_files_criteria.description:
            if '%' in test_files_criteria.description:
                clauses.append(TestFiles.Description.like(test_files_criteria.description))
            else:
                clauses.append(TestFiles.Description == test_files_criteria.description)
        # EntryPoint
        if test_files_criteria.entry_point:
            clauses.append(TestFiles.EntryPoint == test_files_criteria.entry_point)
        # TagId
        if test_files_criteria.tag_id:
            clauses.append(TestFiles.TagId == test_files_criteria.tag_id)
        # Modified from
        if test_files_criteria.modified_from:
            clauses.append(TestFiles.Modified >= test_files_criteria.modified_from)
        # Modified from
        if test_files_criteria.modified_to:
            clauses.append(TestFiles.Modified <= test_files_criteria.modified_to)
        # ModifierId
        if test_files_criteria.modified_id:
            clauses.append(TestFiles.ModifierId == test_files_criteria.modifier_id)
        # FamilyId
        if test_files_criteria.family_id:
            clauses.append(TestFiles.FamilyId == test_files_criteria.family_id)

        # TestCases
        if test_files_criteria.test_cases:
            # TODO: Since SQLAlchemy does not support in_ many to many relationships yet,
            # we have to make an ugly solution.            
            test_files_ids = set()
            for tc in test_files_criteria.test_cases:
                for tf in tc.TestFiles:
                    test_files_ids.add(tf.TestFileId)
            clauses.append(TestFiles.TestFileId.in_(list(test_files_ids)))
        
        # Deleted
        if test_files_criteria.deleted is True:
            clauses.append(TestFiles.Deleted == True)
        elif test_files_criteria.deleted is False:
            clauses.append(TestFiles.Deleted == False)
            
        #if test_files_criteria.deleted:
        #    clauses.append(TestFiles.Deleted == test_files_criteria.deleted)  # @UndefinedVariable    
        return clauses
    
    @staticmethod
    def find(test_files_criteria, session):
        clauses = TestFilesService._criteria_builder(test_files_criteria)
        test_files_criteria.result = session.query(TestFiles).filter(*clauses).all()
        return test_files_criteria.result
    
    @staticmethod
    def count(test_files_criteria, session):
        clauses = TestFilesService._criteria_builder(test_files_criteria)
        test_files_criteria.result = session.query(TestFiles).filter(*clauses).count()
        return test_files_criteria.result

