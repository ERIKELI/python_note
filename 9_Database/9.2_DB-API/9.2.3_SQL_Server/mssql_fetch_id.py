import pymssql

def execute(cnx, sql, *args):
    cur = cnx.cursor()
    cur.execute(sql, *args)
    re = cur.fetchall()
    cnx.commit()
    cur.close()
    [print(x) for x in re]
    return re

cnx = pymssql.connect(host='encitp.cn.ao.ericsson.se\itp', user='ItpReadOnly', password='@readonlyENC', database='ITP', charset='UTF-8')

# Step 1: filter out the info according to 'ProductNumber', 'Revision', 'TestType', 'Deleted'
# Below code by self
re = execute(cnx, "SELECT * FROM V_TestCases AS tc WHERE tc.[ProductNumber] = '%s' AND tc.[Revision] = '%s' AND tc.[TestType] = '%s' AND tc.[Deleted] = 0" % ('ROA 128 6386/1', 'R1E', 'Prod'))
# Below code from sqlalchemy
# execute(cnx, "SELECT [V_TestCases].[TestCaseId] AS [V_TestCases_TestCaseId], [V_TestCases].[Name] AS [V_TestCases_Name], [V_TestCases].[Description] AS [V_TestCases_Description], [V_TestCases].[ProductNumber] AS [V_TestCases_ProductNumber], [V_TestCases].[Revision] AS [V_TestCases_Revision], [V_TestCases].[TestType] AS [V_TestCases_TestType], [V_TestCases].[ProcessStepId] AS [V_TestCases_ProcessStepId], [V_TestCases].[ConfigFilter] AS [V_TestCases_ConfigFilter], [V_TestCases].[TagId] AS [V_TestCases_TagId], [V_TestCases].[Modified] AS [V_TestCases_Modified], [V_TestCases].[ModifierId] AS [V_TestCases_ModifierId], [V_TestCases].[FamilyId] AS [V_TestCases_FamilyId], [V_TestCases].[Deleted] AS [V_TestCases_Deleted] FROM [V_TestCases] WHERE [V_TestCases].[ProductNumber] = %s AND [V_TestCases].[Revision] = %s AND [V_TestCases].[TestType] = %s AND [V_TestCases].[Deleted] = 0", ('KRC 161 697/1', 'R1B', 'Prod'))

# Step 2: use TestCaseId to search TestFileId via relation table
# And COMBINE contents filtered out from TestFiles and TestCaseTestFiles
# Below code by self
re = execute(cnx, "SELECT * FROM V_TestFiles AS v_tf, V_TestCaseTestFiles_Rel AS v_tctf WHERE v_tctf.[TestCaseId]=%d AND v_tf.[TestFileId] = v_tctf.[TestFileId]" % re[0][0])
# Below code from sqlalchemy
# execute(cur, "SELECT [V_TestFiles].[TestFileId] AS [V_TestFiles_TestFileId], [V_TestFiles].[TestFileTypeId] AS [V_TestFiles_TestFileTypeId], [V_TestFiles].[ProductNumber] AS [V_TestFiles_ProductNumber], [V_TestFiles].[Revision] AS [V_TestFiles_Revision], [V_TestFiles].[Description] AS [V_TestFiles_Description], [V_TestFiles].[EntryPoint] AS [V_TestFiles_EntryPoint], [V_TestFiles].[TagId] AS [V_TestFiles_TagId], [V_TestFiles].[Modified] AS [V_TestFiles_Modified], [V_TestFiles].[ModifierId] AS [V_TestFiles_ModifierId], [V_TestFiles].[FamilyId] AS [V_TestFiles_FamilyId], [V_TestFiles].[Deleted] AS [V_TestFiles_Deleted] FROM [V_TestFiles], [V_TestCaseTestFiles_Rel] WHERE [V_TestCaseTestFiles_Rel].[TestCaseId] = %d AND [V_TestFiles].[TestFileId] = [V_TestCaseTestFiles_Rel].[TestFileId]", (23701, ))
print('-'*30)
print(re)
# Step 3: filter out TestFileId(TestCaseId --> multi-TestFileId) from multi-TestFileId according to Description
files = 'LOAD MODULES'
re = execute(cnx, "SELECT * FROM V_TestFiles AS v_tf WHERE v_tf.[Description]='%s' AND v_tf.[TestFileId] IN (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" % tuple([files]+[x[0] for x in re]))
# execute(cur, "SELECT [V_TestFiles].[TestFileId] AS [V_TestFiles_TestFileId], [V_TestFiles].[TestFileTypeId] AS [V_TestFiles_TestFileTypeId], [V_TestFiles].[ProductNumber] AS [V_TestFiles_ProductNumber], [V_TestFiles].[Revision] AS [V_TestFiles_Revision], [V_TestFiles].[Description] AS [V_TestFiles_Description], [V_TestFiles].[EntryPoint] AS [V_TestFiles_EntryPoint], [V_TestFiles].[TagId] AS [V_TestFiles_TagId], [V_TestFiles].[Modified] AS [V_TestFiles_Modified], [V_TestFiles].[ModifierId] AS [V_TestFiles_ModifierId], [V_TestFiles].[FamilyId] AS [V_TestFiles_FamilyId], [V_TestFiles].[Deleted] AS [V_TestFiles_Deleted] FROM [V_TestFiles] WHERE [V_TestFiles].[Description] = %s AND [V_TestFiles].[TestFileId] IN (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", ('LOAD MODULES', 33506, 33507, 23588, 33508, 36515, 36518, 23592, 31048, 33194, 33501, 23597, 36526, 36527, 33489, 36529, 36696, 36695, 35128, 23677, 33502, 28479))
print('TestFileId need to be fetched:', re[0][0])

cnx.close()
