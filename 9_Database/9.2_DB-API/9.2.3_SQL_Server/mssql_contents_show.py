import pymssql as ms
def execute(cur, sql, *args):
    cur.execute(sql, *args)
    re = cur.fetchall()
    return re

def show():
    cnx = ms.connect(host='encitp.cn.ao.ericsson.se\itp', user='ItpReadOnly', password='@readonlyENC', database='ITP', charset='UTF-8')
    
    cur = cnx.cursor()
    
    print('{:-^30}'.format('DATABASES'))
    # Show the database in sys
    re = execute(cur, "SELECT * FROM sys.databases")
    
    # Show custom databases
    # re = execute(cur, "SELECT * FROM sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb')")
    for db in re:
        try:
            # Select databases
            cur.execute("USE %s" % db[0])
        except:
            continue
        # Show tables
        re = execute(cur, "SELECT * FROM information_schema.TABLES")
    
        for i in re:
            print(i)
    
    cur.execute('USE ITP')
    print('{:-^30}'.format('TABLES'))
    re = execute(cur, "SELECT * FROM information_schema.TABLES")
    for tb in re:
        print('\n{:-^30}'.format('TABLE CONTENTS'))
        print(tb)
        print('{:-^30}'.format('%s' % tb[2]))
        re = execute(cur, "SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME='%s'" % tb[2])
        for cl in re:
            print(cl)

def foo(gen):
    [print(x, end='\n\n') for x in gen]

if __name__=='__main__':
    show()
