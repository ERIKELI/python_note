import pyodbc as odbc

def connect(**kwargs):
    cnx = odbc.connect(**kwargs)
    cur = cnx.cursor()
    sql='SELECT * FROM information_schema.TABLES'
    cur.execute(sql)
    re = cur.fetchall()
    print(re)
    cur.close()
    cnx.close()

connect(host='encitp.cn.ao.ericsson.se\itp', user='ItpReadOnly', password='@readonlyENC', database='ITP', charset='UTF-8', driver='SQL Server')

# connect(host='localhost', user='root', password='root', charset='UTF-8', driver='MySQL ODBC 5.3 Driver')
