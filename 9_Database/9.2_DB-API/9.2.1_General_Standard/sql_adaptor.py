
class SqlConnector():
    def __init__(self, adaptor):
        self.adaptor = adaptor
        self.result = None
        # Check Adaptor info
        print('Adaptor %s apply DB-API %s, thread safety: %d, parameter style: %s' % (adaptor.__name__, adaptor.apilevel, adaptor.threadsafety, adaptor.paramstyle))

    # Login by user name and password
    def login(self, **kwargs):
        # Create a connection obj
        self.cnx = self.adaptor.connect(**kwargs)

    def logoff(self):
        self.cnx.close()

    def query_sql(self, sql, show=True):
        # Method one: Use Connection
        '''
        self.cnx.query(sql)
        self.result = self.cnx.store_result()
        r = self.result.fetch_row(0, )
        self.cnx.commit()
        '''
        # Method two: Use Cursor
        cur = self.cnx.cursor()
        cur.execute(sql)
        r = cur.fetchall()
        self.cnx.commit()
        cur.close()

        if show:
            splt = '\n' + ((len(sql)+6)*'-')
            msg = ''.join(('\n{: ^10} | ').format(str(i)) if x.index(i) == 0 else ('{: ^10} | ').format(str(i)) for x in r for i in x)
            s = ('{:-^%d}'%(len(sql)+6)).format(sql) + msg + splt
            print(s)
        return (i for x in r for i in x)

    def exec_sql(self, sql):
        cur = self.cnx.cursor()
        cur.execute(sql)
        self.cnx.commit()
        cur.close()

    def commit(self):
        self.cnx.commit()

if __name__ == '__main__':
    import MySQLdb
    import pymssql
    r = input('Please choose your adaptor:\n(M)MySQL\n(S)MsSQL\n')
    adaptor_list = {'M': MySQLdb, 'S': pymssql}
    c = SqlConnector(adaptor_list[r.upper()])
    # No local MSSQL server
    c.login(host='localhost', user='root', password='root')
    c.logoff()
