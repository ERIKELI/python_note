
class SqlConnector():
    def __init__(self, adaptor):
        self.adaptor = adaptor
        self.result = None
        # Check Adaptor info
        print('Adaptor %s apply DB-API %s, thread safety: %d, parameter style: %s' % (adaptor.__name__, adaptor.apilevel, adaptor.threadsafety, adaptor.paramstyle))

    # Login by user name and password
    def login(self, host='localhost', user='root', password='root', **kwargs):
        # Create a connection obj
        self.cnx = self.adaptor.connect(host=host, user=user, password=password, **kwargs)

    def logoff(self):
        self.cnx.close()

    def query_sql(self, sql, show=True):
        # Method one: Use Connection
        '''
        self.cnx.query(sql)
        self.result = self.cnx.store_result()
        self.cnx.commit()
        r = self.result.fetch_row(0, )
        '''
        # Method two: Use Cursor
        cur = self.cnx.cursor()
        cur.execute(sql)
        self.cnx.commit()
        r = cur.fetchall()
        cur.close()

        if show:
            splt = '\n' + ((len(sql)+6)*'-')
            msg = ''.join(('\n{: ^10} | ').format(str(i)) if x.index(i) == 0 else ('{: ^10} | ').format(str(i)) for x in r for i in x)
            s = ('{:-^%d}'%(len(sql)+6)).format(sql) + msg + splt
            print(s)
        return (i for x in r for i in x)

    def exec_sql(self, sql):
        cur = self.cnx.cursor()
        cur.execute(sql)
        self.cnx.commit()
        cur.close()

    def create_try(self, sql_create, sql_drop, name):
        try:
            self.exec_sql(sql_create)
            print('%s create success' % name)
        # MySQLdb.ProgrammingError or MySQLdb.OperationalError
        except:
            r = input('%s may exist, delete and create a new one?[y/n]' % name)
            if r == 'n':
                print('Failed to create %s' % name)
                return False
            try:
                self.exec_sql(sql_drop)
                print('%s delete success' % name)
                self.exec_sql(sql_create)
                print('%s create success' % name)
            except:
                print('Failed to create %s' % name)
                return False
        return True

    # --------------- For user handle -------------------
    def create_user(self, usn, psw):
        sql = 'CREATE USER %s IDENTIFIED BY "%s";' % (usn, psw)
        try:
            self.exec_sql(sql)
        except MySQLdb.OperationalError:
            print('Create user %s failed' % usn)
            return False
        return True

    def show_grants(self, usn):
        sql = 'SHOW GRANTS FOR %s;' % usn
        # re = self.query_sql(sql)
        self.query_sql(sql)

    def grant_for(self, user):
        pass

    # --------------- For database handle -------------------
    def create_database(self, db_name):
        # Try to CREATE a database, DELETE database if create failed 
        # And try CREATE again, if failed again, exit
        # You can try 'CREATE DATABASE IF NOT EXISTS db_name;' too
        sql_create = 'CREATE DATABASE %s;' % db_name
        sql_drop = 'DROP DATABASE %s;' % db_name 
        return self.create_try(sql_create, sql_drop, db_name)

    def drop_database(self, db_name):
        sql = 'DROP DATABASE IF EXISTS %s;' % db_name
        self.exec_sql(sql)

    def use_database(self, db_name):
        sql = 'USE %s;' % db_name
        self.exec_sql(sql)

    def show_databases(self):
        sql = 'SHOW DATABASES;'
        re = self.query_sql(sql)
        # print(list(re))

    # --------------- For table handle -------------------
    def create_table(self, tb_name, tb_info, db_name=None):
        if db_name:
            self.use_database(db_name)
        tb_info = (', '.join('%s %s' % (k, v) for k, v in tb_info.items())).join('()')
        sql_create = 'CREATE TABLE %s %s;' % (tb_name, tb_info)
        sql_drop = 'DROP TABLE %s;' % tb_name
        return self.create_try(sql_create, sql_drop, tb_name)

    def drop_table(self, tb_name, db_name=None):
        if db_name:
            self.use_database(db_name)
        sql = 'DROP TABLE IF EXISTS %s;' % tb_name
        self.exec_sql(sql)

    #def use_table(self, tb_name, db_name=None):
    #    if db_name:
    #        self.use_database(db_name)
    #    sql = 'USE %s;' % tb_name
    #    self.exec_sql(sql)

    def show_tables(self, db_name):
        self.use_database(db_name)
        sql = 'SHOW TABLES;'
        self.query_sql(sql)

    def describe_table(self, tb_name, db_name=None):
        if db_name:
            self.use_database(db_name)
        sql = 'DESCRIBE %s;' % tb_name
        self.query_sql(sql)

    # --------------- For column handle -------------------
    def insert_column(self, tb_name, value):
        sql = 'INSERT INTO %s VALUES %s' % (tb_name, value)
        self.query_sql(sql)

    def drop_columns(self, tb_name, cl_name, value):
        if isinstance(value, str):
            value = value.join("''")
        sql = "DELETE FROM %s WHERE %s=%s" % (tb_name, cl_name, str(value))
        self.query_sql(sql, show=False)

    def insert_columns(self, tb_name, values):
        n = len(values[0])
        qn = (', '.join('%s' for x in range(n))).join('()')
        sql = 'INSERT INTO %s VALUES%s' % (tb_name, qn)
        print(sql)
        cur = self.cnx.cursor()
        cur.executemany(sql, values)
        cur.close()

    def show_columns(self, tb_name):
        sql = 'SHOW COLUMNS in %s;' % tb_name
        self.query_sql(sql)

    def show_all_rows(self, tb_name):
        sql = 'SELECT * FROM %s' % tb_name
        self.query_sql(sql)

    def select_row(self, tb_name, cl_name):
        row_name = ', '.join(x for x in cl_name)
        sql = 'SELECT %s FROM %s' % (row_name, tb_name)
        self.query_sql(sql)


if __name__ == '__main__':
    import MySQLdb
    c = SqlConnector(MySQLdb)
    #c = SqlConnector(pyodbc)
    c.login()
    c.create_database('mysql_db')
    c.use_database('mysql_db')
    #c.create_user('Ericsson', '123456')
    #c.show_grants('Ericsson')
    #c.show_databases()
    #c.drop_database('test_db')
    c.show_databases()
    from collections import OrderedDict
    ord_dict = OrderedDict()
    ord_dict['id'] = 'TINYINT'
    ord_dict['name'] = 'VARCHAR(8)'
    ord_dict['age'] = 'INT'
    c.create_table('test_tb', ord_dict, db_name='mysql_db')
    c.show_tables('mysql_db')
    c.describe_table('test_tb')
    c.show_all_rows('test_tb')
    c.insert_column('test_tb', (7, 'ANAY', 9))
    c.insert_columns('test_tb', [(4, 'AX', 2), (5, 'SD', 2), (6, 'CSA', 5)])
    c.drop_columns('test_tb', 'name', '1')
    c.show_all_rows('test_tb')
    c.show_columns('test_tb')
    #c.select_row('test_tb', ('age', 'name'))
    c.logoff()
