import socket
ip = '169.254.189.75'
port = 1
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        print('try %d' % port)
        addr = (ip, port)
        sock.connect(addr)
    except ConnectionRefusedError:
        port += 1
        continue
    print('correct %d' % port)
