from PIL import Image
import pylab

im = pylab.array(Image.open('0608.jpg'))

pylab.imshow(im)

x = [100, 100, 400, 400]
y = [200, 500, 200, 500]

pylab.plot(x, y, 'r*')

# plot([x_1, x_2], [y_1, y_2]) will plot a line from (x_1, y_1) to (x_2, y_2)
pylab.plot(x[:2], y[:2])

pylab.title('Plotting: "empire.jpg"')
pylab.axis('off')
pylab.show()
