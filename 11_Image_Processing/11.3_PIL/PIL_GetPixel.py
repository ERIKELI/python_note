from PIL import Image

img = Image.open('0608.jpg')
print(img.format, img.size, img.mode)
new_img = img.convert('F')
print(new_img.format, new_img.size, new_img.mode)

w, h = new_img.size
bin = []
for x in range(w):
    for y in range(h):
        bin.append(new_img.getpixel((x, y)))
print(bin, '\n', len(bin), w*h)

new_img.save('F.bmp')
new_img.show()
