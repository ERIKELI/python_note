import numpy as np

# The simple assignments make no copy, a and b are two names for one same ndarray object
a = np.arange(12)
b = a 
print(b is a) # True
b.shape = (3, 4)
print(a.shape) # (3, 4)
# a and b with same id
print(id(a))
print(id(b))

# The view method creates a new array obj that share with same source data
a = np.arange(12)
c = a.view()
print(c is a) # False
# Change the shape of c will not change the shape of a
c.shape = (2, 6)
print(a.shape) # (12, )
# But Change the data of c will change the data of a 
c[1, 2] = 1111
print(a)
# a and b with different id
print(id(a))
print(id(b))

# The copy method makes a complete copy of array and its data
a = np.arange(12)
d = a.copy()
print(d is a) # False
