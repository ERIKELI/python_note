import numpy as np

# Change type, all the elements in array should be in same type.
matrix = np.array([1, 2, 3, 4])
print(matrix, matrix.dtype)         # [1 2 3 4] int32
matrix = np.array([1, 2, 3, 4.0])
print(matrix, matrix.dtype)         # [ 1.  2.  3.  4.] float64
matrix = np.array([1, 2, 3, '4'])
print(matrix, matrix.dtype)         # ['1' '2' '3' '4'] <U11

# Basic slice operation
matrix = np.array([[1, 2, 3, 4],
             [5, 6, 7, 8],
             [9, 10, 11, 12],
             [13, 14, 15, 16]])
# Get number certain element(7), [row, column]
print(matrix[1,2])          # 7
# Get all row and column 3
print(matrix[:, 2])         # [3 7 11 15]
# Get certain row and column, not contains the max number
print(matrix[0:3, 0:2])     ''' [[ 1  2]
                                 [ 5  6]
                                 [ 9 10]] '''

# Get the number by certain list
# (0,0),(1,1),(2,2),(3,3)
print(matrix[[0, 1, 2, 3], [0, 1, 2, 3]])   # [1 6 11 16]
# Use range method can make it too
print(matrix[range(4), range(4)])           # [1 6 11 16]

# Operation to array will act to each element
equal_to_seven = (matrix == 7)
print(equal_to_seven)       ''' [[False False False False]
                                 [False False  True False]
                                 [False False False False]
                                 [False False False False]] '''

# equal_to_seven can be use as an index, True/False list also can do so
print(matrix[equal_to_seven])       # [7]
get_certain_row = [False, True, True, False]
print(matrix[get_certain_row])      ''' [[ 5  6  7  8]
                                         [ 9 10 11 12]] '''
# Fetch row where the certain number in
matrix = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
print(matrix[matrix[:, 2] == 7])    # [[5 6 7 8]]

vector = np.array([5, 10, 15, 20])
# Find the element equal to ten and five(impossible)
equal_to_ten_and_five = (vector == 5) & (vector == 10)
print(vector[equal_to_ten_and_five])                    # []
# Find the element equal to ten or five
equal_to_ten_or_five = (vector == 5) | (vector == 10)   
print(vector[equal_to_ten_or_five])                     # [5 10]
# Change equal one to other number
vector[equal_to_ten_or_five]=50
print(vector)                                           # [50 50 15 20]

# Change the type of array
vector = np.array(['1', '2', '3'])
print(vector.dtype, vector)     # <U1 ['1' '2' '3']
new_vector = vector.astype(int)
print(new_vector.dtype, new_vector)     # int32 [1 2 3]

# Get the min number
# Use print(help(np.array))
vector = np.array([5, 10, 15, 20])
print(vector.min())     # 5

# Calculate the sum in certain dimension
# 1 for cal sum in row, 0 for cal sum in column
matrix = np.array([[1, 2, 3, 4],
             [5, 6, 7, 8],
             [9, 10, 11, 12],
             [13, 14, 15, 16]])
print(matrix.sum(axis=1))   # [10 26 42 58]
print(matrix.sum(axis=0))   # [28 32 36 40]
