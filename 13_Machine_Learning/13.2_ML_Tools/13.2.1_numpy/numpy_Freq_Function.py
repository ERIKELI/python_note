import numpy as np

# Build a matrix, shape is (2, 4, 2)
matrix = np.array([[[1,2], [2,3], [3,4], [4,5]], 
                    [[1,2], [2,3], [3,4], [4,5]]])

# arange function to quickly create an array(vector)
# Note: the shape is (x, ), one dimension array
vector = np.arange(15)
print(vector)   # [ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14]
# reshape function to reshape an array
matrix = vector.reshape(3, 5)
print(matrix)   ''' [[ 0  1  2  3  4]
                     [ 5  6  7  8  9]
                     [10 11 12 13 14]] '''
# shape is the row&column of matrix(array) - (3, 5)
# ndim is the number of dimensions(axes) of matrix - 2
# dtype is a numpy class, dtype.name is the type name of matrix - int32
# size is the total number of elements of matrix - 15
print(matrix.shape, matrix.ndim, matrix.dtype.name, matrix.size) # (3, 5) 2 int32 15
# arrange(from, to, step)
vector = np.arange(10, 30, 5)
print(vector)   # [10 15 20 25]
vector = np.arange(1, 3, 0.5)
print(vector)   # [ 1.   1.5  2.   2.5]

# Init a matrix with all elements 0, default type is float64
matrix = np.zeros((3, 4), dtype=np.float64)
print(matrix)   ''' [[ 0.  0.  0.  0.]
                     [ 0.  0.  0.  0.]
                     [ 0.  0.  0.  0.]] '''
# Init a matrix with all elements 1
matrix = np.ones((2, 3, 4), dtype=np.int32)
print(matrix)   ''' [[[1 1 1 1]
                      [1 1 1 1]
                      [1 1 1 1]]
                      
                     [[1 1 1 1]
                      [1 1 1 1]
                      [1 1 1 1]]]'''

# random function will return a matrix with random number(between 0 to 1)
# random((row, column))
rd = np.random.random((2, 3))
print(rd)   ''' [[ 0.45595053  0.69816822  0.30391984]
                 [ 0.22757757  0.725762    0.84856338]] '''

# Get the matrix with some same step numbers,
# 得到相应数量的等差数列
# linspace(from, to, number)
# Notice whether the num should be plus one
matrix = np.linspace(0, 2, 11)
print(matrix)   # [ 0.   0.2  0.4  0.6  0.8  1.   1.2  1.4  1.6  1.8  2. ]

# sin/cos function can calculate the sin/cos of each elements
from numpy import pi
matrix = np.linspace(0, 2*pi, 9)
print(matrix)   # [ 0.          0.78539816  1.57079633  2.35619449  3.14159265  3.92699082  4.71238898  5.49778714  6.28318531]
matrix = np.sin(matrix)
print(matrix)   # [  0.00000000e+00   7.07106781e-01   1.00000000e+00   7.07106781e-01   1.22464680e-16  -7.07106781e-01  -1.00000000e+00  -7.07106781e-01  -2.44929360e-16]
