import numpy as np

a = np.array([20, 30, 40, 50])
b = np.arange(4)
# For (matrix - matrix) will operate for each corresponding elements
# Different shape can not be subtracted unless only one element
c = a - b
print(c)    # [20 29 38 47]
# For (matrix (-/'**'/'<') number) will operate for each elements 
c -= 1
print(c)    # [19 28 37 46]
b = b**2 # (b*b)
print(b) # [0, 1, 4, 9]
print(a<35) # [True, True, False, False]

# * will multiply each element in corresponding position
# dot will act the dot multiply for matrix(by using matrix_1.dot(matrix_2) or np.dot(matrix_1, matrix_2))
x = np.array([[1, 1],
              [0, 1]])
y = np.array([[2, 0],
              [3, 4]])
print(x*y)  '''[[2 0]
                [0 4]] '''
print('-------')
print(x.dot(y))     '''[[5 4]
                        [3 4]] '''

print('-------')
print(np.dot(x, y)) '''[[5 4]
                        [3 4]]'''

# exp function to cal e^x for x in matrix
# sqrt function to cal sqrt(x) for x in matrix
m = np.arange(3)
print(m)    # [0 1 2]
print(np.exp(m))    # [ 1.          2.71828183  7.3890561 ]
print(np.sqrt(m))   # [ 0.          1.          1.41421356]

# floor/ceil function to round(down/up) number of each matrix
x = 10*np.random.random((3, 4))
print(x)    '''[[ 6.69732597  1.18238851  4.10109987  6.40797969]
                [ 6.50193132  2.58724942  5.25748965  2.58338795]
                [ 0.2798712   7.89760089  6.03544519  1.5176369 ]] '''
print('-------')
y = np.floor(x)
print(y)     '''[[ 6.  1.  4.  6.]
                 [ 6.  2.  5.  2.]
                 [ 0.  7.  6.  1.]] '''
print('-------')
z = np.ceil(x)
print(z)    ''' [[ 7.  2.  5.  7.]
                 [ 7.  3.  6.  3.]
                 [ 1.  8.  7.  2.]] '''

# ravel function can flatten a matrix into vector
print(y.ravel())    # [ 6.  1.  4.  6.  6.  2.  5.  2.  0.  7.  6.  1.]
# Shape value can change shape too
y.shape = (6, 2)
print(y)    ''' [[ 6.  1.]
                 [ 4.  6.]
                 [ 6.  2.]
                 [ 5.  2.]
                 [ 0.  7.]
                 [ 6.  1.]] '''
print('-------')
# T property function
print(y.T)  ''' [[ 6.  4.  6.  5.  0.  6.]
                 [ 1.  6.  2.  2.  7.  1.]] '''
print('-------')
# If a dimension is given as -1, this dimension will be calculated automatically
print(y.reshape(6, -1)) ''' [[ 6.  1.]
                             [ 4.  6.]
                             [ 6.  2.]
                             [ 5.  2.]
                             [ 0.  7.]
                             [ 6.  1.]] '''

# hstack/vstack function can stack the matrix in h/v direction
a = np.arange(6).reshape(2, 3)
b = np.arange(6).reshape(2, 3)
print(np.hstack((a, b))) '''[[0 1 2 0 1 2]
                             [3 4 5 3 4 5]] '''
print(np.vstack((a, b))) '''[[0 1 2]
                             [3 4 5]
                             [0 1 2]
                             [3 4 5]] '''

# hsplit/vsplit function can split the matrix in h/v direction
x = np.arange(30).reshape(2, -1)
print(x)    ''' [[ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14]
                 [15 16 17 18 19 20 21 22 23 24 25 26 27 28 29]] '''
# Split into 3 part (h direction, from one (2, 15) matrix to three (2, 5) matrixes)
print(np.hsplit(x, 3))  '''[array([[ 0,  1,  2,  3,  4],
                                   [15, 16, 17, 18, 19]]),
                            array([[ 5,  6,  7,  8,  9],
                                   [20, 21, 22, 23, 24]]),
                            array([[10, 11, 12, 13, 14],
                                   [25, 26, 27, 28, 29]])] '''
# Split in certain loction (split the matrix in location(after) column 3 and column 4)
print(np.hsplit(x, (3, 4))) '''[array([[ 0,  1,  2],
                                       [15, 16, 17]]),
                                array([[ 3],
                                       [18]]),
                                array([[ 4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14],
                                       [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]])] '''
# Split into 3 part (v direction, from one (2, 15) matrix to two (1, 15) matrixes)
print(np.vsplit(x, 2))  ''' [array([[ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14]]),
                             array([[15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]])] '''

# Fetch the max value position by row/column
data = np.random.random((5, 4))
print(data) ''' [[ 0.65419205  0.75953852  0.89856871  0.96162281]
                 [ 0.76341568  0.10488636  0.06186101  0.27698986]
                 [ 0.73737843  0.75305691  0.28705743  0.45542513]
                 [ 0.5534984   0.54420756  0.86250921  0.596653  ]
                 [ 0.24295898  0.28894731  0.58726507  0.39418991]] '''
# argmax/argmin function can get the max/min value index by row/column
index = data.argmax(axis=0)
print(index)    # [1 0 0 0]
# Get the max value by position
# data.shape[1] to get the number of column
print(data[index, range(data.shape[1])])    # [ 0.76341568  0.75953852  0.89856871  0.96162281]

# Extend the matrix(like tile does)
x = np.arange(0, 40, 10)
print(x)    # [ 0 10 20 30]
# tile(x, (a, b)) can change origin shape(c, d) --> (a*c, b*d)
y = np.tile(x, (3, 5))
print(y)    ''' [[ 0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30]
                 [ 0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30]
                 [ 0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30  0 10 20 30]] '''
print(y.shape) # (3, 20)

# Sort the array
x = np.array([[4, 3, 5], [1, 2, 1]])
print(x)    '''[[4 3 5]
                [1 2 1]] '''
print('--------')
# Sort the number by row/column
y = np.sort(x, axis=1)
print(y)    '''[[3 4 5]
                [1 1 2]] '''
print('--------')
x.sort(axis=1)
print(x)    '''[[3 4 5]
                [1 1 2]] '''
print('--------')
# argsort can return the sorted index list
x = np.array([4, 3, 1, 2])
y = np.argsort(x)
print(y) # [2, 3, 1, 0] min is 1, index=2, then num 2, index is 3, num 3,index is 1, max is 4, index=0
# Use the sorted index list to fetch value
print('--------')
print(x[y]) # [1, 2, 3, 4]
