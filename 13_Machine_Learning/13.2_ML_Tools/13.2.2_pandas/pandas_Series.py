import pandas as pd

info = pd.read_csv('info.csv')

# Fetch a series from DataFrame
rank_series = info['Rank']
print(type(info)) # <class 'pandas.core.frame.DataFrame'>
print(type(rank_series)) # <class 'pandas.core.series.Series'>
print(rank_series[0:5])

# New a series
from pandas import Series
# Build a rank series
rank = rank_series.values
print(rank)
# DataFrame --> Series --> ndarray
print(type(rank)) # <class 'numpy.ndarray'>
# Build a type series
type_series = info['Type']
types = type_series.values
# Build a new series based on former two(type and rank)
# Series(values, index=)
series_custom = Series(rank, index=types)
print(series_custom)
# Fetch Series by key name list
print(series_custom[['MILK_14', 'MILK_15']])
# Fetch Series by index
print(series_custom[0:2])

# Sorted to Series will return a list by sorted value
print(sorted(series_custom, key=lambda x: 0 if isinstance(x, str) else x))

# Re-sort by index for a Series
original_index = series_custom.index.tolist() 
sorted_index = sorted(original_index)
sorted_by_index = series_custom.reindex(sorted_index)
print(sorted_by_index)
# Series sort function
print(series_custom.sort_index())
print(series_custom.sort_values())

import numpy as np
# Add operation for Series will add the values for each row(if the dimensions of two series are same)
print(np.add(series_custom, series_custom))
# Apply sin funcion to each value
print(np.sin(info['Number']))
# Return the max value(return a single value not a Series)
# If more than one max value exist, only return one
print(np.max(filter(lambda x: isinstance(x, float), series_custom)))

# Filter values in range
criteria_one = series_custom > 'C'
criteria_two = series_custom < 'S'
print(series_custom[criteria_one & criteria_two])

