import pandas as pd

# info = pd.read_csv('info.csv', encoding='latin1')
# info = pd.read_csv('info.csv', encoding='ISO-8859-1')
# info = pd.read_csv('info.csv', encoding='cp1252')
info = pd.read_csv('info.csv')
# Get the info of whole csv list, and the info of row and column
print(info)
print('-----------')
# Get the type of info
print(type(info))       # <class 'pandas.core.frame.DataFrame'>
print('-----------')
# Get the type of each column(The object dtype equal to the string type in python)
print(info.dtypes)      ''' No.         int64
                            Type       object
                            Info       object
                            Number    float64
                            Rank       object
                            Mark.      object
                            dtype: object '''
print('-----------')
# Get the first x row of csv list, default is 5
print(info.head(7))
print('-----------')
# Get the last x row of csv list, default is 5
print(info.tail(7))
print('-----------')
# Get the name of each column
print(info.columns)
print('-----------')
# Get the shape of csv list
print(info.shape)
print('-----------')
# Get the statistics parameter of cvs list(for digit data)
# Such as count, mean, standard deviation, min, 25%, 50%, 75%, max
print(info.describe())
