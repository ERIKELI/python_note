import pandas as pd

info = pd.read_csv('info.csv')
# Get the certain row of csv list
print(info.loc[0])
print(info.loc[3:7])
print('----------')
# Get certain column(columns) by column name(name list)
print(info['Type'])
print(info[['Type', 'No.']])
# Get the column name and save it as a list
col_names = info.columns.tolist()
print(col_names)

# Filter off the column name that end with '.'
dotList = []
for n in col_names:
    if n.endswith('.'):
        dotList.append(n)
newList = info[dotList]
print(newList)

# Operation for column will act to each element as numpy does
print(info['Number'] * 10)

# Operation for two csv with same shape will act each corresponding element
x = info['Number']
y = info['No.']
print(x+y)
# Act for string
x = info['Rank']
y = info['Mark.']
print(x+y)

# Add a column after the tail column(the dimension of new one should be same as origin)
print(info.shape)
info['New'] = x+y
print(info.shape)
print('----------')

# Get the max/min value of a column
print(info['Number'].max())
print(info['Number'].min())

num = info['Number']
num_null_true = pd.isnull(num)
# If these is a null value in DataFrame, the calculated result will be NaN
print(sum(info['Number'])/len(info['Number'])) # return nan
# Use the DataFrame == False to reverse the DataFrame
good_value = info['Number'][num_null_true == False]
print(sum(good_value)/len(good_value))
print(good_value.mean())
# mean method can filter off the missing data automatically
print(info['Number'].mean())
print('---------')

