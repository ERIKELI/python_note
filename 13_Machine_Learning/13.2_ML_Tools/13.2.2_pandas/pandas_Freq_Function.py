import pandas as pd
import numpy as np

info = pd.read_csv('info.csv')

# Sort value by column  
# inplace is True will sort value base on origin, False will return a new DataFrame
new = info.sort_values('Mark.', inplace=False, na_position='last')
print(new)
# Sorted by ascending order in default(ascending=True) 
# No matter ascending or descending sort, the NaN(NA, missing value) value will be placed at tail
info.sort_values('Mark.', inplace=True, ascending=False)
print(info)
print('---------')
# Filter off the null row
num = info['Number']
# isnull will return a list contains the status of null or not, True for null, False for not
num_null_true = pd.isnull(num)
print(num_null_true)
num_null = num[num_null_true]
print(num_null) # 12 NaN
print('---------')

# pivot_table function can calulate certain para that with same attribute group by using certain function
# index tells the method which column to group by
# value is the column that we want to apply the calculation to 
# aggfunc specifies the calculation we want to perform, default function is mean
avg_by_rank = info.pivot_table(index='Rank', values='Number', aggfunc=np.sum)
print(avg_by_rank)
print('---------')
# Operate to multi column
sum_by_rank = info.pivot_table(index='Rank', values=['Number', 'No.'], aggfunc=np.sum)
print(sum_by_rank)
print('---------')

# dropna function can drop any row/columns that have null values
info = pd.read_csv('info.csv')
# Drop the columns that contain NaN (axis=0 for row)
drop_na_column = info.dropna(axis=1)
print(drop_na_column)
print('---------')
# Drop the row that subset certains has NaN 
# thresh to decide how many valid value required
drop_na_row = info.dropna(axis=0, thresh=1, subset=['Number', 'Info', 'Rank', 'Mark.'])
print(drop_na_row)
print('---------')
# Locate to a certain value by its row number(plus 1 for No.) and column name
print(info)
row_77_Rank = info.loc[77, 'Rank']
print(row_77_Rank)
row_88_Info = info.loc[88, 'Info']
print(row_88_Info)
print('---------')

# reset_index can reset the index for sorted DataFrame
new_info = info.sort_values('Rank', ascending=False)
print(new_info[0:10])
print('---------')
# drop=True will drop the index column, otherwise will keep former index colunn (default False)
reset_new_info = new_info.reset_index(drop=True)
print(reset_new_info[0:10])
print('---------')

# Define your own function for pandas
# Use apply function to implement your own function
def hundredth_row(col):
    hundredth_item = col.loc[99]
    return hundredth_item 
hundred_row = info.apply(hundredth_row, axis=0)
print(hundred_row)
print('---------')
# Null count
# The apply function will act to each column
def null_count(column):
    column_null = pd.isnull(column)
    null = column[column_null]
    return len(null)
# Passing in axis para 0 to iterate over rows instead of column
# Note: 0 for act by row but passing by column, 1 for act by column but passing by row
# Passing by column can act for each column then get row
# Passing by row can act for each row than get column
column_null_count = info.apply(null_count, axis=0)
print(column_null_count)
print('---------')

# Example: classify the data by Rank, and calculate the sum for each
def rank_sort(row):
    rank = row['Rank']
    if rank == 'S':
        return 'Excellent'
    elif rank == 'A':
        return 'Great'
    elif rank == 'B':
        return 'Good'
    elif rank == 'C':
        return 'Pass'
    else:
        return 'Failed'
# Format a classified column
rank_info = info.apply(rank_sort, axis=1)
print(rank_info)
print('---------')
# Add the column to DataFrame
info['Rank_Classfied'] = rank_info
# Calculate the sum of 'Number' according to 'Rank_Classfied'
new_rank_number = info.pivot_table(index='Rank_Classfied', values='Number', aggfunc=np.sum)
print(new_rank_number)

# set_index will return a new DataFrame that is indexed by values in the specified column
# And will drop that column(default is True)
# The column set to be index will not be dropped if drop=False
index_type = info.set_index('Type', drop=False, append=True)
print(index_type)
print('---------')

# Use string index to slice the DataFrame
# Note: the index(key) should be unique
print(index_type['MILK_1':'MILK_7'])
print('---------')
print(index_type.loc['MILK_1':'MILK_7'])
# Value index is available too
print('---------')
print(index_type[-15:-8])
print('---------')

# Calculate the standard deviation for each element from two different index
cal_list = info[['Number', 'No.']]
# np.std([x, y]) --> std value
# The lambda x is a Series
# cal_list.apply(lambda x: print(type(x)), axis=1)
print(cal_list.apply(lambda x: np.std(x), axis=1))

