import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

curve = pd.read_csv('curve.csv')
cols = ['A', 'B', 'C', 'D', 'E']

fig, ax = plt.subplots()
ax.boxplot(curve[cols].values) # curve[cols].values is ndarray
print(curve[cols].values)
plt.show()
