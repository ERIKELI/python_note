import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

curve = pd.read_csv('curve.csv')

fig, ax = plt.subplots()

# Hide the tick params
ax.tick_params(bottom='off', top='off', left='off', right='off')
# Hide spine
print(type(ax.spines))
for key, spine in ax.spines.items():
    print(key, spine)
    spine.set_visible(False)
# Set the color, (R, G, B) the value should be between (0, 1)
color_dark_blue = (0/255, 107/255, 164/255)
color_orange = (255/255, 128/255, 14/255)
ax.plot(curve['A'], curve['B'], c=color_dark_blue, label='AB', linewidth=7)
ax.plot(curve['C'], curve['D'], c=color_orange, label='CD', linewidth=7)
plt.legend(loc='upper right')
plt.show()
