import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

curve = pd.read_csv('curve.csv')
print(curve)
print('------------')
# Change format to datetime format
curve['DATE'] = pd.to_datetime(curve['DATE'])
print(curve)

# matplotlib inline
# If plot nothing and show, it will plot and show a blank board
# plt.plot()
# plt.show()
# Similar to pyqtgraph, plot(x_list, y_list)
plt.plot(curve['DATE'], curve['VALUE'])
# If the tick is too long, use rotation to adjust
plt.xticks(rotation=-45)
plt.xlabel('Month')
plt.ylabel('Rate')
plt.title('Unemployment Rate')
#plt.show()

# Sub figure
# figsize decide the size of figure window
fig = plt.figure('figure_name', figsize=(7, 7))
# add_subplot(row, column, location)
# add_subplot will divide fig into several part(according to row and column) and place the subplot in input location
''' fig divided like that:
[1 ... x
 .     .
 .     .
 .     .
 y ... x*y]
'''
# May course some overlap if the shape(row/column) is different
ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 2)
ax4 = fig.add_subplot(2, 2, 4)
ax3 = plt.subplot(2, 2, 3)

ax1.plot(np.random.randint(1, 5, 5), np.arange(5))
# Note np.arange(10)*3 will return an array shape(1, 10) with each element * 3
ax2.plot(np.arange(10)*3, np.arange(10))
ax4.plot(np.random.randint(1, 10, 10), np.random.randint(1, 10, 10))

month = curve['DATE'].dt.month
value = curve['VALUE']
# If no new fig here, the curve will be plot on the latest fig(ax4 here)
# 如果此处没有新生成一个fig，则图像会被显示在最近的一个fig上（此处为ax4）
fig = plt.figure(figsize=(9, 7))
plt.plot(month[0:6], value[0:6], c='red', label='first half year') # c='r'/c=(1, 0, 0)
plt.plot(month[6:12], value[6:12], c='blue', label='second half year')
# If not call legend function, the label would not show
# loc='best' will place the label in a best location
'''
loc : int or string or pair of floats, default: 'upper right'
        The location of the legend. Possible codes are:
    
            ===============   =============
            Location String   Location Code
            ===============   =============
            'best'            0
            'upper right'     1
            'upper left'      2
            'lower left'      3
            'lower right'     4
            'right'           5
            'center left'     6
            'center right'    7
            'lower center'    8
            'upper center'    9
            'center'          10
            ===============   =============
''' 
plt.legend(loc='best')
plt.xlabel('Month')
plt.ylabel('Rate')
plt.title('My rate curve')

plt.show()
