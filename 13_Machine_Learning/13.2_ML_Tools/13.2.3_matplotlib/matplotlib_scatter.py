import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

curve = pd.read_csv('curve.csv')
fig, ax = plt.subplots()
# scatter(x, y) x for x axis para list/Series, y for y axis para list/Series
ax.scatter(curve['A'], curve['B'])
plt.show()
