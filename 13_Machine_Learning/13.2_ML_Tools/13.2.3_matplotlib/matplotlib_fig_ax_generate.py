from matplotlib import pylab as plt

fig, ax = plt.subplots()
print(fig, ax)

fig = plt.figure('New_Figure')
ax = fig.add_subplot(1, 1, 1)
print(fig, ax)

fig = plt.figure('Another_Figure')
ax = plt.subplot(1, 1, 1)
print(fig, ax)

plt.show()
