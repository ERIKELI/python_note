import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

curve = pd.read_csv('curve.csv')
cols = ['A', 'B', 'C', 'D', 'E']
para = curve[cols]
print(para)
print(para[:1])
print('-----------')
# ix function can fetch the data in certain position by index
# ix[row, column], row/column can be a number/list/key_list
# Bar height decide the height of bar graph
bar_height = para.ix[0, cols].values # para.ix[0, cols] type is Series
# Bar position decide the x distance to base point
bar_position = np.arange(5) + 1
# subplots function return a figure and only one subplot
# fig to set figure style, ax(axis) for graph draw
fig, ax = plt.subplots()
# bar(position_list, height_list, width_of_bar)
ax.bar(bar_position, bar_height, 0.3)
# Use barh to create a horizonal bar figure
ax.barh(bar_position, bar_height, 0.3)
# Set position of ticks
ax.set_xticks(range(1, 6))
# Set x tick labels name and rotation
ax.set_xticklabels(cols, rotation=45)
# Set x/y label name
ax.set_xlabel('Type')
ax.set_ylabel('Rate')
ax.set_title('This is a test figure')

# Two ways to create a figure with subplot
# First one: subplots
# plt.subplots(nrow, ncol)
# sharex and sharey decide whether all the subplot should to share the axis label
# If multi subplots, ax will be a array contains all subplots
fig, ax = plt.subplots(2, 2, sharex=False, sharey=False)
ax_1 = ax[0][0]
ax_2 = ax[0][1]
ax_3 = ax[1][0]
ax_4 = ax[1][1]
# Second one: use figure
fig = plt.figure('figure_name', figsize=(7, 7))
ax_1 = fig.add_subplot(2, 2, 1)
ax_2 = fig.add_subplot(2, 2, 2)

plt.show()
