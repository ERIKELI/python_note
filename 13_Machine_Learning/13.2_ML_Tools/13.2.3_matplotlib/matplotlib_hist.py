import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

curve = pd.read_csv('curve.csv')
cols = ['A', 'B', 'C', 'D', 'E']
# value_counts function will return the number of each value
print(curve['C'].value_counts())
fig, ax = plt.subplots()
# hist(Series, range=, bins=) 
# Series is the data to be plotted, range is the plot range, bins is the number of plot hists in range
ax.hist(curve['C'], range=(1,20), bins=20)
# Set the x/y axis limit range
ax.set_xlim(0, 20)
ax.set_ylim(0, 5)
plt.show()
