import socket
import time
from threading import Thread

class Chamber():
    def __init__(self, tag):
        #self.ip = '192.168.121.100'
        #self.port = 2049
        self.ip = '127.0.0.1'
        self.port = 31500
        self.tag = tag
        self.addr = (self.ip, self.port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(self.addr)

    def getTemper(self):
        self.sock.send(('021?8E03'+self.tag).encode('utf-8'))
        time.sleep(2)
        print('Receive blocking', self.tag)
        data = self.sock.recv(1024)
        print(data)
        #self.sock.close()

    def run(self):
        for i in range(10):
            self.getTemper()

cham_1 = Chamber('C_1')
cham_2 = Chamber('C_2')
Thread(target=cham_1.run).start()
Thread(target=cham_2.run).start()
