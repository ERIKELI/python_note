import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
sock.bind(('', 5656))
print('server ready')
while True:
    msg, addr = sock.recvfrom(1024)
    print(msg, addr)
    sock.sendto(b'hi, this is server', addr)
