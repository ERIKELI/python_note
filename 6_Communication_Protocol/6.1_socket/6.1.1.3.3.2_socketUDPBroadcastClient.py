import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
for i in range(5):
    sock.sendto(b'hi, this is client.', ('<broadcast>', 5656))
    data, msg = sock.recvfrom(1024)
    print(data, msg)
