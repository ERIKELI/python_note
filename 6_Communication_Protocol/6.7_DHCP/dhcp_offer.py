import binascii
import struct
import socket

class Offer():
    def __init__(self, transaction_id, client_ip_offer, server_ip, client_mac_id, file_path, pass_sec, msg_type, tftp_ip=None, lease_time=172800):
        SERVER_NAME = ''
        self._server_ip = server_ip
        self._offer_ip = client_ip_offer
        self._lease_time = lease_time
        if not tftp_ip:
            tftp_ip = server_ip
        self._tftp_ip = tftp_ip
        if not file_path:
            file_path = ''
        pass_sec = struct.pack('!H', pass_sec)
        client_mac_id = binascii.unhexlify(client_mac_id.replace(':', ''))
        transaction_id = binascii.unhexlify(transaction_id)

        self.packet = b''
        self.packet += b'\x02'  # op
        self.packet += b'\x01'  # htype
        self.packet += b'\x06'  # hlen
        self.packet += b'\x00'  # hops
        self.packet += transaction_id
        self.packet += pass_sec # secs
        self.packet += b'\x00\x00'  # flags
        self.packet += b'\x00\x00\x00\x00'  # current client ip
        self.packet += socket.inet_aton(client_ip_offer) # offer ip
        self.packet += socket.inet_aton(server_ip)  # server ip
        self.packet += b'\x00\x00\x00\x00'  # gateway ip
        self.packet += client_mac_id # client mac id
        self.packet += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # client mac id padding
        self.packet += SERVER_NAME.encode('utf-8')
        self.packet += b'\x00'*(64-len(SERVER_NAME))
        self.packet += file_path.encode('utf-8')
        self.packet += b'\x00'*(128-len(file_path))
        self.packet += self.optional(msg_type)

    def optional(self, msg_type):
        magic = b'\x63\x82\x53\x63'
        opt = b''
        # Message type
        if msg_type == 'DSCV':
            opt += self.encode_int(53, 1, 2)
        if msg_type == 'RQST':
            opt += self.encode_int(53, 1, 5)
        # Server identifier
        opt += self.encode_ip(54, 4, self._server_ip)
        # Subnet mask
        opt += self.encode_ip(1, 4, '255.255.255.0')
        # Router
        opt += self.encode_ip(3, 4, '127.0.0.1')
        # DNS server
        opt += self.encode_ip(6, 4, '127.0.0.1')
        # NetBios server
        opt += self.encode_ip(44, 4, '127.0.0.1')
        # IP address lease time
        opt += self.encode_int(51, 4, self._lease_time)
        # Extend lease time T1
        opt += self.encode_int(58, 4, int(self._lease_time*0.5))
        # Extend lease time T2
        opt += self.encode_int(59, 4, int(self._lease_time*0.8))
        # Log server
        opt += self.encode_ip(7, 4, '127.0.0.1')
        # TFTP server name
        opt += bytes([66, 11]) + self._tftp_ip.encode()
        # Tail
        # TODO: find out why a b'\xff' for end
        opt += b'\xff'
        return magic+opt

    def encode_int(self, func, length, item):
        m = {1: '!B', 2: '!H', 4: '!I'}
        s = b''
        s += (bytes([func, length]) + struct.pack(m[length], item))
        return s

    def encode_ip(self, func, length, item):
        s = b''
        s += bytes([func, length])
        s += socket.inet_aton(item)
        return s

