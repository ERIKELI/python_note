import socket
import struct
import binascii
import logging
from threading import Thread
from dhcp_offer import Offer

logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] %(threadName)s: %(message)s')

class DHCPServer():
    """
    This class implements parts of RFC-2131
    Only DHCPDISCOVER and DHCPREQUEST allowed
    """
    def __init__(self, boot_file=None, server_ip=None, offer_ip=None, tftp_ip=None):
        Thread.__init__(self)
        self._port = 67
        self._boot_file = boot_file
        self._file_index = 0
        self._offer_ip = offer_ip
        self._tftp_ip = tftp_ip
        self.server_ip = server_ip

    @property
    def server_ip(self):
        return self._server_ip

    @server_ip.setter
    def server_ip(self, server_ip):
        self._server_ip = server_ip
        self.send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
        self.send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.send_socket.bind((self._server_ip, self._port))

    @property
    def boot_file(self):
        return self._boot_file

    @boot_file.setter
    def boot_file(self, boot_file):
        if not isinstance(boot_file, list):
            boot_file = [boot_file]
        self._boot_file = boot_file
        self._file_index = 0

    @property
    def offer_ip(self):
        return self._offer_ip

    @offer_ip.setter
    def offer_ip(self, offer_ip):
        self._offer_ip = offer_ip

    def check_msg(self, m):
        is_valid, msg_type, select_ip = False, None, None
        if (m[0] == b'\x01' and
            m[1] == b'\x01' and
            m[2] == b'\x06' and
            m[3] == b'\x00' and
            m[10:12] == [b'\x00', b'\x00'] and
            m[12:16] == [b'\x00', b'\x00', b'\x00', b'\x00'] and
            m[16:20] == [b'\x00', b'\x00', b'\x00', b'\x00'] and
            m[20:24] == [b'\x00', b'\x00', b'\x00', b'\x00'] and
            m[236:240] == [b'\x63', b'\x82', b'\x53', b'\x63']
            ):
            logging.warning('Valid DHCP message')
            # Valid DHCPDISCOVER
            opt = (x for x in m[240:])
            while opt:
                try:
                    func_code = next(opt)
                    if func_code == b'\x00':
                        break
                    length = next(opt)
                    items = b''
                    for i in range(ord(length)):
                        items += next(opt)
                except StopIteration:
                    break
                else:
                    if func_code == b'\x35' and length == b'\x01':
                        if items == b'\x01':
                            logging.warning('DHCP Discover')
                            msg_type = 'DSCV'
                            is_valid = True
                        if items == b'\x03':
                            logging.warning('DHCP Request')
                            msg_type = 'RQST'

                    # Assure DHCP server selected
                    if func_code == b'\x36' and msg_type == 'RQST':
                        logging.warning('DHCP Server Identifier check')
                        select_ip = socket.inet_ntoa(items)

                    # Double check DHCP offer ip
                    if func_code == b'\x32' and select_ip == self._server_ip:
                        offer_ip = socket.inet_ntoa(items)
                        if offer_ip == self._offer_ip:
                            is_valid = True
                        else:
                            logging.warning('Offer ip double check failed')

        return is_valid, msg_type

    def serve_forever(self):

        self.recv_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
        self.recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.recv_socket.bind(('', self._port))
        
        if self._boot_file:
            if self._file_index >= len(self._boot_file):
                self._file_index = 0

        def handle_msg(msg, addr):
            send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
            send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
            send_socket.bind((self._server_ip, self._port))
            m = list(struct.unpack('c'*len(msg), msg))
            is_valid, msg_type = self.check_msg(m)
            if is_valid:
                logging.warning('Valid %s message, try to response' % msg_type)
                pass_sec = ord(m[8]) * 256 + ord(m[9])
                transaction_id = ''.join(['%02x' % ord(x) for x in m[4:8]])
                client_mac_id = ':'.join(['%02x' % ord(x) for x in m[28:34]])
                if msg_type:
                    offer = Offer(transaction_id=transaction_id,
                                  client_ip_offer=self._offer_ip,
                                  server_ip=self._server_ip,
                                  client_mac_id=client_mac_id,
                                  file_path=self._boot_file,
                                  pass_sec=pass_sec,
                                  msg_type=msg_type,
                                  tftp_ip = self._tftp_ip)
                    send_socket.sendto(offer.packet, ('<broadcast>', 68))
                    self._file_index += 1
                logging.warning('Respone done')
            else:
                logging.warning('Invalid message, discard...')
            send_socket.close()
        
        while True:
            logging.warning('Waiting discovery...')
            msg, addr = self.recv_socket.recvfrom(8192)
            logging.warning('Receive message from %s, port %s' % addr)
            handler = Thread(target=handle_msg, args=(msg, addr))
            handler.start()

if __name__ == '__main__':
    dhcp = DHCPServer(server_ip='127.0.0.1', offer_ip='127.0.0.10')
    dhcp.serve_forever()
