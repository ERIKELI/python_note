import socket
import threading

ip = '192.168.1.1'
server_port = 67
client_port = 68
s_addr = ('', 67)
c_addr = ('', 68)

s_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
s_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
s_sock.bind(s_addr)

c_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
c_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
c_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
c_sock.bind(c_addr)

def listen(sock, name):
    
    while True:
        with open(name+'.txt', 'a') as f:
            print('%s recieving' % name)
            msg, addr = sock.recvfrom(1024)
            print(name, addr, msg)
            f.write(str(addr))
            f.write(str(msg))

ts = threading.Thread(target=listen, args=(s_sock, 'ServerRecv'))
tc = threading.Thread(target=listen, args=(c_sock, 'ClientRecv'))

ts.start()
tc.start()


