from ModbusCodeC import ModbusCodeC
import socket, struct, time


class ModbusProtocol():
    def __init__(self):
        self.ip = '192.168.0.101'
        self.port = 502
        self.addr = (self.ip, self.port)
        self.sock = None

    def open(self):
        if self.sock:
            self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.addr)
        print('=== Chamber driver built')

    def close(self):
        self.sock.close()
        self.sock = None
        print('=== Chamber driver closed')

    def read_register(self, regi, num):
        c = ModbusCodeC.encode('r', regi, num)
        self.sock.send(c)
        msg = self.sock.recv(1024)
        recv_info = ModbusCodeC.decode(msg)
        return recv_info

    def write_single_register(self, regi, data):
        c = ModbusCodeC.encode('w', regi, 1, data)
        self.sock.send(c)
        msg = self.sock.recv(1024)
        # assert c == msg, 'Write register failed'

    def start(self):
        self.write_single_register(1, 2)
        print('=== Start chamber')

    def stop(self):
        self.write_single_register(1, 1)
        print('=== Stop chamber')

    # Switch running mode: 'PRO' for program mode, 'VAL' for constant value mode.
    def switch_mode(self, mode):
        modeList = {'PRO': 0,
                    'VAL': 1}
        self.write_single_register(3, modeList[mode])
        print('=== Switch to %s mode' % mode)

    # Get current parameters
    def get_curr_para(self):
        info = self.read_register(4, 2)
        curr_temp, curr_humi = struct.unpack('!H', info['PDU']['data'][0:2])[0], struct.unpack('!H', info['PDU']['data'][2:4])[0]
        print('<<< Current temperature: %s, humidity: %s' % (curr_temp, curr_humi))
        return int(curr_temp)/10, int(curr_humi)/10

    def get_curr_set_para(self):
        info = self.read_register(6, 2)
        temp_set, humi_set = struct.unpack('!H', info['PDU']['data'][0:2])[0], struct.unpack('!H', info['PDU']['data'][2:4])[0]
        print('<<< Current set temperature: %s, humidity: %s' % (temp_set, humi_set))
        return int(temp_set)/10, int(humi_set)/10

    # Get target parameters
    def get_targ_para(self):
        info = self.read_register(8, 2)
        targ_temp, targ_humi = struct.unpack('!H', info['PDU']['data'][0:2])[0], struct.unpack('!H', info['PDU']['data'][2:4])[0]
        print('<<< Target temperature: %s, humidity: %s' % (targ_temp, targ_humi))
        return int(targ_temp)/10, int(targ_humi)/10

    # TODO: Review this method, and find out the flow to set gradient
    def set_temp_gradient(self, value):
        value *= 10
        value = int(value)
        self.write_single_register(38, value)
        self.write_single_register(37, 1)
        print('=== Set temperature gradient to %s' % (value/10))

    # TODO: review this method, check return time value and format again
    def get_running_time(self):
        info = self.read_register(44, 2)
        h, m = struct.unpack('!H', info['PDU']['data'][0:2])[0], struct.unpack('!H', info['PDU']['data'][2:4])[0]
        time = str(h) + ':' + str(m)
        return time

    def set_targ_temp(self, value):
        value *= 10
        value = int(value)
        self.write_single_register(46, value)
        print('=== Set target temperature to %s℃' % (value/10))

    def set_targ_humi(self, value):
        value *= 10
        value = int(value)
        self.write_single_register(47, value)
        print('=== Set target humidity to %s%%' % (value/10))

    def readAll(self):
        info = self.read_register(1, 55)
        x = struct.unpack('!%dH' % (len(info['PDU']['data'])/2), info['PDU']['data'])
        all = {}
        for i in range(len(x)):
            all[hex(i+1).strip('0x').upper()] = x[i]
        return all
if __name__ == '__main__':
    p = ModbusProtocol()
    p.open()

    #p.set_targ_temp(30)
    time.sleep(1)
    p.start()
    '''
    p.set_targ_humi(60)
    time.sleep(1)
    p.get_para()
    p.start()
    time.sleep(10)
    p.stop()
    p.get_curr_para()
    time.sleep(1)
    # p.get_output_para()
    time.sleep(1)
    p.get_targ_para()
    time.sleep(1)
    '''
    #time.sleep(15)
    #p.stop()
    #p.readAll()
    p.close()
