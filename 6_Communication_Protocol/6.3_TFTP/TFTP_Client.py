from TFTP_Helper import *
import socket


class TFTPClient():
    def __init__(self):
        server_ip = '127.0.0.1'
        port = 69
        self.address = (server_ip, port)

    def readRequest(self, file_name):
        sock_down = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        send_data = TFTPReadWriteRequest.encode('r', file_name)
        sock_down.sendto(send_data, self.address)
        block_num = 1
        try:
            f = open('TFTPClientFIle\copy-'+file_name, 'wb')
        except:
            print('--- File "%s" no found' % file_name)
            exit()
        while True:
            recv_data, recv_addr = sock_down.recvfrom(1024)
            print('<<< DATA', block_num)
            opcode, block_num_recv, data = TFTPData.decode(recv_data)
            if opcode != 3 or block_num != block_num_recv:
                print('--- Opcode or Block Number Error')
                # TODO: add error msg send and handle here
                break
            f.write(data)
            sock_down.sendto(TFTPAck.encode(block_num), recv_addr)
            print('>>> ACK ', block_num)
            if len(data) < 512:
                # TODO: add transmission completed handle here
                block_num = 1
                print('--- File "%s" transmission completed' % file_name)
                f.close()
                break
            block_num += 1

    def writeRequest(self, file_name):
        sock_upload = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        send_data = TFTPReadWriteRequest.encode('w', file_name)
        sock_upload.sendto(send_data, self.address)
        print('>>> WRQ')
        recv_data, addr = sock_upload.recvfrom(1024)
        print('<<< ACK')
        opcode, ack_block = TFTPAck.decode(recv_data)
        block_num = 0
        if opcode != 4 or ack_block != block_num:
            print('--- Error Opcode or Block Number')
            # TODO: add error msg send and handle here
            exit()
        try:
            f = open('TFTPClientFIle\\'+file_name, 'rb')
        except:
            print('--- File "%s" no found' % file_name)
            exit()
        block_num += 1
        while True:
            upload_data = f.read(512)
            sock_upload.sendto(TFTPData.encode(block_num, upload_data), addr)
            print('>>> DATA')
            ack_msg, addr = sock_upload.recvfrom(1024)
            print('<<< ACK')
            opcode, ack_block_num = TFTPAck.decode(ack_msg)
            assert opcode == 4 and ack_block_num == block_num, 'Invalid ack message.'
            if len(upload_data) < 512:
                # TODO: add transmission completed handle here
                block_num = 1
                print('--- File "%s" transmission completed' % file_name)
                f.close()
                break
            block_num += 1


tftp = TFTPClient()
tftp.readRequest('CXP9034469_20_signed.xlf')
#for file in ['downFile.py', 'downFile.docx', 'downFile.txt', 'downFile.zip']:
#    tftp.readRequest(file)
#for file in ['uploadFile.py', 'uploadFile.docx', 'uploadFile.txt', 'uploadFile.zip']:
#    tftp.writeRequest(file)
