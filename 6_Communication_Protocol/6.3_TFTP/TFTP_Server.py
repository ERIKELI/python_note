import importlib
from TFTP_Helper import *
import socket
from threading import Thread


class TFTPServer():
    def __init__(self):
        ip = '127.0.0.1'
        port = 69
        address = (ip, port)
        self.home_path = 'TFTPServerFile'
        self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server.bind(address)

    def readRequest(self, file_name, addr):
        print('<<< RRQ', '\n=== Read request, starting file %s downing to client' % file_name, end='')
        sock_down = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        block_num = 1
        try:
            f = open(self.home_path + '\\' + file_name, 'rb')
        except:
            # file no found, exit thread
            print('=== File %s no found' % file_name)
            exit()
        while True:
            down_data = f.read(512)     # data is byte format
            sock_down.sendto(TFTPData.encode(block_num, down_data), addr)
            print('>>> DATA', block_num)
            ack_msg, ack_addr = sock_down.recvfrom(1024)
            print('<<< ACK ', block_num)
            opcode, ack_block_num = TFTPAck.decode(ack_msg)
            assert opcode == 4 and ack_block_num == block_num, 'Invalid ack message.'
            if len(down_data) < 512:
                block_num = 1
                print('=== Transmission of %s completed' % file_name)
                f.close()
                exit()
            block_num += 1
        print('=== Close file')
        f.close()

    def writeRequest(self, file_name, addr):
        print('<<< WRQ', '\n=== Write request, starting file %s uploading to server' % file_name)
        sock_upload = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        block_num = 0
        sock_upload.sendto(TFTPAck.encode(block_num), addr)
        print('>>> ACK')
        try:
            f = open('TFTPServerFIle\\Copy-'+file_name, 'wb')
        except:
            print('=== File "%s" no found' % file_name)
            exit()
        block_num += 1
        while True:
            recv_data, addr = sock_upload.recvfrom(1024)
            opcode, block_num_recv, data = TFTPData.decode(recv_data)
            print('<<< DATA')
            if opcode != 3 or block_num != block_num_recv:
                print('=== Error Opcode or Block Number')
                # TODO: add error msg send and handle here
                exit()
            f.write(data)
            sock_upload.sendto(TFTPAck.encode(block_num), addr)
            print('>>> ACK')
            if len(data) < 512:
                # TODO: add transmission completed handle here
                block_num = 1
                print('=== File "%s" transmission completed' % file_name)
                f.close()
                exit()
            block_num += 1

    def server_run(self):
        while True:
            print('=== Waiting client')
            msg, addr = self.server.recvfrom(1024)
            print('<<< Receive a connection from client %s, msg is %s' % (addr, msg))
            opcode, file_name = TFTPReadWriteRequest.decode(msg)
            cmdSelect = {1: Thread(target=self.readRequest, args=(file_name, addr)),
                         2: Thread(target=self.writeRequest, args=(file_name, addr))}
            try:
                cmdSelect[opcode].start()
            except:
                print('<<< Receive error opcode%s' % opcode)
                raise ErrorOpcode

server = TFTPServer()
server.server_run()
