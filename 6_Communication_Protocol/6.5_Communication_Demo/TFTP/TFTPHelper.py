from threading import Thread
import struct
RRQ = 1
WRQ = 2
DATA = 3
ACK = 4
ERROR = 5


class TFTPReadWriteRequest():
    @staticmethod
    def encode(op, file_name, mode='octet'):
        opcode_select = {'r': RRQ, 'w': WRQ}
        opcode = opcode_select[op]
        packet = struct.pack('!H%dsb%dsb' % (len(file_name), len(mode)), opcode, file_name.encode(), 0, mode.encode(), 0)
        return packet

    @staticmethod
    def decode(msg_bytes):
        opcode = struct.unpack('!H', msg_bytes[:2])[0]
        fileName = msg_bytes[2:].decode('utf-8').split('\x00')[0]
        mode = msg_bytes[2:].decode('utf-8').split('\x00')[1]
        print('=== Transfer mode is %s' % mode)
        return opcode, fileName


class TFTPData():
    @staticmethod
    def encode(block_number, data):
        packet = b'\x00\x03'
        packet += struct.pack('!H', block_number)
        packet += data
        return packet

    @staticmethod
    def decode(data_msg):
        opcode = struct.unpack('!HH', data_msg[:4])[0]
        block_num = struct.unpack('!HH', data_msg[:4])[1]
        data = data_msg[4:]
        return opcode, block_num, data


class TFTPAck():
    @staticmethod
    def encode(block_num):
        packet = b''
        packet += b'\x00\x04'
        packet += struct.pack('!H', block_num)
        return packet

    @staticmethod
    def decode(msg_bytes):
        opcode = struct.unpack('!HH', msg_bytes)[0]
        ack_block = struct.unpack('!HH', msg_bytes)[1]
        return opcode, ack_block


class ErrorOpcode(Exception):
    pass