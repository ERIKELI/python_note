import socket
import binascii
import struct
from threading import Thread
from BOOTP.BOOTPCodeC import ClientCodeC

class BOOTPClient():
    def __init__(self):
        self.client_ip = '0.0.0.0'
        self.broadcast_ip = '255.255.255.255'   # or '<broadcast>'
        #self.server_ip = '127.0.0.1'
        self.target_port = 67
        self.source_port = 68
        ClientCodeC.get_xid_macid()

    def recv_check(self, m):
        print(
                m['chaddr'] , ClientCodeC.client_mac_id
               )

        if not (
                m['op'] == b'\x02' and # OP
                m['htype'] == b'\x01' and # HTYPE
                m['hlen'] == b'\x06' and # HLEN
                m['hops'] == b'\x00' and # HOPS
                m['secs'] == [b'\x00', b'\x00'] and # SECS
                m['flags'] == [b'\x80', b'\x00'] and   # FLAGS (broadcast)
                m['xid'] == ClientCodeC.transaction_id and
                m['chaddr'] == ClientCodeC.client_mac_id and
                len(m['file'])
        ):
            return False
        return True

    def client_request(self):
        while True:
            self.client_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.client_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
            self.client_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
            self.client_sock.settimeout(5)
            #self.client_sock.bind((self.server_ip, 68))
            self.client_sock.bind(('', 68))

            self.client_sock.sendto(ClientCodeC.request(), ('<broadcast>', self.target_port))
            print('>>> [BOOTP] SEND ("<broadcast>", %d) request command' % self.target_port)
            try:
                data, addr = self.client_sock.recvfrom(1024)
            except socket.timeout:
                print('=== [BOOTP] BOOTP client exit')
                break
            msgBody = ClientCodeC.collect(data)
            print('<<< [BOOTP] RECV', addr)
            if self.recv_check(msgBody):
                print('=== [BOOTP] This is a valid message')
                your_client_ip = msgBody['yiaddr']
                server_name = msgBody['sname']
                file_name = msgBody['file']
                server_ip = msgBody['siaddr']
                print('=== [BOOTP] Server ip: %s, server name: %s, file name: %s, get ip address: %s' %
                      (server_name, server_ip, file_name, your_client_ip))
                return your_client_ip, file_name
        return None
if __name__ == '__main__':
    client = BOOTPClient()
    client.client_request()