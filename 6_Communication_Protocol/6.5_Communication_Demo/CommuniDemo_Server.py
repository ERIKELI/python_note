from BOOTP.BOOTPServer import BOOTPServer
from TFTP.TFTPServer import TFTPServer
from FTP.FTPServer import FTP_Server

class CommuniDemoServer():
    def __init__(self):
        self.bootp = BOOTPServer()
        self.tftp = TFTPServer()
        self.ftp = FTP_Server()

    def server_start(self):
        self.bootp.start()
        self.tftp.start()
        self.ftp.start()

server = CommuniDemoServer()
server.server_start()