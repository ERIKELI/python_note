from ftplib import FTP
from threading import Thread

class FTP_Client():
    def __init__(self):
        # Info for FTP client
        self.ftp_server = '127.0.0.10'
        self.ftp_port = 21
        # user_name = 'Customer'
        # password = '777777'
        self.user_name = 'Admin'
        self.password = '888888'
        # user_name = ''
        # password = ''

    def ftpConnect(self):
        # Create FTP
        self.ftp = FTP()
        # set ftp debuglevel here, default is 0
        self.ftp.set_debuglevel(1)
        self.ftp.connect(self.ftp_server, self.ftp_port)
        self.ftp.login(self.user_name, self.password)
        print('<<< [FTP] Welcome info:', self.ftp.getwelcome())

    def updateFile(self, updateList):
        bufsize = 1024
        if updateList is not []:
            for up in updateList:
                # Open and read client data that need to be transfer
                file_handler = open('FTP\\FTPClientFile\\copy_%s.py' % up, 'rb')
                # srorbinary need 3 para at least,
                # 1st is STOR+dirName, dirName is the file name that save to server,
                # 2nd is file_handler, open and read the client file data,
                # 3rd is bufsize.
                self.ftp.storbinary('STOR ClientTransfer.py', file_handler, bufsize)

    def downloadFile(self, downList):
        bufsize = 1024
        if downList is not []:
            for down in downList:
                # Create an new file to store data received
                file_handler = open('FTP\\FTPClientFile\\copy_%s' % down, 'wb').write
                # retrbinary need 3 para at least,
                # 1st is RETR+dirName, dirName is target file name,
                # 2nd is a write function, will be called inside function, open a new file in client to store file data,
                # 3rd is bufsize.
                self.ftp.retrbinary('RETR %s' % down, file_handler, bufsize)

    def quit(self):
        self.ftp.quit()

    # This function can get all the contains info in server path
    def showDir(self):
        self.ftp.dir()

    # All this below function should base on the directory set in server.
    # Make a new directory
    def newDir(self, dir='.\\FTPtest'):
        self.ftp.mkd(dir)

    # Change working directory
    def changeDir(self, dir='.\\FTPtest'):
        self.ftp.cwd(dir)

    # Return current working directory(base is '/')
    def presentDir(self):
        self.ftp.pwd()

    # Remove certain directory
    def removeDir(self, dir='.\\FTPtest'):
        self.ftp.rmd(dir)

    # Delete file
    def delFile(self, fileName):
        self.ftp.delete(fileName)

    # Rename file
    def renameFile(self, currName='testFile.py', reName='testFileRename.txt'):
        self.ftp.rename(currName, reName)

    def download(self, file_list):
        self.ftpConnect()
        self.downloadFile(file_list)
        self.quit()

    def upload(self, file_list):
        self.ftpConnect()
        self.updateFile(file_list)
        self.quit()

if __name__ == '__main__':
    ftp_client = FTP_Client()
    ftp_client.downloadFile(['testFile.py', 'testFile.docx', 'testFile.zip', 'testFile.txt'])
    ftp_client.quit()
