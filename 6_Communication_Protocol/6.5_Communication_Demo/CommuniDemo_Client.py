from BOOTP.BOOTPClient import BOOTPClient
from TFTP.TFTPClient import TFTPClient
from FTP.FTPClient import FTP_Client

class CommuniDemoClient():
    def __init__(self):
        self.bootp = BOOTPClient()
        self.tftp = TFTPClient()
        self.ftp = FTP_Client()

    def action(self):
        while True:
            recv = self.bootp.client_request()
            if recv is None:
                print('=== [BOOTP and FTTP] Action done')
                break
            self.tftp.server_ip = recv[0]
            self.tftp.readRequest(recv[1])
        self.ftp.download(['etsw.b3'])
        file_list = []
        try:
            with open('TFTP\\TFTPClientFile\\copy-files4.rc') as f:
                info_line = None
                while info_line != '':
                    info_line = f.readline()
                    info = info_line.strip('\n').split(':')
                    if info != [''] and info != [' ']:
                        file_name = info[1]
                        if file_name != '' and file_name != ' ':
                            file_list.append(file_name.strip(' '))
        except FileNotFoundError:
            print('=== [FTP] Download file no found')
            exit()
        self.ftp.download(file_list)
        print('=== [FTP] Exit')

client = CommuniDemoClient()
client.action()

