from pyftpdlib.servers import FTPServer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.authorizers import DummyAuthorizer

class FTP_Server():
    def __init__(self):
        handler = FTPHandler        # handler is a class(type is 'type')
        ip = '127.0.0.10'
        port = 21
        address = (ip, port)
        self.ftpServer = FTPServer(address, handler)
        self.ftpServer.max_cons = 150           # max connection numbers
        self.ftpServer.max_cons_per_ip = 10     # max connection ip numbers
        print('FTP server created')
        print(self.ftpServer)
        # Change welcome info when client logined in
        self.ftpServer.handler.banner = 'Welcome to my FTP server.'
        # Passive port number should be more than max ip number, otherwise may course connection failed
        self.ftpServer.handler.passive_ports = range(2000, 2333)

        # User info bin
        self.userInfo = {'User_1': {'user_name': 'Admin',
                                    'password': '888888',
                                    'home_path': '.\\FTPServerFile',
                                    'permission': 'elradfmwM',
                                    'msg_login': 'Admin login successful',
                                    'msg_quit': 'Goodbye, admin.'},
                         'User_2': {'user_name': 'Customer',
                                    'password': '777777',
                                    'home_path': '.\\FTPServerFile',
                                    'permission': 'elr',
                                    'msg_login': 'Customer login successful',
                                    'msg_quit': 'Goodbye, customer.'}}

    def addUser(self):
        # Add users method_1
        authorizer = DummyAuthorizer()
        # Add new user, user name, password, home path('.' is current root path), permission level
        for user in self.userInfo.values():
            authorizer.add_user(user['user_name'], user['password'], user['home_path'],
                                perm=user['permission'], msg_login=user['msg_login'], msg_quit=user['msg_quit'])
        self.ftpServer.handler.authorizer = authorizer

        # Add users method_2
        # Mark here: handler.authorizer also generate from DummyAuthorizer inside Handler module
        # for user in self.userInfo.values():
        #    self.ftpServer.handler.authorizer.add_user(user['user_name'], user['password'], user['home_path'],
        #                        perm=user['permission'], msg_login=user['msg_login'], msg_quit=user['msg_quit'])

        # Add anonymous
        authorizer.add_anonymous('.\\FTPServerFile',
                                 perm='elr', msg_login='anonymous login successful', msg_quit='Goodbye, anonymous.')

    def run(self):
        print('FTP server start')
        self.ftpServer.serve_forever()

    def stop(self):
        self.ftpServer.close_all()

ftp_server = FTP_Server()
ftp_server.addUser()
ftp_server.run()
