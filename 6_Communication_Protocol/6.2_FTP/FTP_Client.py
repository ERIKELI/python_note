from ftplib import FTP

class FTP_Client():
    def __init__(self):
        # Info for FTP client
        ftp_server = '127.0.0.10'
        ftp_port = 21
        # user_name = 'Customer'
        # password = '777777'
        user_name = 'Admin'
        password = '888888'
        # user_name = ''
        # password = ''

        # Create FTP
        self.ftp = FTP()
        # set ftp debuglevel here, default is 0
        self.ftp.set_debuglevel(1)
        self.ftp.connect(ftp_server, ftp_port)
        self.ftp.login(user_name, password)
        print('<<< Welcome info:', self.ftp.getwelcome())

    def updateFile(self):
        bufsize = 1024
        updateList = []
        # Open and read client data that need to be transfer
        file_handler = open('FTPClientFile\\testFileCopy.py', 'rb')
        # srorbinary need 3 para at least,
        # 1st is STOR+dirName, dirName is the file name that save to server,
        # 2nd is file_handler, open and read the client file data,
        # 3rd is bufsize.
        self.ftp.storbinary('STOR ClientTransfer.py', file_handler, bufsize)

    def downloadFile(self):
        bufsize = 1024
        downList = ['testFile.py', 'testFile.docx', 'testFile.zip', 'testFile.txt']
        # Create an new file to store data received
        for down in downList:
            file_handler = open('FTPClientFile\\copy_%s' % down, 'wb').write
            # retrbinary need 3 para at least,
            # 1st is RETR+dirName, dirName is target file name,
            # 2nd is a write function, will be called inside function, open a new file in client to store file data,
            # 3rd is bufsize.
            self.ftp.retrbinary('RETR %s' % down, file_handler, bufsize)

    def quit(self):
        self.ftp.quit()

    # This function can get all the contains info in server path
    def showDir(self):
        self.ftp.dir()

    # All this below function should base on the directory set in server.
    # Make a new directory
    def newDir(self, dir='.\\FTPtest'):
        self.ftp.mkd(dir)

    # Change working directory
    def changeDir(self, dir='.\\FTPtest'):
        self.ftp.cwd(dir)

    # Return current working directory(base is '/')
    def presentDir(self):
        self.ftp.pwd()

    # Remove certain directory
    def removeDir(self, dir='.\\FTPtest'):
        self.ftp.rmd(dir)

    # Delete file
    def delFile(self, fileName):
        self.ftp.delete(fileName)

    # Rename file
    def renameFile(self, currName='testFile.py', reName='testFileRename.txt'):
        self.ftp.rename(currName, reName)

ftp_client = FTP_Client()
ftp_client.downloadFile()
ftp_client.quit()
