import binascii
import socket
import struct
import wmi
import random
w = wmi.WMI()

class ServerCodeC:

    @staticmethod
    def offer(transaction_id, client_ip_offer, server_ip, client_mac_id, file_path):
        SERVER_NAME = 'bootpserver'
        VENDOR = ''
        client_mac_id = binascii.unhexlify(client_mac_id.replace(':', ''))
        transaction_id = binascii.unhexlify(transaction_id)
        packet = b''
        packet += b'\x02' # op
        packet += b'\x01' # htype
        packet += b'\x06' # hlen
        packet += b'\x00' # hops
        packet += transaction_id
        packet += b'\x00\x00' # secs
        packet += b'\x80\x00' # flags (broadcast)
        packet += b'\x00\x00\x00\x00' # current client ip
        packet += socket.inet_aton(client_ip_offer) # next current client ip offer
        packet += socket.inet_aton(server_ip) # server ip
        packet += b'\x00\x00\x00\x00' # gateway ip
        packet += client_mac_id # Client mac id #TODO: Change it
        packet += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' # client mac id padding
        packet += SERVER_NAME.encode('utf-8')
        packet += b"\x00"*(64-len(SERVER_NAME))
        packet += file_path.encode('utf-8')
        packet += b"\x00"*(128-len(file_path))
        packet += VENDOR.encode('utf-8')
        packet += b"\x00"*(64-len(VENDOR))
        return packet

    @staticmethod
    def collect(msg):
        msgBody = {}
        m = list(struct.unpack('%dc' % len(msg), msg))
        msgBody['op'] = m[0]
        msgBody['htype'] = m[1]
        msgBody['hlen'] = m[2]
        msgBody['hops'] = m[3]
        msgBody['xid'] = ''.join(['%02x' % ord(x) for x in m[4:8]]) # transaction_id
        msgBody['secs'] = m[8:10]
        msgBody['flags'] = m[10:12]
        msgBody['ciaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[12:16]), msg[12:16]))).rstrip('.')
        msgBody['yiaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[16:20]), msg[16:20]))).rstrip('.')
        msgBody['siaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[20:24]), msg[20:24]))).rstrip('.')
        msgBody['giaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[24:28]), msg[24:28]))).rstrip('.')
        msgBody['chaddr'] = ''.join(['%02x:' % ord(x) for x in m[28:34]])[:-1]  # Client_mac_id (Delete last one ':')
        msgBody['sname'] = msg[44:108].decode('utf-8').strip('\x00')
        msgBody['file'] = msg[108:236].decode('utf-8').strip('\x00')
        msgBody['vend'] = msg[236:300]
        return msgBody

class ClientCodeC():
    transaction_id = None
    client_mac_id = None

    @classmethod
    def get_xid_macid(cls):
        xid = ''
        for i in range(8):
            xid += hex(random.randint(0, 15))[-1]
        cls.transaction_id = xid

        mac_id = []
        for network in w.Win32_NetworkAdapterConfiguration(IPEnabled=1):
            mac_id.append(network.MACAddress)
        cls.client_mac_id = mac_id[0].lower()

    @staticmethod
    def request():
        SERVER_NAME = ''
        VENDER = ''
        transaction_id = binascii.unhexlify(ClientCodeC.transaction_id)
        client_mac_id = binascii.unhexlify(ClientCodeC.client_mac_id.replace(':', ''))

        packet = b''
        packet += b'\x01'  # request op    1   1
        packet += b'\x01'  # htype         2   1
        packet += b'\x06'  # hlen          3   1
        packet += b'\x00'  # hops          4   1
        packet += transaction_id # transaction_id  5-8     4
        packet += b'\x00\x00'  # secs              9-10    2
        # TODO: Add resend time count
        packet += b'\x80\x00'  # flags(broadcast)  11-12   2
        packet += b'\x00\x00\x00\x00'  # client ip 13-16   4
        packet += b'\x00\x00\x00\x00'  # your client ip 17-20   4
        packet += b'\x00\x00\x00\x00'  # server ip 21-24   4
        packet += b'\x00\x00\x00\x00'  # gateway ip 25-28  4
        packet += client_mac_id    # mac id 29-34  6
        packet += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' # mac id placeholder 35-44   10
        packet += b'\x00' * 64     # server name   45-108  64
        packet += b'\x00' * 128    # file name 109-236 128
        packet += VENDER.encode('utf-8')   # vender info 237-300
        packet += b"\x00"*(64-len(VENDER))
        return packet

    @staticmethod
    def collect(msg):
        msgBody = {}
        m = list(struct.unpack('%dc' % len(msg), msg))
        msgBody['op'] = m[0]
        msgBody['htype'] = m[1]
        msgBody['hlen'] = m[2]
        msgBody['hops'] = m[3]
        msgBody['xid'] = ''.join(['%02x' % ord(x) for x in m[4:8]]) # transaction_id
        msgBody['secs'] = m[8:10]
        msgBody['flags'] = m[10:12]
        msgBody['ciaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[12:16]), msg[12:16]))).rstrip('.')
        msgBody['yiaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[16:20]), msg[16:20]))).rstrip('.')
        msgBody['siaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[20:24]), msg[20:24]))).rstrip('.')
        msgBody['giaddr'] = ''.join(str(ord(i)) + '.' for i in list(struct.unpack('%dc' % len(msg[24:28]), msg[24:28]))).rstrip('.')
        msgBody['chaddr'] = ''.join(['%02x:' % ord(x) for x in m[28:34]])[:-1]  # Client_mac_id (Delete last one ':')
        msgBody['sname'] = msg[44:108].decode('utf-8').strip('\x00')
        msgBody['file'] = msg[108:236].decode('utf-8').strip('\x00')
        msgBody['vend'] = msg[236:300]
        return msgBody
