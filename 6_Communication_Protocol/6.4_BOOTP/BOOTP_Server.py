import socket
import struct
import os
from BOOTP_CodeC import ServerCodeC
SERVERNAME = 'Bootpserver'

class BOOTPServer():
    def __init__(self):
        self.server_ip = '127.0.0.1'
        self.offer_ip = '127.0.0.8'
        self.server_port = 68   # Respond client via this port
        self.client_port = 67   # Receive client data via this port
        self.send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.send_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
        self.send_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        #self.send_sock.bind((self.server_ip, self.client_port))
        self.file_bin = ['secondboot.b3', 'files4.rc']

    def requestCheck(self, m):
        if not (
                m['op'] == b'\x01' and # OP
                m['htype'] == b'\x01' and # HTYPE
                m['hlen'] == b'\x06' and # HLEN
                m['hops'] == b'\x00' and # HOPS
                m['secs'] == [b'\x00', b'\x00'] and # SECS
                m['flags'] == [b'\x80', b'\x00'] and   # FLAGS (broadcast)
                m['ciaddr'] == '0.0.0.0' and # Client IP Address
                m['yiaddr'] == '0.0.0.0' and # Your Client IP Address
                m['siaddr'] == '0.0.0.0' and # Server IP
                m['giaddr'] == '0.0.0.0'  # Gateway IP
                ):
            return False
        if not (
                m['sname'] == SERVERNAME or
                m['sname'] == ''):
            return False
        return True

    def server_start(self):
        file_index = 0
        while True:
            if file_index >= len(self.file_bin):
                break
            self.rec_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.rec_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
            self.rec_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
            self.rec_sock.bind(('', self.client_port))
            print('=== Waiting for packet')
            msg, addr = self.rec_sock.recvfrom(1024)
            msgBody = ServerCodeC.collect(msg)
            print('<<< RECV from %s, client ip is %s, xid: %s, mac id: %s' %
                  (addr, msgBody['ciaddr'], msgBody['xid'], msgBody['chaddr']))
            if self.requestCheck(msgBody):
                print('=== This is a valid message')
                offer = ServerCodeC.offer(transaction_id=msgBody['xid'],
                                          client_ip_offer=self.offer_ip,
                                          server_ip=self.server_ip,
                                          client_mac_id=msgBody['chaddr'],
                                          file_path=self.file_bin[file_index])
                self.send_sock.sendto(offer, ('<broadcast>', 68))
                print('>>> SEND ("<broadcast>", 68), offer ip "%s", file name is "%s"' %
                      (self.offer_ip, self.file_bin[file_index]))
                file_index += 1
        print('=== BOOTP server exit')

server = BOOTPServer()
server.server_start()
