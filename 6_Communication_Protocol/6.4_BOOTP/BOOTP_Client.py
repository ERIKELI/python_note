import socket
import binascii
import struct
from BOOTP_CodeC import ClientCodeC

class BOOTPClient():
    def __init__(self):
        self.client_ip = '0.0.0.0'
        self.broadcast_ip = '255.255.255.255'   # or '<broadcast>'
        self.server_ip = '127.0.0.1'
        self.target_port = 67
        self.source_port = 68
        ClientCodeC.get_xid_macid()

    def recv_check(self, m):
        if not (
                m['op'] == b'\x02' and # OP
                m['htype'] == b'\x01' and # HTYPE
                m['hlen'] == b'\x06' and # HLEN
                m['hops'] == b'\x00' and # HOPS
                m['secs'] == [b'\x00', b'\x00'] and # SECS
                m['flags'] == [b'\x80', b'\x00'] and   # FLAGS (broadcast)
                m['xid'] == ClientCodeC.transaction_id and
                m['chaddr'] == ClientCodeC.client_mac_id and
                len(m['file'])
        ):
            return False
        return True

    def client_request(self):

        while True:
            self.client_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.client_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
            self.client_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
            self.client_sock.settimeout(5)
            #self.client_sock.bind((self.server_ip, 68))
            self.client_sock.bind(('', 68))

            self.client_sock.sendto(ClientCodeC.request(), ('<broadcast>', self.target_port))
            print('>>> SEND ("<broadcast>", %d) request command' % self.target_port)
            try:
                data, addr = self.client_sock.recvfrom(1024)
            except socket.timeout:
                print('=== BOOTP client exit')
                break
            msgBody = ClientCodeC.collect(data)
            print('<<< RECV', addr)
            if self.recv_check(msgBody):
                print('=== This is a valid message')
                client_ip = msgBody['yiaddr']
                server_name = msgBody['sname']
                file_name = msgBody['file']
                server_ip = msgBody['siaddr']
                print('=== Server ip: %s, server name: %s, file name: %s, get ip address: %s' %
                      (server_name, server_ip, file_name, client_ip))
client = BOOTPClient()
client.client_request()
