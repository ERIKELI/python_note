import subprocess

# subprocess.run()
print('---------subprocess.run------------')
re = subprocess.run(['python', 'Called_Function.py', 'para_1', 'para_2'])
print('subprocess.run() test returns obj:', re)
print('Return code is: {0}, stdout is: {1}, stderr is: {2}'.format(re.returncode, re.stdout, re.stderr))

# subprocess.call()
print('\n---------subprocess.call------------')
print('subprocess.call() test returns code:', subprocess.call(['python', 'Called_Function.py', 'para_1', 'para_2']))

# subprocess.ckeck_call()
print('\n---------subprocess.check_call------------')
try:
    print('subprocess.check_call() test returns code:', subprocess.check_call(['python', 'Called_Function.py']))
except subprocess.CalledProcessError:
    print('Failed to call.')

# subprocess.getstatusoutput()
print('\n---------subprocess.getstatusoutput------------')
print('subprocess.getstatusoutput() test returns:', subprocess.getstatusoutput(['python', 'Called_Function.py']))

# subprocess.getoutput()
print('\n---------subprocess.getstatusoutput------------')
print('subprocess.getoutput() test returns:', subprocess.getoutput(['python', 'Called_Function.py']))

# subprocess.check_output()
print('\n---------subprocess.check_output------------')
print('subprocess.check_output() test returns:', subprocess.check_output(['python', 'Called_Function.py']))
