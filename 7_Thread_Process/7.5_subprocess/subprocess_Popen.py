import subprocess

prcs = subprocess.Popen(['python', 'Called_Function_Popen.py'],
                        stdout=subprocess.PIPE,
                        stdin=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        universal_newlines=True,
                        shell=True)

print('subprocess pid:', prcs.pid)

re = prcs.communicate('These string are from stdin')
print('\nSTDOUT:', re[0])
print('\nSTDERR:', re[1])
if prcs.poll():
    print('\nThe subprocess has been done')
