from concurrent.futures import ThreadPoolExecutor as tpe
from concurrent.futures import ProcessPoolExecutor as ppe
from time import ctime, sleep
from random import randint

def foo(x, name):
    print('%s%d starting at' % (name, x), ctime())
    sleep(randint(1, 7))
    print('%s%d completed at' % (name, x), ctime())

# Use submit function
print('-----Using submit function-----')
#executor = tpe(7)
#with executor:
with tpe(7) as executor:
    for i in range(5):
        executor.submit(foo, i, 'No.')

# Use map function
print('-----Using map function-----')
with tpe(7) as executor:
    executor.map(foo, range(5), ['No_a.', 'No_b.', 'No_c.', 'No_d.', 'No_e.'])

# TODO: Learn more about ProcessPoolExecutor
"""
with ppe(2) as executor:
    executor.submit(foo, 1, 'No.')
"""
