from random import randint
from time import sleep, ctime
from queue import Queue, LifoQueue
from threading import Thread

COUNT = 0


class MyThread(Thread):
    """
    Bulid up a Module to make this subclass more general
    And get return value by add a function named 'getResult()'
    """
    def __init__(self, func, args, name=''):
        Thread.__init__(self)
        self.name = name
        self.func = func
        self.args = args

    def getResult(self):
        return self.res

    def run(self):
        print('Starting', self.name, 'at:', ctime())
        # Call function here and calculate the running time
        self.res = self.func(*self.args)
        print(self.name, 'finished at:', ctime())


class MyQueue():
    def __init__(self):
        self.funcs = [self.writer, self.reader]
        self.nfuncs = range(len(self.funcs))

    def writeQ(self, queue):
        global COUNT
        print('Producing object OBJ_%d for Q...' % COUNT, end=' ')
        queue.put('OBJ_%d' % COUNT, True)
        print('size now:', queue.qsize())
        COUNT += 1
    
    def readQ(self, queue):
        # If queue is empty, block here until queue available
        val = queue.get(True)
        print('Consumed object %s from Q... size now:' % val, queue.qsize())
    
    def writer(self, queue, loops):
        for i in range(loops):
            self.writeQ(queue)
            sleep(randint(1, 3))
    
    def reader(self, queue, loops):
        for i in range(loops):
            self.readQ(queue)
            sleep(randint(2, 5))
    
    def main(self):
        nloops = randint(2, 5)
        fifoQ = Queue(32)
        lifoQ = LifoQueue(32)
    
        # First In First Out mode for Queue
        print('-----Start FIFO Queue-----')
        threads = []
        for i in self.nfuncs:
            threads.append(MyThread(self.funcs[i], (fifoQ, nloops), self.funcs[i].__name__))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        # Last In First Out mode for LifoQueue
        print('-----Start LIFO Queue-----')
        threads = []
        for i in self.nfuncs:
            threads.append(MyThread(self.funcs[i], (lifoQ, nloops), self.funcs[i].__name__))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
    
        print('All DONE')

if __name__ == '__main__':
     MyQueue().main()
