from Queue_FIFO_LIFO import *

class NewQueue(MyQueue):
    def __init__(self):
        MyQueue.__init__(self)

    def writer(self, queue, loops):
        for i in range(loops):
            self.writeQ(queue)
            sleep(randint(1, 3))
        print('Producing join here, waiting consumer')
        queue.join()
    
    def reader(self, queue, loops):
        for i in range(loops):
            self.readQ(queue)
            sleep(randint(2, 5))
            print('OBJ_%d task done' % i)
            queue.task_done()

    def main(self):
        nloops = randint(2, 5)
        fifoQ = Queue(32)

        print('-----Start FIFO Queue-----')
        threads = []
        for i in self.nfuncs:
            threads.append(MyThread(self.funcs[i], (fifoQ, nloops), self.funcs[i].__name__))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

        print('All DONE')

if __name__ == '__main__':
    NewQueue().main()
