import threading
import time
import random

class MyThread(threading.Thread):
    def __init__(self, count):
        threading.Thread.__init__(self)
        print('%s: There are %d numbers need to be counted' % (self.name, count))
        self.count = count

    def run(self):
        name = self.name
        for i in range(0, self.count):
            print('%s counts %d' % (name, i))
            time.sleep(1)
        print('%s finished' % name)


def main():
    print('-------Starting-------')
    count = random.randint(4, 7)
    t_1 = MyThread(count*count)
    t_2 = MyThread(count)
    t_1.daemon = True
    t_2.daemon = False
    t_1.start()
    t_2.start()
    time.sleep(3)
    print('---Main Thread End---')

if __name__ == '__main__':
    main()
    
