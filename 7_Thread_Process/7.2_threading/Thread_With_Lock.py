import atexit
from random import randrange
import threading
from threading import Thread, Lock
from time import ctime, sleep

class CleanOutputSet(set):
    def __str__(self):
        return ', '.join(x for x in self)

# Use generator is much better than list
# loops = (randrange(1, 7) for x in range(2, 8))
loops = list(randrange(1, 7) for x in range(2, 8))
remaining = CleanOutputSet()
lock = Lock()

def loop(nsec):
    myname = threading.current_thread().name
    with lock:
        remaining.add(myname)
        print('[{0}] Start {1}'.format(ctime(), myname))
    # loops is generator, after 'for' iteration, remains []
    if len(remaining) == len(loops):
        func_for_trial()
    sleep(nsec)
    with lock:
        remaining.remove(myname)
        print('[{0}] Completed {1} ({2} secs)'.format(ctime(), myname, nsec))
        print('    (remaining: {0})'.format(remaining or 'NONE'))

def func_for_trial():
    count = threading.active_count()
    active_thread = threading.enumerate()
    print('There are %d active threads, \n%s' % (count, str(active_thread).replace(', ', '\n')))

def _main():
    threads = []
    for pause in loops:
        threads.append(Thread(target=loop, args=(pause, )))
    for t in threads:
        t.start()
    for t in threads:
        t.join()

@atexit.register
def _atexit():
    print('All DONE at:', ctime())

if __name__ == '__main__':
    _main()
