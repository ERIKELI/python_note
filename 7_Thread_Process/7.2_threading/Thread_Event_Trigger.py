from threading import Thread, Event
import time

evt = Event()
class Mate_1(Thread):
    def run(self):
        print('Mate_1 is running') 
        time.sleep(1)
        print('Mate_1 got the ball')
        time.sleep(1)
        evt.set()
        print('Mate_1 pass the ball to others')
        evt.clear()
        evt.wait()
        time.sleep(1)
        print('Mate_1 got the ball again')
        time.sleep(1)
        print('Mate_1 makes a goal')
        evt.set()

class Mate_2(Thread):
    def run(self):
        print('Mate_2 is running')
        if evt.is_set():
            evt.clear()
        evt.wait()
        time.sleep(1)
        print('Mate_2 got the ball')
        time.sleep(1)
        evt.set()
        print('Mate_2 pass the ball to others')
        time.sleep(1)
        evt.clear()
        evt.wait()
        time.sleep(1)
        print('Mate_2 hugs Mate_1 for congratulation')
if __name__ == '__main__':
    m = Mate_1()
    n = Mate_2()
    m.start()
    n.start()
