from threading import Thread
import time
from time import ctime


class MyThread(Thread):
    """
    Bulid up a Module to make this subclass more general
    And get return value by add a function named 'getResult()'
    """
    def __init__(self, func, args, name=''):
        Thread.__init__(self)
        self.name = name
        self.func = func
        self.args = args

    def getResult(self):
        return self.res

    def run(self):
        print('Starting', self.name, 'at:', ctime())
        # Call function here and calculate the running time
        self.res = self.func(*self.args)
        print(self.name, 'finished at:', ctime())

def fib(x):
    time.sleep(0.005)
    if x < 2:
        return 1
    return (fib(x-2) + fib(x-1))

def fac(x):
    time.sleep(0.1)
    if x < 2:
        return 1
    return (x * fac(x-1))

def sumx(x):
    time.sleep(0.1)
    if x < 2:
        return 1
    return (x + sumx(x-1))

funcs = [fib, fac, sumx]
n = 12

def main():
    nfuncs = range(len(funcs))
    print('***SINGLE THREADS')
    for i in nfuncs:
        print('Starting', funcs[i].__name__, 'at:', ctime())
        print(funcs[i](n))
        print(funcs[i].__name__, 'finished at:', ctime())

    print('\n***MULTIPLE THREADS')
    threads = []
    for i in nfuncs:
        t = MyThread(funcs[i], (n,), funcs[i].__name__)
        threads.append(t)
    for i in nfuncs:
        threads[i].start()
    for i in nfuncs:
        threads[i].join()
        print(threads[i].getResult())
    print('All DONE')

if __name__ == '__main__':
    main()

