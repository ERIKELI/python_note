from threading import Thread, Condition
import time
cond = Condition()

class Waiter(Thread):
    def __init__(self, alarm):
        super(Waiter, self).__init__()
        self.alarm = alarm
        print('Waiter ready')

    def run(self):
        cond.acquire()
        print('Waiter waiting for alarm...')
        cond.wait_for(self.alarm, timeout=1)
        print('Waiter received alarm')
        cond.release()
        print('Waiter completed')

def alarm():
    print('Sending alarm...')
    time.sleep(5)
    print('alarm sent')
    return True

waiter = Waiter(alarm)
waiter.run()
