import atexit
from random import randrange
from threading import Thread, Lock, current_thread # or currentThread
from time import ctime, sleep


# Use set to record the running thread
# If we print set directly, it will shows set([a, b, c])
# So we reload the __str__ function to show it more clean(shows a, b, c as we want)
class CleanOutputSet(set):
    def __str__(self):
        return ', '.join(x for x in self)
    
# randrange(2, 5) will generate a num in range(2, 5)
# for x in range(randrange(3, 7)) will do "for" loop 3-7 times
# Below code generate a list which contains 3-6 numbers in range 2-4
# If not list(), it will return a generator, and it will be null after one time iteration
loops = list((randrange(2, 5) for x in range(randrange(3, 7))))

remaining = CleanOutputSet()
lock = Lock()

def loop_without_lock(nsec):
    myname = current_thread().name
    remaining.add(myname)
    print('[%s] Start %s' % (ctime(), myname))
    sleep(nsec)
    remaining.remove(myname)
    print('[%s] Completed %s (%d secs)' % (ctime(), myname, nsec))
    # Note: remaining or 'NONE', will return 'NONE' if remaining is Null
    # Null including: None, False, (), [], {}, 0
    print('    (remaining: %s)' % (remaining or 'NONE'))

def loop_with_lock(nsec):
    myname = current_thread().name
    # When we need to modifiy public resource, acquire lock to block other threads
    # Lock acquire and release can use 'with lock' to simplify
    lock.acquire()
    remaining.add(myname)
    print('[%s] Start %s' % (ctime(), myname))
    # After using resource, release lock for other threads to use
    lock.release()
    sleep(nsec)

    lock.acquire()
    remaining.remove(myname)
    print('[%s] Completed %s (%d secs)' % (ctime(), myname, nsec))
    print('    (remaining: %s)' % (remaining or 'NONE'))
    lock.release()
    
def _main():
    print('-----Below threads without lock-----')
    threads = []
    for pause in loops:
        threads.append(Thread(target=loop_without_lock, args=(pause, )))
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    print('-----Below threads with lock-----')
    threads = []
    for pause in loops:
        threads.append(Thread(target=loop_with_lock, args=(pause, )))
    for t in threads:
        t.start()
    for t in threads:
        t.join()

# This is an exit function, when script exit, this function will be called
# You can use atexit.register(_atexit) to replace @atexit.register
# The function name '_atexit' can be change to others
@atexit.register
def _atexit():
    print('All DONE at:', ctime())

if __name__ == '__main__':
    _main()
