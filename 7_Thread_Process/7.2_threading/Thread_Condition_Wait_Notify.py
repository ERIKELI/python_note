from threading import Thread, Condition
import time
cond = Condition()

class Sleeper(Thread):
    def __init__(self):
        super(Sleeper, self).__init__()

    def run(self):
        print('Sleeper gets into room, starts sleeping')
        cond.acquire()
        print('Sleeper is sleeping, waiting for wakeup')
        cond.wait()
        print('Sleeper is waked up')
        cond.release()
        print('Sleeper out of room')


class Waker(Thread):
    def __init__(self):
        super(Waker, self).__init__()

    def run(self):
        print('Waker waiting sleeper getting into room and sleeping...')
        time.sleep(1)
        cond.acquire()
        print('Waker gets into the room')
        cond.notify()
        print('Waker trying to wake up sleeper')
        time.sleep(3)
        print('Waker finished wake up, leave the room')
        cond.release()

sleeper = Sleeper()
waker = Waker()
sleeper.start()
waker.start()
