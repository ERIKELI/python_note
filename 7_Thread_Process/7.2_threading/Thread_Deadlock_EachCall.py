from threading import Thread, Lock, RLock, current_thread
import time

mutex_1 = Lock()
mutex_2 = Lock()
reentrant_1 = RLock()
reentrant_2 = RLock()
COUNT = 0
class MyThread(Thread):
    def __init__(self, lock_1, lock_2):
        Thread.__init__(self)
        self.lock_1 = lock_1
        self.lock_2 = lock_2

    def run(self):
        self.name = current_thread().name
        if self.lock_1.acquire():
            print('%s got its first lock' % self.name)
            global COUNT
            COUNT += 1
            print('%s make COUNT plus one, now COUNT is %d' % (self.name, COUNT))
            time.sleep(2)
            print('%s trying to get another one...' % self.name)
            if self.lock_2.acquire():
                print('%s got its second lock' % self.name)
                self.lock_2.release()
                print('%s release its second lock' % self.name)
            self.lock_1.release()
            print('%s release its first lock' % self.name)

threads = [MyThread(mutex_1, mutex_2), MyThread(mutex_2, mutex_1)]
#threads = [MyThread(reentrant_1, reentrant_2), MyThread(reentrant_2, reentrant_1)]
for t in threads:
    t.start()
for t in threads:
    t.join()
