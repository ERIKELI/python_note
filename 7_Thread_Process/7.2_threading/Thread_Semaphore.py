from atexit import register
from random import randrange
from threading import Semaphore, BoundedSemaphore, Lock, Thread
from time import sleep, ctime
import threading

"""
# This Obj reload the __len__ method to return current number of semaphore
class MySemaphore(BoundedSemaphore):
    def __len__(self):
        return self._value
candy = MySemaphore(5)
print(len(candy))
"""
lock = Lock()
MAX = 5

def refill():
    lock.acquire()
    print('Refilling candy...')
    try:
        candyTray.release()
    except ValueError:
        print('Full, skipping')
    else:
        print('OK, current candy num is %d' % candyTray._value)
    lock.release()

def buy():
    lock.acquire()
    print('Buying candy...')
    if candyTray.acquire(False):
        print('OK, current candy num is %d' % candyTray._value)
    else:
        print('Empty, skipping')
    lock.release()

def producer(loops):
    for i in range(loops):
        refill()
        sleep(randrange(3))

def consumer(loops):
    for i in range(loops):
        buy()
        sleep(randrange(3))

def _main():
    print('Starting at', ctime())
    nloops = randrange(2, 6)
    print('THE CANDY MACHINE (full with %d bars)!' % MAX)
    # Buyer Thread
    buyer = Thread(target=consumer, args=(randrange(nloops, nloops+MAX+2), ))
    buyer.start()
    # Vendor Thread
    vendor = Thread(target=producer, args=(nloops, ))
    vendor.start()
    for t in [buyer, vendor]:
        t.join()

@register
def _atexit():
    print('All DONE at:', ctime())

if __name__ == '__main__':
    print('-------BoundedSemaphore-------')
    candyTray = BoundedSemaphore(MAX)
    _main()
    print('-------Semaphore------')
    candyTray = Semaphore(MAX)
    _main()

    
