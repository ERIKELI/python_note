from threading import Thread, Barrier
import random
import time
COUNT = 0

def action():
    print('ACTION')

def foo():
    global COUNT
    time.sleep(random.randint(1, 7))
    print('Barrier_%d waiting' % COUNT)
    COUNT += 1
    barr.wait()
    print('Hello, world', COUNT)

barr = Barrier(7, action=action, timeout=20)

threads = []
for i in range(7):
    threads.append(Thread(target=foo))
for t in threads:
    t.start()
for t in threads:
    t.join()

print('Pass barrier')

