from threading import Thread, Lock
import time
lock = Lock()
COUNT = 0

def gentleCounter():
    name = 'gentleCounter'
    lock.acquire()
    print('%s acquired the lock' % name)
    global COUNT
    COUNT += 1
    print('%s made a count plus, now the COUNT is %d' % (name, COUNT))
    print('%s is taking a rest...' % name)
    time.sleep(3)
    COUNT += 1
    print('%s made a count plus again, now the COUNT is %d' % (name, COUNT))
    lock.release()
    print('%s released the lock' % name)

def wildCounter():
    time.sleep(1)
    name = 'wildCounter'
    print('%s didn\'t acquire the lock' % name)
    global COUNT
    COUNT += 1
    print('%s made a count plus, now the COUNT is %d' % (name, COUNT))

Thread(target=gentleCounter).start()
Thread(target=wildCounter).start()
