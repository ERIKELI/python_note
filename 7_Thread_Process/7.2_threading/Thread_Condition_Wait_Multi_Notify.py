from threading import Thread, Condition
import random
import time
cond = Condition()

class Sleeper(Thread):
    def __init__(self):
        super(Sleeper, self).__init__()
        self.num = self.name[-1]
        print('Sleeper_%s ready' % self.num)

    def run(self):
        print('Sleeper_%s gets into room, starts sleeping' % self.num)
        cond.acquire()
        print('Sleeper_%s is sleeping, waiting for wakeup' % self.num)
        cond.wait()
        print('Sleeper_%s is waked up' % self.num)
        cond.release()
        print('Sleeper_%s out of room' % self.num)


class Waker(Thread):
    def __init__(self, all=False):
        super(Waker, self).__init__()
        self.all = all
        self.num = self.name[-1]
        print('Waker_%s ready' % self.num)

    def run(self):
        print('Waker_%s waiting sleeper getting into room and sleeping...' % self.num)
        time.sleep(1)
        cond.acquire()
        print('Waker_%s gets into the room' % self.num)
        if self.all:
            cond.notify_all()
        else:
            cond.notify()
        print('Waker_%s trying to wake up sleeper' % self.num)
        time.sleep(3)
        print('Waker_%s finished wake up, leave the room' % self.num)
        cond.release()

if __name__ == '__main__':
    print('-------Order Wake-------')
    sleepers = []
    wakers = []
    for i in range(3):
        sleepers.append(Sleeper())
    for i in range(3):
        wakers.append(Waker())
    
    random.shuffle(sleepers)
    random.shuffle(wakers)
    
    for s in sleepers:
        s.start()
    for s in wakers:
        s.start()
    for s in sleepers:
        s.join()
    
    print('-------All Wake-------')
    sleepers = []
    for i in range(3):
        sleepers.append(Sleeper())
    waker = Waker(all=True)

    random.shuffle(sleepers)

    for s in sleepers:
        s.start()
    waker.start()
