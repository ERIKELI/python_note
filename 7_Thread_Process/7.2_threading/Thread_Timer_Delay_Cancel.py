from threading import Timer
from time import ctime, sleep

def func():
    print('Current time is', ctime(), 'Hello world')

timex = Timer(3, func)
print('Current time is', ctime())
timex.start()
timex.join()

timex = Timer(3, func)
print('Current time is', ctime())
timex.start()
sleep(1)
timex.cancel()
print('End')
