from threading import Thread, Lock, RLock, current_thread


mutex = Lock()
reentrant = RLock()
class MyThread(Thread):
    def __init__(self, lock):
        Thread.__init__(self)
        self.lock = lock

    def run(self):
        self.name = current_thread().name
        print('-------This is %s-------' % self.name)
        if self.lock.acquire():
            print('%s get lock one' % self.name, '\nTrying to get second lock')
            self.lock.acquire()
            print('Got second lock')
            print('Trying to release lock...')
            self.lock.release()
            print('First lock released')
            self.lock.release()
            print('Second lock released')
            print('Lock all released')
        print('--------Exit %s---------' %
              self.name)

t = MyThread(reentrant)
t.start()
t.join()
t = MyThread(mutex)
t.start()
t.join()
