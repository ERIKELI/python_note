from threading import Lock, RLock

def call():
    print('This is call() function')
    with lock:
        g()
        h()

def g():
    if not lock.acquire(True, 1):
        print('g() acquires lock failed')
    else:
        print('This is g() function')
        lock.release()
    h()

def h():
    if not lock.acquire(True, 1):
        print('h() acquires lock failed')
    else:
        print('This is h() function')
        lock.release()

print('\n-------Using Lock-------')
lock = Lock()
call()
print('\n-------Using RLock-------')
lock = RLock()
call()
