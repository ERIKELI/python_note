# There are three ways to create a thread
# The first is create a thread instance, and pass a function
# The second one is create a thread instance, and pass the callable instance(obj)
# The third one is create a subclass of Thread, then generate an instance

# Method one: pass function
from threading import Thread
from time import sleep, ctime

loops = [4, 2]

def loop(nloop, nsec):
    print('start loop', nloop, 'at', ctime())
    sleep(nsec)
    print('loop', nloop, 'done at:', ctime())

def main():
    print('starting at:', ctime())
    threads = []
    nloops = range(len(loops))

    # Create all threads
    for i in nloops:
        t = Thread(target=loop, args=(i, loops[i]))
        threads.append(t)
    for i in threads:
        i.start()
    for i in threads:
        i.join()
    print('All DONE at:', ctime())
    
if __name__ == '__main__':
    main()

