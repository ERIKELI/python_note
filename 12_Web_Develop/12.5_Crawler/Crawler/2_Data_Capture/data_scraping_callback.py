#!/usr/bin/python
# =============================================================
# File Name: data_scraping_callback.py
# Author: LI Ke
# Created Time: 2/24/2018 15:30:11
# =============================================================


import re
import lxml.html
import csv
import time
from urllib import request, parse


class ScrapeCallback:
    def __init__(self):
        self.writer = csv.writer(open('countries.csv', 'w'))
        self.fields = ('area', 'population', 'iso', 'country', 'capital', 'continent', 'tld',
          'currency_code', 'currency_name', 'phone', 'postal_code_format',
          'postal_code_regex', 'languages', 'neighbours',)
        self.writer.writerow(self.fields)

    def __call__(self, url, html):
        if re.search('/view/', url):
            tree = lxml.html.fromstring(html)
            row = []
            for field in self.fields:
                row.append(tree.cssselect('table > tr#places_%s__row > td.w2p_fw' % field)[0].text_content())
            self.writer.writerow(row)


def link_crawler(seed_url, link_regex, user_agent='wswp', proxy=None, max_depth=2, scrape_callback=None):
    """Avoid seen url and over depth"""
    crawl_queue = [seed_url]
    # Keep track which URL's have seen before
    max_depth = max_depth
    seen = {}
    while crawl_queue:
        url = crawl_queue.pop()
        time.sleep(1)
        html = download(url, user_agent=user_agent, proxy=proxy)
        # Get current url depth
        depth = seen.get(url, 0)
        if depth != max_depth:
            for link in get_links(html):
                # Check if link matches expected regex
                if re.match(link_regex, link):
                    # Form absolute link
                    link = parse.urljoin(seed_url, link)
                    # Check if have already seen this link
                    if link not in seen:
                        # New url, depth add 1 based on current depth
                        seen[link] = depth + 1
                        crawl_queue.append(link)
                        # Data save
                        if scrape_callback:
                            time.sleep(1)
                            scrape_callback(link, download(link))


def get_links(html):
    """Return a list of links from html"""
    webpage_regex = re.compile('<a[^>]+href=["\'](.*?)["\']', re.IGNORECASE)
    return webpage_regex.findall(html)


def download(url, num_retries=2, user_agent='wswp', proxy=None):
    """Download function with agent and proxy"""
    print('Downloading:', url)

    # Set proxy
    if proxy:
        proxy = request.ProxyHandler(proxy)
        opener = request.build_opener(proxy, request.HTTPHandler)
        request.install_opener(opener)

    # Add user agent
    headers = {'User-agent': user_agent}
    agent_request = request.Request(url, headers=headers)

    try:
        html = request.urlopen(agent_request).read().decode(errors='ignore')
    except request.URLError as e:
        print('Download error:', e.reason)
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code <= 600:
                # recursively retry 5xx HTTP errors
                return download(url, num_retries-1)
    return html

seed_url = 'http://example.webscraping.com/index'
link_regex = r'/places/default/(index|view)'
link_crawler(seed_url, link_regex, max_depth=1, scrape_callback=ScrapeCallback())
