#!/usr/bin/python
# =============================================================
# File Name: data_scraping.py
# Author: LI Ke
# Created Time: 2/24/2018 13:05:29
# =============================================================


# Three kind of scraping method
# 1 - re
# 2 - beautiful soup
# 3 - lxml

# BeautifulSoup
# pip install beautifulsoup4
print('--------------------------------------------')
from bs4 import BeautifulSoup
broken_html = '<ul class=country><li>Area<li>Population</ul>'
soup = BeautifulSoup(broken_html, features='html5lib')   # html.parser can not fix the html well
# BeautifulSoup can prettify the broken HTML 
fixed_html = soup.prettify()
print(fixed_html)
'''
<html>
    <head>
    </head>
    <body>
        <ul class="country">
            <li>
                Area
            </li>
            <li>
                Population
            </li>
        </ul>
    </body>
</html>
'''
ul = soup.find('ul', attrs={'class': 'country'})
# return first match
print(ul.find('li'))        # <li>Area</li>
# return all matches
print(ul.find_all('li'))    # [<li>Area</li>, <li>Population</li>]

# Use beautiful soup to scraping area data
from bs4 import BeautifulSoup
# url = 'http://example.webscraping.com/places/default/view/China-47'
with open('China-47.html', 'r') as f:
    html = f.read()
soup = BeautifulSoup(html, features='html5lib')
tr = soup.find(attrs={'id': 'places_area__row'})
td = tr.find(attrs={'class': 'w2p_fw'})     # locate the area tag
area = td.text  # extract the text from this tag
print(area)     # 9,596,960 square kilometres

# Lxml
# pip install lxml
print('--------------------------------------------')
import lxml.html
broken_html = '<ul class=country><li>Area<li>Population</ul>'
tree = lxml.html.fromstring(broken_html)    # parse the HTML
fixed_html = lxml.html.tostring(tree, pretty_print=True).decode()
print(fixed_html)
'''
<ul class="country">
<li>Area</li>
<li>Population</li>
</ul>
'''

# Use lxml css selector
# pip install cssselect
with open('China-47.html', 'r') as f:
    html = f.read()
tree = lxml.html.fromstring(html)
# CSS selector: 
# Link: http://www.w3school.com.cn/cssref/css_selectors.asp

# .class (.w2p_fw) -> select all tags where class='w2p_fw'
# tag.class (td.w2p_fw) -> select td tags where class='w2p_fw'
# #id (#places_area__row) -> select all tags where id='places_area_row'
# tag#id (tr#places_area__row) -> select tr tags where id='places_area_row'
# tag > sub-tag (tr > td) -> select td tags where the upper tag is tr

# Note: inside lxml, css selector is changed into XPath in actually
td = tree.cssselect('tr#places_area__row > td.w2p_fw')[0]
area = td.text_content()
print(area)     # 9,596,960 square kilometres
