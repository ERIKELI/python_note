#!/usr/bin/python
# =============================================================
# File Name: data_scraping_compare.py
# Author: LI Ke
# Created Time: 2/24/2018 14:24:13
# =============================================================


# Basic method
FIELDS = ('area', 'population', 'iso', 'country', 'capital', 'continent', 'tld',
          'currency_code', 'currency_name', 'phone', 'postal_code_format',
          'postal_code_regex', 'languages', 'neighbours',)

import re
def re_scraper(html):
    results = {}
    for field in FIELDS:
        results[field] = re.search('<tr id="places_%s__row">.*?<td class="w2p_fw">(?P<item>.*?)</td>' % field, html).group('item')
    return results

from bs4 import BeautifulSoup
def bs_scraper(html):
    soup = BeautifulSoup(html, 'html5lib')
    results = {}
    for field in FIELDS:
        results[field] = soup.find('table').find('tr', id='places_%s__row' % field).find('td', class_='w2p_fw').text
    return results

import lxml.html
def lxml_scraper(html):
    tree = lxml.html.fromstring(html)
    results = {}
    for field in FIELDS:
        results[field] = tree.cssselect('table > tr#places_%s__row > td.w2p_fw' % field)[0].text_content()
    return results

# Compare with three methods
import time

NUM_ITERATIONS = 1000   # number of times to test each scraper
with open('China-47.html', 'r') as f:
    html = f.read()

for name, scraper in [('Regular expressions', re_scraper),
                      ('BeautifulSoup', bs_scraper),
                      ('Lxml', lxml_scraper)]:
    # Record start time of scrape
    start = time.time()
    for i in range(NUM_ITERATIONS):
        if scraper is re_scraper:
            re.purge()
        result = scraper(html)
    # Check scraped result is as expected
    assert (result['area'] == '9,596,960 square kilometres')
    # Record end time of scrape and output the total
    end = time.time()
    print('%s: %.2f seconds' % (name, end-start))

# >>>
# Regular expressions: 3.36 seconds
# BeautifulSoup: 35.16 seconds
# Lxml: 4.52 seconds
