#!/usr/bin/python
# =============================================================
# File Name: about_crawler.py
# Author: LI Ke
# Created Time: 2/23/2018 18:57:24
# =============================================================


baidu = 'http://www.baidu.com'
douban = 'http://www.douban.com'

# Website technology type
print('---------------------------')
import builtwith

print(builtwith.parse(baidu))
# {'javascript-frameworks': ['jQuery']}
print(builtwith.parse(douban))
# {'analytics': ['Piwik'], 'javascript-frameworks': ['jQuery'], 'tag-managers': ['Google Tag Manager']}

# Website owner
print('---------------------------')
import whois

print(whois.whois(baidu))
print(whois.whois(douban))

# First crawler
print('---------------------------')
from urllib import request


def download(url, num_retries=2):
    """Original download function"""
    print('Downloading:', url)
    try:
        html = request.urlopen(url).read().decode()
    except request.URLError as e:
        print('Download error:', e.reason)
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code <= 600:
                # recursively retry 5xx HTTP errors
                return download(url, num_retries-1)
    return html
print(download('http://httpstat.us/500'))

# User agent
print('---------------------------')
from urllib import request
def download(url, num_retries=2, user_agent='wswp'):
    """Download function with agent"""
    print('Downloading:', url)
    headers = {'User-agent': user_agent}
    agent_request = request.Request(url, headers=headers)
    try:
        html = request.urlopen(agent_request).read().decode()
    except request.URLError as e:
        print('Download error:', e.reason)
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code <= 600:
                # recursively retry 5xx HTTP errors
                return download(url, num_retries-1)
    return html
print(download('http://httpstat.us/500'))

# Get all links from sitemap
print('---------------------------')
import re
import time
def crawl_sitemap(url, max=0):
    # Download the sitemap file
    sitemap = download(url)
    # Extract the sitemap links
    links = re.findall('<loc>(.*?)</loc>', sitemap)
    if max:
        links = links[:max]
    # Download each link
    for link in links:
        time.sleep(1)
        # Sitemap link is not available
        html = download(link.replace('web-scraping.appspot.com', 'example.webscraping.com'))
        # Scrape html here
        pass
crawl_sitemap('http://example.webscraping.com/places/default/sitemap.xml', 7)

# ID traverse crawler
print('---------------------------')
import itertools

def id_traverse():
    # Maximum number of consecutive download errors allowed
    max_errors = 5
    # Current number of consecutive download errors
    num_errors = 0
    for page in itertools.count(1):
        url = 'http://example.webscraping.com/view/-%d' % page
        time.sleep(1)
        html = download(url)
        if html is None:
            # Received an error trying to download this webpage
            num_errors += 1
            if num_errors == max_errors:
                # Reached maximum number of error
                break
        else:
            # Success - can scrape the result
            num_errors = 0
            pass
id_traverse()

# URL traverse crawler
print('---------------------------')
from urllib import parse

def link_crawler(seed_url, link_regex):
    """Ignore seen url"""
    crawl_queue = [seed_url]
    # Keep track which URL's have seen before
    seen = set(crawl_queue)
    while crawl_queue:
        url = crawl_queue.pop()
        time.sleep(1)
        html = download(url)
        for link in get_links(html):
            # Check if link matches expected regex
            if re.match(link_regex, link):
                # Form absolute link
                link = parse.urljoin(seed_url, link)
                # Check if have already seen this link
                if link not in seen:
                    seen.add(link)
                    crawl_queue.append(link)

def get_links(html):
    """Return a list of links from html"""
    webpage_regex = re.compile('<a[^>]+href=["\'](.*?)["\']', re.IGNORECASE)
    return webpage_regex.findall(html)

link_crawler('http://example.webscraping.com', r'/places/default/(index|view)')

# Robots.txt handle
print('---------------------------')
from urllib import robotparser
rp = robotparser.RobotFileParser()
# rp.set_url('http://example.webscraping.com/robots.txt')
rp.set_url('http://www.baidu.com/robots.txt')
rp.read()
# url = 'http://example.webscraping.com'
url = 'http://www.baidu.com'
user_agent = 'BadCrawler'
print(rp.can_fetch(user_agent, url))    # False
user_agent = 'Baiduspider'
print(rp.can_fetch(user_agent, url))    # True

# Set proxy
print('---------------------------')
from urllib import request

def download(url, num_retries=2, user_agent='wswp', proxy=None):
    """Download function with agent and proxy"""
    print('Downloading:', url)

    # Set proxy
    if proxy:
        proxy = request.ProxyHandler(proxy)
        opener = request.build_opener(proxy, request.HTTPHandler)
        request.install_opener(opener)

    # Add user agent
    headers = {'User-agent': user_agent}
    agent_request = request.Request(url, headers=headers)

    try:
        html = request.urlopen(agent_request).read().decode(errors='ignore')
    except request.URLError as e:
        print('Download error:', e.reason)
        html = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code <= 600:
                # recursively retry 5xx HTTP errors
                return download(url, num_retries-1)
    return html

proxy = {'http': 'http://www-proxy.ericsson.se:8080'}
download('http://www.google.com', proxy=proxy)

# Download frequency limit
print('---------------------------')
import time
import datetime
from urllib import parse

class Throttle:
    """Add a delay between downloads to the same domain"""
    def __init__(self, delay):
        # Amount of delay between downloads for each domain
        self.delay = delay
        # timestamp of when a domain was last accessed
        self.domains = {}

    def wait(self, url):
        # Get domain(net location) from url
        domain = parse.urlparse(url).netloc
        last_accessed = self.domains.get(domain, None)

        if self.delay > 0 and last_accessed is not None:
            sleep_secs = self.delay - (datetime.datetime.now() - last_accessed).seconds
            if sleep_secs > 0:
                # Domain has been accessed recently
                # So need to sleep
                print('Wait throttle limit...')
                time.sleep(sleep_secs)
        # Update the last accessed time
        self.domains[domain] = datetime.datetime.now()

url = 'http://www.baidu.com'
throttle = Throttle(3)

throttle.wait(url)
download(url)

throttle.wait(url)
download(url)

# Avoid crawler trap (over depth)
print('---------------------------')
# Finally one
def link_crawler(seed_url, link_regex, user_agent='wswp', proxy=None, max_depth=2):
    """Avoid seen url and over depth"""
    crawl_queue = [seed_url]
    # Keep track which URL's have seen before
    max_depth = max_depth
    seen = {}
    while crawl_queue:
        url = crawl_queue.pop()
        time.sleep(1)
        html = download(url, user_agent=user_agent, proxy=proxy)
        # Get current url depth
        depth = seen.get(url, 0)
        if depth != max_depth:
            for link in get_links(html):
                # Check if link matches expected regex
                if re.match(link_regex, link):
                    # Form absolute link
                    link = parse.urljoin(seed_url, link)
                    # Check if have already seen this link
                    if link not in seen:
                        # New url, depth add 1 based on current depth
                        seen[link] = depth + 1
                        crawl_queue.append(link)

seed_url = 'http://example.webscraping.com/index'
link_regex = r'/places/default/(index|view)'
link_crawler(seed_url, link_regex, max_depth=1)
