#!/usr/bin/python
# =============================================================
# File Name: disk_cache.py
# Author: LI Ke
# Created Time: 2/24/2018 16:16:15
# =============================================================
import os
import re
import time
import datetime
import random
import pickle
import zlib
from urllib import parse, request


class Downloader:
    def __init__(self, delay=5, user_agent='wswp', proxies=None, num_retries=1, cache=None):
        self.throttle = Throttle(delay)
        self.user_agent = user_agent
        self.proxies = proxies
        self.num_retries = num_retries
        self.cache = cache

    def __call__(self, url):
        result = None
        if self.cache:
            try:
                result = self.cache[url]
            except KeyError:
                # url is not available in cache
                pass
            else:
                if self.num_retries > 0 and 500 <= result['code'] < 600:
                    # Server error so ignore result from cache and re-download
                    result = None
        if result is None:
            # result was not loaded from cache
            # so still need to download
            self.throttle.wait(url)
            proxy = random.choice(self.proxies) if self.proxies else None
            headers = {'User-agent': self.user_agent}
            result = self.download(url, headers, proxy, self.num_retries)
            if self.cache:
                # Save result to cache
                self.cache[url] = result
        return result['html']

    def download(self, url, headers, proxy, num_retries):
        """Download function with agent and proxy"""
        print('Downloading:', url)
        code = 200
    
        # Set proxy
        if proxy:
            proxy = request.ProxyHandler(proxy)
            opener = request.build_opener(proxy, request.HTTPHandler)
            request.install_opener(opener)
    
        # Add user agent
        agent_request = request.Request(url, headers=headers)
    
        try:
            html = request.urlopen(agent_request).read().decode(errors='ignore')
        except request.URLError as e:
            print('Download error:', e.reason)
            html = None
            if num_retries > 0:
                if hasattr(e, 'code'):
                    code = e.code
                if 500 <= code <= 600:
                    # recursively retry 5xx HTTP errors
                    return self.download(url, num_retries-1)
        return {'html': html, 'code': code}


class Throttle:
    """Add a delay between downloads to the same domain"""
    def __init__(self, delay):
        # Amount of delay between downloads for each domain
        self.delay = delay
        # timestamp of when a domain was last accessed
        self.domains = {}

    def wait(self, url):
        # Get domain(net location) from url
        domain = parse.urlparse(url).netloc
        last_accessed = self.domains.get(domain, None)

        if self.delay > 0 and last_accessed is not None:
            sleep_secs = self.delay - (datetime.datetime.now() - last_accessed).seconds
            if sleep_secs > 0:
                # Domain has been accessed recently
                # So need to sleep
                print('Wait throttle limit...')
                time.sleep(sleep_secs)
        # Update the last accessed time
        self.domains[domain] = datetime.datetime.now()


class DiskCache:
    def __init__(self, cache_dir='cache', expires=30):
        self.cache_dir = cache_dir
        self.expires = datetime.timedelta(days=expires)

    def url_to_path(self, url):
        """Create file system path for URL"""
        components = parse.urlsplit(url)
        # Append index.html to empty paths
        path = components.path
        if not path:
            path = '/index.html'
        elif path.endswith('/'):
            path += 'index.html'
        else:
            path += '.html'
        filename = components.netloc + path + components.query
        # Replace invalid characters
        filename = re.sub('[^/0-9a-zA-Z\-.,;_ ]', '_', filename)
        # Restrict maximum number of characters
        filename = '/'.join(segment[:255] for segment in filename.split('/'))
        return os.path.join(self.cache_dir, filename)

    def __getitem__(self, url):
        """Load data from disk for this URL"""
        path = self.url_to_path(url)
        if os.path.exists(path):
            with open(path, 'rb') as fp:
                result, timestamp = pickle.loads(zlib.decompress(fp.read()))
                # Check expired time
                if self.has_expired(timestamp):
                    raise KeyError(url + ' has expired')
                return result
        else:
            # URL has not yet been cached
            raise KeyError(url + ' does not exist')

    def __setitem__(self, url, result):
        """Save data to disk for this URL"""
        path = self.url_to_path(url)
        folder = os.path.dirname(path)
        if not os.path.exists(folder):
            os.makedirs(folder)
        timestamp = datetime.datetime.utcnow()
        with open(path, 'wb') as fp:
            # Use zlib to compress data, saving the disk space
            # But the text will be unreadable in files
            fp.write(zlib.compress(pickle.dumps((result, timestamp))))

    def has_expired(self, timestamp):
        """Return whether this timestamp has expired"""
        return datetime.datetime.utcnow() > timestamp + self.expires

def get_links(html):
    """Return a list of links from html"""
    webpage_regex = re.compile('<a[^>]+href=["\'](.*?)["\']', re.IGNORECASE)
    return webpage_regex.findall(html)


def timeit(f):
    def _deco(*args, **kwargs):
        start = time.time()
        r = f(*args, **kwargs)
        end = time.time()
        print(f.__name__, 'costs %s sec' % (end-start))
        return r
    return _deco


@timeit
def link_crawler(seed_url, link_regex, user_agent='wswp', proxy=None, max_depth=2, scrape_callback=None, expires=30):
    """Avoid seen url and over depth"""
    crawl_queue = [seed_url]
    # Keep track which URL's have seen before
    max_depth = max_depth
    seen = {}
    down = Downloader(delay=1, user_agent=user_agent, proxies=proxy, cache=DiskCache(expires=expires))
    while crawl_queue:
        url = crawl_queue.pop()
        # time.sleep(1)
        html = down(url)
        # Get current url depth
        depth = seen.get(url, 0)
        if depth != max_depth:
            for link in get_links(html):
                # Check if link matches expected regex
                if re.match(link_regex, link):
                    # Form absolute link
                    link = parse.urljoin(seed_url, link)
                    # Check if have already seen this link
                    if link not in seen:
                        # New url, depth add 1 based on current depth
                        seen[link] = depth + 1
                        crawl_queue.append(link)
                        # Call back function
                        if scrape_callback:
                            # time.sleep(1)
                            scrape_callback(link, down(link))


if __name__ == '__main__':
    import os
    import shutil
    seed_url = 'http://example.webscraping.com/index'
    link_regex = r'/places/default/(index|view)'
    
    # Clean cache
    if os.path.exists('cache\\'):
        shutil.rmtree('cache\\')
    print('No cache:')
    link_crawler(seed_url, link_regex, max_depth=1)
    print('With cache:')
    link_crawler(seed_url, link_regex, max_depth=1)
    # No cache:
    # link_crawler costs 19.002000093460083 sec
    # With cache:
    # link_crawler costs 0.00800013542175293 sec

    # Expires time is 0 day, re-download
    link_crawler(seed_url, link_regex, max_depth=1, expires=0)
