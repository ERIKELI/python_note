from suds.client import Client
class BarApixServiceException(Exception):
    pass
class BarApixWrongUser(Exception):
    pass
class BarApixIDNotFoundException(Exception):
    pass


class Service():
    def __init__(self, url, userid, password, barApix_id, timeout=0):
        self.client = None
        self.userid = userid
        self.password = password
        self.barApix_id = barApix_id

        try:
            if timeout == 0:
                self.client = Client(url=url)
            else:
                self.client = Client(url=url, timeout=timeout)
        except:
            raise BarApixServiceException
        # Show the function of service 
        # print(self.client)
        self.login()

    def login(self):
        try:
            response = self.client.service.WS_XStartup(self.userid, self.password)
            # print(response)
            if response['WS_XStartupResult'] == 0:
                raise BarApixWrongUser
            print('=== Login in success')
        except:
            raise BarApixServiceException

    def get_id_info(self):
        try:
            response = self.client.service.WS_XGetIdInfoBaso(self.userid, self.barApix_id)
            if response['WS_XGetIdInfoBasoResult'] == 0:
                raise BarApixIDNotFoundException
        except:
            raise BarApixServiceException
        return_response = {}
        for r in response:
            if isinstance(r[1], str):
                return_response[str(r[0])] = str(r[1].strip())
            else:
                # Handle None value
                return_response[str(r[0])] = r[1]
        print(return_response)
        return return_response

    def get_test_status(self):
        try:
            response = self.client.service.WS_XGetTestStatus(self.userid, self.barApix_id)
            if response['WS_XGetTestStatusResult'] == 0:
                raise BarApixIDNotFoundException
        except:
            raise BarApixServiceException
        return_response = response
        return return_response

info = {'service_1': {'userid': 'module',
                      'password': 'welcome',
                      'barApix_id': ['D825437451'],
                      'url': 'http://encbartrack.cn.ao.ericsson.se/BarApix/BarApixService/?wsdl'
                      },
        'service_2': {'userid': 'pcclient',
                      'password': 'pcclient1',
                      'barApix_id': ['C829151132'],
                      'url': 'http://eseklmw0019.eemea.ericsson.se/BarApix/BarApixService/?wsdl'
                      },
        }

user = info['service_1']
service = Service(user['url'], user['userid'], user['password'], user['barApix_id'][0])
# More Function please check 'barApixFunc.txt'
service.get_id_info()
service.get_test_status()
