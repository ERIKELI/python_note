from html.parser import HTMLParser

# An HTMLParser instance is fed HTML data and calls handler methods when start tags, end tags, text, comments, and other markup elements are encountered
# Subclass HTMLParser and override its methods to implement the desired behavior

class MyHTMLParser(HTMLParser):
    # attrs is the attributes set in HTML start tag
    def handle_starttag(self, tag, attrs):
        print('Encountered a start tag:', tag)
        for attr in attrs:
            print('     attr:', attr)

    def handle_endtag(self, tag):
        print('Encountered an end tag :', tag)

    def handle_data(self, data):
        print('Encountered some data  :', data)

parser = MyHTMLParser()
parser.feed('<html><head><title>Test</title></head>'
            '<body><h1>Parse me!</h1></body></html>'
            '<img src="python-logo.png" alt="The Python logo">')
