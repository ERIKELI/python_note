from urllib import parse

# urlparse --> urllib.parse since python 3.0

# urlparse
print(parse.urlparse('http://blog.csdn.net/hxsstar/article/details/17240975'))
# ParseResult(scheme='http', netloc='blog.csdn.net', path='/hxsstar/article/details/17240975', params='', query='', fragment='')
print(parse.urlparse('http://www.python.org/doc/FAQ.html'))
# ParseResult(scheme='http', netloc='www.python.org', path='/FAQ.html', params='', query='', fragment='')

# urlunparse
# http://blog.csdn.net/hxsstar/article/details/17240975
print(parse.urlunparse(parse.urlparse('http://blog.csdn.net/hxsstar/article/details/17240975')))
print(parse.urlunparse(parse.ParseResult(scheme='http', netloc='blog.csdn.net', path='/hxsstar/article/details/17240975', params='', query='', fragment='')))

# urljoin
# urljoin will join schema, net_loc and part of path of baseurl, with new url
print(parse.urljoin('http://www.python.org/doc/FAQ.html', 'current/lib/lib.html'))
# http://www.python.org/doc/current/lib/lib.html
