from urllib import request

url = 'https://www.baidu.com'

# urlopen: 
# urlopen(urlstr) will open an URL that pointed by urlstr, if no Schema or Schema is 'file' in urlstr, it will open a local file
# it return a file object like open() does
with request.urlopen(url) as f:
    print(f)    # http.client.HTTPResponse object
    re = f.read()   # read all bytes
    print(re)
    re = f.info()   # return MIME(Multipurpose Internet Mail Extension)
    print(re)   
    re = f.geturl() # return real URL
    print(re)   

# urlretrieve: 
# urlretrieve will download full HTML and save it as a file
# filename -- file save path and file name, default None, and path is AppData/temp
# reporthook -- pass a function to this para, and three para(blocknum, block_size, total_size) will be passed to your function
print(request.urlretrieve(url, filename='baidu_url', reporthook=print))

# quote:
# quote function can encode some symbol that not allowed in URL into %xx
print(request.quote('diss act&cat/sad')) # diss%20act%26cat/sad 
print(request.quote('diss act&cat/sad', safe='/&')) # diss%20act&cat/sad 

# unquote:
print(request.unquote('diss%20act%26cat/sad')) # diss act&cat/sad

