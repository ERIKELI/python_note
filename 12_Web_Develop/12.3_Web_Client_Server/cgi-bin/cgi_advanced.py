from cgi import FieldStorage
from os import environ
from io import StringIO
from urllib.parse import quote, unquote

class AdvCGI(object):
    header = 'Content-Type: text/html\n\n'
    url = '/cgi-bin/cgi_advanced.py'

    formhtml = '''<html><head><title>
Advanced CGI Demo</title></head>
<body><h2>Advanced CGI Demo Form</h2>
<form method=post action="%s" enctype="multipart/form-data">
<h3>My Cookie Setting</h3>
<li><code><b>CPPuser = %s</b></code>
<h3>Enter cookie value<br>
<input name=cookie value="%s"> (<i>optional</i>)</h3>
<h3>Enter your name<br>
<input name=person value="%s"> (<i>required</i>)</h3>
<h3>What languages can you program in?
(<i>at least one required<i>)</h3>
%s
<h3>Enter file to upload <small>(max size 4K)</small></h3>
<input type=file name=upfile value="%s" size=45>
<p><input type=submit>
</form></body></html>'''

    langSet = ('Python', 'Ruby', 'Java', 'C++', 'PHP', 'C', 'JavaScript')
    # Set checkbox for language items,
    # First %s for real value, second one for CHECKED or not, third one for presented value
    langItem = '<input type=checkbox name=lang value="%s"%s> %s\n'

    # Read cookies from client
    def getCPPCookies(self):
        # environ contains the environment info, similar to dict usage
        # Check whether cookie exists
        if 'HTTP_COOKIE' in environ:
            # environ['HTTP_COOKIE'] could be like: 'CPPinfo=Like%3APython%2CJava%3AC%3A\path...; CPPuser=Self'
            # %3A -> :
            # %2C -> ,
            cookies = [x.strip() for x in environ['HTTP_COOKIE'].split(';')]
            # cookies = ['CPPinfo=Like%3APython%2CJava%3AC%3A\path...', 'CPPuser=Self']
            for eachCookie in cookies:
                if len(eachCookie) > 6 and eachCookie[:3]=='CPP':
                    # tag -> info / user
                    tag = eachCookie[3:7]
                    try:
                        self.cookies[tag] = eval(unquote(eachCookie[8:]))
                    except (NameError, SyntaxError):
                        self.cookies[tag] = unquote(eachCookie[8:])
            if 'info' not in self.cookies:
                self.cookies['info'] = ''
            if 'user' not in self.cookies:
                self.cookies['user'] = ''
        else:
            self.cookies['info'] = self.cookies['user'] = ''

        if self.cookies['info'] != '':
            print(self.cookies['info'])
            try:
                self.who, langStr, self.fn = self.cookies['info'].split(' : ')
            except:
                self.who = self.fn = self.cookies['info']
                langStr = ' '
            self.langs = langStr.split(',')
        else:
            self.who = self.fn = ' '
            self.langs = ['Python']

    def showForm(self):
        self.getCPPCookies()

        # Put together language checkboxes
        langStr = []
        for eachLang in AdvCGI.langSet:
            # Add CHECKED if language name exists in cookies
            langStr.append(AdvCGI.langItem % (eachLang, 'CHECKED' if eachLang in self.langs else '', eachLang))

        # See if user cookie set up yet
        if not ('user' in self.cookies and self.cookies['user']):
            cookStatus = '<i>(cookies has not been set yet)</i>'
            userCook = ''
        else:
            userCook = cookStatus = self.cookies['user']

        print('%s%s' % (AdvCGI.header, AdvCGI.formhtml % (AdvCGI.url, cookStatus, 
                        userCook, self.who, ''.join(langStr), self.fn)))

    errhtml = '''<html><head><title>
Advanced CGI Demo</title></head>
<body><h3>Error</h3>
<b>%s</b><p>
<form><input type=button value=back onclick='window.history.back()'></form>
</body></html>'''

    def showError(self):
        print(AdvCGI.header + AdvCGI.errhtml % (self.error))

    # Some html tags:
    #   <li>: for list
    #   <ol>: odered list
    #   <ul>: unodered list
    #   <br>: newline
    reshtml = '''<html><head><title>
Advanced CGI Demo</title></head>
<body><h2>Your Uploaded Data</h2>
<h3>Your cookie value is: <b>%s</b></h3>
<h3>Your name is: <b>%s</b></h3>
<h3>You can program in the following languages:</h3>
<ul>%s</ul>
<h3>Your uploaded file...<br>
Name: <i>%s</i><br>
Contents:</h3>
<pre>%s</pre>
Click <a href="%s"><b>here</b></a> to return to form.
</body></html>'''
    
    # Tell client to store cookies
    def setCPPCookies(self):
        for eachCookie in self.cookies.keys():
            print('Set-cookie: CPP%s=%s; path=/' % (eachCookie, quote(self.cookies[eachCookie])))

    # Display results page
    def doResults(self):
        MAXBYTES = 4096
        langList = ''.join('<li>%s<br>' % eachLang for eachLang in self.langs)
        filedata = self.fp.read(MAXBYTES)
        # Check file size
        if len(filedata) == MAXBYTES and self.fp.read():
            filedata = '%s%s' % (filedata, 
                '...<b><i>(file truncated due to size)</i></b>')
        self.fp.close()
        if filedata == '':
            filedata = '<b><i>(file not given or upload error)</i></b>'
        filename = self.fn

        # See if user cookie set up yet
        if not ('user in self.cookies and self.cookies["user"]'):
            cookStatus = '<i>(cookie has not been set yet)</i>'
            userCook = ''
        else:
            userCook = cookStatus = self.cookies['user']

        # Set cookies
        # Use ' : ' rather than ':' to join, because filename may contains ':' like 'c:\windows\...'
        self.cookies['info'] = ' : '.join((self.who, ','.join(self.langs), filename))
        self.setCPPCookies()

        print('%s%s' % (AdvCGI.header, AdvCGI.reshtml % (cookStatus, self.who, langList,
                            filename, filedata, AdvCGI.url)))

    # Determine which page to return
    def go(self):
        self.cookies = {}
        self.error = ''
        form = FieldStorage()
        # No form received
        if not form.keys():
            self.showForm()
            return

        if 'person' in form:
            self.who = form['person'].value.strip().title()
            if self.who == '':
                self.error = 'Your name is required. (blank)'
        else:
            self.error = 'Your name is required. (missing)'

        self.cookies['user'] = unquote(form['cookie'].value.strip()) if 'cookie' in form else ''
        if 'lang' in form:
            langData = form['lang']
            if isinstance(langData, list):
                self.langs = [eachLang.value for eachLang in langData]
            else:
                self.langs = [langData.value]
        else:
            self.error = 'At least one language required.'

        if 'upfile' in form:
            upfile = form['upfile']
            # form['upfile'] ->.filename for file name, .file for file data
            self.fn = upfile.filename or ''
            if upfile.file:
                self.fp = upfile.file
            else:
                self.fp = StringIO('(no data)')
        else:
            # StringIO as data container
            self.fp = StringIO('(no file)')
            self.fn = ''

        if not self.error:
            self.doResults()
        else:
            self.showError()

if __name__ == '__main__':
    page = AdvCGI()
    page.go()

