"""
Run Server first
Connect address: Http://localhost:8000/cgi-bin/cgi_combined_advanced.py
"""
import cgi
from urllib.parse import quote_plus

# Http header
header = 'Content-Type: text/html\n\n'
url = '/cgi-bin/cgi_combined_advanced.py'

errhtml = '''<html><head><title>
Sleep CGI Demo</title></head>
<body><h3>ERROR</h3>
<b>%s</b><p>
<form><input Type=button value=Back onclick="window.history.back()"</form>
</body></html>'''

def showError(error_str):
    print(header + errhtml % error_str)

formhtml = '''<html><head><title>
Sleep CGI Demo</title></head>
<body><h3>Sleep list for: <i>%s</i></h3>
<form action="%s">
<b>Enter your Name: </b>
<input type=hidden name=action value=edit>
<input type=text name=person value="%s" SIZE=15>
<p><b>How many hours you sleep a day?</b>
%s
<p><input type=submit></form></body></html>'''

fradio = '<input type=radio name=howmany value="%s" %s> %s\n'

def showForm(who, howmany):
    sleep = []
    for i in range(5, 10):
        checked = ''
        if str(i) == howmany:
            checked = 'CHECKED'
        sleep.append(fradio % (str(i), checked, str(i)))
    print('%s%s' % (header, formhtml % (who, url, who, ''.join(sleep))))

reshtml = '''<html><head><title>
Sleep CGI Demo</title></head>
<body><h3>Sleep list for: <i>%s</i></h3>
Your name is: <b>%s</b><p>
You sleep <b>%s</b> hours a day.
<p>Click <a href="%s">here</a> to edit your data again.
</body></html>'''

def doResults(who, howmany):
    newurl = url + '?action=reedit&person=%s&howmany=%s' % (quote_plus(who), howmany)
    print(header + reshtml % (who, who, howmany, newurl))

def process():
    error = ''
    form = cgi.FieldStorage()

    if 'person' in form:
        who = form['person'].value.title()  # Resp
    else:
        who = 'NEW USER'    # Show

    if 'howmany' in form:
        howmany = form['howmany'].value # Resp
    else:
        if 'action' in form and form['action'].value == 'edit':
            error = 'Please select hours of sleep.' # Error
        else:
            howmany = 0     # Show

    if not error:
        if 'action' in form and form['action'].value != 'reedit':
            doResults(who, howmany) # Resp
        else:
            showForm(who, howmany)  # Show
    else:
        showError(error)    # Error

if __name__ == '__main__':
    process()


