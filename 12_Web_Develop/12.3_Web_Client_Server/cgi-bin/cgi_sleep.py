import cgi

reshtml = '''Content-Type: text/html\n
<html><head><title>
Sleep(Dynamic Screen CGI demo)
</title></head>
<body><h3>sleep list for: <i>%s</i></h3>
Your name is: <b>%s</b><p>
You sleep <b>%s</b> hours a day.
</body></html>'''

# cgi.FieldStorage will catch the form from web client
form = cgi.FieldStorage()

# Get the value
who = form['person'].value
howmany = form['howmany'].value

# print should be html format, and will be return back to web client
print(reshtml % (who, who, howmany))
