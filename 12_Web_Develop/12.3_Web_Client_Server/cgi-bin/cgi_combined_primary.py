"""
Run Server first
Connect address: Http://localhost:8000/cgi-bin/cgi_combined_primary.py
"""
import cgi

# Http header
header = 'Content-Type: text/html\n\n'

# Html form
formhtml = '''<html><head><title>
Sleep CGI Demo</title></head>
<body><h3>Sleep list for: <i>NEW USER</i></h3>
<form action='/cgi-bin/cgi_combined_primary.py'>
<b>Enter your Name: </b>
<input type=hidden name=action value=edit>
<input type=text name=person value='NEW USER' SIZE=15>
<p><b>How many hours you sleep a day?</b>
%s
<p><input type=submit></form></body></html>'''

fradio = '<input type=radio name=howmany value="%s" %s> %s\n'

def showForm():
    sleep = []
    for i in range(5, 10):
        checked = ''
        if i == 5:
            checked = 'CHECKED'
        sleep.append(fradio % (str(i), checked, str(i)))
    print('%s%s' % (header, formhtml % ''.join(sleep)))

reshtml = '''<html><head><title>
Sleep CGI Demo</title></head>
<body><h3>Sleep list for: <i>%s</i></h3>
Your name is: <b>%s</b><p>
You sleep <b>%s</b> hours a day.
</body></html>'''

def doResults(who, howmany):
    print(header + reshtml % (who, who, howmany))

def process():
    form = cgi.FieldStorage()
    if 'person' in form:
        who = form['person'].value  # resp
    else:
        who = 'NEW USER'    # show

    if 'howmany' in form:
        howmany = form['howmany'].value # resp

    else:
        howmany = 0     # show

    # Use action label to judge show or response
    if 'action' in form:
        doResults(who, howmany) # resp
    else:
        showForm()  # show

if __name__ == '__main__':
    process()

