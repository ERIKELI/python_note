from http.server import BaseHTTPRequestHandler, HTTPServer

# http://localhost:80/first_html.html to GET file
class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            f = open(self.path[1:], 'r')
            self.send_response(200)
            self.send_header('Content-type', 'text/html'.encode())
            self.end_headers()
            self.wfile.write(f.read().encode())
            f.close()
        except IOError:
            self.send_error(404,
                    'File NOt Found: %s' % self.path)

def main():
    try:
        server = HTTPServer(('', 80), MyHandler)
        print('Welcome to the machine... Press ^C once or twice to quit.')
        server.serve_forever()
    except:
        print('^C received, shutting down server')
        server.socket.close()

if __name__ == '__main__':
    main()

