from html.parser import HTMLParser
from io import StringIO
from urllib import request

from bs4 import BeautifulSoup, SoupStrainer
from html5lib import parse, treebuilders


URLs = ('http://python.org',
        'http://www.baidu.com')

def output(x):
    print('\n'.join(sorted(set(x))))

def simple_beau_soup(url, f):
    'simple_beau_soup() - use BeautifulSoup to parse all tags to get anchors'
    # BeautifulSoup returns a BeautifulSoup instance
    # find_all function returns a bs4.element.ResultSet instance, 
    # which contains bs4.element.Tag instances,
    # use tag['attr'] to get attribute of tag
    output(request.urljoin(url, x['href']) for x in BeautifulSoup(markup=f, features='html5lib').find_all('a'))

def faster_beau_soup(url, f):
    'faster_beau_soup() - use BeautifulSoup to parse only anchor tags'
    # Add find_all('a') function
    output(request.urljoin(url, x['href']) for x in BeautifulSoup(markup=f, features='html5lib', parse_only=SoupStrainer('a')).find_all('a'))

def htmlparser(url, f):
    'htmlparser() - use HTMLParser to parse anchor tags'
    class AnchorParser(HTMLParser):
        def handle_starttag(self, tag, attrs):
            if tag != 'a':
                return
            if not hasattr(self, 'data'):
                self.data = []
            for attr in attrs:
                if attr[0] == 'href':
                    self.data.append(attr[1])
    parser = AnchorParser()
    parser.feed(f.read())
    output(request.urljoin(url, x) for x in parser.data)
    print('DONE')
    
def html5libparse(url, f):
    'html5libparse() - use html5lib to parser anchor tags'
    #output(request.urljoin(url, x.attributes['href']) for x in parse(f) if isinstance(x, treebuilders.etree.Element) and x.name == 'a')

def process(url, data):
    print('\n*** simple BeauSoupParser')
    simple_beau_soup(url, data)
    data.seek(0)
    print('\n*** faster BeauSoupParser')
    faster_beau_soup(url, data)
    data.seek(0)
    print('\n*** HTMLParser')
    htmlparser(url, data)
    data.seek(0)
    print('\n*** HTML5lib')
    html5libparse(url, data)
    data.seek(0)

if __name__=='__main__':
    for url in URLs:
        f = request.urlopen(url)
        data = StringIO(f.read().decode())
        f.close()
        process(url, data)

