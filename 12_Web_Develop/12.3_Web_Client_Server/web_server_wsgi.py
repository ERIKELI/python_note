
"""
Run server
Connect to http://localhost:8000/
"""

def simple_wsgi_app(environ, start_response):
    status = '200 OK'
    headers = [('Content-type', 'text/plain')]
    # This function pass status code and header, status code/header decided inside wsgi app
    start_response(status, headers)
    return [b'Hello world']

from io import StringIO
import sys

def simple_wsgi_server(app, environ=None):
    sio = StringIO()

    def start_response(status, headers):
        sio.write('Status: %s\r\n' % status)
        for header in headers:
            sio.write('%s: %s\r\n' % header)
        return sio.write

    iterable = app(environ, start_response)
    try:
        if not sio.getvalue():
            raise RuntimeError("start_response() not called by app!")
        sio.write('\r\n%s\r\n' % '\r\n'.join(str(line) for line in iterable))
    finally:
        if hasattr(iterable, 'close') and callable(iterable.close):
            iterable.close()

    sys.stdout.write(sio.getvalue())
    sys.stdout.flush()

from wsgiref.simple_server import make_server, demo_app
def wsgi_server(app):
    httpd = make_server('', 8000, app)
    print('Started app serving on port 8000...')
    httpd.serve_forever()

em_app = False
em_server = True
wsgi_app = demo_app if em_app else simple_wsgi_app
wsgi_server = wsgi_server if em_server else simple_wsgi_server
wsgi_server(wsgi_app)

