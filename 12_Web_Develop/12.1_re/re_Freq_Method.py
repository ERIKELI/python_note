import re
"""
Regular Expression
"""

def regexp(pattern, target, *args, grp='group', prt=True, func='search'):
    pat = re.compile(pattern)
    try:
        r = getattr(getattr(re, func)(pattern, target), grp)(*args)
    except AttributeError as e:
        r = None
        # print(e)
    if prt:
        print(r)
    return r

# Use . to match all
print(30*'-')
regexp('.+', 'exit soon\nHurry up.') # 'exit soon'

# Use () to make sub-groups
print(30*'-')
regexp('(.+) (.+)', 'exit soon', 1) # 'exit'
regexp('(.+) (.+)', 'exit soon', 2) # 'soon'
regexp('(.+) (.+)', 'exit soon', grp='groups')  # ('exit', 'soon')

# Use ^ to search from head
print(30*'-')
regexp('^The', 'The End')   # 'The'
regexp('^The', 'In The End')    # None

# Use \b to search boundary
print(30*'-')
regexp(r'\bThe\b', 'In The End') # 'The'
regexp(r'\bThe', 'In TheEnd')   # 'The'
regexp(r'The\b', 'In TheEnd')   # None

# match and search
print(30*'-')
regexp('The', 'In The End', func='search') # 'the'
regexp('The', 'In The End', func='match')   # None

# findall and finditer
# Note: 
# findall returns a list that contains string of matched result
# finditer returns a iterator that contains obj of matched result
# re.IGNORECASE can ignore capitalized
print(30*'-')
print(re.findall('The', 'In The End, these things merged', re.I)) # ['The', 'The']
itera = re.finditer('The', 'In The End, these things merged', re.I)
for x in itera:
    print(x.group())                                        # 'The'

# sub and subn
print(re.sub('X', 'LIKE', 'This is X, X is acting'))
print(re.subn('X', 'LIKE', 'This is X, X is acting'))

# split: split(re-expression, string)
print(re.split(', |\n', 'This is amazing\nin the end, those things merged'))    #['This is amazing', 'in the end', 'those things merged']

# \N: use \N to represent sub group, N is the number of sub group
print(re.sub(r'(.{3})-(.{3})-(.{3})', r'\2-\3-\1', '123-def-789'))  # 'def-789-123'
# (?P<name>): similar to \N, add tag name for each sub group, 
# and use \g<name> to fetch sub group
print(re.sub(r'(?P<first>\d{3})-(?P<second>\d{3})-(?P<third>\d{3})', r'\g<second>-\g<third>-\g<first>', '123-456-789')) # 456-789-123

# (?P=name): use this expression to reuse former sub group result
# Note: this expression only get the matched result, not the re pattern
print(re.match(r'(?P<digit>\d{3})-(?P<char>\w{3})-(?P=char)-(?P=digit)', '123-abc-abc-123'))    # Match obj, '123-abc-abc-123'
print(re.match(r'(?P<digit>\d{3})-(?P<char>\w{3})-(?P=char)-(?P=digit)', '123-abc-def-456'))    # None

# Note: should use (?P=name) in a re expression with former named group
print(re.sub(r'(?P<myName>potato)(XXX)(?P=myName)', r'YY\2YY', 'potatoXXXpotato'))  # 'YYXXXYY'
print(re.sub(r'(?P<digit>\d{3})-(?P<char>\w{4})', r'(?P=char)-(?P=digit)', '123-abcd')) # failed:(?P=char)-(?P=digit)

print(re.sub(r'(?P<digit>\d{3})-(?P<char>\w{4})', r'\2-\1', '123-abcd'))    # 'abcd-123'
print(re.sub(r'(?P<digit>\d{3})-(?P<char>\w{4})', r'\g<char>-\g<digit>', '123-abcd'))   # 'abcd-123'


# groupdict(): return dict of named pattern
print(re.match(r'(?P<digit>\d{3})-(?P<char>\w{4})', '123-abcd').groupdict())    # {'char': 'abcd', 'digit': '123'}
print(re.match(r'(?P<digit>\d{3})-(?P<char>\w{4})', '123-abcd').group('char'))  # 'abcd'
print(re.match(r'(?P<digit>\d{3})-(?P<char>\w{4})', '123-abcd').group('digit')) # '123'

# re extensions
# use (?aiLmsux): re.A, re.I, re.L, re.M, re.S, re.X
# use (?imsx)
# (?i) --> re.I/re.IGNORECASE
print(re.findall(r'(?i)yes', 'yes, Yes, YES'))  # ['yes, Yes, YES']
# (?m) --> re.M/re.MULTILINE: match multiline, ^ for head of line, $ for end of line
print(re.findall(r'(?im)^The|\w+e$', '''The first day, the last one, 
the great guy, the puppy love'''))  # ['The', 'the', 'love']
# (?s) --> re.S/re.DOTALL: dot(.) can be use to replace \n(default not)
print(re.findall(r'(?s)Th.+', '''This is the biggest suprise
I'd ever seen'''))  # ['This is the biggest suprise\nI'd ever seen']
# (?x) --> re.X/re.VERBOSE: make re ignore the blanks and comments after '#' of re pattern
# This extension can make re pattern easy to read and you can add comments as you want
print(re.search(r'''(?x)
                    \((\d{3})\) # Area code
                    [ ]         # blank
                    (\d{3})     # prefix
                    -           # connecting line
                    (\d{4})     # suffix
                    ''', '(123) 456-7890').groups())    # ['123', '456', '789']

# (?:...): make a sub group that no need to save and never use later
print(re.findall(r'(?:\w{3}\.)(\w+\.com)', 'www.google.com'))   # ['google.com']

# (?=...) and (?!...)
# (?=...): match the pattern which end with ..., pattern should place before (?=...)
print(re.findall(r'\d{3}(?=Start)', '222Start333, this is foo, 777End666')) # ['222']
# (?!...): match the pattern which not end with ..., pattern should place before (?!...)
print(re.findall(r'\d{3}(?!End)', '222Start333, this is foo, 777End666')) # ['222', '333', '666']

# (?<=...) and (?<!...)
# (?<=...): match the pattern which start with ..., pattern should place after (?<=...)
print(re.findall(r'(?<=Start)\d{3}', '222Start333, this is foo, 777End666')) # ['333']
# (?<!...): match the pattern which not stert with ..., pattern should place after (?<!...)
print(re.findall(r'(?<!End)\d{3}', '222Start333, this is foo, 777End666')) # ['222', '333', '777']

# (?(id/name)Y|X): if sub group \id or name exists, match Y, otherwise match X
# Below code first match the first char, if 'x' matched, store a sub group, if 'y', not to store
# then match second char, if sub group stored('x' matched), match 'y', otherwise match 'x', finally return result
print(re.search(r'(?:(x)|y)(?(1)y|x)', 'yx'))

# Greedy match: '+' and '*' act greedy match, appending '?' to make no greedy match
print(re.search(r'.+(\d+-\d+-\d+)', 'asdfgh1234-123-123').group(1)) # 4-123-123
print(re.search(r'.+?(\d+-\d+-\d+)', 'asdfgh1234-123-123').group(1)) # 1234-123-123
print(re.search(r'.*(\d+-\d+-\d+)', 'asdfgh1234-123-123').group(1)) # 4-123-123
print(re.search(r'.*?(\d+-\d+-\d+)', 'asdfgh1234-123-123').group(1)) # 1234-123-123

# match and fullmatch
print(re.match(r'This is a full match', 'this is a full match string', re.I))   # this is a full match
print(re.fullmatch(r'This is a full match', 'this is a full match string', re.I))   # None

# span function
print(re.search(r'Google', 'www.google.com', re.I).span())  # (4, 10)
