import re

s = 'AD892SDA213VC2'

def double(matched):
    value = int(matched.group('value'))
    return str(value*2)
print(re.sub(r'(?P<value>\d+)', double, s))

print(re.sub(r'(?P<value>\d+)', lambda x: str(int(x.group('value'))*2), s))

# Output: 'AD1784SDA426VC4'
