x = [1, 2, 3]
y = [1, 2, 3]
z = x
print('x id is %d, y id is %d, z id is %d' % (id(x), id(y), id(z)))
# x id is 52275016, y id is 13096648, z id is 52275016

# is judge by id, == judge by value
print(x is y, x==y) # False, True
print(x is z, x==z) # True, True
