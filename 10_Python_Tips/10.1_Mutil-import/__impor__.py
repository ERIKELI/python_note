import sys
__import__('foo')
print(sys.modules['foo'])
del sys.modules['foo']
__import__('foo')
__import__('foo')
'''
# euqal to "from foo import act"
f = __import__('foo', fromlist=('act',))
f.act()
'''
