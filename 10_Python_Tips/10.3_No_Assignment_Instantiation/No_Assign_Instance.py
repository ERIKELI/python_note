class Foo():
    def __init__(self, str_1, str_2):
        self.foo = ''
        self.foo += str_1
        self.foo += str_2
# Foo('Hello', 'World') is an instance,
# With no assignment to any para or attribute(not do x = class()).
print(Foo('Hello', 'World').foo)
