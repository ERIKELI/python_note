
# n is mutable
def foo_1(x, n=[]):
    print(id(n))
    n += [x]
    print(id(n))
    print(n)

foo_1(2)
foo_1(3)
foo_1(5)

print(20*'-')
# n is immutable
def foo_2(x, n=()):
    print(id(n))
    n += (x, )
    print(id(n))
    print(n)

foo_2(2)
foo_2(3)
foo_2(5)
