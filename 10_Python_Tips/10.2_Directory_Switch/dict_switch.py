def hello():
    print('Hello!')

def world():
    print('World!')

d = {'Hello': hello,
     'World': world}

def foo(case):
    return d[case]

foo('Hello')()
foo('World')()

