import random


def foo(chooseBin):
    x = list(chooseBin.keys())
    i = random.randint(0, 2)
    random_key = x[i]
    print('Random choose %s: %s' % (random_key, chooseBin[random_key]))
    return chooseBin[random_key]

assert foo({'a' : True,
            'b' : True,
            'c' : False
            }), 'c is False!'

