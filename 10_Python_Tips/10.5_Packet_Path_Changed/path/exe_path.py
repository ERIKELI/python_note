import os

def call_path():
    with open('log.txt', 'w') as f:
        f.write(os.path.realpath(__file__))

if __name__ == '__main__':
    call_path()
