import sys
from cx_Freeze import setup, Executable

sys.path.append(r'./path/')

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"include_files": ['path/bpack/bpack.exe']}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"
    #base = 'Console'

setup(  name = "exe_path",
        version = "1.1.1.1",
        description = "Test the path change",
        options = {"build_exe": build_exe_options},
        executables = [Executable("call_path.py", base=base)])
