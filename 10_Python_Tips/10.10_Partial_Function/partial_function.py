#!/usr/bin/python
# =============================================================
# File Name: partial_function.py
# Author: LI Ke
# Created Time: 1/29/2018 13:06:45
# =============================================================


import time
from functools import partial as pto


def add(a, b):
    return a+b

pto_add = pto(add, 1)

print(add(1, 2))
print(pto_add(2))

def test_1(x):
    start = time.time()
    for i in range(10**7):
        x(1, i)
    end = time.time()
    return end-start

def test_2(x):
    start = time.time()
    for i in range(10**7):
        pto_add(i)
    end = time.time()
    return end-start

print(sum(test_1(add) for i in range(3))/3) # 1.6598326365152996
print(sum(test_2(pto_add) for i in range(3))/3) # 2.585925261179606
