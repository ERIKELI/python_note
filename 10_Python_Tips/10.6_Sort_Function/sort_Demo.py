# List built-in function, sort
x = [4, 2, 5, 3, 8, 3]

# Slice up the list x, then generate a new list y
y = x[:]
x.sort()
print(x) # [2, 3, 3, 4, 5, 8]
print(y) # [4, 2, 5, 3, 8, 3]

y = sorted(y) # [2, 3, 3, 4, 5, 8]

z = sorted('python') # ['h', 'n', 'o', 'p', 't', 'y']
z = sorted({3: 'c', 4: 'w', 1: 'c'}) # [1, 3, 4]

# key parameter
x = ['mmm', 'm', 'mmmm', 'mmmmm', 'mm'] 
x.sort(key=len) # ['m', 'mm', 'mmm', 'mmmm', 'mmmmm']
sorted(x, key=len) # ['m', 'mm', 'mmm', 'mmmm', 'mmmmm']

# reverse parameter
x = ['3', '2', '1', '6', '4'] 
x.sort(key=int, reverse=True) # ['6', '4', '3', '2', '1']
sorted(x, key=int, reverse=False) # ['1', '2', '3', '4', '6']
