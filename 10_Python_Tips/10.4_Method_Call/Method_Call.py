import dis


def count():
    fs = []
    for i in range(1, 4):
        def f():
            return i*i
        fs.append(f)
    return fs


def run():
    f1, f2, f3 = count()
    # When the function called, the value of i is 3
    print(f1())
    print(f2())
    print(f3())

dis.dis(count)
# run()
