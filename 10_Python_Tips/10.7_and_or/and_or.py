print(1 and 0 and 2)    # 0
print(1 and 2 and 3)    # 3
print(0 or 1 or 0)      # 1
print(0 or '' or None)  # None
# Equal to (1 and 0) or (3 and 2 and 4) or (None and 5)
print(1 and 0 or 3 and 2 and 4 or None and 5)   # 4
