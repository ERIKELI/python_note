"""
The return of 'and' and 'or'
As for 'or', it will return the first valid one, if all invalid, return the last invalid one
As for 'and', it will return the first invalid one, if all valid, return the last valid one
"""
class Foo(): pass
    
def foo(): pass

valid = [7, True, (7,), [7], {'7': 7}, foo, Foo]
invalid = [0, None, False, '', (), [], {}]

# 'or': Invalid or Valid --> return the first valid one
print('----Below is Invalid or Valid----')
for iv in invalid:
    print(iv or 'Valid' or 'Real')

# 'or': Invalid or Invalid --> return the last invalid one
print('----Below is Invalid or Invalid----')
for iv in invalid:
    print(iv or None or False)

# 'or': Valid or Valid --> return the first valid one
print('----Below is Valid or Valid----')
for vl in valid:
    print(vl or 'Valid')

# 'and': Invalid and Valid --> return the first invalid one
print('----Below is Invalid and Valid----')
for iv in invalid:
    print(iv and 'Valid' and 'Real')

# 'and': Valid and Valid --> return the last valid one
print('----Below is Valid and Valid----')
for vl in valid:
    print(vl and 7 and True)

# 'and': Valid or Invalid --> return the first invalid one
print('----Below is Valid and Invalid----')
for vl in valid:
    print(vl and None and False)

print('----Below is tricky print----')
# We can use it to make a tricky print
for iv in invalid:
    print('I have %s' % (iv or 'None'))
for vl in valid:
    print('I have %s' % (vl or 'None'))
