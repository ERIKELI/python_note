"""
This script use single thread to handle I/O intensive program
Fetch book rank from Amazon
"""

from atexit import register
from re import compile
from threading import Thread
from time import ctime
from urllib.request import urlopen


REGEX = compile(b'#([\d,]+) in Books')
AMZN = 'https://www.amazon.com/dp/'
ISBNs = {
        '0132269937': 'Core Python Programming',
        '0132356139': 'Python Web Development with Django',
        '0137143419': 'Python Fundamentals'
        }

def getRanking(isbn):
    page = urlopen('%s%s' % (AMZN, isbn)) # or str.format()
    data = page.read()
    page.close()
    return REGEX.findall(data)[0]

def _showRanking(isbn):
    print('- %r ranked %s' % (ISBNs[isbn], getRanking(isbn)))

def _main():
    print('At', ctime(), 'on Amazon...')
    for isbn in ISBNs:
        _showRanking(isbn)

@register
def _atexit():
    print('All DONE at:', ctime())

if __name__ == '__main__':
    _main()
