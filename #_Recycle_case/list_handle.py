
def zero(nums):
    n = nums.count(0)
    while 0 in nums:
        nums.remove(0)
    nums = nums + [0,]*n
    return nums

print(zero([1, 0, 3, 4, 0, 0, 5, 9]))
