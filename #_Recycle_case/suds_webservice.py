from suds.client import Client
url="http://example.com/test.asmx?wsdl"

client=Client(url)

#查看远程方法
print(client)
 
#简单调用
client.service.hello()

#传递对象参数（方法1）
#假定我做了一个myc的class,有p1,p2两个属性
m=client.factory.create('myc')
m.p1="my name is "
m.p2="walker"
result=client.service.ClassIn(m)
print(result)


#传递对象参数（方法2）
m={"p1":"my name is ","p2":"walker"}
client=Client(url)
result=client.service.ClassIn(m)
print(result)
  
#返回对象   
result=client.service.ClassOut()
print(result.p1) #可见返回的对象是可以直接用的

# 演示suds把异常包装成元组送出
client=Client(url,faults=False)
result=client.service.ClassIn("fadlskfjsdafjk")
print(result)
