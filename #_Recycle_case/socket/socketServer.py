# server

import socket

address = ('127.0.0.1', 31500)                          # set IP and Port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   # choose IPV4, stream type
s.bind(address)                                         # bind address and ip
s.listen(5)                                             # allow max five connection

while 1:
    cs, address = s.accept()                            # wait client's request, cs is a new socket object and server use it to communicate with client
    print('got connected from', address)
    cs.send(b'hello I am server, welcome')              # send message to client
    cs.send(b'hello I am server, welcome twice')
    ra = cs.recv(1024).decode('utf-8')                  # receive message from client and decode
    print(ra)
    cs.close()
