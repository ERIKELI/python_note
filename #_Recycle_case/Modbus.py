import socket, struct, time
#Modbus = '000000000006010300000016'
setTemp = '000000000006000600160190'
#setTemp = '0000000000060006001A0190'
start =  '000000000006000600010002'
readStatus = '000000000006000300010001'


def encode(Modbus):
    Modbus_16 = b''
    while Modbus:
        # TODO: Use '!h' to replace 'B', and re-struct this encode
        Modbus_16 += struct.pack('B', int(Modbus[0:2], 16))
        Modbus = Modbus[2:]
    #print(Modbus_16)
    return Modbus_16
ip = '192.168.0.100'
port = 502
addr = (ip, port)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Connection built, connecting')
sock.connect(addr)
print('Connect success')

print('read status')
sock.send(encode(readStatus))
print('send', encode(readStatus))
msg = sock.recv(1024)
print('recv', msg)
print(msg[-2:])
'''
print('set Temp')
sock.send(encode(setTemp))
print('send', encode(setTemp))
msg = sock.recv(1024)
print('recv', msg)
'''
print('set Act')
sock.send(encode(start))
print('send', encode(start))
msg = sock.recv(1024)
print('recv', msg)

sock.close()
