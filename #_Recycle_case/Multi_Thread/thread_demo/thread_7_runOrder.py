'''
线程的运行关系：
同时运行因此会输出1a，2b，3c，4d
'''
import threading
import time

class myThreada(threading.Thread):
    def run(self):
        print(self.name)
        print("1")
        print("2")
        print("3")
        print("4")
        print(self.name)

class myThreadb(threading.Thread):
    def run(self):
        print(self.name)
        print("a")
        print("b")
        print("c")
        # print("d")
        print(self.name)

evt = threading.Event()
def main():
    John = myThreada()
    John.name = 'John'
    Smith = myThreadb()
    Smith.name = 'Smith'
    John.start()
    Smith.start()

if __name__ == '__main__':
    main()
