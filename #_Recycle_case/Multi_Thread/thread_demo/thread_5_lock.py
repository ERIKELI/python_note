'''
线程锁，可查看不加锁的情况并分析运行方式
Lock和RLock（可重入锁）
基本使用方法：
lock = threading.RLock()
lock.acquire()
pass
lock.release()
若acquire方法调用了n次，则release方法也要调用n次
'''
import threading
import time
class myThread(threading.Thread):
    def run(self):
        global x
        lock.acquire()
        for i in range(3):
            x += 10
        time.sleep(1)
        print(x)
        lock.release()
x = 0
lock = threading.RLock()
def main():
    thrs = []
    for item in range(5):
        thrs.append(myThread())
    for item in thrs:
        item.start()

if __name__ == '__main__':
    main()
        
