'''
通过threading.Thread函数在线程中直接运行函数
Thread(group = None, target = None, args = (), kwargs = {}, *, daemon = None)
其中target参数就是要运行的函数，args是传入函数的参数元组
首先定义了一个thrfun函数，然后以线程的方式运行它，且每次传入的参数不同
'''
import threading
def thrfun(x, y):
    for i in range(x, y):
        print(str(i*i)+';')
ta = threading.Thread(target = thrfun, args = (1, 6))
tb = threading.Thread(target = thrfun, args = (16, 21))
ta.start()
tb.start()
