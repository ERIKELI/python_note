'''
python 3 对于多进程支持的是，multiprocessing模块和subprocess模块，基本使用与threading相同
multiprocessing.Process对象完成，与threading.Thread一样，具有Lock，RLock，Event
subprocess模块可以创建新的进程，其中基本函数有：
call(args, *, stdin=None, stdout=None, stderr=None, shell=False, timeout=None)
    # 创建新进程运行程序，输入和输出绑定到父进程，返回新进程退出码
check_call(args, *, stdin=None, stdout=None, stderr=None, shell=False, timeout=None)
    # 创建新进程运行程序，输入和输出绑定到父进程，退出码为0正常返回，否则引发
    # CalledProcessError
getstatusoutput(cmd)
    # 创建新进程运行程序，元组形式返回新进程退出码和输出
getoutput(cmd)
    # 创建新进程运行程序，返回新进程的输出（字符串）
check_output(args, *, input=None, stdin=None, stderr=None, shell=False,
            universal_newlines=False, timeout=None)
            创建新进程运行程序，返回新进程的输出(bytesarray)
参数的基本意义如下：
stdin, stdout, stderr    用来处理新进程的输入、输出和错误信息
shell                    是否使用一个中间shell来执行（可以使用shell相关变量等）
input                    为命令行提供一个输入信息（字符串），不能与stdin同时用
universal_newline        返回值和输入值为字符串而不是bytes
'''
import subprocess
print('call() test:', subprocess.call(['python', 'protest.py']))
print('')
print('check_call() test:', subprocess.check_call(['python', 'protest.py']))
print('')
print('getstatusoutput() test:', subprocess.getstatusoutput(['python', 'protest.py']))
print('')
print('getoutput() test:', subprocess.getoutput(['python', 'protest.py']))
print('')
print('check_output() test:', subprocess.check_output(['python', 'protest.py']))
