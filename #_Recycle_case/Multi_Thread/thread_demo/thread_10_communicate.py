'''
利用communicate在进程之间传递信息
程序建立了psum个子进程，用来执行protestPsum.py的源代码，然后将各个进程的输入输出连接起来，
并输出各子进程的输出信息。
'''
import subprocess
processes = []
psum = 5
for i in range(psum):
    processes.append(subprocess.Popen(['python', 'protestPsum.py'],
                                    stdout=subprocess.PIPE,
                                    stdin=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    universal_newlines=True,
                                    shell=True))
processes[0].communicate('0 bouquet of flowers!')
# Mark Here: the processes[:psum] return a list reads-->[Popen0, Popen1,..., PopenPsum]
#            the processes[1:] return a list reads-->[Popen1, Popen2,..., PopenPsum]
#            finally the zip function make it reads-->[(Popen0, Popen1), (Popen1, Popen2),...,(PopenPsum-1, PopenPsum)]
for before, after in zip(processes[:psum], processes[1:]):
    after.communicate(before.communicate()[0])
print('\nSum of Processes :%d' % psum)
print()
for item in processes:
    print(item.communicate()[0])
