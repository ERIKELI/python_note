'''
Popen的类初始化参数有：
class Popen(args, bufsize=-1, executable=None, stdin=None,
            stdout=None, stderr=None, preexec_fn=None, close_fds=True
            shell=False, cwd=None, env=None, universal_newlines=False
            startupinfo=None, creationflags=0, restore_signals=True,
            start_new_session=False, pass_fds=())
主要参数同subprocess，对象常用方法有：
poll()                                   检查子进程是否结束
wait(timeout=None)                       等待子进程结束
communicate(input=None, timeout=None)    用于和子进程交互；发送标准输入数据；
                                         返回由标准输出和错误输出构成的元组
常用属性有：
pid                                      子进程的pid
returncode                               子进程的退出码（None时子进程未退出）
'''
import subprocess
prcs = subprocess.Popen(['python', 'protestPopen.py'],
                        stdout=subprocess.PIPE,
                        stdin=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        universal_newlines=True,
                        shell=True)
prcs.communicate('These strings are from stdin.')
print('subprocess pid:', prcs.pid)
print('\nSTDOUT:')
print(str(prcs.communicate()[0]))
print('STDERR:')
print(prcs.communicate()[1])
'''
代码中首先用Popen一个子进程，用来执行protestPopen.py（该代码要求一个输入，随后返回一个输出，并打印一个未定义的变量
从而能产生报错）,最后输出子进程的标准输出和错误信息
返回的communicate为一个元组，第一项为子进程的输出信息，第二项为子进程的报错信息，信息为字符串类型
'''
