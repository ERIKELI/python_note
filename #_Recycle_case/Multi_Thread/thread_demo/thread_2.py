'''
通过继承threading.Thread类来创建线程
运用start方法创建启动线程
'''
import threading
class myThread(threading.Thread):
    def __init__(self, mynum):
        super().__init__()
        self.mynum = mynum

    def run(self):
        for i in range(self.mynum, self.mynum + 5):
            print(str(i*i) + ';')
print(threading.stack_size())
ma = myThread(1)
mb = myThread(16)
print(ma.name, ma.ident)
print(mb.name, mb.ident)
ma.start()
print('x')
mb.start()
print('y')
