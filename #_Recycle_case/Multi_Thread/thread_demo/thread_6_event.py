'''
线程之间通过Event进行通信，Event中管理着一个内部标志，
通过set()方法可以将标志设置为True，通过clear()方法可以将标志重置为False，
wait([timeout])方法会使当前线程阻塞至标志位为True。
set()会唤醒另外一个线程的动作，随后两个线程同时进行
'''
import threading
import time

class myThreada(threading.Thread):
    def run(self):
        evt.wait()
        print(self.name, ':Good morning!')
        evt.clear()
        time.sleep(1)
        evt.set()
        time.sleep(1)
        evt.wait()
        print(self.name, ":I'm fine, thank you.")

class myThreadb(threading.Thread):
    def run(self):
        time.sleep(1)
        print(self.name, ':Good morning!')
        time.sleep(1)
        evt.set()
        time.sleep(1)
        evt.wait()
        print(self.name, ":How are you?")
        evt.clear()
        time.sleep(1)
        evt.set()

evt = threading.Event()
def main():
    John = myThreada()
    John.name = 'John'
    Smith = myThreadb()
    Smith.name = 'Smith'
    John.start()
    Smith.start()

if __name__ == '__main__':
    main()
