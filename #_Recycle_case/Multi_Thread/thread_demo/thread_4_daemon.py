'''
Some threads do background tasks, like sending keepalive packets,
or performing periodic garbage collection, or whatever.
These are only useful when the main program is running,
and it's okay to kill them off once the other, non-daemon, threads have exited.

Without daemon threads, you'd have to keep track of them, and tell them to exit,
before your program can completely quit. By setting them as daemon threads,
you can let them run and forget about them, and when your program quits,
any daemon threads are killed automatically.

'''
import threading
import time
class myThread(threading.Thread):
    def __init__(self, mynum):
        super().__init__()
        self.mynum = mynum

    def run(self):
        time.sleep(1)
        for i in range(self.mynum, self.mynum+5):
            print(str(i*i)+';')

def main():
    print('start...')
    ma = myThread(1)
    mb = myThread(16)
    ma.daemon = True
    mb.daemon = True
    ma.start()
    mb.start()
    print('end...')

if __name__ == '__main__':
    main()
