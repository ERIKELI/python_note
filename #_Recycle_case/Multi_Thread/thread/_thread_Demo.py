import time
import _thread


def foo_1():
    print(111111111111111111)
    time.sleep(5)
    print(333333333333333333)


def foo_2():
    print(222222222222222222)
    time.sleep(5)
    print(444444444444444444)

_thread.start_new_thread(foo_1, ())
_thread.start_new_thread(foo_2, ())

