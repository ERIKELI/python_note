"""
Filter out the value that with same sign as former one
Filter out the value that abs less than certain value
"""
x = [1, 1, 0.2, 0.2, -2, -2, 3, 3, -0.1, -0.1, 4, 4, 5, 5, -6, -6, 7, 7, -8, -8]
fd = []
for d in range(0, len(x), 2):
    if (abs(x[d])>0.5):
        if len(fd) > 1:
            if x[d] * fd[-1] < 0:
                fd.append(x[d])
        else:
            fd.append(x[d])
print(fd)
