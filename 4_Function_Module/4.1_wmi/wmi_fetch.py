import wmi 
hardware=open('Hardware.txt', 'w')
   
w=wmi.WMI()

hardware.write("cpu type，main frequency：\n")
for processor in w.Win32_Processor():
    hardware.write("Processor ID: %s" % processor.DeviceID)
    hardware.write("\nProcess Name: %s" % processor.Name.strip()+'\n\n') 
hardware.write('Memory size：')
totalMemSize=0
for memModule in w.Win32_PhysicalMemory():
    totalMemSize+=int(memModule.Capacity)
    hardware.write("\nMemory Capacity: %.2fMB" %((totalMemSize+1048575)/1048576)+'\n\n') 
hardware.write('Hard disk usage：')
for disk in w.Win32_LogicalDisk (DriveType=3):
    temp=disk.Caption+" %0.2f%% free" %(100.0 * int(disk.FreeSpace) / int(disk.Size))
    hardware.write('\n'+temp)
    hardware.write('\n') 
hardware.write('\nIP and MAC：\n')
for interface in w.Win32_NetworkAdapterConfiguration(IPEnabled=1):
    hardware.write('Network card driver information：')
    hardware.write(interface.Description+'\n') 
    hardware.write('Network card MAC address：')
    hardware.write(interface.MACAddress+'\n')
    hardware.write('IP address：')
    hardware.write(interface.IPAddress[0]+'\n')
    hardware.write('Network IP interface')
    hardware.write(interface.IPAddress[1]+'\n')
hardware.close()
