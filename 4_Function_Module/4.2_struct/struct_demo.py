import struct
from ctypes import create_string_buffer

a = 20
b = 400

# python data --> bytes
def pack():
    s = struct.pack('ii', a, b)
    x = struct.pack('!ii', a, b)
    print('length:', len(s))
    print('pack without "!"', s)
    print(repr(s))
    print('pack with "!"', x)
    return s

# bytes --> python data
def unpack():
    s = struct.unpack('ii', struct.pack('ii', a, b))
    x = struct.unpack('!ii', struct.pack('ii', a, b))
    print('length:', len(s))
    print('pack without "!"', s)
    print(repr(s))
    print('pack with "!"', x)
    return s

# calculate the size of corrsponding format
def cal():
    print("len: ", struct.calcsize('i'))       # len:  4
    print("len: ", struct.calcsize('ii'))      # len:  8  
    print("len: ", struct.calcsize('f'))       # len:  4  
    print("len: ", struct.calcsize('ff'))      # len:  8  
    print("len: ", struct.calcsize('s'))       # len:  1  
    print("len: ", struct.calcsize('ss'))      # len:  2  
    print("len: ", struct.calcsize('d'))       # len:  8  
    print("len: ", struct.calcsize('dd'))      # len:  16

def _into():
    buf = create_string_buffer(12)
    # '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    print(repr(buf.raw))     
    print(struct.pack_into("iii", buf, 0, 1, 2, -1))

def _from():
    buf = create_string_buffer(12)
    # '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    print(repr(buf.raw))       
    print(struct.unpack_from("iii", buf, 0)) 

pack()
unpack()
cal()
_into()
_from()

