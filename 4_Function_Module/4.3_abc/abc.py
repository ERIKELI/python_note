from abc import ABCMeta, abstractmethod

# Generate a abstract base obj - MyABC
class MyABC(metaclass=ABCMeta):     # 生成一个抽象的基类MyABC
    #__metaclass__ = ABCMeta
    @abstractmethod                 # 抽象方法，抽象方法可以不执行任何程序
    def run(self):                  # 但是子类中必须重定义这个函数
        pass

MyABC.register(tuple)               # 将tuple变成为MyABC的虚拟子类

assert issubclass(tuple, MyABC)     # tuple是否是MyABC的子类
assert isinstance((), MyABC)        # 判断空元组是否是MyABC的实例
print(MyABC.__mro__)

class Foo(MyABC):                   # 定义Foo为MyABC的子类，则必须定义run函数
    def run(self):
        print('run')
    pass
f = Foo()
f.run()
