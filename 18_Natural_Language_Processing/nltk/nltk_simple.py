#!/usr/bin/python
# =============================================================
# File Name: nltk_simple.py
# Author: LI Ke
# Created Time: 2/1/2018 13:40:22
# =============================================================

# http://vdisk.weibo.com/s/4ffue/1334656530
import nltk
#nltk.download('punkt')
sentence = """What You’ll Learn: through a dynamic mix of video lectures, quizzes, and hands-on labs, this certificate program will introduce you to troubleshooting and customer service, networking, operating systems, system administration, automation, and security."""
tokens = nltk.word_tokenize(sentence)
print(tokens)
