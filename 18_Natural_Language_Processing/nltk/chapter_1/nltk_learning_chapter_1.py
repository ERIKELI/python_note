#!/usr/bin/python
# =============================================================
# File Name: nltk_learning_chapter_1.py
# Author: LI Ke
# Created Time: 2/1/2018 16:49:43
# =============================================================


import nltk
# nltk.download('webtext')
# nltk.download('treebank')
from nltk.book import *

# Find words position
text1.concordance('monstrous')  
print('----------------')

# Find similar meaning words in text
text1.similar('monstrous')      
print('----------------')
text2.similar('monstrous')
print('----------------')

# Contexts of two or more words
text2.common_contexts(['monstrous', 'very'])    
print('----------------')

text4.dispersion_plot(['citizens', 'democracy', 'freedom', 'duties', 'America'])
print('----------------')

# Generate some random words with similar language style
text3.generate(30)                
print('----------------')
print(texts())
print('----------------')

t = nltk.text.Text(['This', 'is', 'a', 'test', 'text', '.'])
print(t)
print(t.count('is'))
