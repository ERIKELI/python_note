#!/usr/bin/python
# =============================================================
# File Name: nltk_learning_chapter_2-2_cond_freq_dist.py
# Author: LI Ke
# Created Time: 2/22/2018 14:24:54
# =============================================================


# ConditionalFreqDist
print('------------------------------------------------')
import nltk
from nltk.corpus import brown

# ConditionalFreqDist require a pair list -> [(contidion, sample), ]
# e.g. -> ConditionalFreqDist([('news', 'The'), ('news', 'one')])
# ConditionalFreqDist will count the sample and classify it by condition

# Condition pair
genre_word = [(genre, word) for genre in brown.categories()
                            for word in brown.words(categories=genre)]
print(len(genre_word))
print(genre_word[:4])
print(genre_word[-4:])

# Condition frequency dist
cfd = nltk.ConditionalFreqDist(genre_word)
print(cfd)      # <ConditionalFreqDist with 15 conditions>
print(cfd.conditions())     # ['belles_lettres', 'humor', 'mystery', 'editorial', 'lore', 'adventure', 'romance', 'reviews', 'fiction', 'science_fiction', 'government', 'religion', 'hobbies', 'news', 'learned']
print(cfd['news'])          # <FreqDist with 14394 samples and 100554 outcomes>
print(cfd['romance'])
print(cfd['romance']['could'])  # 193

# Condition plot and tabulate
print('------------------------------------------------')
import nltk
from nltk.corpus import inaugural
cfd = nltk.ConditionalFreqDist((target, fileid[:4]) for fileid in inaugural.fileids()
                                                    for w in inaugural.words(fileid)
                                                    for target in ['america', 'citizen']
                                                    if w.lower().startswith(target))
print(cfd.conditions())
print(cfd['america'])
cfd.plot()
cfd.tabulate()

# parameter for plot and tabulate
print('------------------------------------------------')
import nltk
from nltk.corpus import udhr

# Count word length frequency in different languages(word length from 1-37 in this sample)
languages = ['Chickasaw', 'English', 'German_Deutsch', 'Greenlandic_Inuktikut', 'Hungarian_Magyar', 'Ibibio_Efik']
cfd = nltk.ConditionalFreqDist((lang, len(word)) for lang in languages
                                                 for word in udhr.words(lang + '-Latin1'))
cfd.plot(cumulative=True, conditions=['English', 'German_Deutsch'], samples=range(10))
cfd.tabulate(cumulative=True, conditions=['English', 'German_Deutsch'], samples=range(10))

# Bigrams
print('------------------------------------------------')
import nltk
sent = 'In the beginnig God created the heaven and the earth .'.split(' ')
print(list(nltk.bigrams(sent)))

# Bigrams freq
print('------------------------------------------------')
import nltk

def generate_model(cfdist, word, num=15):
    for i in range(num):
        print(word, end=' ')
        word = cfdist[word].max()

text = nltk.corpus.genesis.words('english-kjv.txt')
bigrams = nltk.bigrams(text)
cfd = nltk.ConditionalFreqDist(bigrams)     # Create a cfd that counts the freq for (next of each word)
print(cfd['living'])    # <FreqDist with 6 samples and 16 outcomes>
generate_model(cfd, 'living')   # living creature that he said , and the land of the land of the land (endless loop here)
