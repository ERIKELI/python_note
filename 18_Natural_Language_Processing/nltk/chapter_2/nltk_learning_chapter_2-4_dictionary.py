#!/usr/bin/python
# =============================================================
# File Name: nltk_learning_chapter_2-4_dictionary.py
# Author: LI Ke
# Created Time: 2/22/2018 16:16:11
# =============================================================


import nltk
# nltk.download('words')

# Filter text and return unusual vocabularies
def unusual_words(text):
    text_vocab = set(w.lower() for w in text if w.isalpha())
    english_vocab = set(w.lower() for w in nltk.corpus.words.words())
    unusual = text_vocab.difference(english_vocab)
    return sorted(unusual)

print(unusual_words(nltk.corpus.gutenberg.words('austen-sense.txt')))
print('------------------------------------------')
print(unusual_words(nltk.corpus.nps_chat.words()))

# stopwords contains the high freq words (such as 'the', 'in', ...)
print('------------------------------------------')
from nltk.corpus import stopwords
# nltk.download('stopwords')
print(stopwords.words('English'))

# Calculate the rate of unsual words
print('------------------------------------------')
import nltk
# nltk.download('reuters')
def context_fraction(text):
    stopwords = nltk.corpus.stopwords.words('english')
    context = [w for w in text if w.lower() not in stopwords]   # Cost many seconds
    return len(context) / len(text)
print(context_fraction(nltk.corpus.reuters.words()))    # 0.735240435097661

# Puzzle letters game
print('------------------------------------------')
import nltk
puzzle_letters = nltk.FreqDist('egivrvonl')
obligatory = 'r'
wordlist = nltk.corpus.words.words()
print([w for w in wordlist if len(w) >= 6
                           and obligatory in w
                           and nltk.FreqDist(w) <= puzzle_letters])

# Name corpus
print('------------------------------------------')
from nltk.corpus import names
print(names.fileids())      # ['female.txt', 'male.txt']
male_names = names.words('male.txt')
female_names = names.words('female.txt')
print([w for w in male_names if w in female_names]) # name used by both male and female
# Show the end letter freq with sex
import nltk
cfd = nltk.ConditionalFreqDist(
        (fileid, name[-1])
        for fileid in names.fileids()
        for name in names.words(fileid))
cfd.plot()

# cmudict
print('------------------------------------------')
import nltk
entries = nltk.corpus.cmudict.entries()
print(len(entries))
# Each word contains some voice code
for entry in entries[39943:39951]:
    print(entry)
# ('explorer', ['IH0', 'K', 'S', 'P', 'L', 'AO1', 'R', 'ER0'])
# ('explorers', ['IH0', 'K', 'S', 'P', 'L', 'AO1', 'R', 'ER0', 'Z'])
# ('explores', ['IH0', 'K', 'S', 'P', 'L', 'AO1', 'R', 'Z'])
# ('exploring', ['IH0', 'K', 'S', 'P', 'L', 'AO1', 'R', 'IH0', 'NG'])
# ('explosion', ['IH0', 'K', 'S', 'P', 'L', 'OW1', 'ZH', 'AH0', 'N'])
# ('explosions', ['IH0', 'K', 'S', 'P', 'L', 'OW1', 'ZH', 'AH0', 'N', 'Z'])
# ('explosive', ['IH0', 'K', 'S', 'P', 'L', 'OW1', 'S', 'IH0', 'V'])
# ('explosively', ['EH2', 'K', 'S', 'P', 'L', 'OW1', 'S', 'IH0', 'V', 'L', 'IY0'])

# Search 3 pron phoneme entry
print('------------------------------------------')
for word, pron in entries:
    if len(pron) == 3:
        ph1, ph2, ph3 = pron
        if ph1 == 'F' and ph3 == 'T':
            print(word, ph2)

# Find similar pronunciation words with 'nicks'
print('------------------------------------------')
import nltk
entries = nltk.corpus.cmudict.entries()
syllable = ['N', 'IH0', 'K', 'S']
print([word for word, pron in entries if pron[-4:] == syllable])

# Stress patern
print('------------------------------------------')
import nltk
entries = nltk.corpus.cmudict.entries()

def stress(pron):
    return [char for phone in pron for char in phone if char.isdigit()]

print([w for w, pron in entries if stress(pron) == ['0', '1', '0', '2', '0']])
print('------------------------------------------')
print([w for w, pron in entries if stress(pron) == ['0', '2', '0', '1', '0']])

# Find certain header and 3 pron phoneme words and sorted by first and last phoneme
p3 = [(pron[0] + '-' + pron[2], word) for word, pron in entries
                                      if pron[0] == 'P' and len(pron) == 3]
cfd = nltk.ConditionalFreqDist(p3)
for template in cfd.conditions():
    if len(cfd[template]) > 10:
        words = cfd[template].keys()
        wordlist = ' '.join(words)
        print(template, wordlist[:70] + '...')

# P-S piece purse pasts posts pus perce pease pers puss pos pass pace piss p...
# P-R pare poore pier pear pair por pore par paar peer porr pour parr poor...
# P-Z pays pies pows pez pease perz pao's peas p's pei's purrs pais poe's p....
# P-UW1 prue peru pew prew pru plew pugh pshew prugh peugh plue...
# P-P paape peep pipp pope papp pape pap pipe poppe pip pup popp paap pop pa...
# P-K perc paik peake poke pic paque paek pak pake pyke pock pech pick pack ...
# P-N payne pyne pane pawn peine pine pinn penh poon paine penn pin pon pun ...
# P-L peele pol paull piehl pahl peel pehl pihl poul pool pearle pole piel p...
# P-T pert piette pitt puett pote pate pit pete pot piet pott pet peat pait ...
# P-CH puche putsch pautsch peach piech piche poche patch petsch pitch petsch...

# Get pron dict
import nltk
prondict = nltk.corpus.cmudict.dict()
print(prondict['fire'])

# Translate dict
print('------------------------------------------')
import nltk
# nltk.download('swadesh')
from nltk.corpus import swadesh
print(swadesh.fileids())
# ['be', 'bg', 'bs', 'ca', 'cs', 'cu', 'de', 'en', 'es', 'fr', 'hr', 'it', 'la', 'mk', 'nl', 'pl', 'pt', 'ro', 'ru', 'sk', 'sl', 'sr', 'sw', 'uk']
fr2en = swadesh.entries(['fr', 'en'])
# print(fr2en)
# [('je', 'I'), ('tu, vous', 'you (singular), thou'), ('il', 'he'), ('nous', 'we'), ...]
translate = dict(fr2en)
print(translate['chien'])   # dog

# Add other language
en2ch = swadesh.entries(['cs', 'en'])
translate.update(dict(en2ch))
print(translate['pes'])     # dog

# Toolbox and Shoebox
print('------------------------------------------')
import nltk
nltk.download('toolbox')
from nltk.corpus import toolbox
print(toolbox.entries('rotokas.dic')[0])
