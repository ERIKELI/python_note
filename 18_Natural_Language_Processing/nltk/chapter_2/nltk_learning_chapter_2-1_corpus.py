#!/usr/bin/python
# =============================================================
# File Name: nltk_learning_chapter_2-1_corpus.py
# Author: LI Ke
# Created Time: 2/22/2018 09:44:15
# =============================================================


# Gutenberg
# gutenberg is PlaintextCorpusReader
print('---------------------------------------------')
import nltk
print(nltk.corpus.gutenberg.fileids())
emma = nltk.corpus.gutenberg.words('austen-emma.txt')
emma = nltk.Text(emma)  # Change common text into nltk Text object
emma.concordance('surprize')

from nltk.corpus import gutenberg
print('Show: average length of each word, average length of each sentence, average use time of each word(diversity)')
# average length of each word is 4 in this count script, but in actually it is 3, since the <blank space> was counted here.
for fileid in gutenberg.fileids():
    # num_chars = len(gutenberg.raw(fileid).replace(' ', ''))
    num_chars = len(gutenberg.raw(fileid))      # return str                charachter
    num_words = len(gutenberg.words(fileid))    # return list(str)          words
    num_sents = len(gutenberg.sents(fileid))    # return list(list(str))    sentences
    num_vocab = len(set([w.lower() for w in gutenberg.words(fileid)]))
    print(int(num_chars/num_words), int(num_words/num_sents), int(num_words/num_vocab), fileid)

# Webtext
# webtext is PlaintextCorpusReader
print('---------------------------------------------')
from nltk.corpus import webtext
print(webtext.fileids())

# Brown
# brown is CategorizedTaggedCorpusReader
print('---------------------------------------------')
from nltk.corpus import brown
print(brown.categories())
print(brown.fileids())
print(len(brown.words(categories='news')))  # return all text of news type
print(len(brown.words(['ca16', 'ca17'])))   # return text of certain fileid or fileid list

# Count frequency of modal vocabularies
import nltk
news_text = brown.words(categories='news')
fdist = nltk.FreqDist([w.lower() for w in news_text])
modal_vocabs = ['can', 'could', 'may', 'might', 'must', 'will']
for m in modal_vocabs:
    print(m + ':', fdist[m])

# Count frequency of modal vocabularies for all different categories
cfd = nltk.ConditionalFreqDist((genre, word) for genre in brown.categories()
                                             for word in brown.words(categories=genre))
genres = brown.categories()
modals = ['can', 'could', 'may', 'might', 'must', 'will']
cfd.tabulate(conditions=genres, samples=modals)

# Inaugural
# inaugural is PlaintextCorpusReader
print('---------------------------------------------')
import nltk
from nltk.corpus import inaugural
# print(inaugural.fileids())
cfd = nltk.ConditionalFreqDist((target, fileid[:4]) for fileid in inaugural.fileids()
                                                  for w in inaugural.words(fileid)
                                                  for target in ['america', 'citizen']
                                                  if w.lower().startswith(target))
cfd.plot()

# Other language
print('---------------------------------------------')
import nltk
from nltk.corpus import udhr
# nltk.download('udhr')
# [print(i) for i in nltk.corpus.udhr.fileids()]
print(udhr.raw('Chinese_Mandarin-GB2312'))

# Count word length frequency in different languages(word length from 1-37 in this sample)
languages = ['Chickasaw', 'English', 'German_Deutsch', 'Greenlandic_Inuktikut', 'Hungarian_Magyar', 'Ibibio_Efik']
cfd = nltk.ConditionalFreqDist((lang, len(word)) for lang in languages
                                                 for word in udhr.words(lang + '-Latin1'))
cfd.plot(cumulative=False)


# Help
print('---------------------------------------------')
import nltk
print(help(nltk.corpus.reader))

# Create self corpus
from nltk.corpus import PlaintextCorpusReader
import os
corpus_root = os.getcwd() + '\corpus_for_2-1'
word_lists = PlaintextCorpusReader(corpus_root, '.*')
print(word_lists)   # <PlaintextCorpusReader in 'C:\\Users\\XXX\\nltk\\corpus_for_2-1'>
print(word_lists.fileids())
print(word_lists.words('text_7.txt'))

# BracketParse use for corpora that consist of parentheisi-delineated parse trees
from nltk.corpus import BracketParseCorpusReader
# import os
# corpus_root = os.getcwd() + '\corpus_for_2-1'
# word_lists = BracketParseCorpusReader(corpus_root, '.*')
# print(word_lists)   # <PlaintextCorpusReader in 'C:\\Users\\XXX\\nltk\\corpus_for_2-1'>
# print(word_lists.fileids())
# print(word_lists.words('text_7.txt'))

