import sys
from cx_Freeze import setup, Executable


build_exe_options = {"packages": ["os", "pkg_resources"],
                     "excludes": ["tkinter"],
                     "include_files": ["icons/", "scripts/"]}

base = None
if sys.platform == "win32":
    base = "Win32GUI"
    #base = "Console"

setup(  name = "Test",
        version = 1.0,
        description = "Python Test",
        options = {"build_exe": build_exe_options},
        executables = [Executable("Test.py", base=base)])
