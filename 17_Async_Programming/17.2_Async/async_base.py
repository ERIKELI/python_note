#!/usr/bin/python
# =============================================================
# File Name: async_base.py
# Author: LI Ke
# Created Time: 1/29/2018 09:18:50
# =============================================================


import types
import time


@types.coroutine
def switch():
    print('Switch: Start')
    yield
    print('Switch: Done')

async def coro_1():
    print('C1: Start')
    await switch()
    print('C1: Stop')


async def coro_2():
    print('C2: Start')
    print('C2: 1')
    print('C2: 2')
    print('C2: 3')
    print('C2: Stop')

c_1 = coro_1()
c_2 = coro_2()

try:
    c_1.send(None)
except StopIteration:
    pass
try:
    c_2.send(None)
except StopIteration:
    pass
try:
    c_1.send(None)
except StopIteration:
    pass

print('--------------------------------')

def run(coros):
    coros = list(coros)

    while coros:
        # Duplicate list for iteration so we can remove from original list
        for coro in list(coros):
            try:
                coro.send(None)
            except StopIteration:
                coros.remove(coro)

c_1 = coro_1()
c_2 = coro_2()
run([c_1, c_2])

print('--------------------------------')

@types.coroutine
def action(t):
    trace=[]
    while True:
        trace.append(time.time())
        if trace[-1] - trace[0] > t:
            break # This break will end this function and raise a StopIteration
        yield

async def coro_1():
    print('C1: Start')
    await action(2)
    print('C1: Stop')


async def coro_2():
    print('C2: Start')
    await action(3)
    print('C2: Stop')

def timeit(f):
    def _wrapper(*args, **kwargs):
        start = time.time()
        re = f(*args, **kwargs)
        end = time.time()
        print('Time cost:', f.__name__, end-start)
        return re
    return _wrapper

c_1 = coro_1()
c_2 = coro_2()
timeit(run)([c_1])
timeit(run)([c_2])

print('--------------------------------')

c_1 = coro_1()
c_2 = coro_2()
timeit(run)([c_1, c_2])

