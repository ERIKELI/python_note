import time


def ask(a):
    next(a)    # Start generator
    # a.send(None)
    n = 0
    while n < 7:    # Ask certain number questions then exit.
        print('Ask: Try to ask question %d' % n)    
        r = a.send(n)       # Send Ques number (ask question), receive is r
        print('Ask: Received answer <%s>' % r)
        n += 1
    a.close()   # End loop
    # try:
    #     a.send(None)
    # except StopIteration as e:
    #     pass
        

def answer():   # Answer generator
    ans = ''    # First answer for generator start
    while True:
        qus = yield ans     # Return answer
        if qus is None:
            return
        print('Answer: Received question %s' % qus)
        time.sleep(1)
        ans = 'Best answer'

ask(answer())
