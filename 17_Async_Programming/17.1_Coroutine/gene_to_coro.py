#!/usr/bin/python
# =============================================================
# File Name: gene_to_coro.py
# Author: LI Ke
# Created Time: 1/29/2018 15:34:50
# =============================================================


print('-------- Generator ----------')

def switch_1():
    print('Switch_1: Start')
    yield
    print('Switch_1: Stop')


def switch_2():
    print('Switch_2: Start')
    yield
    print('Switch_2: Stop')

a = switch_1()
b = switch_2()
a.send(None)
b.send(None)
try:
    b.send(None)
except StopIteration as e:
    re = e.value

try:
    a.send(None)
except StopIteration as e:
    re = e.value

print('-------- Async Coro ----------')

async def switch_1():
    print('Switch_1: Start')
    await switch_2()
    print('Switch_1: Stop')

async def switch_2():
    print('Switch_2: Start')
    print('Switch_2: Stop')

a = switch_1()
try:
    a.send(None)
except StopIteration as e:
    re = e.value
