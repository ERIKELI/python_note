from variable_type import *

print(GLOBAL_VARIABLE)

try:
    print(_PRIVATE_VARIABLE)
except NameError:
    print("Name Error, re-import.")
    from variable_type import _PRIVATE_VARIABLE
    print(_PRIVATE_VARIABLE)

