"""
Python LEGB rule: local, enclosing, global, built-in
"""
A = 7
# Local -- L  
def errorFuncLocal():  
    print('\n-------------Func: errorFuncLocal----------------')  
    try:  
        print('The global value of A is %d' % A)  
    except NameError:  
        print('Function <errorFuncLocal> raises a NameError')  
    A = 6  
      
def correctFuncLocal():  
    print('\n-------------Func: correctFuncLocal------------')  
    global A  
    try:  
        print('The global value of A is %d' % A)  
    except NameError:  
        print('Function <correctFuncLocal> raises a NameError')  
    # A = 6  
      
def correctFuncLocal_2():  
    print('\n-------------Func: correctFuncLocal_2------------')  
    A = 5  
    try:  
        print('The local value of A is %d' % A)  
    except NameError:  
        print('Function <correctFuncLocal> raises a NameError')  

# Enclosing -- E  
def errorFuncEnclosing():  
    print('\n-------------Func: errorFuncEnclosing-----------')  
    def foo():  
        print('The enclosing value of A is %d' % A)  
    try:  
        foo()  
        A = 6  
    except NameError:  
        print('Function <errorFuncEnclosing> raises a NameError')  
      
def correctFuncEnclosing():  
    print('\n-------------Func: correctFuncEnclosing-----------')  
    def foo():  
        print('The enclosing value of A is %d' % A)  
    try:  
        A = 6  
        foo()  
    except NameError:  
        print('Function <errorFuncEnclosing> raises a NameError')  

# Global -- G  
def correctFuncGlobal():  
    print('\n-------------Func: correctFuncGlobal-----------')  
    print('The global value of A is %d' % A)  

# Build-in -- B  
def correctFuncBuildin():  
    print('\n-------------Func: correctFuncBuildin-----------')  
    print('The value of build-in variable __name__ is:', __name__) 

if __name__ == '__main__':  
    errorFuncLocal()  
    correctFuncLocal()  
    correctFuncLocal_2()  
    print('The global value of A is %d' % A)  
      
    errorFuncEnclosing()  
    correctFuncEnclosing()  
    print('The global value of A is %d' % A)  
      
    correctFuncGlobal()  
    correctFuncBuildin()  
