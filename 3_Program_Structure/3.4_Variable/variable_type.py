print(vars()) # Show built_in variables

GLOBAL_VARIABLE = "This is global variable"
_PRIVATE_VARIABLE = "This is private variable"

def call_local():
    local_variable = "This is local variable"
    print(local_variable)

def call_global():
    global GLOBAL_VARIABLE
    print(GLOBAL_VARIABLE)

def call_private():
    print(_PRIVATE_VARIABLE)

if __name__ == '__main__':
    call_local()
    call_global()
    call_private()
