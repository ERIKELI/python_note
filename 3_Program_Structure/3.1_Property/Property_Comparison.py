class Foo():
    classProperty = 'classProperty'
    _privatePropertyCallable = 'privatePropertyCallable'
    __privateProperty = 'privateProperty'
    def __init__(self):
        self.instanceProperty = 'instanceProperty'
        self.__privateProperty = 'selfPrivateProperty'

# Call class property
print(Foo.classProperty)
print(Foo().classProperty)
# Call instance property
print(Foo().instanceProperty)
# Call private property
print(Foo._privatePropertyCallable)
print(Foo._Foo__privateProperty)
print(Foo()._Foo__privateProperty)
