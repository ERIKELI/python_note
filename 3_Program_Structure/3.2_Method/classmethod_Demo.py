class Foo:
    num = 1
    
    @classmethod
    def instance(cls):
        cls.num = 7
        cls = cls()
        return cls
    
    def __init__(self):
        self.foo = Foo.num

    def test(self):
        print('This is a test')

cls_1 = Foo()
print("foo is: %d, num is: %d" % (cls_1.foo, cls_1.num))
        
# Change num from 1 to 7 before __init__
cls_2 = Foo.instance()
print("foo is: %d, num is: %d" % (cls_2.foo, cls_2.num))
cls_2.test()
