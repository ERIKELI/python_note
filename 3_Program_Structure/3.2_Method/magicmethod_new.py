class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __new__(cls, *args, **kwargs):
        print(id(cls), id(Person)) # Id is same
        # If instance of class has exist, return the instance without generate a new one
        if not hasattr(cls, 'instance'):
            cls.instance = super(Person, cls).__new__(cls) # Generate a instance
        return cls.instance
    
a = Person('p1', 20)
print(a.name, a.age)    # 'p1', 20
b = Person('p2', 21) 
print(a.name, a.age)    # 'p2', 21
print(b.name, b.age)    # 'p2', 21
print(a is b)           # True
