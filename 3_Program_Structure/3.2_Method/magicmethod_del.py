class Foo():
    def __init__(self):
        print('__init__ method called')
        
    def __del__(self):
        print('__del__ method called')
        
f = Foo()
del(f)

