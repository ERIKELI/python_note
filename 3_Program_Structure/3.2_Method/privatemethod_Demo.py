class Foo():
    def __privatemethod(self):
        print('This is a private method.')

    def _privatemethod_callable(self):
        print('This is a private method but callable.')

f = Foo()
f._Foo__privatemethod()
f._privatemethod_callable()
