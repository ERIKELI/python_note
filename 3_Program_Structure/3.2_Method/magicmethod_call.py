class Foo():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __call__(self, m, n):
        print('x is %s, y is %s, m is %s, n is %s' % (self.x, self.y, m, n))

Foo(1, 2)(3, 4)

f = Foo(5, 6)
f(7, 8)
