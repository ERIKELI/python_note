class Foo(object):
    
    def __init__(self):
        self._foo = None
        
    @property
    def foo(self):
        return self._foo
    
    @foo.setter
    def foo(self, value):
        if isinstance(value, int):
            self._foo = (value + 0.5)
            
if __name__ == "__main__":
    f = Foo()
    print("foo is %s" % f.foo)
    f.foo = 1
    print("foo is %s" % f.foo)
