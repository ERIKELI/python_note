class Foo():
    @staticmethod
    def foo(a, b):
        print('This is a staticmethod, cal result: %d' % (a+b))

Foo.foo(1, 2)
Foo().foo(3, 4)
