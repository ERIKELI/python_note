class Foo:
    def speak(self):
        print('Speak hello world')

    def act(self, action):
        print('Act', action)

    def eat(self, food, fruit):
        print('Eat', food, fruit)

    def place(self):
        print('Place things by Foo')

class NoProxy:
    def __init__(self):
        self._foo = Foo()

    def speak(self):
        self._foo.speak()

    def act(self, action):
        self._foo.act(action)

    def eat(self, food, fruit):
        self._foo.eat(food, fruit)

    def watch(self):
        self.act('watching')
        
class Proxy:
    def __init__(self):
        self._foo = Foo()

    def __getattr__(self, item):
        print('Use Proxy')
        return getattr(self._foo, item)

    def watch(self):
        self.act('Watching')

    def place(self):
        print('Place things by Proxy')


for x in [NoProxy(), Proxy()]:
    x.speak()
    x.act('run')
    x.eat('rice', 'apple')
    x.watch()
Proxy().place()
