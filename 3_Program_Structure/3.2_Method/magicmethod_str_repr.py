class new(int):
    # Reload the __str__ method, when print(or format) it, this method will be called
    def __str__(self):
        return 'Null'

    # Reload the __repr__ method, when repr(or %r) it, this method will be called
    def __repr__(self):
        return str(self+1)
    
n = new(7)
print(n)    # Null
print('%s' % n) # Null
print('{}'.format(n)) # Null
print('%r' % n) # 8

# Now the n is int type, not new
n = 3
print(eval(repr(n))==n)
