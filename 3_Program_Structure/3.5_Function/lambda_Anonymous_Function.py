
# Use directly
f = lambda x: x+1
def foo(x): return x
print('f type is:', type(f), 'foo type is:', type(foo))

# Use as a calculate func in other function
print(list(map(lambda x: x*x, [1, 2, 3, 4])))

# Use as a return func in other function
def foo(n): return lambda x: n+x
print(foo('n')('x'))
