# Enclosure Function and Decorator
def deco(func):
    # This is a sbstitute returned to replace input function
    def wrapper():
        return func()+1
    return wrapper

def foo():
    return 1

print(foo())   # 1
print(deco(foo)()) # 2

# Use Syntactic Sugar to make Decorator
@ deco
def fox():
    return 1
print(fox())    # 2

@ deco
@ deco
def fox():
    return 1
print(fox())    # 3

# Make a Decorator can receive function para
def decor(func):
    def wrapper(a, b):
        return func(a, b)+1
    return wrapper

def fun(a, b):
    return a+b

@ decor
def funx(a, b):
    return a+b

print(fun(1, 2))   # 3
print(decor(fun)(1, 2)) # 4
print(funx(1, 2))   # 4

# Common function para Decorator
def decor(func):
    def wrapper(*argv, **kwargv):
        return func(*argv, **kwargv)+1
    return wrapper

@ decor
def fun(a, b, c=0):
    return a+b+c

@ decor
def funx(a, b, c, d, e=0):
    return a+b+c+d+e

print(fun(1, 2, c=3))   # 7
print(funx(1, 2, 3, 4, e=5))    # 16

# Decorator receive its own para
def decora(c):
    def _decor(func):
        def _wrapper(a, b):
            return func(a, b)+c
        return _wrapper
    return _decor

# Note: decora(3) return _decor
# @ decora(3) equals to @ _decor
# _decor record c value thanks to Closure
@ decora(3) # @ _decor
def fun(a, b):
    return a+b

print(fun(1, 2))   # 6

