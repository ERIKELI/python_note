
# Use () to replace [], make it into a generator
gen = (x**2 for x in range(7))
lis = [x**2 for x in range(7)]
print(type(gen), type(lis))

# Define function to create generator by using yield
# Return will cause StopIertation and return value will be present as an explaination for StopIteration
def gener(m):
    n = 1
    while n < m:
        yield n
        n += 1
        if n == 3:
            return 'Stop at 3'

# gener is function, while gener(7) is generator
print(type(gener), type(gener(7)))
help(gener(10))

# Iterate the generator
# Use "for"
for i in gener(7):
    print(i)
g = gener(7)
# Use "next"
while g:
    try:
        print(next(g))
    except StopIteration as e:
        print(e)
        break

# Test close() function: close function will cause later iteration call a StopIteration
def gx():
    yield 1
    yield 2
    yield 3
    
g = gx()
for i in range(3):
    try:
        c = next(g)
        print(c)
    except StopIteration:
        print('Stop at %d' % c)
    if c == 2:
        g.close()

# Test send() function: send function will pass a value to generator
def gx():
    i = 'Init'
    while True:
        r = yield i
        if r == 'Stop':
            print('Got: Stop, stop at here')
            break
        i = 'Got: %s' % r
g = gx()
for i in [0, None, None, 'First', 'Second', 'Third', 'Stop']:
    try:
        print(g.send(i))
    except TypeError as e:
        print('Send "%s" into g, and cause TypeError: %s.' % (i, e))
    except StopIteration:
        print('Send "%s" into g, and cause StopIteration.' % i)


def gx():
    c = 1
    while True:
        try:
            print('First yield')
            yield c
            c += 1
            print('Second yield')
            yield c
            c += 1
            print('Third yield')
            yield c
            c += 1
        except ValueError:
            print('Receive an ValueError, and c is %d now.' % c)
        except TypeError:
            print('Receive a TypeError, and c is %d now.' % c)
            break
g = gx()
print('First time using next(g) got:', next(g), '\n')
print('Second time using g.throw() got:', g.throw(ValueError), '\n')
print('Thrid time using next(g) got:', next(g), '\n')
print('Forth time using g.throw() got:',g.throw(TypeError), '\n')
