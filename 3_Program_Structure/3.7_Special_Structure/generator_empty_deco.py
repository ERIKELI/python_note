"""
Decorator for generator empty test
"""
import itertools

def deco(f):
    def wap(*args):
        it = f(*args)
        try:
            first = next(it)
        except StopIteration:
            return None
        return itertools.chain([first], it)
    return wap

@deco
def gen(g):
    for i in g:
        yield i


x = gen([1, 2])
print("Generator is:", x)
for i in x:
    print("Generator element:", i)
x = gen([])
print("Generator is:", x)
