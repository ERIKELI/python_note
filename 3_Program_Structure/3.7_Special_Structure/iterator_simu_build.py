class MyIterator():
    def __init__(self, imin, imax):
        self.count = imin - 1
        self.limit = imax

    def __iter__(self):
        return self

    def __next__(self):
        self.count += 1
        if self.count >= self.limit:
            raise StopIteration
        return self.count

it = MyIterator(0, 7)
for i in range(7):
    print(next(it))
# Output: 0 1 2 3 4 5 6

for i in MyIterator(0, 7):
    print(i)
# Output: 0 1 2 3 4 5 6

class MyIterable():
    def __init__(self, imin, imax):
        self.imin = imin
        self.imax = imax
        self.count = imin - 1
        self.limit = imax

    #def __iter__(self):
    #    return self

    def __iter__(self):
        return GeneIterator(self)

class GeneIterator(MyIterable):
    def __init__(self, iterable):
    # This __init__ function pass two value to the instance of geneIterator
        MyIterable.__init__(self, iterable.imin, iterable.imax)

    def __next__(self):
        self.count += 1
        if self.count >= self.limit:
            raise StopIteration
        return self.count

for i in MyIterable(0, 7):
    print(i)
# Output: 0 1 2 3 4 5 6
