class A:
    def __init__(self, name):
        print("A init.")
        self.name = name

class B(A):
    def __init__(self, age):
        print("B init.")
        self.age = age
        super(B, self).__init__("LIKE") # This super() will call C.__init__()

class C(A):
    def __init__(self, age):
        print("C init.")
        self.age = age
        super(C, self).__init__("like")

class D(B, C):
    def __init__(self):
        print("D init.")
        super(D, self).__init__(7)

d = D()
print(d.__dict__)
