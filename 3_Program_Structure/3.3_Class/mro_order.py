"""
    A
   /\
  /  \
 B    C
  \  /
   \/
   D
"""
from distutils.log import warn as printf

class A:
    def __init__(self):
        pass

    def show(self):
        printf("This is A")


class B(A): pass


class C(A):
    def show(self):
        printf("This is C")

class D(B, C): pass

d = D()
d.show()    
# Python2 --> This is A
# Python3 --> This is C
