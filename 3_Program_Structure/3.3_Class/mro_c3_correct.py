# C3 MRO
# ----------------------
"""
    Object
    /  |  \
   /   |   \
  X _  Y   Z
  \  \/  /
   \ /\ /
    A  B
     \/
     C
"""

class X(object): pass

class Y(object): pass

class Z(object): pass

class A(X, Y): pass

class B(X, Z): pass

class C(A, B): pass

print(C.__mro__)
# ----------------------

# ----------------------
"""
    Object
    /  |  \
   /   |   \
  D    E    F
  \   / \  /
   \ /   \/
    J    K
     \  /
      L
"""

class D(object): pass

class E(object): pass

class F(object): pass

class J(D, E): pass

class K(E, F): pass

class L(J, K): pass

print(L.__mro__)

"""
C3 Algorithm Calculation:
L[object] = [object]
L[D] = [D] + merge(L[object]) = [D, object]
L[E] = [E] + merge(L[object]) = [E, object]
L[F] = [F] + merge(L[object]) = [F, object]
L[J] = [J] + merge(L[D], L[E], [D], [E]) = [J, D, E, object]
L[K] = [K] + merge(L[E], L[F], [E], [F]) = [K, E, F, object]
L[L] = [L] + merge(L[J], L[K], [J], [K]) 
     = [L, J] + merge([D, E, object], [K, E, F, object], [K])
     = [L, J, D] + merge([E, object], [K, E, F, object], [K])
     = [L, J, D, K] + merge([E, object], [E, F, object])
     = [L, J, D, K, E] + merge([object], [F, object])
     = [L, J, D, K, E, F] + merge([object], [object])
     = [L, J, D, K, E, F, object]
"""
# ----------------------
