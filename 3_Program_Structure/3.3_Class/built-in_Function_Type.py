builtin_func_list = [abs, round, pow, divmod, max, min, sum, ord, chr, bin, hex, oct]
builtin_type_list = [int, complex, float, str, type, bool, tuple, list, dict, slice]

for f in builtin_func_list:
    print(f)
for t in builtin_type_list:
    print(t)
