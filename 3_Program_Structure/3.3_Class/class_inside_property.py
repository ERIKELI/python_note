class Foo():
    """
    This is the text that can be called by __doc__
    """
    def __init__(self):
        self.foo = None

    def foo_method(self):
        self.foom = True

print('>>>', Foo.__name__)
print('>>>', Foo.__doc__)
print('>>>', Foo.__bases__)
print('>>>', Foo.__dict__)
print('>>>', Foo.__module__)
print('>>>', Foo.__class__)
'''
>>> Foo
>>> 
    This is the text that can be called by __doc__
    
>>> (<class 'object'>,)
>>> {'fooMethod': <function Foo.fooMethod at 0x02FE6300>, '__weakref__': <attribute '__weakref__' of 'Foo' objects>, '__doc__': '\n    This is text that can be called by __doc__\n    ', '__module__': '__main__', '__init__': <function Foo.__init__ at 0x02F9C8E8>, '__dict__': <attribute '__dict__' of 'Foo' objects>}
>>> __main__
>>> <class 'type'>
'''
