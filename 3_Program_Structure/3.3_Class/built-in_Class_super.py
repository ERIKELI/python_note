class A:
    def __init__(self):
        pass

    def show(self):
        print("This is A.")

class B(A):
    def __init__(self):
        super(B, self).__init__()
        print(super(B, self))

    def show(self):
        print("This is B.")
        super(B, self).show()
        super().show()

class C(A):
    def __init__(self):
        self.a = super(C, self)
        print(self.a)

    def show(self):
        print("This is C.")
        self.a.show()

b = B()
c = C()
b.show()
c.show()
