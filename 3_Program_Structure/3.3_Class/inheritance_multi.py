from inheritance_base import Bird, Fish
# ------- Multi inheritance --------
class Duck(Bird, Fish): pass

class Goose(Bird, Fish):
    def __init__(self):
        super(Goose, self).__init__()

d = Duck()
g = Goose()
print("I am %s, I can fly: %s" % (d.species, d.fly))
print("I am %s, I can swim: %s" % (d.species, d.swim))
print("I am %s, I can fly: %s" % (g.species, g.fly))
print("I am %s, I can swim: %s" % (g.species, g.swim))

class Duck(Bird, Fish):
    def __init__(self):
        Bird.__init__(self)
        Fish.__init__(self)

d = Duck()
print("I am %s, I can fly: %s" % (d.species, d.fly))
print("I am %s, I can swim: %s" % (d.species, d.swim))
