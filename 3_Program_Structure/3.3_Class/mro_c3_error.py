"""
     ________
    /        \   
    X   Y    |
    \  /\   /
     \/  \ /
      A   B
      \  /
       \/
       C
"""

class X(object): pass

class Y(object): pass

class A(X, Y): pass

class B(Y, X): pass

class C(A, B): pass

print(C.__mro__)

"""
C3 Algothrim Calculation:
L[object] = [object]
L[X] = [X] + merge(L[object]) = [X, object]
L[Y] = [Y] + merge(L[object]) = [Y, object]
L[A] = [A] + merge(L[X], L[Y], [X], [Y]) = [A, X, Y, object]
L[B] = [B] + merge(L[Y], L[X], [Y], [X]) = [B, Y, X, object]
L[C] = [C] + merge(L[A], L[B], [A], [B])
     = [C] + merge([A, X, Y, object], [B, Y, X, object], [A], [B])
     = [C, A, B] + merge([X, Y, object], [Y, X, objcet])
--> Raise Error
"""
