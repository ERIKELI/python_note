"""
    A
    /\
   /  \
  /    \
 B      C
  \    /
   \  /
    \/
    D
"""

# ------- no super ----------
class A(object):
    def __init__(self):
        object.__init__(self)
        print("This is A init.")

class B(A):
    def __init__(self):
        A.__init__(self)
        print("This is B init.")

class C(A):
    def __init__(self):
        A.__init__(self)
        print("This is C init.")
        
class D(B, C):
    def __init__(self):
        B.__init__(self)
        C.__init__(self)
        print("This is D init.")

d = D()
print(30*"-")

# ------- super ------------
class A(object):
    def __init__(self):
        super(A, self).__init__()
        print("This is A init.")

class B(A):
    def __init__(self):
        super(B, self).__init__()
        print("This is B init.")

class C(A):
    def __init__(self):
        super(C, self).__init__()
        print("This is C init.")
        
class D(B, C):
    def __init__(self):
        super(D, self).__init__()
        print("This is D init.")

d = D()
