import inspect

# Diamond inherit
class A: pass

class B(A): pass

class C(A): pass

class D(B, C): pass

print(inspect.getmro(D))
print(D.__mro__)
