# ------- Basic inheritance --------
class Animal:
    def __init__(self):
        self.base = "Creature"
        self.species = "Animal"
        self.eat = True

    def __getattr__(self, item):
        return False

    def show(self):
        print("This is %s." % self.species)


# Note: Do not mix super and Object.__init__
class Bird(Animal):
    def __init__(self):
        Animal.__init__(self)
        self.species = "Bird"
        self.fly = True

    def show(self):
        print("This is %s from %s." % (self.species, self.base))

class Fish(Animal):
    def __init__(self):
        Animal.__init__(self)
        self.species = "Fish"
        self.swim = True

b = Bird()
f = Fish()
b.show()
f.show()


