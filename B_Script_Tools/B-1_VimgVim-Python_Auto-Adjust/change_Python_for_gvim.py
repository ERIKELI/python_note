"""
This script help to change diferent Python.exe for gvim (linked with <F5>)
"""
#import fileinput
#
#for line in fileinput.input('_vimrc', backup='bak', inplace=1):
#    line = line.replace('python27', 'python35')
import sys

filename = r'C:\Program Files (x86)\Vim\_vimrc'
version_list = ['python27', 'python35', 'python34']
new_version = sys.argv[1]

class VersionNoFound(Exception):
    pass

with open(filename, 'r+') as f:
    context = f.read()
    for version in version_list:
        if version in context:
            context = context.replace(version, new_version)
            print('Change {0} to {1}'.format(version, new_version))
            break
    else:
        raise VersionNoFound

    f.seek(0)
    f.write(context)
    f.close()
