# coding=utf-8
import re
import logging
from time import time
from urllib import request
from bs4 import BeautifulSoup as Bs
from threading import Lock

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s - %(filename)7s - %(name)10s - %(levelname)5s] %(message)s',
                    datefmt='%Y-%m-%d %a %H:%M:%S',)


class Helper:
    total_char = 0
    _count_lock = Lock()

    @staticmethod
    def timeit(logger=print):
        def _timeit(f):
            def _wrapper(*args, **kwargs):
                start = time()
                r = f(*args, **kwargs)
                end = time()
                logger('Function <%s> costs time %s sec.' % (f.__name__, end - start))
                return r
            return _wrapper
        return _timeit
    
    @staticmethod
    def countit(f):
        def _wrapper(*args, **kwargs):
            r = f(*args, **kwargs)
            with Helper._count_lock:
                Helper.total_char += r[0]
            return r
        return _wrapper
    

class CatalogCreator:
    _NPG = r'href="([^<]+?\.html\?page=\d+)">下一页</a>'           # Next page link
    _ESY = r'href="(.+?/p/\d+\.html)">(.+)</a>'                    # Essay link and title
    _CHR = r'[\u4e00-\u9fa5]|[A-Za-z]+|[0-9]+'                     # Chinese characters / English words / Arabic Numbers
    _TIM = r'posted @ (?P<time>\d{4}-\d{2}-\d{2} \d{2}:\d{2}) '    # Issue time
    NPG_RE = re.compile(_NPG)        
    ESY_RE = re.compile(_ESY)        
    CHR_RE = re.compile(_CHR)        
    TIM_RE = re.compile(_TIM)        
    A_TAG = ('<a ', '</a>')         # Set link info between anchor tag
    UL_TAG = ('<ul>\n', '</ul>\n')
    LI_TAG = ('<li>', '</li>\n')
    HEAD = '<p>&gt;&gt;&gt; 统计信息 -&gt; <span style="font-family: courier;"> 随笔总篇数: {:,}  随笔总字数: {:,}</p>\n<p>&nbsp;</p>'
    HREF = 'href="{}" target="_blank">{}'
    EXT = '<pre>\t\t随笔字数: {:,}\t发布时间: {}</pre>'          # Extend information
    _logger = logging.getLogger('CatalogCreator')

    def __init__(self, hp=None):
        self._home_page = hp

    @staticmethod
    def get_html(url):
        crawler = request.Request(url)
        crawler.add_header('User-Agent', 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)')
        resp = request.urlopen(crawler)
        return resp.read().decode('utf-8')
        # return request.urlopen(url).read().decode('utf-8')

    def get_next_page_html(self, html):
        # Use re to get next page link, and return its html.
        # Two link will be found in a page (one at top, another bottom), choose any one is fine.
        url_list = re.findall(self.NPG_RE, html)
        return self.get_html(url_list[0]) if url_list else None
        
    def get_essay_list(self, html):
        # Use re to get all essay urls.
        return re.findall(self.ESY_RE, html)

    @Helper.countit
    @Helper.timeit(_logger.debug)
    def get_essay_info(self, link):
        source = Bs(self.get_html(link), 'html5lib')   # This function would cost several seconds.
        # Purify the source
        [x.extract() for x in source.find_all('script')]
        [x.extract() for x in source.find_all('div', attrs={"class": "cnblogs_code_hide"})]
        # Eval word number and issue time of essay
        text = source.get_text()
        essay_length = len(re.findall(self.CHR_RE, text))
        essay_time = re.search(self.TIM_RE, text)
        return essay_length, essay_time.group('time') if essay_time else None
    
    def create_catalog(self, links):
        item = []
        for i in sorted(links, key=lambda x: x[1]):
            self._logger.debug('Fetching %s.' % i[1])
            item.append((self.HREF.format(*i).join(self.A_TAG) +
                         self.EXT.format(*self.get_essay_info(i[0]))).join(self.LI_TAG))
        self._logger.info(self.HEAD.format(len(links), Helper.total_char)+''.join(item).join(self.UL_TAG))

    @Helper.timeit(_logger.info)
    def fetch(self):
        self._logger.debug('Fetching essay list.')
        essay_list = []
        html = self.get_html(self._home_page)
        while html:
            essay_list.extend(self.get_essay_list(html))
            html = self.get_next_page_html(html)

        self.create_catalog(essay_list)


if __name__ == '__main__':
    home = "http://www.cnblogs.com/stacklike/"
    c = CatalogCreator(home)
    c.fetch()
    # print(c.get_essay_info("http://www.cnblogs.com/stacklike/p/8283988.html"))
